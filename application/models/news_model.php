<?php
class News_model extends CI_Model {
     
     var $gallery_path;
     var $gallery_path_url;
	public function __construct()
	{
		$this->load->database();
      //  parent::CI_Model();
        $this->gallery_path = realpath(APPPATH. '../files');
      }
	
	public function get_news($slug = FALSE)
{
	if ($slug === FALSE)
	{
		$query = $this->db->get('news');
		return $query->result_array();
	}

	$query = $this->db->get_where('news', array('slug' => $slug));
	return $query->row_array();

}
public function get_detail_students_jobs($no)
{
      $user = $this->session->userdata('username');
 $query=$this->db->select('*')->from('students_jobs')->join('jobs','students_jobs.job_id=jobs.id')
  ->where(array('user'=>$user,'students_jobs.delete_status !='=>'true','applied_id'=>$no));

      
   //   $query= $this->db->get_where('jobs', array('applied_id'=>$no,'user'=>$user,'delete_status !='=>'false'));
      
      return $query->get()->row_array();
      


}
public function get_student_name($user)
{
     
   $name=$this->db->select('name')->from('students')->where('user',$user)->get()->row_array();
   if(!empty($name))
   {
   return $name['name'];
    }
    else
    {
      return '';
    }


}
public function get_company_user_name($user)
{

  $name=$this->db->select('name')->from('caccounts')->where('username',$user)->get()->row_array();
  return $name['name'];
  if(!empty($name))
   {
   return $name['name'];
    }
    else
    {
      return '';
    }

}
public function get_admin_name($user)
{

  $name=$this->db->select('name')->from('administrator')->where('user',$user)->get()->row_array();
  return $name['name'];
  if(!empty($name))
   {
   return $name['name'];
    }
    else
    {
      return '';
    }

}
public function get_detail_job($no,$a=true)
{   
        if($a==true)
        {
       $user_c =$this->session->userdata('username');
        $query = $this->db->get_where('jobs',array('user_c'=>$user_c,'id'=>$no,'job_status !='=>'expired','delete_status !='=>"true"));
       //print_r($query->result_array());
       return $query->row_array();
     }
     elseif($a==false)
     {
        $user_c =$this->session->userdata('username');
        $query = $this->db->get_where('jobs',array('user_c'=>$user_c,'id'=>$no));
       //print_r($query->result_array());
       return $query->row_array();
     }

      
}
public function get_expired_job($job_id)
{
   $user_c=$this->session->userdata('username');
   $q=$this->db->get_where('jobs',array('user_c'=>$user_c,'id'=>$job_id,'job_status'=>'expired','delete_status !='=>'true'));
   return $q->row_array();


}

public function get_jobs($user)

{
	$query = $this->db->get_where('jobs', array('user_c'=>$user));
	 return $query->result_array();
    

}
public function get_available_job($id)
{
   $query = $this->db->get_where('jobs',array('id'=>$id,'delete_status !='=>"true",'job_status'=>'opened'));
       //print_r($query->result_array());
       return $query->row_array();

      

}
public function get_jobs_rows($user)
{
    $query = $this->db->get_where('jobs',array('user_c'=>$user,'delete !='=>'true'));
    return $query->num_rows();

}
public function get_jobs_total($user)
{
$result= $this->db->get_where('students_jobs',array('user'=>$user,'delete !='=>'true'));
  $result = $result->num_rows();
  return $result;

}
public function get_applicant_info($user)
{

  $result = $this->db->get_where('applicants',array('user'=>$user));
  $result = $result->result_array();
  
  return $result;


}
public function get_applicant_details($id)
{
    $user_c=$this->session->userdata('username');
    $result =$this->db->select("*")->from('applicants')->where(array('user_c'=>$user_c,'applicant_id'=>$id,'delete !='=>'true'))
    ->join('students','applicants.user=students.user')->get();
    $result=$result->result_array();
    return $result;

}
public function get_my_jobs($user)
{
  $result= $this->db->get_where('students_jobs',array('user'=>$user));
  $result = $result->result_array();
 
  return $result;



}
public function get_all_jobs()
{

	$query = $this->db->get('jobs');
	return $query->result_array();
}
public function get_all_companies()
{

  $result = $this->db->get('caccounts');
  return $result->result_array();

}

public function get_company($user)
{
      $role=$this->session->userdata('role');
     
      
      $query = $this->db->get_where('caccounts',array('username'=>$user));
      return $query->result_array();
   
  /*  elseif($role=="admin")
    {
  $query = $this->db->get_where('administrator',array('user'=>$user));
      return $query->result_array();

    }*/

}


public function search($query_array,$limit,$offset,$sort_by,$sort_order)
{
    //echo "the query array category is: " . $query_array['category'];
      
	//$sort_order= ($sort_order == 'desc')?'desc': 'asc';
	//$sort_columns = array('c_name','id','category','title');
	  //$sort_by = (in_array($sort_by,$sort_columns)?$sort_by:'c_name');
   
   //   $q= $this->db->select('*')->from('jobs')->
     // limit($limit,$offset)->order_by($sort_by,$sort_order)->where(array('expired !='=>'true',
       // 'delete_status !='=>'true'));
        $q="";
        $ob="";
     $message="";
     if(strlen($query_array['c_name']))
     {
        $c_name=$query_array['c_name'];
        $c_name=mysql_real_escape_string($c_name);
      $keyword=preg_split("/[^a-zA-Z]+|[\s]+/",$c_name);
       $some='';
       foreach($keyword as $values)
      {
        if(!empty($values))
        {
        $some[]=$values;
         }
      }

$word="' '";
$word2="' '";
if(!empty($some))
{
  $word=" c_name regexp ";
$re=(implode('|',$some));
$word.="'$re'";
}

//$word.="regexp "
           $q.=" and (c_name like '%$c_name%' or $word) " ;
         
         $message.= "Company Name= " . $query_array['c_name']."; ";

     }
    if(strlen($query_array['id']))
     {
          $id=$query_array['id'];
          $id=mysql_real_escape_string($id);
        
            $q.="and (id='$id') ";
         
   
      $message.= "job id = " .$query_array['id']."; ";

     }
     if(strlen($query_array['category']))
     {
             
            if($query_array['category']=='all')
            {
                $message.="Category = ". "All Categories;";

            }
            else{

               $category=$query_array['category'];
          $category=mysql_real_escape_string($category);
         
         
            $q.=" and (category ='$category') ";
        
     
      $message.= "Category = " .$query_array['category']."; ";
                }
     }

    
     if(strlen($query_array['title']))
     {
          $title=$query_array['title'];
          $title=mysql_real_escape_string($title);
      $keyword=preg_split("/[^a-zA-Z]+|[\s]+/",$title);
     $some='';
      foreach($keyword as $values)
      {
        if(!empty($values))
        {
        $some[]=$values;
         }
      }

$word="' '";
$word2="' '";
if(!empty($some))
{
  $word=" title regexp ";
$re=(implode('|',$some));
$word.="'$re'";
//$word.="regexp "

 
foreach($some as $key=>$values)
{
  if($key==0)
  {
    $word2=" title Like '%$values%' ";
  }
  else
  {
  $word2.="and title Like '%$values%' ";
    }
}         
}       
        
        $q.=" and ((title like '%$title%') or ($word2) or ($word)) "; 
        $ob=" $word, $word2 desc,title like '%$title%' desc ";
     
   //   $q->LIKE('title',$query_array['title']);
      $message.= "Title = " . $query_array['title']. "; ";
     }
     if(strlen($query_array['job_type']))
     {
           if($query_array['job_type']=='all')
            {
                $message.="Job Type = All Job Types;";

            }
            else

            {


               $job_type=$query_array['job_type'];
          $job_type=mysql_real_escape_string($job_type);
        
          
          $q.=" and (job_type='$job_type') ";
          

           
      $message.= "Job Type = " . $query_array['job_type'] . "; ";
            }
     }
     if(strlen($query_array['city']))
     {
           if($query_array['city']=='all')
            {
                $message.="City = All Cities;";

            }
            else
            {
   
               $city=$query_array['city'];
          $city=mysql_real_escape_string($city);
          
          
          $q.=" and (city='$city') ";
          
      $message.= "City = " . $query_array['city'] . "; ";
          }
     }
     if(strlen($query_array['salary']))
     {
          if($query_array['salary']=='all')
            {
                $message.="Salary = All Salaries;";

            }
            else
            {


               $salary=$query_array['salary'];
          $salary=mysql_real_escape_string($salary);
         
         
          $q.=" and (salary='$salary') ";
        
       $message.= "Salary = ". $query_array['salary'] . "; ";
     }

       }        
      if($sort_order=='none')
        {
          $sort_order="";
        }
      
      
       if($sort_by !=='none' && empty($ob))
       {

          $ob=' order by ' .$sort_by.' '.$sort_order;
       }
       else if(!empty($ob) &&$sort_by=='none')
       {
          $ob='order by '. $ob;
       }
       else if($sort_by !=='none' && !empty($ob))
       {
          $ob ='order by ' .$sort_by. ' ' .$sort_order.','. $ob;//.','.$sort_by .' '.$sort_order;
       }
       else if($sort_by == 'none' && empty($ob))
       {

          $ob='';
       }
      
       $qu="select * from jobs
        where job_status !='expired' and delete_status !='true' ".$q . $ob ." limit $offset,$limit";
       $qu=$this->db->query($qu);
     // echo "some:"; print_r($qu->result_array());echo "<br>";
      $ret['rows'] = $qu->result_array();
      $ret['message']= $message;
      

  $q2="";
    

    if(strlen($query_array['c_name']))
     {
        $c_name=$query_array['c_name'];
        $c_name=mysql_real_escape_string($c_name);
      $keyword=preg_split("/[^a-zA-Z]+|[\s]+/",$c_name);
$some='';
 foreach($keyword as $values)
      {
        if(!empty($values))
        {
        $some[]=$values;
         }
      }

$word="' '";
$word2="' '";
if(!empty($some))
{
  $word=" c_name regexp ";
$re=(implode('|',$some));
$word.="'$re'";
//$word.="regexp "
} 
         
           $q2.=" and (c_name like '%$c_name%' or $word) " ;
         
    
         $message.= "Company Name= " . $query_array['c_name']."; ";

     }
    if(strlen($query_array['id']))
     {
          $id=$query_array['id'];
          $id=mysql_real_escape_string($id);
          
          
          $q2.=" and (id='$id') ";
         
   
      $message.= "job id = " .$query_array['id']."; ";

     }
     if(strlen($query_array['category']))
     {
             
            if($query_array['category']=='all')
            {
                $message.="Category = ". "All Categories;";

            }
            else{

               $category=$query_array['category'];
          $category=mysql_real_escape_string($category);
          
          
          $q2.=" and (category='$category') ";
        
      $message.= "Category = " .$query_array['category']."; ";
                }
     }

    
     if(strlen($query_array['title']))
     {
          $title=$query_array['title'];
          $title=mysql_real_escape_string($title);
      $keyword=preg_split("/[^a-zA-Z]+|[\s]+/",$title);
     $some='';
      foreach($keyword as $values)
      {
        if(!empty($values))
        {
        $some[]=$values;
         }
      }

$word="' '";
$word2="' '";
if(!empty($some))
{
  $word=" title regexp ";
$re=(implode('|',$some));
$word.="'$re'";
//$word.="regexp "

 
foreach($some as $key=>$values)
{
  if($key==0)
  {
    $word2=" title Like '%$values%' ";
  }
  else
  {
  $word2.="and title Like '%$values%' ";
    }
}         
}       
        
        
        $q2.=" and ((title like '%$title%') or ($word2) or ($word)) "; //order by (title regexp '$word'), ($word2)desc,(title like '%$title%')desc)";
      
  
      $message.= "Title = " . $query_array['title']. "; ";
     }
     if(strlen($query_array['job_type']))
     {
           if($query_array['job_type']=='all')
            {
                $message.="Job Type = All Job Types;";

            }
            else

            {


               $job_type=$query_array['job_type'];
          $job_type=mysql_real_escape_string($job_type);
        
         
          $q2.=" and (job_type='$job_type') ";
         
      $message.= "Job Type = " . $query_array['job_type'] . "; ";
            }
     }
     if(strlen($query_array['city']))
     {
           if($query_array['city']=='all')
            {
                $message.="City = All Cities;";

            }
            else
            {
   
               $city=$query_array['city'];
          $city=mysql_real_escape_string($city);
         
         
          $q2.=" and (city='$city') ";
       
      $message.= "City = " . $query_array['city'] . "; ";
          }
     }
     if(strlen($query_array['salary']))
     {
          if($query_array['salary']=='all')
            {
                $message.="Salary = All Salaries;";

            }
            else
            {


               $salary=$query_array['salary'];
          $salary=mysql_real_escape_string($salary);
        
         
          $q2.=" and (salary='$salary') ";
       
       $message.= "Salary = ". $query_array['salary'] . "; ";
     }

       }    
       $qu2="select COUNT(*) as total from jobs 
        where job_status !='expired' and delete_status !='true' ".$q2;
      $qu2=$this->db->query($qu2);
    $tmp = $qu2->result_array();
    //$ab=$this->db->query("select id,title from jobs where expired !='true' and delete_status !='true'
      //".$q2);
  // print_r($ab->result_array());
   
   
    $ret['total_rows'] = $tmp[0]['total'];


    return $ret;

}
public function search_category($query,$sort_by,$sort_order,$limit,$offset)
{

 $result= $this->db->select('*')->from('job_categories')->where('delete_status !=','true')->where_not_in('category',array('Others','Other(Specify)'))
     ->limit($limit,$offset)->order_by($sort_by,$sort_order);
        
     $message="";
     if(strlen($query['category']))
     {
          $result->like('category',$query['category']);
      //   $message.= "Company Name= " . $query_array['c_name']."; ";

     }

      
 
      $ret['rows'] = $result->get()->result_array();
      $ret['message']= $message;
      

$result=$this->db->select('COUNT(*) as count',FALSE)->where('delete_status !=','true')->where_not_in('category',array('Others','Other(Specify)'))->
    from('job_categories');
     if(strlen($query['category']))
     {
         $result->like('category',$query['category']);
     }
     
    $tmp = $result->get()->result();
   
   
    $ret['total_rows'] = $tmp[0]->count;


    return $ret;


}
public function search_other_categories($query,$sort_by,$sort_order,$limit,$offset)
{


 $query= $this->db->select('*')->from('other_categories')->where('delete_status !=','true')
     ->limit($limit,$offset)->order_by($sort_by,$sort_order);
        
     $message="";
/*     if()
     {
        $query->like('category',$q);
      //   $message.= "Company Name= " . $query_array['c_name']."; ";

     }
*/
      
 
      $ret['rows'] = $query->get()->result_array();
      $ret['message']= $message;
      

$query=$this->db->select('COUNT(*) as count',FALSE)->where('delete_status !=','true')
   ->from('other_categories');
/*     if(!empty($q))
     {
         $query->like('category',$q);
     }
*/     
    $tmp = $query->get()->result();
   
   
    $ret['total_rows'] = $tmp[0]->count;


    return $ret;



}

public function search_city($query,$sort_by,$sort_order,$limit,$offset)
{

 $result= $this->db->select('*')->from('cities')->where('delete_status !=','true')->where_not_in('cities',array('Others','Other(Specify)'))->
      limit($limit,$offset)->order_by($sort_by,$sort_order);
        
     $message="";
     if(strlen($query['city']))
     {
          $result->like('cities',$query['city']);
      //   $message.= "Company Name= " . $query_array['c_name']."; ";

     }

      
 
      $ret['rows'] = $result->get()->result_array();
      $ret['message']= $message;
      

$result=$this->db->select('COUNT(*) as count',FALSE)->where('delete_status !=','true')->where_not_in('cities',array('Others','Other(Specify)'))->
    from('cities');
     if(strlen($query['city']))
     {
         $result->like('cities',$query['city']);
     }
     
    $tmp = $result->get()->result();
   
   
    $ret['total_rows'] = $tmp[0]->count;


    return $ret;


}



public function search_other_cities($query,$sort_by,$sort_order,$limit,$offset)
{


 $query= $this->db->select('*')->from('other_cities')->where('delete_status !=','true')
      ->limit($limit,$offset)->order_by($sort_by,$sort_order);
        
     $message="";
/*     if()
     {
        $query->like('category',$q);
      //   $message.= "Company Name= " . $query_array['c_name']."; ";

     }
*/
      
 
      $ret['rows'] = $query->get()->result_array();
      $ret['message']= $message;
      

$query=$this->db->select('COUNT(*) as count',FALSE)->where('delete_status !=','true')
   -> from('other_cities');
/*     if(!empty($q))
     {
         $query->like('category',$q);
     }
*/     
    $tmp = $query->get()->result();
   
   
    $ret['total_rows'] = $tmp[0]->count;


    return $ret;



}
public function top_jobs()
{
//having(array('expired_status !='=>'false'))
  
$result = $this->db->select('job_id,jobs.job_status,delete,delete_status, Count("job_id") as top_jobs')->from('applicants')
  ->group_by('job_id')->order_by('top_jobs','desc')->limit(20)
  ->join('jobs','applicants.job_id=jobs.id')->having(array('delete_status !='=>'true','jobs.job_status'=>'opened'));
  //print_r($result->get()->result_array()); 
  
  $result=$result->get()->result_array();
    foreach($result as $value)
          {
              $top_ten_jobs[] =  $this->get_top_job($value['job_id']);
              
          }
          if(empty($top_ten_jobs))
          {
             return array();
          }
          else
          {
          return $top_ten_jobs;
          }
}
public function get_top_job($no)
{
          $this->db->select('id,user_c,logo,c_name,category,title')->where('id',$no);
         $result =  $this->db->get('jobs')->row_array();
          // echo "from model";  print_r($result);
           // echo $result['category'];



           //   $logo=$this->news_model->get_logo($result['user_c']);
        //     $result['logo']=$logo;
   
                return $result;

}
public function recommended()
{


$user=$this->session->userdata('username');
      $major=$this->db->select('major')->from('students')->where('user',$user)->get()->row_array();
      $major=$major['major'];
     
     global $title,$t_word,$t_word2,$c_word,$c_word2,$d_word,$d_word2,$r_word,$r_word2;
   // $title= $t_word=$t_word2=$c_word=$c_word2=$d_word=$d_word2=$r_word=$r_word2="";
function search_key($string)
{
  global $title,$t_word,$t_word2,$c_word,$c_word2,$d_word,$d_word2,$r_word,$r_word2;

$title=mysql_real_escape_string($string);
 



$keyword=preg_split("/[^a-zA-Z]+|[\s]+/",$title);

 

//print_r($some);
$some='';
foreach ($keyword as $values)
{
  if(!empty($values))
  {
    $some[]=$values;
  }
}

 $t_word="''";
if(!empty($some))
{ 
  $t_word=" title regexp ";
 $t_word.="'".implode('|',$some)."'";
}





$t_word2="''";
if(!empty($some))
{
foreach($some as $key=>$values)
{
  if($key==0)
  {
    $t_word2="  title like '%$values%' ";
  }
  else
  {
  $t_word2.="and title like '%$values%' ";
    }
}

}



 $c_word="''";
if(!empty($some))
{ 
 $c_word=" category regexp ";
 $c_word.="'".implode('|',$some)."'";
}





$c_word2="''";
if(!empty($some))
{
foreach($some as $key=>$values)
{
  if($key==0)
  {
    $c_word2="  category like '%$values%' ";
  }
  else
  {
  $c_word2.="and category like '%$values%' ";
    }
}

}

 $d_word="''";
if(!empty($some))
{ 
 $d_word=" descriptions regexp ";
 $d_word.="'".implode('|',$some)."'";
}





$d_word2="''";
if(!empty($some))
{
foreach($some as $key=>$values)
{
  if($key==0)
  {
    $d_word2="  descriptions like '%$values%' ";
  }
  else
  {
  $d_word2.="and descriptions like '%$values%' ";
    }
}

}


 $r_word="''";
if(!empty($some))
{ 
 $r_word=" responsibilities regexp ";
 $r_word.="'".implode('|',$some)."'";
}





$r_word2="''";
if(!empty($some))
{
foreach($some as $key=>$values)
{
  if($key==0)
  {
    $r_word2="  responsibilities like '%$values%' ";
  }
  else
  {
  $r_word2.="and responsibilities like '%$values%' ";
    }
}

}
 

 
}
     

$id=$this->db->select('job_id')->where('user',$user)->from('students_jobs')->get()->result_array();
 
if(!empty($id))
{
foreach ($id as $value)
{

     $apid[]=$value['job_id'];
}
}
else
{
    
//echo "applied Id" ;print_r($id);
//echo "<br><br>";print_r($apid);
 $apid_condition="' '";
}
 if(!empty($apid))
 {                 
 
    
 $apid_condition="'".implode("','",$apid)."'";
  
}


      search_key($major);
   
?>



 <?php


          
 $recommended=array();
$a=$this->db->query("SELECT jobs.user_c,category,c_name,id,title,count('*')as total from Jobs,applicants where jobs.id=applicants.job_id and ((jobs.title like '%$title%') or ($t_word) or ($t_word2)) and job_id not in ($apid_condition) and job_status='opened' and delete_status !='true' group by job_id order by (jobs.title like '%$title%')desc,($t_word2)desc,total desc");
$a=$a->result_array();
 
 



$b=$this->db->query("SELECT jobs.user_c,category,c_name,id,title,Count('*')as total from Jobs,applicants where jobs.id=applicants.job_id and ((jobs.category like '%$title%') or ($c_word) or ($c_word2)) and job_id not in ($apid_condition) and job_status='opened' and delete_status !='true' group by job_id order by (jobs.category like '%$title%')desc,($c_word2)desc, total desc"); 

 
 
$c=$this->db->query("SELECT jobs.user_c,category,c_name,id,title,Count('*')as total from Jobs,applicants where jobs.id=applicants.job_id and ((jobs.descriptions like '%$title%') or($d_word) or ($d_word2)) and job_id not in ($apid_condition) and job_status='opened' and delete_status !='true' group by job_id order by (jobs.descriptions like '%$title%')desc,($d_word2)desc,total desc"); 


$cc=$this->db->query("SELECT jobs.user_c,category,c_name,id,title,Count('*')as total from Jobs,applicants where jobs.id=applicants.job_id and ((jobs.responsibilities like '%$title%') or($r_word) or ($r_word2)) and job_id not in ($apid_condition) and job_status='opened' and delete_status!='true' group by job_id order by (jobs.responsibilities like '%$title%')desc,($r_word2)desc, total desc"); 


function search_array($needle,$haystack)
{

        foreach($haystack as $value)
        {


          if($needle==$value['id'])
          {

            return true;
          }
        }



}
 $max=4;
  

 for($i=0;$i<2;$i++)
 {  
 foreach($a as $keys=>$value)
 {      
        
          
          if( !search_array($value['id'],$recommended)&&count($recommended)<$max)
          {
             $recommended[]=$value;
             break;
              

          }
         
          
 }

 foreach($b->result_array() as $keys=>$value)
 {
    
          if(!search_array($value['id'],$recommended)&&count($recommended)<$max)
          {
             $recommended[]=$value;
             
             break;

          }
        

 }
 foreach($c->result_array() as $keys=>$value)
 {
          if(!search_array($value['id'],$recommended)&&count($recommended)<$max)
          {
             $recommended[]=$value;
             
             break;

          }
         

 }
foreach($cc->result_array() as $keys=>$value)
 {
          if(!search_array($value['id'],$recommended)&&count($recommended)<$max)
          {
             $recommended[]=$value;
             
             break;

          }
         

 }
}


 


$my_top_views=$this->db->query("SELECT c_name,category,jobs.id,title,view_times from jobs,view_history where jobs.id=view_history.job_id and user ='$user' order by view_times desc limit 25"); 
 
$my_top_views=$my_top_views->result_array();
 
for($count=0;$count<4;$count++)
 { 

foreach($my_top_views as $values)
{
     
//    echo $values['title']; 
       if(count($recommended)>=20)
       {
          break;
       }
       else
       {

       }


search_key($values['title']);
//search_key($d[0]['title']);

 

 
$e=$this->db->query("SELECT jobs.user_c,c_name,category,jobs.id,title,sum(view_times) from jobs,view_history where jobs.id=view_history.job_id and ((jobs.title like '%$title%') or ($t_word) or ($t_word2)) and job_id not in ($apid_condition) and job_status='opened' and delete_status !='true' group by job_id order by (jobs.title like '%$title%')desc,($t_word2)desc, view_times desc"); 


$f=$this->db->query("SELECT jobs.user_c,c_name,category,jobs.id,title,sum(view_times) from jobs,view_history where jobs.id=view_history.job_id and ((jobs.category like '%$title%') or ($c_word) or ($c_word2)) and job_id not in ($apid_condition) and job_status='opened' and delete_status !='true' group by job_id order by (jobs.category like '%$title%')desc,($c_word2)desc,view_times desc"); 
 

      
$g=$this->db->query("SELECT jobs.user_c,c_name,category,jobs.id,title,sum(view_times) from jobs,view_history where jobs.id=view_history.job_id and ((jobs.descriptions like '%$title%') or ($d_word) or ($d_word2)) and job_id not in ($apid_condition) and job_status='opened' and delete_status !='true' group by job_id order by (jobs.descriptions like '%$title%')desc,($d_word2)desc,view_times desc"); 


$i=$this->db->query("SELECT jobs.user_c,c_name,category,jobs.id,title,sum(view_times) from jobs,view_history where jobs.id=view_history.job_id and ((jobs.responsibilities like '%$title%') or ($r_word) or ($r_word2)) and job_id not in ($apid_condition) and job_status='opened' and delete_status !='true' group by job_id order by (jobs.responsibilities like '%$title%')desc,($r_word2)desc, view_times desc"); 

     


 $max=20;
  

 
   
      
 foreach($e->result_array() as $keys=>$value)
 {      
        
          
          if(!search_array($value['id'],$recommended)&&count($recommended)<$max)
          {
            
             $recommended[]=$value;
             break;
              

          }
         
          
 }

 foreach($f->result_array() as $keys=>$value)
 {
    
          if(!search_array($value['id'],$recommended)&&count($recommended)<$max)
          {
             $recommended[]=$value;
             
             break;

          }
         

 }
 foreach($g->result_array() as $keys=>$value)
 {
          if(!search_array($value['id'],$recommended)&&count($recommended)<$max)
          {
             $recommended[]=$value;
             
             break;

          }
       

 }
foreach($i->result_array() as $keys=>$value)
 {
          if(!search_array($value['id'],$recommended)&&count($recommended)<$max)
          {
             $recommended[]=$value;
             
             break;

          }
         

 }
}//close main foreach         



}//close for

search_key($major);



$m_array=$this->db->query("SELECT jobs.user_c,c_name,category,jobs.id,title,sum(view_times) from jobs,view_history where jobs.id=view_history.job_id and ((jobs.title like '%$title%') or ($t_word) or ($t_word2)) and job_id not in ($apid_condition) and job_status='opened' and delete_status !='true' group by job_id order by (jobs.title like '%$title%')desc,($t_word2)desc, view_times desc"); 


$m_array2=$this->db->query("SELECT jobs.user_c,c_name,category,jobs.id,title,sum(view_times) from jobs,view_history where jobs.id=view_history.job_id and ((jobs.category like '%$title%') or ($c_word) or ($c_word2)) and job_id not in ($apid_condition) and job_status='opened' and delete_status !='true' group by job_id order by (jobs.category like '%$title%')desc,($c_word2)desc,view_times desc"); 
 

      
$m_array3=$this->db->query("SELECT jobs.user_c,c_name,category,jobs.id,title,sum(view_times) from jobs,view_history where jobs.id=view_history.job_id and ((jobs.descriptions like '%$title%') or ($d_word) or ($d_word2)) and job_id not in ($apid_condition) and job_status='opened' and delete_status !='true' group by job_id order by (jobs.descriptions like '%$title%')desc,($d_word2)desc,view_times desc"); 


$m_array4=$this->db->query("SELECT jobs.user_c,c_name,category,jobs.id,title,sum(view_times) from jobs,view_history where jobs.id=view_history.job_id and ((jobs.responsibilities like '%$title%') or ($r_word) or ($r_word2)) and job_id not in ($apid_condition) and job_status='opened' and delete_status !='true' group by job_id order by (jobs.responsibilities like '%$title%')desc,($r_word2)desc, view_times desc");



 $max=20;
  

 
 for($count=0;$count<4;$count++)
 {  
      
 foreach($m_array->result_array() as $keys=>$value)
 {      
        
          
          if(!search_array($value['id'],$recommended)&&count($recommended)<$max)
          {
            
             $recommended[]=$value;
             break;
              

          }
         
          
 }

 foreach($m_array2->result_array() as $keys=>$value)
 {
    
          if(!search_array($value['id'],$recommended)&&count($recommended)<$max)
          {
             $recommended[]=$value;
             
             break;

          }
         

 }
 foreach($m_array3->result_array() as $keys=>$value)
 {
          if(!search_array($value['id'],$recommended)&&count($recommended)<$max)
          {
             $recommended[]=$value;
             
             break;

          }
       

 }
foreach($m_array4->result_array() as $keys=>$value)
 {
          if(!search_array($value['id'],$recommended)&&count($recommended)<$max)
          {
             $recommended[]=$value;
             
             break;

          }
         

 }
}
 
$top_views=$this->db->query("SELECT jobs.user_c,c_name,category,jobs.id,title,view_times from jobs,view_history where jobs.id=view_history.job_id and job_id not in ($apid_condition) and job_status='opened' and delete_status !='true' group by job_id order by view_times desc limit 40"); 

foreach($top_views->result_array() as $keys=>$value)
 {      
        
          
          if(!search_array($value['id'],$recommended)&&count($recommended)<$max)
          {
            
             $recommended[]=$value;
              
              

          }
         
          
 }

return $recommended;






}



public function recommended2($job_id='')
{


     
     global $title,$t_word,$t_word2,$c_word,$c_word2,$d_word,$d_word2,$r_word,$r_word2;
     global $recommended;$user;
     $recommended=array();
     $user=$this->session->userdata('username');

   // $title= $t_word=$t_word2=$c_word=$c_word2=$d_word=$d_word2=$r_word=$r_word2="";
function search_key($string,$max1,$max2,$job_id)
{
  global $title,$t_word,$t_word2,$c_word,$c_word2,$d_word,$d_word2,$r_word,$r_word2;
  global $recommended;$user;
$title=mysql_real_escape_string($string);
 $job_id="'".$job_id."'";



$keyword=preg_split("/[^a-zA-Z]+|[\s]+/",$title);

 

//print_r($some);
$some='';
foreach ($keyword as $values)
{
  if(!empty($values))
  {
    $some[]=$values;
  }
}

 $t_word="''";
if(!empty($some))
{ 
  $t_word=" title regexp ";
 $t_word.="'".implode('|',$some)."'";
}





$t_word2="''";
if(!empty($some))
{
foreach($some as $key=>$values)
{
  if($key==0)
  {
    $t_word2="  title like '%$values%' ";
  }
  else
  {
  $t_word2.="and title like '%$values%' ";
    }
}

}



 $c_word="''";
if(!empty($some))
{ 
 $c_word=" category regexp ";
 $c_word.="'".implode('|',$some)."'";
}





$c_word2="''";
if(!empty($some))
{
foreach($some as $key=>$values)
{
  if($key==0)
  {
    $c_word2="  category like '%$values%' ";
  }
  else
  {
  $c_word2.="and category like '%$values%' ";
    }
}

}

 $d_word="''";
if(!empty($some))
{ 
 $d_word=" descriptions regexp ";
 $d_word.="'".implode('|',$some)."'";
}





$d_word2="''";
if(!empty($some))
{
foreach($some as $key=>$values)
{
  if($key==0)
  {
    $d_word2="  descriptions like '%$values%' ";
  }
  else
  {
  $d_word2.="and descriptions like '%$values%' ";
    }
}

}


 $r_word="''";
if(!empty($some))
{ 
 $r_word=" responsibilities regexp ";
 $r_word.="'".implode('|',$some)."'";
}





$r_word2="''";
if(!empty($some))
{
foreach($some as $key=>$values)
{
  if($key==0)
  {
    $r_word2="  responsibilities like '%$values%' ";
  }
  else
  {
  $r_word2.="and responsibilities like '%$values%' ";
    }
}

}
 
$CI = & get_instance(); 
 $user=$CI->session->userdata('username');

     

$id=$CI->db->select('job_id')->where('user',$user)->from('students_jobs')->get()->result_array();

if(!empty($id))
{
foreach ($id as $value)
{

     $apid[]=$value['job_id'];
}
}
else
{
    
//echo "applied Id" ;print_r($id);
//echo "<br><br>";print_r($apid);
 $apid_condition="' '";
}
 if(!empty($apid))
 {                 
 
    
 $apid_condition="'".implode("','",$apid)."'";
  
}


      
   
?>



 <?php


          
 
$a=$CI->db->query("SELECT jobs.user_c,category,c_name,id,title,count('*')as total from Jobs,applicants where jobs.id=applicants.job_id and ((jobs.title like '%$title%') or ($t_word) or ($t_word2)) and job_id not in ($apid_condition) and job_id not in ($job_id) and job_status='opened' and delete_status !='true' group by job_id order by (jobs.title like '%$title%')desc,($t_word2)desc,total desc");
$a=$a->result_array();

 
 



$b=$CI->db->query("SELECT jobs.user_c,category,c_name,id,title,Count('*')as total from Jobs,applicants where jobs.id=applicants.job_id and ((jobs.category like '%$title%') or ($c_word) or ($c_word2)) and job_id not in ($apid_condition) and job_id not in ($job_id) and job_status='opened' and delete_status !='true' group by job_id order by (jobs.category like '%$title%')desc,($c_word2)desc, total desc"); 

 
 
$c=$CI->db->query("SELECT jobs.user_c,category,c_name,id,title,Count('*')as total from Jobs,applicants where jobs.id=applicants.job_id and ((jobs.descriptions like '%$title%') or($d_word) or ($d_word2)) and job_id not in ($apid_condition) and job_id not in ($job_id) and job_status='opened' and delete_status !='true' group by job_id order by (jobs.descriptions like '%$title%')desc,($d_word2)desc,total desc"); 


$cc=$CI->db->query("SELECT jobs.user_c,category,c_name,id,title,Count('*')as total from Jobs,applicants where jobs.id=applicants.job_id and ((jobs.responsibilities like '%$title%') or($r_word) or ($r_word2)) and job_id not in ($apid_condition) and job_id not in ($job_id) and job_status='opened' and delete_status!='true' group by job_id order by (jobs.responsibilities like '%$title%')desc,($r_word2)desc, total desc"); 



 $max=$max1;
 

 for($i=0;$i<2;$i++)
 {  
 foreach($a as $keys=>$value)
 {      
        
          
          if( !search_array($value['id'],$recommended)&&count($recommended)<$max)
          {
             $recommended[]=$value;
             break;
              

          }
         
          
 }

 foreach($b->result_array() as $keys=>$value)
 {
    
          if(!search_array($value['id'],$recommended)&&count($recommended)<$max)
          {
             $recommended[]=$value;
             
             break;

          }
        

 }
 foreach($c->result_array() as $keys=>$value)
 {
          if(!search_array($value['id'],$recommended)&&count($recommended)<$max)
          {
             $recommended[]=$value;
             
             break;

          }
         

 }
foreach($cc->result_array() as $keys=>$value)
 {
          if(!search_array($value['id'],$recommended)&&count($recommended)<$max)
          {
             $recommended[]=$value;
             
             break;

          }
         

 }
}


 


//$my_title;//=$this->db->query("SELECT c_name,category,jobs.id,title,view_times from jobs,view_history where jobs.id=view_history.job_id and user ='$user' order by view_times desc limit 25"); 

//$my_top_views=$my_top_views->result_array();
 $max=$max2;
for($count=0;$count<4;$count++)
 { 

//foreach($my_top_views as $values)
//{
     
//    echo $values['title']; 
       if(count($recommended)>=$max)
       {
          break;
       }
       else
       {

       }


//search_key($values['title']);
//search_key($d[0]['title']);

 

 
$e=$CI->db->query("SELECT jobs.user_c,c_name,category,jobs.id,title,sum(view_times) from jobs,view_history where jobs.id=view_history.job_id and ((jobs.title like '%$title%') or ($t_word) or ($t_word2)) and job_id not in ($apid_condition) and job_id not in ($job_id) and job_status='opened' and delete_status !='true' group by job_id order by (jobs.title like '%$title%')desc,($t_word2)desc, view_times desc limit 40"); 


$f=$CI->db->query("SELECT jobs.user_c,c_name,category,jobs.id,title,sum(view_times) from jobs,view_history where jobs.id=view_history.job_id and ((jobs.category like '%$title%') or ($c_word) or ($c_word2)) and job_id not in ($apid_condition) and job_id not in ($job_id) and job_status='opened' and delete_status !='true' group by job_id order by (jobs.category like '%$title%')desc,($c_word2)desc,view_times desc limit 40"); 
 

      
$g=$CI->db->query("SELECT jobs.user_c,c_name,category,jobs.id,title,sum(view_times) from jobs,view_history where jobs.id=view_history.job_id and ((jobs.descriptions like '%$title%') or ($d_word) or ($d_word2)) and job_id not in ($apid_condition) and job_id not in ($job_id) and job_status='opened' and delete_status !='true' group by job_id order by (jobs.descriptions like '%$title%')desc,($d_word2)desc,view_times desc limit 40"); 


$i=$CI->db->query("SELECT jobs.user_c,c_name,category,jobs.id,title,sum(view_times) from jobs,view_history where jobs.id=view_history.job_id and ((jobs.responsibilities like '%$title%') or ($r_word) or ($r_word2)) and job_id not in ($apid_condition) and job_id not in ($job_id) and job_status='opened' and delete_status !='true' group by job_id order by (jobs.responsibilities like '%$title%')desc,($r_word2)desc, view_times desc limit 40"); 

     


 
  

 
   
      
 foreach($e->result_array() as $keys=>$value)
 {      
        
          
          if(!search_array($value['id'],$recommended)&&count($recommended)<$max)
          {
            
             $recommended[]=$value;
             break;
              

          }
         
          
 }

 foreach($f->result_array() as $keys=>$value)
 {
    
          if(!search_array($value['id'],$recommended)&&count($recommended)<$max)
          {
             $recommended[]=$value;
             
             break;

          }
         

 }
 foreach($g->result_array() as $keys=>$value)
 {
          if(!search_array($value['id'],$recommended)&&count($recommended)<$max)
          {
             $recommended[]=$value;
             
             break;

          }
       

 }
foreach($i->result_array() as $keys=>$value)
 {
          if(!search_array($value['id'],$recommended)&&count($recommended)<$max)
          {
             $recommended[]=$value;
             
             break;

          }
         

 }
       



}//close for

}//close search_key function

function search_array($needle,$haystack)
{

        foreach($haystack as $value)
        {


          if($needle==$value['id'])
          {

            return true;
          }
        }



}

 $user=$this->session->userdata('username');
    $title=$this->db->select('id,title')->from('jobs')->where('id',$job_id)->get()->row_array();
    $title=$title['title'];
   
     $category=$this->db->select('category')->from('jobs')->where('id',$job_id)->get()->row_array();
     $category=$category['category'];
 search_key($title,4,10,$job_id);
  search_key($category,4,20,$job_id);
 $max=20;
 

 $user=$this->session->userdata('username');

     

$id=$this->db->select('job_id')->where('user',$user)->from('students_jobs')->get()->result_array();
 
if(!empty($id))
{
foreach ($id as $value)
{

     $apid[]=$value['job_id'];
}
}
else
{
    
//echo "applied Id" ;print_r($id);
//echo "<br><br>";print_r($apid);
 $apid_condition="' '";
}
 if(!empty($apid))
 {                 
 
    
 $apid_condition="'".implode("','",$apid)."'";
  
}
$top_views=$this->db->query("SELECT jobs.user_c,c_name,category,jobs.id,title,view_times from jobs,view_history where jobs.id=view_history.job_id and job_id not in ($apid_condition) and job_status='opened' and delete_status !='true' group by job_id order by view_times desc limit 40"); 

/*foreach($top_views->result_array() as $keys=>$value)
 {      
        
          
          if(!search_array($value['id'],$recommended)&&count($recommended)<$max)
          {
            
             $recommended[]=$value;
              
              

          }
         
          
 }
*/
return $recommended;


}
public function search_all_students($query_array,$limit,$offset,$sort_by,$sort_order)
{
//$sort_order= ($sort_order == 'desc')?'desc': 'asc';
    //$sort_columns = array('JobC','JobT','JobS','JobD');
     // $sort_by = (in_array($sort_by,$sort_columns))?$sort_by:'JobC';
$user = $this->session->userdata('username');
//$university = $this->news_model->get_university_name($user);
      $q=$this->db->select('*')->from('students')
      ->limit($limit,$offset)->order_by($sort_by,$sort_order);
      $search_results = "";  
     
    if(strlen($query_array['id']))
     {

        $q->where('id',$query_array['id']);
        $search_results.='Student Id:'. $query_array['id'].";";

     }
     if(strlen($query_array['nrc']))
     {
         $q->where('nrc',$query_array['nrc']);
         $search_results.='NRC: ' .$query_array['nrc'].";";

     }
     if(strlen($query_array['u_name']))
     {
         $q->where('u_name',$query_array['u_name']);
         $search_results.="University Name: ". $query_array['u_name'].";";
     }
     if(strlen($query_array['name']))
     {

        $q->like('name',$query_array['name']);
        $search_results.='Name: ' . $query_array['name'].";";
     }
         if(strlen($query_array['industry']))
     {

        $q->like('industry',$query_array['industry']);
        $search_results.='Major: '. $query_array['industry']. ";";
     }    
      if(strlen($query_array['status']))
     {
          
        
        $q->where("status",$query_array['status']);
        $search_results.='Status: ' .$query_array['status']. ";";

       }        
     

      //->order_by($sort_by,$sort_order);
     //  return $q->get()->result();
 
      //$ret['rows'] 
       $ret['rows'] = $q->get()->result_array();
       $ret['search_results']= $search_results;
       //return $ret;
      
      
  $q=$this->db->select('COUNT(*) as count',FALSE)->
    from('students');
    ////->where('u_name',$university);
       if(strlen($query_array['id']))
     {

        $q->where('id',$query_array['id']);
     }        
     if(strlen($query_array['nrc']))
     {
       $q->where('nrc',$query_array['nrc']);


     }
     if(strlen($query_array['u_name']))
     {
          $q->where('u_name',$query_array['u_name']);

     }
     if(strlen($query_array['name']))
     {

        $q->like('name',$query_array['name']);
     } 

     if(strlen($query_array['industry']))
     {

        $q->like('industry',$query_array['industry']);
     }
      if(strlen($query_array['status']))
     {

     
        $q->where("status",$query_array['status']);

       }     
    $tmp = $q->get()->result_array();
  // print_r($tmp);

   $ret['num_rows'] = $tmp[0]['count'];
  // print_r($ret);
   
   // $ret['num_rows'] = $tmp[0]->count;
    //print_r($ret);


    return $ret;

}

public function search_students($query_array,$limit,$offset,$sort_by,$sort_order)
{
//$sort_order= ($sort_order == 'desc')?'desc': 'asc';
    //$sort_columns = array('JobC','JobT','JobS','JobD');
     // $sort_by = (in_array($sort_by,$sort_columns))?$sort_by:'JobC';
$user = $this->session->userdata('username');

      $q=$this->db->select('*')->from('students')
      ->limit($limit,$offset)->order_by($sort_by,$sort_order);
      $search_results = "";  
     
    if(strlen($query_array['id']))
     {

        $q->where('id',$query_array['id']);
        $search_results.='Student Id:'. $query_array['id'].";";

     }
     if(strlen($query_array['nrc']))
     {
         $q->where('nrc',$query_array['nrc']);
         $search_results.='NRC: ' .$query_array['nrc'].";";

     }
     if(strlen($query_array['name']))
     {
         $q->like('name',$query_array['name']);
         $search_results.="Name: ". $query_array['name'].";";
     }
     if(strlen($query_array['name']))
     {

        $q->like('major',$query_array['major']);
        $search_results.='Major: ' . $query_array['major'].";";
     }
         if(strlen($query_array['nationality']))
     {

        $q->like('nationality',$query_array['nationality']);
        $search_results.='Nationality: '. $query_array['nationality']. ";";
     }    
      if(strlen($query_array['status']))
     {
          
        
        $q->where("status",$query_array['status']);
        $search_results.='Status: ' .$query_array['status']. ";";

       }        
     

      //->order_by($sort_by,$sort_order);
     //  return $q->get()->result();
 
      //$ret['rows'] 
       $ret['rows'] = $q->get()->result_array();
       $ret['search_results']= $search_results;
       //return $ret;
      
      
  $q=$this->db->select('COUNT(*) as count',FALSE)->
    from('students');
    ////->where('u_name',$university);
     if(strlen($query_array['id']))
     {

        $q->where('id',$query_array['id']);
        $search_results.='Student Id:'. $query_array['id'].";";

     }
     if(strlen($query_array['nrc']))
     {
         $q->where('nrc',$query_array['nrc']);
         $search_results.='NRC: ' .$query_array['nrc'].";";

     }
     if(strlen($query_array['name']))
     {
         $q->like('name',$query_array['name']);
         $search_results.="Name: ". $query_array['name'].";";
     }
     if(strlen($query_array['name']))
     {

        $q->like('major',$query_array['major']);
        $search_results.='Major: ' . $query_array['major'].";";
     }
         if(strlen($query_array['nationality']))
     {

        $q->like('nationality',$query_array['nationality']);
        $search_results.='Nationality: '. $query_array['nationality']. ";";
     }    
      if(strlen($query_array['status']))
     {
          
        
        $q->where("status",$query_array['status']);
        $search_results.='Status: ' .$query_array['status']. ";";

       }        
     

    $tmp = $q->get()->result_array();
   

   $ret['total_rows'] = $tmp[0]['count'];
  


    return $ret;

}


public function search_job_entries($query,$limit,$offset,$sort_by,$sort_order)
{
  $user = $this->session->userdata('username');
$q= $this->db->select('*')->from('jobs')->
      limit($limit,$offset)->order_by($sort_by,$sort_order)->where(array('user_c'=>
        $user,'delete_status !='=>'true','job_status !='=>'expired'));
        
     
    if(strlen($query['id']))
     {

        $q->where('id',$query['id']);
     }
     if(strlen($query['category']))
     {
  
            if($query['category']=='all')
            {
                

            }
            else{
        $q->where('category',$query['category']);
      }
     }
     if(strlen($query['title']))
     {
          
        
        $q->LIKE("title",$query['title']);

       }        
       if(strlen($query['salary']))
       {  
            if($query['salary']=='all')
            {
                

            }
            else{
          $q->where('salary',$query['salary']);
             }
       }
        if(strlen($query['job_type']))
       {  
            if($query['job_type']=='all')
            {
                

            }
            else{
          $q->where('job_type',$query['job_type']);
        }

       }
           if(strlen($query['job_status']))
       {  
           
            
          $q->where('job_status',$query['job_status']);
        }

             

     

     
       $ret['rows'] = $q->get()->result_array();
    
      
      
  $q=$this->db->select('COUNT(*) as count',FALSE)->
    from('jobs')->where(array('user_c'=>$user,'delete_status !='=>'true','job_status !='=>'expired'));
     
   if(strlen($query['id']))
     {

        $q->where('id',$query['id']);
     }
     if(strlen($query['category']))
     {
  
            if($query['category']=='all')
            {
                

            }
            else{
        $q->where('category',$query['category']);
      }
     }
     if(strlen($query['title']))
     {
          
        
        $q->LIKE("title",$query['title']);

       }        
       if(strlen($query['salary']))
       {  
            if($query['salary']=='all')
            {
                

            }
            else{
          $q->where('salary',$query['salary']);
             }
       }
        if(strlen($query['job_type']))
       {  
            if($query['job_type']=='all')
            {
                

            }
            else{
          $q->where('job_type',$query['job_type']);
        }

       }
           if(strlen($query['job_status']))
       {  
            
          $q->where('job_status',$query['job_status']);
        }

           
     
    $tmp = $q->get()->result_array();
 

   $ret['num_rows'] = $tmp[0]['count'];
  


    return $ret;


}


public function search_expired_jobs($query,$limit,$offset,$sort_by,$sort_order)
{
  $user = $this->session->userdata('username');
$q= $this->db->select('*')->from('jobs')->
      limit($limit,$offset)->order_by($sort_by,$sort_order)->where(array('user_c'=>
        $user,'job_status'=>'expired','delete_status !='=>'true'));
        
     
    if(strlen($query['id']))
     {

        $q->where('id',$query['id']);
     }
     if(strlen($query['category']))
     {
  
            if($query['category']=='all')
            {
                

            }
            else{
        $q->where('category',$query['category']);
      }
     }
     if(strlen($query['title']))
     {
          
        
        $q->LIKE("title",$query['title']);

       }        
       if(strlen($query['salary']))
       {  
            if($query['salary']=='all')
            {
                

            }
            else{
          $q->where('salary',$query['salary']);
             }
       }
        if(strlen($query['job_type']))
       {  
            if($query['job_type']=='all')
            {
                

            }
            else{
          $q->where('job_type',$query['job_type']);
        }

       }
     

     
       $ret['rows'] = $q->get()->result_array();
    
      
      
  $q=$this->db->select('COUNT(*) as count',FALSE)->
    from('jobs')->where(array('user_c'=>$user,'job_status'=>'expired','delete_status !='=>'true'));
     
   if(strlen($query['id']))
     {

        $q->where('id',$query['id']);
     }
     if(strlen($query['category']))
     {
  
            if($query['category']=='all')
            {
                

            }
            else{
        $q->where('category',$query['category']);
      }
     }
     if(strlen($query['title']))
     {
          
        
        $q->LIKE("title",$query['title']);

       }        
       if(strlen($query['salary']))
       {  
            if($query['salary']=='all')
            {
                

            }
            else{
          $q->where('salary',$query['salary']);
             }
       }
        if(strlen($query['job_type']))
       {  
            if($query['job_type']=='all')
            {
                

            }
            else{
          $q->where('job_type',$query['job_type']);
        }

       }
     
    $tmp = $q->get()->result_array();
 

   $ret['num_rows'] = $tmp[0]['count'];
  


    return $ret;


}


public function search_applicants($query_array,$limit,$offset,$sort_by,$sort_order)
{

  $user = $this->session->userdata('username');
  $role=$this->session->userdata('role');
 
$q= $this->db->select('*')->from('applicants')->
      limit($limit,$offset)->where(array('applicants.user_c'=>
        $user,'delete !='=>'true'))->join('students','applicants.user=students.user')
      ->order_by($sort_by,$sort_order);
    
     
    if(strlen($query_array['name']))
     {

        $q->like('name',$query_array['name']);
    

       }
       
        if(strlen($query_array['job_id']))
     {

        $q->where('job_id',$query_array['job_id']);
    

       }

        if(strlen($query_array['title']))
     {

        $q->like('job_title',$query_array['title']);
    

       }
           if(strlen($query_array['id']))//// for admin search
     {

        $q->where('id',$query_array['id']);
    

       }
       if(strlen($query_array['applicant_id']))
     {

        $q->where('applicant_id',$query_array['applicant_id']);
    

       }

      //$ret['rows'] 
       $ret['rows'] = $q->get()->result_array();
       //return $ret;
      
      
  $q=$this->db->select('COUNT(*) as count',FALSE)->
    from('applicants')->where(array('user_c'=>$user,'delete !='=>'true'))
    ->join('students','applicants.user=students.user');
       if(strlen($query_array['name']))
     {

        $q->like('name',$query_array['name']);
     }   

               if(strlen($query_array['job_id']))
     {

        $q->where('job_id',$query_array['job_id']);
    

       }

        if(strlen($query_array['title']))
     {

        $q->like('job_title',$query_array['title']);
    

       }
           if(strlen($query_array['id']))//// for admin search
     {

        $q->like('id',$query_array['id']);
    

       }
   if(strlen($query_array['applicant_id']))
     {

        $q->where('applicant_id',$query_array['applicant_id']);
    

       }

    
    $tmp = $q->get()->result_array();
  // print_r($tmp);

   $ret['total_rows'] = $tmp[0]['count'];
  // print_r($ret);
   
   // $ret['num_rows'] = $tmp[0]->count;
    //print_r($ret);


    return $ret;
  }
      public function search_students_jobs($query,$limit,$offset,$sort_by,$sort_order)
{
  $user = $this->session->userdata('username');
  $q=$this->db->select('*')->from('students_jobs')->join('jobs','students_jobs.job_id=jobs.id')
  ->where(array('user'=>$user,'students_jobs.delete_status !='=>'true'))->order_by($sort_by,$sort_order)->limit($limit,$offset);
/*$q= $this->db->select('*')->from('students_jobs')->
      limit($limit,$offset)->order_by($sort_by,$sort_order)->where(array('user'=>
        $user,'delete_status !='=>'true'));

        */
     
    if(strlen($query['job_id']))
     {
        
        $q->where('job_id',$query['job_id']);
     }
     if(strlen($query['c_name']))
     {
 


        $q->LIKE('c_name',$query['c_name']);
     }
     if(strlen($query['category']))
     {
          
          if($query['category']=='all')
            {
                

            }
            else{
        $q->where("category",$query['category']);
             }

       }       



         if(strlen($query['title']))
     {
        
        $q->like('title',$query['title']);
     }
     if(strlen($query['salary']))
     {
  if($query['salary']=='all')
            {
              

            }
            else{
        $q->where('salary',$query['salary']);
              }
     }
     if(strlen($query['job_type']))
     {
          
          if($query['job_type']=='all')
            {
                

            }
            else{
        $q->where("job_type",$query['job_type']);
                 }
       }        
     
      //$ret['rows'] 
      $rows= $q->get()->result_array();
      $result['rows']=$rows;
    
      
      
 /* $q=$this->db->select('COUNT(*) as count',FALSE)->
    from('students_jobs')->where(array('user'=>$user,'delete_status !='=>'true'));*/
      $q=$this->db->select('COUNT(*) as count',FALSE)->from('students_jobs')->join('jobs','students_jobs.job_id=jobs.id')
  ->where(array('user'=>$user,'students_jobs.delete_status !='=>'true'));
    if(strlen($query['job_id']))
     {
        
        $q->where('job_id',$query['job_id']);
     }
     if(strlen($query['c_name']))
     {

        $q->LIKE('c_name',$query['c_name']);
     }
     if(strlen($query['category']))
     {
          
          if($query['category']=='all')
            {
                

            }
            else{
        $q->where("category",$query['category']);
             }

       }       



         if(strlen($query['title']))
     {
        
        $q->like('title',$query['title']);
     }
     if(strlen($query['salary']))
     {
  if($query['salary']=='all')
            {
              

            }
            else{
        $q->where('salary',$query['salary']);
              }
     }
     if(strlen($query['job_type']))
     {
          
          if($query['job_type']=='all')
            {
                

            }
            else{
        $q->where("job_type",$query['job_type']);
                 }
       }        
    $tmp = $q->get()->result_array();
   

   $result['total_rows'] = $tmp[0]['count'];
  
    return $result;
  }
  public function search_save_jobs($query_array,$limit,$offset,
            $sort_by,$sort_order)
  {

  $user = $this->session->userdata('username');
  $this->db->select('*')->limit($limit,$offset)->where(array('user'=>$user,'save_jobs.delete_status !='=>'true'))
  ->order_by($sort_by,$sort_order);
    $this->db->from('save_jobs');
    $this->db->join('jobs','jobs.id=save_jobs.job_id');
        
    $q = $this->db->get();

/*$this->db->select('*')->from('save_jobs')->limit($limit,$offset)->where(array('user'=>$user,'save_jobs.delete_status !='=>'true'));
    $this->db->join('jobs','jobs.id=save_jobs.job_id');
    $this->db->order_by($sort_by,$sort_order);    
    $q = $this->db->get();
*/ 

      
        
     
    /*if(strlen($query_array['job_id']))
     {

        $q->where('job_id',$query_array['job_id']);
     }
     if(strlen($query_array['name']))
     {

        $q->LIKE('name',$query_array['name']);
     }
     if(strlen($query_array['date_applied']))
     {
          
        
        $q->LIKE("date_applied",$query_array['date_applied']);

       }        */
     

     
      //// $ret['rows'] = $q->get()->result_array();
      $result['rows'] = $q->result_array();
         
    //  $ret['rows_count']=$q->num_rows();
      
      
  $this->db->select('COUNT(*) as count',FALSE)->
    from('save_jobs')->where(array('user'=>$user,'save_jobs.delete_status !='=>'true'));
    $q=$this->db->join('jobs','save_jobs.job_id = jobs.id');
 /*      if(strlen($query_array['job_id']))
   /*  {

        $q->where('job_id',$query_array['job_id']);
     }        
     if(strlen($query_array['name']))
     {

        $q->where('name',$query_array['name']);
     } 
      if(strlen($query_array['date_applied']))
     {

     
        $q->where("date_applied",$query_array['date_applied']);

       }*/     
    $tmp = $q->get()->result_array();
 

   $result['total_rows'] = $tmp[0]['count'];
  
    return $result;

  }
  public function search_universities($query_array,$limit,$offset,$sort_by,$sort_order)
  {
$user = $this->session->userdata('username');
    $q= $this->db->select('*')->from('university')->
      limit($limit,$offset)->order_by($sort_by,$sort_order);
        
     
    if(strlen($query_array['user']))
     {

        $q->where('user',$query_array['user']);
     }
     if(strlen($query_array['name']))
     {

        $q->LIKE('name',$query_array['name']);
     }
     if(strlen($query_array['email']))
     {
          
        
        $q->where("email",$query_array['email']);

       }
       if(strlen($query_array['u_name']))
       {
          $q->where('u_name',$query_array['u_name']);

       }
       if(strlen($query_array['status']))
       {

        $q->where('status',$query_array['status']);
       }        
     
       $ret['rows'] = $q->get()->result_array();
       
      
  $q=$this->db->select('COUNT(*) as count',FALSE)->
    from('university');
       if(strlen($query_array['user']))
     {

        $q->where('user',$query_array['user']);
     }        
     if(strlen($query_array['name']))
     {

        $q->like('name',$query_array['name']);
     } 
      if(strlen($query_array['email']))
     {

     
        $q->where("email",$query_array['email']);

       }     
       if(strlen($query_array['u_name']))
       {

         $q->where('u_name',$query_array['u_name']);
       }
       if(strlen($query_array['status']))
       {

        $q->where('status',$query_array['status']);
       }     
    $tmp = $q->get()->result_array();
   

   $ret['total_rows'] = $tmp[0]['count'];
  
    return $ret;


  }
  public function search_companies($query_array,$limit,$offset,$sort_by,$sort_order)
  {
  $user = $this->session->userdata('username');
    $q= $this->db->select('*')->from('caccounts')->
      limit($limit,$offset)->order_by($sort_by,$sort_order);
        
   
     if(strlen($query_array['c_name']))
     {
       $q->like('c_name',$query_array['c_name']);
     }
     if(strlen($query_array['name']))
     {

        $q->like('name',$query_array['name']);
     }
     if(strlen($query_array['email']))
     {
          
        
        $q->where('email',$query_array['email']);

       }
       if(strlen($query_array['status']))
       {
          $q->where('status',$query_array['status']);

       }        
      
       $ret['rows'] = $q->get()->result_array();
          
      
      
  $q=$this->db->select('COUNT(*) as count',FALSE)->
    from('caccounts');
   
     if(strlen($query_array['c_name']))
     {
         $q->like('c_name',$query_array['c_name']); 


     }
     if(strlen($query_array['name']))
     {

        $q->like('name',$query_array['name']);
     } 
      if(strlen($query_array['email']))
     {

     
        $q->where("email",$query_array['email']);

       }
       if(strlen($query_array['status']))
       {
           $q->where('status',$query_array['status']);

       }     
    $tmp = $q->get()->result_array();
  

   $ret['total_rows'] = $tmp[0]['count'];
  
   
   // $ret['num_rows'] = $tmp[0]->count;
    //print_r($ret);


    return $ret;

  }
  function get_abused_reports($user_c)
  {
$result = $this->db->select('user,job_id,user_c, Count("user") as user_total')->from('abused_reports')
  ->group_by('user_c')->where(array('user_c'=>$user_c))->order_by('user_total','desc');
   $result = $result->get()->result_array();
   $result = (empty($result)?0:$result[0]['user_total']);
   
   
  /* $result2 = $this->db->get('students');
   $result['total_students'] = $result2->num_rows();*/
   return $result;

  }
  function university_names()
  {
       $rows = $this->db->select('*')->from('university_name')->get()->result_array();
       $university_names= array(''=>'');
       foreach($rows as $row)
       {
            $university_names[$row['u_name']] =$row['u_name']; 

       }
       return $university_names;


  }
  function get_job_types($string=false)
  {      $result=$this->db->query('SELECT DISTINCT type from job_types order by type="Others",type asc')->result_array();
   
      if($string==true)
   {
      $job_types=array(''=>'');
    
   }
   else
   {
   $job_types=array(''=>'');
   }
    
    foreach($result as $value)
    {
        $job_types[$value['type']] = $value['type'];

    }
    return $job_types;


  }
function get_categories($string=false)
{


   
  
   if($string==true)
   {
    $result=$this->db->query('SELECT DISTINCT category from job_categories order by category="Other(Specify)",category="Others",category asc')->result_array();
    $categories=array(''=>'');
   
   }
   else
   {
     $result=$this->db->query('SELECT DISTINCT category from job_categories where category != "Other(Specify)" order by category="Others",category asc')->result_array();
      $categories=array(''=>'');
       
   }
   
    foreach($result as $value)
    {

    	$categories[$value['category']] =$value['category'];


    }
    //print_r($category_options);
    return $categories;

}

public function get_cities($string=false){

  

   // $rows= $rows->get()->result();
if($string==true)
   {
     $result=$this->db->query('SELECT DISTINCT cities from cities order by cities="Other(Specify)",cities="Others", cities asc')->result_array();
     $cities=array(''=>'');
   }
   else
   {
    $result=$this->db->query('SELECT DISTINCT cities from cities where cities != "Other(Specify)" order by cities="Others",cities asc')->result_array();
    $cities=array(''=>'');
   }

    
    foreach($result as $value)
    {

      $cities[$value['cities']] =$value['cities'];


    }
    return $cities;

}

 function get_salaries($string=false)
  {      $result=$this->db->query('SELECT DISTINCT salary from salary_table' )->result_array();
   
      if($string==true)
   {
     $salaries=array(''=>'');
   }
   else
   {
    $salaries=array(''=>'');
   }
    
    foreach($result as $value)
    {
        $salaries[$value['salary']] = $value['salary'];

    }
    return $salaries;


  }
  public function get_days()
  {

 $result=$this->db->select('day')->from('days')->get()->result_array();
  
                  $days=array(''=>'');
                  foreach($result as $values)
                  {
                        
                    $days[$values['day']]=$values['day'];
                  }
                          return $days;



  }
  public function get_months()
  {

$result=$this->db->select('month')->from('months')->get()->result_array();
  
                  $months=array(''=>'');
                  foreach($result as $values)
                  {
                        
                    $months[$values['month']]=$values['month'];
                  }
                          return $months;


  }
public function insert_type()
{
       $type = $this->input->post('specify');
       $this->db->where('type',$type);
       $answer=$this->db->get('job_types');
       if($answer->num_rows()>0)
       {
           return false;
       }
       else
       {
           $type= strtolower($type);
           $type = ucfirst($type);

       $result = $this->db->insert('job_types',array('type'=>$type));
        }


       
}
public function insert_city()
{
       $city = $this->input->post('s_city');
      $this->db->where('city',$city);
       $answer=$this->db->get('other_cities');
       if($answer->num_rows()>0)
       {
           return false;
       }
       else{

                  $city= strtolower($city);
           $city = ucfirst($city);

       $result = $this->db->insert('other_cities',array('city'=>$city,'added_date'=>date('Y-m-d'),'status'=>'new'));
        }


       
}
public function insert_category()
{
     $category = $this->input->post('s_category');
         
      $this->db->where('category',$category);
       $answer=$this->db->get('other_categories');
       if($answer->num_rows()>0)
       {
           return false;
       }
       else
       {
           $category= strtolower($category);
           $category = ucfirst($category);

       $result = $this->db->insert('other_categories',array('category'=>$category,'added_date'=>date('Y-m-d'),'status'=>'new'));
        }


}
public function check_save_job($save_job_id)
{
  $user=$this->session->userdata('username');
  $this->db->where(array('user'=>$user,'save_job_id'=>$save_job_id));
  $result =$this->db->get('save_jobs')->num_rows();
  if($result==1)
  {
    return true;
  }
  else
  {
    return false;
  }

}
public function get_save_job($save_job_id)
{


$user = $this->session->userdata('username');
  $this->db->select('*')->where(array('user'=>$user,'save_job_id'=>$save_job_id,'save_jobs.delete_status !='=>'true'));
  
    $this->db->from('save_jobs');
    $result=$this->db->join('jobs','jobs.id=save_jobs.job_id')->get()->row_array();
      return $result;
        
    

}

public function get_applicants($user)
{
   $result = $this->db->get_where('applicants', array('user_c'=>$user));
   $result = $result->result_array();
   return $result;

}
public function get_applicants_rows($user)
{
  $result = $this->db->get_where('applicants', array('user_c'=>$user));
   $result = $result->num_rows();
   return $result;


}


public function get_students()
{
$query = $this->db->get('students');
		return $query->result_array();

}
public function get_students_rows()
{
     $query = $this->db->get('students');
        return $query->num_rows();

}
public function get_each_student($user)
{  
  if($user=="")
  {
   // $user=$this->session->userdata('username');
  }
  
 $this->db->where('user',$user);
 $query = $this->db->get('students');

      return $query->result_array();
      
}
public function get_university_account($user)
{

    $query = $this->db->get_where('university',array('user'=>$user));
    return $query->result_array();
}
public function get_admin_account($user)
{
      $result = $this->db->get_where('administrator',array('user'=>$user));
      return $result->result_array();

}
public function get_students_total($university)

{

    $query = $this->db->get_where('students',array('u_name'=>$university));
    return $query->num_rows();


}
public function get_all_universities()
{
    $query = $this->db->get('university');
    return $query->result_array();

}
public function update_university()
{


    


    $data = array('user'=>$user,
        'name'=>$name,'u_name'=>$u_name, 'phone'=>$phone, 'email'=>$email,
        'address'=>$address, 'township'=>$township,'city'=>$city,
        'country'=>$country);
    $result =  $this->db->where('user', $user);
    
   
    return $result;
   
$this->db->update('university', $data); 

}
public function get_students_by_u($university)
{
      $query = $this->db->get_where('students', array('u_name'=>$university));
      
      return $query->result_array();


}

public function get_admin_university_name($user)
{

   
   $query = $this->db->get_where('administrator',array('user'=>$user));
   $query = $query->result_array();
   //print_r($query);

  return $query[0]['u_name'];



}
public function get_university_name($user)
{
   $role=$this->session->userdata('role');
   $query='';
   /*if(!empty($user))
   {*/
   if($role=='administrator')
   {
    $query = $this->db->get_where('administrator',array('user'=>$user));
   $query = $query->result_array();
   }
   elseif($role=='university')
   {
   $query = $this->db->get_where('university',array('user'=>$user));
   $query = $query->result_array();
   }
   //print_r($query);
  if(!empty($query))
  {
  return $query[0]['u_name'];
   }
}
public function get_university_records()
{
$query = $this->db->get('university');
    return $query->num_rows;


}

public function set_news()
{
	$this->load->helper('url');

	$slug = url_title($this->input->post('title'), 'dash', TRUE);

	$data = array(
		'title' => $this->input->post('title'),
		'slug' => $slug,
		'text' => $this->input->post('text')
	);

	return $this->db->insert('news', $data);
}
public function change_password($user)
{
        $password=md5($this->input->post('password'));
        $role = $this->session->userdata('role');
       
        $data = array('password'=>$password);
       
        
        if($role=='student'){
        $this->db->where('user',$user);
        
        
       $result=  $this->db->update('students',$data);
       return $result;
     }
     elseif($role=='university'){
       $this->db->where('user',$user);
       $result2= $this->db->update('university',$data);
       return $result2;
     }
     elseif($role=='company')
     {
       $this->db->where('username',$user);
       $result3 =    $this->db->update('caccounts',$data);
       return $result3;
     }
     elseif($role=='admin')
     {
      $this->db->where('user',$user);
      $result4 = $this->db->update('administrator',$data);
      return $result4;
     }
     else
     {
      return false;
     }
      
}

public function renew_password($user)
{
 $password=md5($this->input->post('password'));
        $data = array('password'=>$password);
       // $this->db->where('user',$user);
       // $result1 = $this->db->get('students');
         //if($result1->num_rows()==1)
         //{
            $this->db->where('user',$user);
            $this->db->update('students',$data);
         //}
        // $this->db->where('user',$user);
        // $result2= $this->db->get('university');
          //if($result2->num_rows()==1)
          //{
           
          //}
           $this->db->where('username',$user);
           $this->db->update('caccounts',$data);

           $this->db->where('user',$user);
           $this->db->update('administrator',$data); 

           return true;

             

}
public function erase_random_number($user)
{
    
$data = array('random_number'=>"",'random_number_date'=>"");
       // $this->db->where('user',$user);
       // $result1 = $this->db->get('students');
         //if($result1->num_rows()==1)
         //{
            $this->db->where('user',$user);
            $this->db->update('students',$data);
         //}
        // $this->db->where('user',$user);
        // $result2= $this->db->get('university');
          //if($result2->num_rows()==1)
          //{
           
          //}
           $this->db->where('username',$user);
           $this->db->update('caccounts',$data);

           $this->db->where('user',$user);
           $this->db->update('administrator',$data);
           return true; 
}

public function create_students()
{
$this->load->helper('url');
   $b_day = $this->input->post('b_day');
   $b_month = $this->input->post('b_month');
   $b_year = $this->input->post('b_year');
	 $date_o_b=$b_day.'-'.$b_month.'-'.$b_year;
  
$date_o_b= date_create($date_o_b);
$date_o_b= date_format($date_o_b,"Y-m-d");

	$data = array(
		'user' => $this->input->post('user'),
		'password'=>md5($this->input->post('password')),
		
		'name'=>$this->input->post('name'),
    'nrc'=>$this->input->post('nrc'),
		'id'=>$this->input->post('no'),
    'gender'=>$this->input->post('gender'),
    'm_status'=>$this->input->post('m_status'),
     'date_o_b'=>$date_o_b,
     'nationality'=>$this->input->post('nationality'),
     'phone'=>$this->input->post('phone'),
     'email'=>$this->input->post('email'),
     'qualifications'=>$this->input->post('qualifications'),
     'experience'=>$this->input->post('experience'),
     'major'=>$this->input->post('major'),
     
     'address'=>$this->input->post('address'),
     'township'=> $this->input->post('township'),
     'city'=> $this->input->post('city'),
     'country'=>$this->input->post('country'),

        'status'=>'New',
        'registered_date'=>date('Y-m-d'),  
		'role'=>'student',
    'photo'=>$this->input->post('photo_path'),
    'photo_server_path'=>$this->input->post('photo_server_path')
		
	);

	return $this->db->insert('students', $data);

}
public function get_photo_path($user)
{

  $photo_path = $this->db->select('photo')->from('students')->where('user',$user)
  ->get()->result_array();
  
  if(!empty($photo_path[0]['photo']))
  {
       
  return $photo_path[0]['photo'];
  }
  else
  {
     
    return "";
  }
  
}
public function get_company_photo($user)
{
   
    
    
  $photo = $this->db->select('photo')->from('caccounts')->where('username',$user)
  ->get()->result_array();
       
    


  
  if(!empty($photo[0]['photo']))
  {
         return $photo[0]['photo'];
  }
  else
  {
          return "";
  }

}

public function get_logo($user)
{
   
  
   
  $photo = $this->db->select('logo')->from('caccounts')->where('username',$user)
  ->get()->result_array();
   
  
  if(!empty($photo[0]['logo']))
  {
         return $photo[0]['logo'];
  }
  else
  {
          return "";
  }

}
public function get_university_logo($user)
{
   
  
   
  $photo = $this->db->select('logo')->from('administrator')->where('user',$user)
  ->get()->result_array();
   
  
  if(!empty($photo[0]['logo']))
  {
         return $photo[0]['logo'];
  }
  else
  {
          return "";
  }

}
public function get_university_photo($user)
{
    
    
    
     

$photo = $this->db->select('photo')->from('administrator')->where('user',$user)
  ->get()->result_array();
     


  
  if(!empty($photo[0]['photo']))
  {
         return $photo[0]['photo'];
  }
  else
  {
          return "";
  }

}
public function get_job_logo($job_id)
{


   
  $photo = $this->db->select('logo')->from('jobs')->where('id',$job_id)
  ->get()->result_array();
   
  
  if(!empty($photo[0]['logo']))
  {
         return $photo[0]['logo'];
  }
  else
  {
          return "";
  }


}

public function update_student_photo()
{


$base = base_url();
     
 //    print_r($photo);
   //  echo ($photo);
  
    $config = array('upload_path'=>"./images/students_photos",
    'allowed_types'=>'jpg|jpeg|png|gif',
    'max_size'=>'100');


   
    $this->load->library('upload',$config);
   
      if ( ! $this->upload->do_upload())
    {
       
      $data['error'] = $this->upload->display_errors('<span style="text-align:center;color:red">','</span>');
     // $data['previous_path'] = $previous_path;
      
        $data['photo_path']=$this->input->post('photo_path');
        $data['photo_server_path']=$this->input->post('photo_server_path');
      return $data;
    
    
    }
    else
    {
      
        $image_info = $this->upload->data();

        $data['photo_server_path'] = $image_info['full_path'];
   //     $data['existing_server_path']=$this->input->post('existing_server_path');
      
/*if($this->input->post('update_form')!=="update_form")
        {        
*/            if($this->input->post('previous_s_path')!==$image_info['full_path']
          && $this->input->post('previous_s_path')!== $this->input->post('existing_server_path'))
            {     
                  if($this->input->post('previous_s_path'))
                  { 
                  @unlink($this->input->post('previous_s_path'));
                  }
            }
//        } 
            $config2['source_image']=$image_info['full_path'];
      //  $config2['create_thumb']= TRUE;
        $config2['width']="500";
        $config2['height']="500";
        $config2['maintain_ratio']=TRUE;
        $this->load->library('image_lib',$config2);
        $this->image_lib->resize();
      $data['photo_path']= base_url() . "images/students_photos/". $image_info['file_name'];
      
        
       return $data;

    }



}
public function student_photo()
{
  $base = base_url();
 
    $config = array('upload_path'=>"./images/students_photos",
    'allowed_types'=>'jpg|jpeg|png|gif',
    'max_size'=>'100');


   
    $this->load->library('upload',$config);
   
      if ( ! $this->upload->do_upload())
    {
       
      $data['error'] = $this->upload->display_errors('<span style="text-align:center;color:red">','</span>');
     // $data['previous_path'] = $previous_path;
      
        $data['photo_server_path']=$this->input->post('photo_server_path');
        $data['photo_path']=$this->input->post('photo_path');
      return $data;
    
    
    }
    else
    {
      
        $image_info = $this->upload->data();

        $data['photo_server_path'] = $image_info['full_path'];

       
               
                  if($this->input->post('previous_s_path'))
                  { 
                  @unlink($this->input->post('previous_s_path'));
                  }
            

            $config2['source_image']=$image_info['full_path'];
      //  $config2['create_thumb']= TRUE;
        $config2['width']="500";
        $config2['height']="500";
        $config2['maintain_ratio']=TRUE;
        $this->load->library('image_lib',$config2);
        $this->image_lib->resize();
      $data['photo_path']= base_url() . "images/students_photos/". $image_info['file_name'];
      
        
       return $data;

    }


}
public function company_photo()
{
       $base_url = base_url(); 
     $config = array('upload_path'=>"./images/company_photo",
      'allowed_types'=>'jpg|jpeg|gif|png',
       'max_size'=>'100'
      );

     $this->load->library('upload',$config);
     if(!$this->upload->do_upload())
     {
          $data['error'] = $this->upload->display_errors('<span style="text-align:center;color:red">','</span>');
     // $data['previous_path'] = $previous_path;
      
        $data['photo_server_path']=$this->input->post('photo_server_path');
        $data['photo_path']=$this->input->post('photo_path');
      return $data;

      /////////////////////////////////
         /* if($this->input->post('previous_s_path')!==$image_info['full_path']
          && $this->input->post('previous_s_path')!== $this->input->post('e_server_path'))
            {     
                  if($this->input->post('previous_s_path'))
                  { 
                  @unlink($this->input->post('previous_s_path'));
                  }
            }*/
     }
     else
     {
         $image_info = $this->upload->data();

        $data['photo_server_path'] = $image_info['full_path'];

       
               
                  if($this->input->post('previous_s_path'))
                  { 
                  @unlink($this->input->post('previous_s_path'));
                  }
            

            $config2['source_image']=$image_info['full_path'];
      //  $config2['create_thumb']= TRUE;
        $config2['width']="1000";
        $config2['height']="1000";
        $config2['maintain_ratio']=TRUE;
        $this->load->library('image_lib',$config2);
        $this->image_lib->resize();
      $data['photo_path']= base_url() . "images/company_photo/". $image_info['file_name'];
      
        
       return $data;

    }

     

}

public function upload_file()
{ 
  
  
  
  $config=array('allowed_types'=>'gif|png|jpeg|docx|x-zip|doc|txt|word',
    'upload_path'=>'./files/'
    );
$this->load->library('upload',$config);
//$result =$this->upload->do_upload();
//echo "result is: <br>"; print_r($result);

//$info =$this->upload->data();



if ( !$this->upload->do_upload())
    {
      $data['error'] = $this->upload->display_errors('<span style=color:red;text-align:center>','</span>');
        return $data;
       
      
    }
    else
    {
      $data['info']= $this->upload->data();
     
      return $data;
     // print_r($info);
    }

  



       
}
public function upload_logo()
{

 $config = array('upload_path'=>"./images/logo",
      'allowed_types'=>'jpg|jpeg|gif|png',
      'max_size'=>'100'
      );
 $this->load->library('upload',$config);

  if(!$this->upload->do_upload('logo'))
     {
          $data['error'] = $this->upload->display_errors('<span style="text-align:center;color:red">','</span>');
     
      
        $data['photo_server_path']=$this->input->post('logo_server_path');
        $data['photo_path']=$this->input->post('logo_path');
      return $data;
         
     }

      else
     {
       $image_info = $this->upload->data();

        $data['photo_server_path'] = $image_info['full_path'];
      
        if($this->input->post('logo_previous_s_path')!==$image_info['full_path']
          && $this->input->post('logo_previous_s_path')!== $this->input->post('logo_e_server_path'))
               {
                 if($this->input->post('logo_previous_s_path'))
                  { 
                  @unlink($this->input->post('logo_previous_s_path'));
                  }
                }
            
            $config2['source_image']=$image_info['full_path'];
      //  $config2['create_thumb']= TRUE;
        $config2['width']="500";
        $config2['height']="500";
        $config2['maintain_ratio']=TRUE;
        $this->load->library('image_lib',$config2);
        $this->image_lib->resize();

      $data['photo_path']= base_url() . "images/logo/". $image_info['file_name'];
      
        
       return $data;

    }

    


    


}

public function set_university()
{

     $data = array('user'=>$this->input->post('user'),
     'password'=>md5($this->input->post('password')),
     'name'=>$this->input->post('name'),
     	'u_name'=>$this->input->post('u_name'),
     	'email'=>$this->input->post('email'),
     	
     'phone'=>$this->input->post('phone'),
   
     'website'=>$this->input->post('website'),
     'address'=>$this->input->post('address'),
     'township'=> $this->input->post('township'),
     'city'=> $this->input->post('city'),
     'country'=>$this->input->post('country'),
     	'status'=>$this->input->post('status'),
        'registered_date'=>date('Y-m-d'),
       'role'=>'university',
        );
     	return $this->db->insert('university',$data);	
}



public function set_company()
{
$this->load->helper('url');

	//$slug = url_title($this->input->post('title'), 'dash', TRUE);
$data = array('username'=>$this->input->post('user'),
     'password'=>md5($this->input->post('password')),
     'name'=>$this->input->post('name'),
      'c_name'=>$this->input->post('c_name'),
      'type'=>$this->input->post('type'),
      'email'=>$this->input->post('email'),
      
     'phone'=>$this->input->post('phone'),
     
     'address'=>$this->input->post('address'),
     'township'=> $this->input->post('township'),
     'city'=> $this->input->post('city'),
     'country'=>$this->input->post('country'),
      'status'=>$this->input->post('status'),
        'registered_date'=>date('Y-m-d'),
       'role'=>$this->input->post('role') );
      return $this->db->insert('caccounts',$data);

 		
	

	

}


public function check_update_user($user)
{
     

}

public function check_user($user)
{
	
     $query = $this->db->get_where('caccounts',array('username'=>$user));
$query = $query->num_rows;
      $query2 = $this->db->get_where('students',array('user'=>$user));
      $query2 = $query2->num_rows;
      $query3 = $this->db->get_where('university',array('user'=>$user));
      $query3 = $query3->num_rows;
      $query4 = $this->db->get_where('administrator',array('user'=>$user));
      $query4= $query4->num_rows;
     if($query == 0 && $query2 == 0 && $query3 == 0 && $query4== 0 )
     {
     	return true;

     	
     }
     else
     {

     	return false;


     }

     //else{
     	//$this->form_validation->set_message('check_user','%choose another user name');
          //return FALSE;
    // }
       

}
public function cvalidate()

{  
	$this->db->where('username',$this->input->post('username'));
	$this->db->where('password', md5($this->input->post('password')));
  $this->db->where('status','Approved');
	$query = $this->db->get('caccounts');

	if($query->num_rows() == 1)
	{
         return true;

	}
  else
  {
    return false;
  }

}
public function validate_student()

{
    
    $this->db->where('user', $this->input->post('username'));
    $this->db->where('password', md5($this->input->post('password')));
    $this->db->where('status','Approved');

    $query = $this->db->get('students');
   
    if($query->num_rows() == 1)
    {
    	return true;
    }
    else
    {
      return false;
    }

}

public function validate_university()

{
  
   
    
    $this->db->where('user',$this->input->post('username'));
    $this->db->where('password',md5($this->input->post('password')));
    //$this->db->where('status','Approved');
$this->db->where('status','Approved');
    $query =$this->db->get('university');
   
    if($query->num_rows() == 1)
    {
         
        return TRUE;

    }
    else
    {
       
      return FALSE;
    }



}
public function validate_admin()
{
$this->db->where('user',$this->input->post('username'));
    $this->db->where('password',md5($this->input->post('password')));
    
    $query =$this->db->get('administrator');
     
    if($query->num_rows() == 1)
    {
     
        return TRUE;

    }
    else
    {

      return FALSE;
    }




}
public function check_login_number($number)
{
    $this->db->where('login_number',$number);
    $result1 = $this->db->get('caccounts');

   
    $total= $result1->num_rows();
    
    if($total>0)
    {

      return false;
    }
    else
    {
      return true;
    }




}

public function create_company()
{

	$new_member = array(
		'username'=>$this->input->post('user'),
		'password'=>md5($this->input->post('password')),
	     'name'=>$this->input->post('name'),
       'c_name'=>$this->input->post('c_name'),
       'crn'=>$this->input->post('crn'),
       'c_information'=>$this->input->post('c_information'),
       'b_type'=>$this->input->post('b_type'),
	     'role'=>'company',
       
       'phone'=>$this->input->post('phone'),
       'email'=>$this->input->post('email'),
       'website'=>$this->input->post('website'),
       'address'=>$this->input->post('address'),
       'township'=>$this->input->post('township'),
       'city'=>$this->input->post('city'),
       'country'=>$this->input->post('country'),
       'registered_date'=>date('Y-m-d'),
       'photo'=>$this->input->post('photo_path'),
       'logo'=>$this->input->post('logo_path'),
       'status'=>'Approved'
   

		);

	$insert = $this->db->insert('caccounts',$new_member);
  return true;

}

public function send_login_email($email)
{

        
          $number= mt_rand(10000000,99999999);
 //     $answer = $this->check_random_numbers($number);
      while(!$this->check_login_number($number))
      {
        
        $number = mt_rand(10000000,99999999);
       
      }
     // echo "end number: ". $number;

      $data = array('login_number'=>$number,'status'=>'New');
    //  $email=$this->input->post('email');
      $user=$this->input->post('user');
      $this->db->where('username',$user);
      $result=$this->db->update('caccounts',$data);
          



            $config = Array( 'protocol'=>'smtp',
    'smtp_host'=>'ssl://smtp.googlemail.com',
    'smtp_port'=> 465,
    'smtp_user'=> 'jobmatchingmm@gmail.com',
    'smtp_pass'=> '12february'
   );
$this->load->library('email',$config);

$this->email->set_newline("\r\n");
$this->email->from('jobmatchingmm@gmail.com','Job Career Development Site');
$this->email->to($email);
$this->email->subject('Job Career Development Login confirmation');

$this->email->message("Links for login\n"
 . base_url(). "pages/login_confirmation/".$email."/".$number);

if(@$this->email->send())
        {
           $this->load->view('templates/header');
           $this->load->view('pages/email_confirmation');
           $this->load->view('templates/footer');



         }
else
       {
        $data = array('login_number'=>'');
    
     
      $this->db->where('username',$user);
      $this->db->update('caccounts',$data);
           redirect('pages/login_error');
           
           exit();
           
        }
  


}

public function login_confirmation($email,$number)
{
$this->db->where('login_number',$number);
    $this->db->where('email',$email);
    $result1 = $this->db->get('caccounts');
  
    
    
    $total = $result1->num_rows();

    if($total==1)
    {   
            

                  $this->db->where('login_number',$number);
                  $this->db->where('email',$email);
                  $this->db->update('caccounts',array
                    ('status'=>'Approved','login_number'=>""));
                  return true;

              
             
           
            
        
    }
    else
    {
        
        
       return false;
    }



}

public function job_create()
{       
       
        $user_c=$this->session->userdata('username');
        $role=$this->session->userdata('role');
        if($role=='admin')
        {
          $logo=$this->news_model->get_university_logo($user_c);
          $photo=$this->news_model->get_university_photo($user_c);
           // echo "the date from model is:" . $date;
        }
        elseif($role=='company')
        {
          $logo=$this->news_model->get_logo($user_c);
          $photo=$this->news_model->get_company_photo($user_c);

        }
        $published_date = date("Y-m-d");
  

  $vt_day = $this->input->post('vt_day');
   $vt_month = $this->input->post('vt_month');
   $vt_year = $this->input->post('vt_year');
  $valid_to = $vt_day."-".$vt_month."-".$vt_year;
  $valid_to= date_create($valid_to);
  $valid_to= date_format($valid_to,'Y-m-d');

         $type="";
         $s_category="";
         $s_city="";
         $specified="";
         if($this->input->post('s_category'))
         {
            $s_category = "Others";
            $specified='true';
           
         }
        if($this->input->post('s_city'))
         {
             $s_city = "Others";
             $specified='true';
         }
   
        

$rows = $this->db->count_all('jobs');
 $job_id ='J'.($rows+1);
 $j_key=1;
 $result=$this->db->select('id')->from('jobs')->where('id',$job_id)->get()->result_array();
 if(!empty($result))
 {
while($job_id==$result[0]['id'])
 {
    
    
    $job_id='J'.$j_key;
   
    $j_key++;
    $result=$this->db->select('id')->from('jobs')->where('id',$job_id)->get()->result_array();
    if(empty($result))
    {
      break;
    }
 }
}

	$data = array(
    'id'=>$job_id,
   'c_name'=>$this->input->post('c_name'),
    'c_information'=>$this->input->post('c_information'),
		'category'=>(!empty($s_category)?$s_category:$this->input->post('category')),
		'title'=>$this->input->post('title'),
		'salary'=>$this->input->post('salary'),
		'descriptions'=>$this->input->post('descriptions'),
    'responsibilities'=>$this->input->post('responsibilities'),
    'requirements'=>$this->input->post('requirements'),
    'job_type'=>$this->input->post('job_type'),
    'phone'=>$this->input->post('phone'),
    'website'=>$this->input->post('website'),
    'email'=>$this->input->post('email'),
    'address'=>$this->input->post('address'),
    'township'=>$this->input->post('township'),
    'city'=>(!empty($s_city)?$s_city:$this->input->post('city')),
    'country'=>$this->input->post('country'),
   
    'valid_to'=>$valid_to,
    'published_date'=>$published_date,
    'last_updated'=>date('Y-m-d H:i:s'),
		'user_c'=>$user_c,
    'logo'=>$logo,
    'photo'=>$photo,
    'job_status'=>'closed',
     'specified'=>$specified
    );
    
	$result =$this->db->insert('jobs',$data);
  $last_id = $this->db->insert_id();
  
 // echo "the last inserted id is:" . $last_id . "<br>";
      
  
           if($result == true )
           {

            return true;
           }


}
public function delete_specified()
{

  /*$this->db->where('id',$id);
  $this->db->update('');*/
}

function News_model()
{
   parent::Model();
  // $this->gallery_path = realpath(APPPATH. '../files');
   //$this->gallery_path_url = base_url(). 'images/';

}



public function get_company_details($user)
{

        //$this->db->;
}
public function create_applicants()
{
 ////$file = $this->input->post('tmp_file');



$file_path = $this->input->post('file_path');
$file_name = $this->input->post('file_name');
$file_orig_name =$this->input->post('orig_name');

 
 
 $job_no = $this->input->post('job_id');
 $title=$this->input->post('title');
 $user = $this->session->userdata('username');
 $applicant = $this->news_model->get_each_student($user);
 
 $rows = $this->db->count_all('applicants');
 $applicant_id ='AP'.($rows+1);
 $result=$this->db->select('applicant_id')->from('applicants')->where('applicant_id',$applicant_id)->get()->result_array();
 $a_key=1;
 if(!empty($result))
 {
while($applicant_id==$result[0]['applicant_id'])
 {
    
    
    $applicant_id='AP'.$a_key;

   
    $a_key++;
    $result=$this->db->select('applicant_id')->from('applicants')->where('applicant_id',$applicant_id)->get()->result_array();
   // echo "applicant_id" ;print_r($result);echo "<br>";
    if(empty($result))
    {
      break;
    }
 }
}

 $job_rows=$this->db->count_all('students_jobs');
 $applied_id ='JA'.($job_rows+1);
 $ap_key=1;
 $result2=$this->db->select('applied_id')->from('students_jobs')->where('applied_id',$applied_id)->get()->result_array();
  if(!empty($result2))
  {
while($applied_id==$result2[0]['applied_id'])
 {
    
    
    $applied_id='JA'.$ap_key;
   
    $ap_key++;
    $result2=$this->db->select('applied_id')->from('students_jobs')->where('applied_id',$applied_id)->get()->result_array();
    if(empty($result2))
    {
      break;
    }
 }
}
$job=$this->news_model->get_available_job($job_no);
 $date = date('Y-m-d');
 $data =   array(
  'applicant_id'=>$applicant_id,
  'job_id'=> $job_no,
  'job_title'=>$title,
  'user'=>$user,
   'file' => $file_path,
  'user_c' => $this->input->post('user_c'),
  'file_name'=>$file_name,
  'file_orig_name'=>$file_orig_name,
  
  'date_applied' => $date,
  'job_valid_to'=> $job['valid_to']

  );

  //$data['filename'] = $this->input->post('file');
  $result= $this->db->insert('applicants', $data);
  
  $data2=   array(
    'applied_id'=>$applied_id,
   
  'job_id'=> $job_no,
  'user'=>$user,
  'user_c'=>$job['user_c'],
  
   

  
  
  
  
  
  
  
  
 
  
  
  
  
  
 
  
  'applied_date'=>$date,
  'file_path'=>$file_path,
  'file_name'=>$file_name,
  'file_orig_name'=>$file_orig_name,
   



);
$result2= $this->db->insert('students_jobs',$data2);
//return $result2;
return $result;
}

public function get_images()
{
 $files = scandir($this->gallery_path);
 $files = array_diff($files,array('.','..'));
 $images= array();

 foreach($files as $file)
 {
    $images[]=array('url'=>"$this->gallery_path_url .$file");
 }

}
public function check_random_number($number)
{
    $this->db->where('random_number',$number);
////    $this->db->where('email !=',$email);
    $result1 = $this->db->get('students');

    $this->db->where('random_number',$number);
    $result2 = $this->db->get('university');

    $this->db->where('random_number',$number);
    $result3 = $this->db->get('caccounts');

    $this->db->where('random_number',$number);
    $result4 = $this->db->get('administrator');
   
    $total= $result1->num_rows()+$result2->num_rows()
    +$result3->num_rows()+$result4->num_rows();
    if($total>0)
    {

      return false;
    }
    else
    {
      return true;
    }

}
public function check_duplicate_email($email)
{
$this->db->where('email',$email);
    $result1 = $this->db->from('students')->get();
    //echo "number of rows: " .$result1->num_rows();
    $this->db->where('email',$email);
    $result2 = $this->db->from('university')->get();
    $this->db->where('email',$email);
    $result3 = $this->db->from('caccounts')->get();
    $this->db->where('email',$email);
    $result4 = $this->db->from('administrator')->get();

    $total = $result1->num_rows()+$result2->num_rows()+
    $result3->num_rows()+$result4->num_rows();
  
    if($total>0)
    {
      return false;
    }
    else
    {
      return true;
    }

}
public function check_update_email($stri)
{
  $user = $this->session->userdata('username');
  //$role= $this->session->userdata('role');
  $r1 = $this->db->select('email')->from('caccounts')->where(array('username'=>$user,'email'=>$stri))->get();
  $r2 = $this->db->select('email')->from('students')->where(array('user'=>$user,'email'=>$stri))->get();
  $r3 = $this->db->select('email')->from('university')->where(array('user'=>$user,'email'=>$stri))->get();
  $r4 = $this->db->select('email')->from('administrator')->where(array('user'=>$user,'email'=>$stri))->get();
  $total=$r1->num_rows()+$r2->num_rows()+$r3->num_rows()+$r4->num_rows();

  if($total==1)
  {
    return true;
  }
  else
  {
    return false;
  }


}
public function check_admin_recovery_email($email)
{


    
    $this->db->where('email',$email);
    $result = $this->db->from('administrator')->get();
  
    if($result->num_rows()==1)//maybe check this again.
    {
      


      
      return true;

    }
    else
    {

      return false;
    }



}
public function send_admin_recovery_email($email)
{

$number= mt_rand(10000000,99999999);
 //     $answer = $this->check_random_numbers($number);
      while(!$this->check_random_number($number))
      {
        
        $number = mt_rand(10000000,99999999);
       // echo ": ". $number;
      }
     // echo "end number: ". $number;
date_default_timezone_set('Asia/Rangoon');
      $data = array('random_number'=>$number,'random_number_date'=>date("Y-m-d"));
    
     
      $this->db->where('email',$email);
      $this->db->update('administrator',$data);
$config = Array( 'protocol'=>'smtp',
    'smtp_host'=>'ssl://smtp.googlemail.com',
    'smtp_port'=> 465,
    'smtp_user'=> 'jobmatchingmm@gmail.com',
    'smtp_pass'=> '12february'
   );
$this->load->library('email',$config);

$this->email->set_newline("\r\n");
$this->email->from('jobmatchingmm@gmail.com','Phyo');
$this->email->to($email);
$this->email->subject('Recover username/password');

$this->email->message("Links for new username/password\n"
 . base_url(). "admin_role/recover_u_p/".$email."/".$number."\n
Link will be expired after 7 days.");

if(@$this->email->send())
       {
           $this->load->view('templates/admin_header');
           $this->load->view('pages/email_confirmation');
           $this->load->view('templates/footer');



         }
else
       {
        $data = array('random_number'=>'','random_number_date'=>'');
    
     
      $this->db->where('email',$email);
      $this->db->update('administrator',$data);
           redirect('admin_role/email_error');
           
           exit();
           
        }


}

public function send_recovery_email($email)
{

$number= mt_rand(10000000,99999999);
 //     $answer = $this->check_random_numbers($number);
      while(!$this->check_random_number($number))
      {
        
        $number = mt_rand(10000000,99999999);
       // echo ": ". $number;
      }
     // echo "end number: ". $number;
date_default_timezone_set('Asia/Rangoon');
      $data = array('random_number'=>$number,'random_number_date'=>date("Y-m-d"));
    
     
      $this->db->where('email',$email);
      $this->db->update('administrator',$data);
$config = Array( 'protocol'=>'smtp',
    'smtp_host'=>'ssl://smtp.googlemail.com',
    'smtp_port'=> 465,
    'smtp_user'=> 'jobmatchingmm@gmail.com',
    'smtp_pass'=> '12february'
   );
$this->load->library('email',$config);

$this->email->set_newline("\r\n");
$this->email->from('jobmatchingmm@gmail.com','Phyo');
$this->email->to($email);
$this->email->subject('Recover username/password');

$this->email->message("Links for new username/password\n"
 . base_url(). "admin_role/recover_u_p/".$email."/".$number."\n
Link will be expired after 7 days.");

if(@$this->email->send())
       {
           $this->load->view('templates/header');
           $this->load->view('pages/email_confirmation');
           $this->load->view('templates/footer');



         }
else
       {
        $data = array('random_number'=>'','random_number_date'=>'');
    
      $this->db->where('email',$email);
     $this->db->update('students',$data);
    $this->db->where('email',$email);
    $this->db->where('caccounts',$data);
   

           redirect('login/email_error');
           
           exit();
           
        }


}

public function check_recovery_email($email)
{
    $this->db->where('email',$email);
    $result1 = $this->db->from('students')->get();
    $this->db->where('email',$email);
   
    $result2 = $this->db->from('caccounts')->get();
  

    $total = $result1->num_rows()+
    $result2->num_rows();
    
    if($total==1)//maybe check this again.
    {
      
       return true;
      
        
    }
    else
    {

      return false;
    }


}

public function recover_u_p($email,$number)
{
     
    $this->db->where('random_number',$number);
    $this->db->where('email',$email);
    $result1 = $this->db->get('students');
   
   
    $this->db->where('random_number',$number);
    $this->db->where('email',$email);
    $result2 = $this->db->get('caccounts');
   
    $this->db->where('random_number',$number);
    $this->db->where('email',$email);
    $result4 = $this->db->get('administrator');
    

    $total = $result1->num_rows() + $result2->num_rows() + $result4->num_rows();
    if($total==1)
    {   
            if($result1->num_rows()==1)
             {


               $data['profile'] = $result1->result_array();
              }
               if($result2->num_rows()==1)
             {
      $data['profile']=$result2->result_array(); 
              }
           
              if($result4->num_rows()==1)
             {

              $data['profile']= $result4->result_array();
                }
            $data['expired']=false;
        return $data;
    }
    else
    {
        
        $data['profile']=array();
       $data['expired']=true;
       return $data;
    }


}
public function get_save_jobs($user)
{


  $limit= "";$offset = 0;$sort_by="published_date";$sort_order="asc";
$this->db->select('*')->from('save_jobs')->where(array('user'=>$user,'delete !='=>'false'));
    $this->db->join('jobs','save_jobs.job_id = jobs.id');
    $this->db->order_by($sort_by,$sort_order);    
    $result= $this->db->get();
    $total_records = $result->num_rows();
    /* $this->db->select('job_id')->where(array('user'=>$user,'delete !='=>'false'))->limit(2,0);

     $result = $this->db->get('save_jobs');
     $this->db->select('job_id')->where(array('user'=>$user,'delete !='=>'false'));
     $total_rows = $this->db->get('save_jobs');
*/
     
     $save_jobs = $result->result_array();
     $save_jobs_info['total_records'] = $total_records;
     /*if($job_id)
     {
     foreach($job_id as $values)
     {

          $j_id[] = $values['job_id'];
     }
     
     $this->db->where_in('id',$j_id);
     $jobs = $this->db->get('jobs');*/
     $save_jobs_info['save_jobs'] = $save_jobs;
     return $save_jobs_info; 
     


}



}