
<!DOCTYPE html>
<html lang="en">
<head>
<meta HTTP-EQUIV="content-type" CONTENT="text/html; charset=UTF-8" />

<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- <meta http-equiv="Cache-Control" content="no-cache" /> -->

 <meta charset="UTF-8">
<?php $this->load->helper('html');?>
  <title>Job Career Development Site</title>
   <link rel="stylesheet" href="<?php echo base_url();?>css/style.css" type="text/css"> 
<script type="text/javascript" src="<?php echo base_url(). "jquery/jq.js" ?>"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
 <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="<?php echo base_url().'bootstrap/css/bootstrap.min.css';?>">
  <link rel="stylesheet" href="<?php echo base_url(). 'bootstrap/css/bootstrap-theme.min.css';?>">
  <!-- <script src="<?php echo base_url(). 'bootstrap/js/bootstrap.min.js';?>"></script>-->
   <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body style="height:100%;margin:0;padding:0">
<div style="min-height:100%;position:relative">
  <style>

   #photo_header
   {

    /*width:200px;*/
    border:1px solid red;
    padding:4px;
    background-color:lightyellow;
   /* float:right;*/
    


   


   }
   #dropdown_accounts,#dropdown_jobs,#dropdown_category
   {
    background:none;
   }
    .navbar .collapse ul li > ul li a
{

  color:#3366ff;
  font-size:16px;
}
.navbar .collapse > ul > li > a
{

  color:white;
}
#admin_h_l_profile>ul,#admin_h_l_jobs>ul,#admin_h_l_category>ul
{

  padding:0;margin:0;
}
#admin_h_l_profile>a,#admin_h_l_jobs>a,#admin_h_l_category>a,#admin_h_l_logout>a
{

  color:white;
}
   
  </style>


<?php $user = $this->session->userdata('username');
$role=$this->session->userdata('role');
 $logo = $this->news_model->get_university_logo($user);
 ?>
 
 

  
   
   
<nav class="navbar navbar-default" style="background-color:yellow">
  <div class="container-fluid" style="background-color:#2196F3">
    <div class="navbar-header col-sm-12 text-center" style="">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="" style="color:white;height:auto;font-size:40px;margin-left:12%" href="<?php echo base_url().'admin_role/is_logged_in';?>">Job Career Development Site</a>
    <ul class="nav navbar-nav navbar-right">
     
      <li><a href="<?php echo base_url().'admin_role/is_logged_in';?>"><img id="photo_header" class="img-thumbnail"
  src="<?php echo (empty($logo)?base_url()."images/icons/Blank-photo.png": $logo);?>" style="width:180px;height:150px"></a>
  </li>
    
  


      
    </ul> 
    </div>
   

   
   
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav col-sm-12 text-center" style="padding-bottom:20px;">

        
            <li id="admin_h_l_profile" class="dropdown col-sm-2 col-sm-offset-2" style="">
        <a id="dropdown_accounts" class="dropdown-toggle" data-toggle="dropdown" style="font-size:24px"  href="#">Accounts
        <span class="caret"></span></a>
        <ul class="col-sm-12 text-center dropdown-menu" style="">
          <li id="admin_h_l_account" class="text-center"><?php echo anchor('admin_role/is_logged_in','My Account');?></li>
          <li id="admin_h_l_students" class="text-center"><?php echo anchor('admin_role/display_students',"Students' Accounts");?></a></li>
          <li id="admin_h_l_companies" class="text-center"><?php echo anchor('admin_role/display_companies',"Companies' Accounts");?></li>
          <li id="admin_h_l_applicants" class="text-center"><?php echo anchor('admin_role/display_applicants',"My Applicants");?></li>
        </ul>
      </li>
         <li id="admin_h_l_jobs" class="dropdown col-sm-2" style="">
        <a id="dropdown_jobs" class="dropdown-toggle" data-toggle="dropdown" style="font-size:24px" href="#">Jobs
        <span class="caret"></span></a>
        <ul class="col-sm-12 text-center dropdown-menu" style="">
          <li id="admin_h_l_available_jobs" class="text-center"><?php echo anchor('admin_role/display_jobs','Available Jobs');?></li>

          <li id="admin_h_l_create_jobs" class="text-center"><?php echo anchor('admin_role/create_job','Create Job');?></li>
          <li id="admin_h_l_entries" class="text-center"><?php echo anchor('admin_role/display_job_entries','My Job Entries');?></li>
              <li id="admin_h_l_expired"class="text-center"><?php echo anchor('admin_role/display_expired_jobs','Expired Jobs');?></li>
        </ul>
      </li>
           <li id="admin_h_l_category" class="dropdown col-sm-2" style="">
        <a id="dropdown_category" class="dropdown-toggle" data-toggle="dropdown" style="font-size:24px;" href="#">Add New Category/City
        <span class="caret"></span></a>
        <ul class="col-sm-12 text-center dropdown-menu" style="">
          <li id="admin_h_l_categories" class="text-center"><?php echo anchor('admin_role/display_categories','Add New Category');?></li>
          <li id="admin_h_l_cities" class="text-center"><?php echo anchor('admin_role/display_cities','Add New City');?></li>
         
        </ul>
      </li>

        <li id="admin_h_l_logout"  class="col-sm-2">
             <?php echo anchor('admin_role/signout','Sign Out','style=font-size:24px;');?>
         </li>

      </ul>
     
    
    </div>
 
  </div><!--close container-->
</nav>
