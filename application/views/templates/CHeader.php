<!DOCTYPE html>
<html lang="en">
<head>
<meta HTTP-EQUIV="content-type" CONTENT="text/html; charset=UTF-8" />

<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- <meta http-equiv="Cache-Control" content="no-cache" /> -->

 <meta charset="UTF-8">
<?php $this->load->helper('html');?>
	<title>Job Career Development Site</title>
	 <link rel="stylesheet" href="<?php echo base_url();?>css/style.css" type="text/css"> 
<script type="text/javascript" src="<?php echo base_url(). "jquery/jq.js" ?>"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
 <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="<?php echo base_url().'bootstrap/css/bootstrap.min.css';?>">
  <link rel="stylesheet" href="<?php echo base_url(). 'bootstrap/css/bootstrap-theme.min.css';?>">
   <!--<script src="<?php echo base_url(). 'bootstrap/js/bootstrap.min.js';?>"></script>-->
	 <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body style="height:100%;margin:0;padding:0">
<div style="min-height:100%;position:relative">
  <style>

   #photo_header
   {

    /*width:200px;*/
    
    padding:4px;
    background-color:lightyellow;
   /* float:right;*/
    


   


   }

   .navbar .collapse > ul > li > a
{

  color:white;
  font-size:24px;
}

  .navbar .collapse ul li > ul li a
{

  color:#3366ff;
  font-size:16px;
}
#dropdown_profile
{

  background:inherit;
}
#header_l_profile>ul
{
  padding:0px;
}


#header_contact>a:hover
{
  color:white;
}
#header_login>a:hover
{
 color:white;
}
#header_l_profile>a,#header_l_jobs>a,#header_l_contact>a,#header_l_logout>a,
#header_l_j_registration>a

{

  color:white;
}
.navbar .navbar-right li
{
/*
  padding:0px;
  margin:0px;
  border:1px solid blue;*/
}
   
  </style>


<?php $user = $this->session->userdata('username');
$role=$this->session->userdata('role');
 $photo_path = $this->news_model->get_photo_path($user);
 $logo=$this->news_model->get_logo($user);
 ?>
 
 

  
   
   
<nav class="navbar navbar-default" style="background-color:yellow">
  <div class="container-fluid" style="background-color:#2196F3"><!--#1976d2-->
    <div class="navbar-header col-sm-12 text-center" style="">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="header" style="height:auto;color:white;font-size:40px;margin-left:12%" href="<?php echo base_url().'login/is_logged_in';?>">Job Career Development Site</a>
     <ul class="nav navbar-nav navbar-right" style="padding:0px;margin:0px">
     <?php if($role=='student'):?>
      <li><a href="<?php echo base_url().'login/is_logged_in';?>"><img id="photo_header" class="img-thumbnail"
  src="<?php echo (empty($photo_path)?base_url()."images/icons/Blank-photo.png": $photo_path);?>" style="width:180px;height:150px"></a></li>
     <?php elseif($role=='company'):?>
  <li><a href="<?php echo base_url() . 'login/is_logged_in';?>"><img id="photo_header" class="img-thumbnail"
  src="<?php echo (empty($logo)?base_url()."images/icons/Blank-photo.png": $logo);?>" style='width:180px;height:150px'></a></li>
      <?php endif;?>


      
    </ul> 
    </div>
   <?php if($role=="company"):?>

 
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav col-sm-12 text-center" style="padding-bottom:40px;">

        <li id="header_l_j_registration" class="dropdown col-sm-2 col-sm-offset-2" style="">
          <?php echo anchor('pages/create_job','Jobs Registration');?>
        
        </li>
         <li id="header_l_profile" class="dropdown col-sm-2" style="">
        <a id="dropdown_profile" class="dropdown-toggle" data-toggle="dropdown" style="font-size:24px;" href="#">Profile
        <span class="caret"></span></a>
        <ul class="col-sm-12 text-center dropdown-menu" style="">
          <li id="header_l_account" class="text-center"><?php echo anchor('login/is_logged_in','My Account');?></li>
          <li id="header_l_entries"class="text-center"><?php echo anchor('pages/display_job_entries','My Job Entries');?></li>
           <li id="header_l_expired"class="text-center"><?php echo anchor('pages/display_expired_jobs','Expired Jobs');?></li>
          <li id="header_l_applicants" class="text-center"><?php echo anchor('pages/display_applicants','Applicants');?></li>
        </ul>
      </li>
        
        <li id="header_l_contact" class="col-sm-2" style=""><?php echo anchor('pages/contact','Contact Us');?></li>
        <li id="header_l_logout" class="col-sm-2">
             <?php echo anchor('login/signout','Sign Out');?>
         </li>

      </ul>
     
    
    </div>


    <?php elseif($role=="student"):?>
   
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav col-sm-12 text-center" style="padding-bottom:40px;">

        <li id="header_l_jobs" class="dropdown col-sm-2 col-sm-offset-2" style="">
          <?php echo anchor('pages/display_jobs','Jobs');?>
        
        </li>
         <li id="header_l_profile" class="dropdown col-sm-2" style="">
        <a id="dropdown_profile" class="dropdown-toggle" data-toggle="dropdown"  href="#">Profile
        <span class="caret"></span></a>
        <ul class="col-sm-12 text-center dropdown-menu" style="">
          <li id="header_l_account"  class="text-center"><?php echo anchor('login/is_logged_in','My Account');?></li>
          <li id="header_l_applied" class="text-center"><?php echo anchor('pages/display_students_jobs','My Applied Jobs');?></li>
          <li id="header_l_saved" class="text-center"><?php echo anchor('pages/display_save_jobs','Saved Jobs');?></li>
        </ul>
      </li>
        
        <li id="header_l_contact" class="col-sm-2" style=""><?php echo anchor('pages/contact','Contact Us');?></li>
        <li id="header_l_logout" class="col-sm-2">
             <?php echo anchor('login/signout','Sign Out');?>
         </li>

      </ul>
     
    
    </div>
  <?php endif;?>
  </div><!--close container-->
</nav>




  
	   
       
	
	 

