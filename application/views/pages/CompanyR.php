<?php $role=$this->session->userdata('role');?>
<script>
$(document).ready(function(){
    
  $('#header_companyr').css('background','yellow');
  $('#header_registration').css('background','#2874a6');
})
$(document).ready(function(){
    $('#photo_div').on('click','#delete_photo',function(){
        var b =  $('#photo_server_path').val();
       
        $.ajax({
            url:"<?php echo base_url() . 'pages/delete_company_photo';?>",
            type: "POST",
            data:{server_path:b,existing_path:""},
            success:function(result)
            {
               
                 $('#photo').attr('src',"<?php echo base_url() . 'images/icons/no-image.jpg';?>");
                 $('#delete_photo').css('visibility','hidden');
                 $('#photo_path').attr('value',"");
                 $('#photo_server_path').attr('value',"");
                 $('#previous_s_path').attr('value',"");



            }


        });

    });

});

$(document).ready(function(){
    $('#logo_div').on('click','#delete_logo',function(){
        var b =  $('#logo_server_path').val();
     
        $.ajax({
            url:"<?php echo base_url() . 'pages/delete_company_logo';?>",
            type: "POST",
            data:{server_path:b,existing_path:""},
            success:function(result)
            {
               
                 $('#logo').attr('src',"<?php echo base_url() . 'images/icons/no-image.jpg';?>");
                 $('#delete_logo').css('visibility','hidden');
                 $('#logo_path').attr('value',"");
                 $('#logo_server_path').attr('value',"");
                 $('#logo_previous_s_path').attr('value',"");



            }


        });

    });

});


$(document).ready(function(){

    $('#photo_div').on('click','#upload_button',function(e){
      e.preventDefault();
     //  $('#photo_form').submit(function(){


       

   
    
    var formData = new FormData($('#companyr_form')[0]);
$.ajax({
       url:"<?php echo base_url() . 'pages/upload_company_photo';?>",
       data: formData,
       async: false,
       contentType: false,
       processData: false,
       cache: false,
       type: 'POST',
       success: function(data)
       {
           
           $('#photo_div').html(data);
       },
    // })//return false;    
        });
    });

});

$(document).ready(function(){

    $('#logo_div').on('click','#upload_logo',function(e){
      e.preventDefault();
     //  $('#photo_form').submit(function(){


       

   
    
    var formData = new FormData($('#companyr_form')[0]);
$.ajax({
       url:"<?php echo base_url() . 'pages/upload_logo';?>",
       data: formData,
       async: false,
       contentType: false,
       processData: false,
       cache: false,
       type: 'POST',
       success: function(data)
       {
           
           $('#logo_div').html(data);
       },
    // })//return false;    
        });
    });

});



$(document).ready(function(){
   
    
    $("#register").click(function(){
        $("form").submit(function()
          {
            
            $('#progress').css('display','block');
          });
    });  
});


 

</script>


<style>

.progress{
height:70px;


width:50%;
position:fixed;
top:50%;
left:25%;
z-index:1;

}

.progress-bar {
  padding:15px;
}

.panel .panel-body input:focus
{
     border-color:blue;


  }
  .panel .panel-body input[type=text],.panel .panel input[type=password]
     {
        border-style:solid;
      
         
      border-width:0px 0px 1px 0px;
     
     border-color:gray;
      box-shadow:none;
      border-radius:0px;
     }
     .form-horizontal .form-group .form-control 
     {
       
      padding-bottom:0px;
      padding-top:16px;
      
      
      font-size:20px;
     }
     .form-horizontal .form-group .control-label
     {

      padding-top:18px;
      width:auto;
    
     }
     .panel .panel
     {
         margin-left:4%;
         margin-right:4%;
         border-color:lightgray;

     }
      .form-horizontal .form-group select
      {

      
        text-align:center;
      }




</style>
<script>




</script>
     
 
 <div class="container-fluid">
 <div class="panel panel-default">
 <div class="panel-heading" style="background:#5dade2">



 
 <div class="progress" id="progress" style="display:none">
 
  <div  class="progress-bar progress-bar-striped active" role="progressbar"
  aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="font-size:24px;width:100%">
    Please Wait
  </div>
  
</div>
<h2 style="color:white">Create Company Account</h2>
</div>
<div class="panel-body" style="background">

<?php 

$ask= array('onsubmit'=>"return confirm('Register As Company?')",'id'=>'companyr_form'
  ,'class'=>'form-horizontal','role'=>'form');
     echo form_open_multipart('login/create_company',$ask);
   
     echo "<h4 style=text-align:center;color:red>* means required</h4><br>";
     echo "<h2 style=text-align:center;color:red>" . (empty($message)?"":$message). "</h2>";
     ?>
     <h4 style="text-align:center;color:red"><?php echo (empty($message2)?"":$message2);?></h4>
     <div class="form-group form-group-lg" >

<div class="panel panel-default">
<div class="panel-heading text-center" style="background:lightblue">

<p style="margin:0px">Login Information</p>

</div>
<div class="panel-body">

 

 <div class="form-group form-group-lg">
<label class="col-sm-1 control-label"  >* Username:</label>
<div class="col-sm-4">

<?php echo form_input('user',set_value('user'),'class=form-control');?>
 <?php echo form_error('user');?>
</div>

</div>

      <div class="form-group form-group-lg">
      <label class="control-label col-sm-1" style="">* Password:</label>
      <div class="col-sm-4" style="">
       <?php echo form_password('password',set_value('password'),'class=form-control');?>
        <?php echo form_error('password');?>
      </div>
             
      <label class="control-label col-sm-2" style="">* Confirm Password:</label>
      <div class="col-sm-4" style="">
       <?php echo form_password('c_password',set_value('c_password'),'class=form-control');?>
        <?php echo form_error('c_password');?>
      </div>
   

    </div>

  </div>
  </div>



<div class="panel panel-default">
<div class="panel-heading text-center" style="background:lightblue" >

<p style="margin:0px">Company Information</p>

</div>
<div class="panel-body">

 

   
  <div class="form-group form-group-lg">
  <label class="control-label col-sm-2">* Full Name:  </label>
     <div class="col-sm-4">
   <?php echo form_input('name',set_value('name'),'class=form-control');?>
    <?php echo form_error('name');?>
   </div>
  
    
     
     <label class="control-label col-sm-2">* Company Name:</label>
     <div class="col-sm-4">
      <?php echo form_input('c_name',set_value('c_name'),'class=form-control');?>
        <?php echo form_error('c_name');?>
      </div>
    
      </div>
         <div class="form-group form-group-lg">
     <label class="control-label col-sm-3">* Company Registration Number:</label>
     <div class="col-sm-4">
      <?php echo form_input('crn',set_value('crn'),'class=form-control');?>
     <?php echo form_error('crn');?>
      </div>
       <label class="control-label col-sm-1">* Business Type:</label> 
     <div class="col-sm-3">
      <?php echo form_input('b_type',set_value('b_type'),'class=form-control');?>
   
      <?php echo form_error('b_type');?>
         </div>
      
      </div>
      

      <div class="form-group form-group-lg">
     <br/>
     <label class="control-label col-sm-2">* Company Information:</label>
     <div class="col-sm-8">
     <?php echo form_textarea('c_information',set_value('c_information'),'class=form-control');?>
       <?php echo form_error('c_information');?>
     </div>
   
     </div>
      
     <div class="row">
      <div class="form-group form-group-lg col-sm-6"> 
      <label class="control-label col-sm-2">* Phone:</label>. 
     <div class="col-sm-10">
      <?php echo form_input('phone',set_value('phone'),'class=form-control');?> 
    
      <?php echo form_error('phone');?>
        </div>
        </div>
      
       <div class="form-group form-group-lg col-sm-6">
      <label class="control-label col-sm-2"> Website:</label> 

           <div class="col-sm-10">
           <?php echo form_input('website',set_value('website'),'class=form-control');?>
           
           <?php echo form_error('website');?>
           </div>
           </div>
           </div><!--close row-->
      <div class="row">
      <div class="form-group form-group-lg col-sm-6">
      <label class="control-label col-sm-2">* Email:</label> 
     <div class="col-sm-10">
      <?php echo form_input('email',set_value('email'),'class=form-control');?>
      
      <?php echo form_error('email');?>
      </div>
      </div>
      
       <div class="form-group form-group-lg col-sm-6">
       <label class="control-label col-sm-2">* Confirm Email:</label> 

            <div class="col-sm-9">
            <?php echo form_input('c_email',set_value('c_email'),'class=form-control');?>
            
            <?php echo form_error('c_email');?>
            </div>
            </div>
            </div><!--close row-->




            <div class="row">
      
            <div class="form-group form-group-lg col-sm-6">
      <label class="control-label col-sm-3">* Address:</label> 

           <div class="col-sm-9">
           <?php echo form_input('address',set_value('address'),'class=form-control');?>
            
           <?php echo form_error('address');?>
           </div>
           </div>
           
       <div class="form-group form-group-lg col-sm-6">
       <label class="control-label col-sm-3">* Township(Address):</label>
     <div class="col-sm-8">
     <?php echo form_input('township',set_value('township'),'class=form-control');?>
     
     <?php echo form_error('township');?>
     </div>
     </div>
     </div><!--close row-->



     <div class="row">
       <div class="form-group form-group-lg col-sm-6">
       <label class="control-label col-sm-4">* City(Address):</label> 
     <div class="col-sm-8">
       <?php echo form_input('city', set_value('city'),'class=form-control');?>
       
       <?php echo form_error('city');?>
       </div>
       </div>
       <div class="form-group form-group-lg col-sm-6">
      <label class="control-label col-sm-3">* Country(Address):</label> 
     <div class="col-sm-8">
     <?php echo form_input('country',set_value('country'), 'class=form-control');?>
     
     <?php echo form_error('country');?>
     </div>
     </div>
     </div><!--close row-->

      
     
     
     
     <br/>
    <div id="photo_div" class="form-group form-group-lg">
    <label class="control-label col-sm-3">Company Photo:</label>
    <div class="col-sm-8">
    <input type="hidden" id="photo_path" name="photo_path" value="<?php echo(empty($result['photo_path'])?"":$result['photo_path']);?>">
    <input type="hidden" id="photo_server_path" name="photo_server_path" value="<?php echo (empty($result['photo_server_path'])?"":$result['photo_server_path']);?>">
     <input type="hidden" id ="previous_s_path" name="previous_s_path" value="<?php echo (empty($result['photo_server_path'])?"":$result['photo_server_path']);?>">
          
          <img id="photo" class="img-thumbnail img-responsive" src="<?php echo (empty($result['photo_path'])?base_url() .'images/icons/no-image.jpg':$result['photo_path']);?>" style="width:480px;height:250px"> 
        <?php  //(empty($result['photo_path'])?base_url() .'images/icons/no-image.jpg':$result['photo_path']);?>
<?php

  $delete_photo_path =  base_url() . 'images/icons/delete.jpg';
 echo (!empty($result['photo_path'])? "<img id=delete_photo src=$delete_photo_path width=50 height=50>":"");?>
 
 </div>
 
       <div class="col-sm-8 col-sm-offset-2" >
       <br/>
             <input type="file" name="userfile" class="">
        </div>   
        <div class="col-sm-8 col-sm-offset-2">
        <br/>  
             <input type="button" class="btn btn-primary" style="margin-top:2%"  
             id="upload_button" name="upload_button" value='Upload Photo'>
              <?php echo (empty($result['error'])?"":$result['error']);?>
             <br/> <span style="font-size:14px">(max-upload-size:100kb)
      <br/><a href="<?php echo base_url(). 'files/reduce_size.docx';?>">How To Reduce Image Size</a></span>
            
      </div>
</div><!--close photo div-->



<br/><br/>
<hr>

<div class="form-group form-group-lg" id="logo_div">
     <label class="control-label col-sm-3">* Company Logo:</label>
    <div class="col-sm-8">
    <input type="hidden" id="logo_path" name="logo_path" value="<?php echo(empty($logo['photo_path'])?"":$logo['photo_path']);?>">
    <input type="hidden" id="logo_server_path" name="logo_server_path" value="<?php echo (empty($logo['photo_server_path'])?"":$logo['photo_server_path']);?>">
     <input type="hidden" id ="logo_previous_s_path" name="logo_previous_s_path" value="<?php echo (empty($logo['photo_server_path'])?"":$logo['photo_server_path']);?>">

          <img id="logo" class="img-thumbnail img-responsive" src="<?php echo (empty($logo['photo_path'])?base_url() .'images/icons/no-image.jpg':$logo['photo_path']);?>" style="width:240px;height:190px">
<?php
  echo form_error('logo_path');
  $delete_photo_path =  base_url() . 'images/icons/delete.jpg';
 echo (!empty($logo['photo_path'])? "<img id=delete_logo src=$delete_photo_path width=50 height=50>":"");?>
 
 </div>
 
       <div class="col-sm-8 col-sm-offset-2" >
       <br/>
             <input type="file" name="logo" class="">
        </div>   
        <div class="col-sm-8 col-sm-offset-2">
        <br/>  
             <input type="button" class="btn btn-primary" style="margin-top:2%"  
             id="upload_logo" name="upload_logo" value='Upload Logo'>
              <?php echo (empty($logo['error'])?"":$logo['error']);?>
             <br/> <span style="font-size:14px">(max-upload-size:100kb)
      <br/><a href="<?php echo base_url(). 'files/reduce_size.docx';?>">How To Reduce Image Size</a></span>
            
      </div>
</div><!--logo div-->
<br/><br/>



</div><!--close panel body-->
</div><!-- close panel-->
   
    
    
     

     <div style="text-align:center">
     <button name="register" id="register" style="font-size:18px;" class="btn btn-primary">Register Company</button>
    
     </div>
   
   


 
 </form>
 </div><!--close panel body-->
 </div><!--close panel-->
 </div><!--close container-->
  
 
