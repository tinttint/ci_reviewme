<br/>
<label class="control-label col-sm-3">* University Logo:</label>
<div class="col-sm-8">
<img id="logo" class="img-thumbnail" src="<?php echo (empty($logo['photo_path'])?base_url().'images/icons/no-image.jpg':$logo['photo_path']);?>" style="width:240px;height:190px">
<?php 

$delete_photo_path =  base_url() . 'images/icons/delete.jpg';
 echo (!empty($my_profile[0]['photo']) ||(!empty($logo['photo_path']))? "<img id=delete_logo src=$delete_photo_path width=50 height=50>":"");?>
 </div>
<div class="col-sm-8 col-sm-offset-2">
<br/>
<input type="file" name="logo">
</div>
<div class="col-sm-8 col-sm-offset-2" style="margin-top:1%">
<br/>

<input type="button" class="btn btn-primary" id ="upload_logo" name="upload_logo" value='Upload Logo'>
<?php echo (empty($logo['error'])?"":$logo['error']);?>
   <br/><span style="font-size:14px">(max-upload-size:100kb)
      <br/><a href="<?php echo base_url(). 'files/reduce_size.docx';?>">How To Reduce Image Size</a></span>
</div>

<input type="hidden" id="logo_previous_s_path" name="logo_previous_s_path" value="<?php echo (empty($logo['photo_server_path'])?"":$logo['photo_server_path']);?>">
<input type='hidden' id="logo_server_path" name="logo_server_path" value="<?php echo (empty($logo['photo_server_path'])?"":$logo['photo_server_path']);?>">
<input type="hidden" id="logo_path" name ="logo_path" value="<?php echo (empty($logo['photo_path'])?"":$logo['photo_path']);?>">
