<?php

$is_logged_in = $this->session->userdata('is_logged_in');
$role=$this->session->userdata('role');
    
    if(!isset($is_logged_in) || $is_logged_in != true|| ($role!=='company'&&$role!=='admin'))
    {
/*$this->load->view('templates/Cheader');
                $this->load->view('pages/members',$data);*/

                  // $this->load->view('templates/footer');
      echo "You must be logged in to access this page.";  
      echo anchor('login','login');
             exit();
    }
   

?>
<script>

</script>
<script>

$(document).ready(function(){
    
  $('#header_l_expired').css('background','yellow');
  $('#header_l_profile').css('background','#2874a6');
})





$(document).ready(function(){

    $('#jobs_div').on('change','#select_sort_by',function(e){
     e.preventDefault();
   
     var sort_by= $(this).val();
    
   
     var sort_order='asc';
    
     window.location.href="<?php echo base_url();?>"+'pages/display_expired_jobs/'+sort_by+'/'+sort_order;
  });
});


$(document).ready(function(){

    $('#jobs_div').on('change','#select_sort_order',function(e){
     e.preventDefault();
     
     var sort_by=$('#select_sort_by').val();
    
   
     var sort_order=$(this).val();
    
     window.location.href="<?php echo base_url();?>"+'pages/display_expired_jobs/'+sort_by+'/'+sort_order;
  });
});
$(document).ready(function(){
  
    $('#jobs_div').on('click','#refresh_button',function(e){
      e.preventDefault();
              window.location.href="<?php echo base_url();?>"+'pages/display_expired_jobs';
 

    });
});
$(document).ready(function(){

   $("#search_div").on('click','#search_button',function(e){
     e.preventDefault();
   
     
     var q = $('#search_div :input').serialize();
   // var q=encodeURIComponent(q);
    q= btoa(q);
     
    
      window.location.href="<?php echo base_url();?>"+'pages/display_expired_jobs/'+q;
    
    });

});


$(document).ready(function(){

    $("#jobs_div").on('click','#delete',function(e){
     e.preventDefault();  
     // alert(e.target.id);
      
      var a =$("#jobs_div input:checked:not(#check_all)");
      var number = a.length;
   
var q= $('#q').val();
     var sort_by = $('#sort_by').attr('value');
       var sort_order=$('#sort_order').val();
       
       var count = $('#count').val();
      

       var offset = $('#offset').val();

       var limit = $('#limit').val();
       var previous_url =$('#back_url').val();
     

       var count= count-number;
     /*  if(count==0 && offset !=0)
       {
        
         offset = offset - limit;
       
         previous_url="<?php echo base_url();?>"+'pages/display_job_entries/'+
         q+'/'+sort_by+'/'+sort_order+'/'+offset;

     }*/
  
      var selected = {};
  var b= $('#jobs_div input:checked');

  $('#jobs_div input:checked:not(#check_all)').each(function(i) {
      
        selected[i] = $(this).val();
       
});
  
     if(b.length==0)
     {
        alert("You must check the checkbox(s)");
        return;
      

     }
     var answer = confirm('delete job(s)');
    
     if(answer == false)
     {
      
      return false;
     }
   
      
       $.ajax({
               url: "<?php echo base_url() .'pages/delete_expired_jobs';?>",
                type:"POST", 
              //  dataType: 'json',
                data:{selected:selected,previous_url:
                previous_url},
                success: function(result){
                  $('#jobs_div').html(result);
               // window.location.href="<?php echo base_url();?>"+previous_url;
          
           }
        });    

    });

});





$(document).ready(function(){
  $('#jobs_div').on('click','#check_all',function(){
    //      e.preventDefault();
        var checked =$(this).is(':checked');
      
          if(checked==true)
          {
              
           $('input:checkbox').prop('checked','true');
         
          }
          else
          {
             $('input:checkbox').removeProp('checked');

          }


    });

});

</script>


<style>
#jobs_wrapper label,span
{

  font-size:16px;
}
.job_info
{
   max-height:50px;
   overflow:hidden;



}
#search_form label
{

  color:blue;
}



</style>

<?php 
?>


<div class="container-fluid">
<div class="panel panel-default">
<div class="panel-heading" style="background:#5dade2">
<?php $f_name=$this->session->userdata('name');?>
<h2 style="color:white;margin:0"><?php echo "Welcome " .$f_name;?></h2>
<h3 style="color:white"><?php echo "Expired Jobs";?></h3>


</div>
<div class="panel-body">
<div id="practice">



</div>
<div id="search_div" class="col-sm-12"style="margin-bottom:40px;background-color:ivory;border:1px solid blue">   
<form id="search_form" class="form-horizontal" role="form">
<br/><br/>
<div class="row">
  <div class="form-group col-sm-6 form-group-lg ">
  <label class="control-label col-sm-3" style="padding-right:0" >Job Id:</label>
  <div class="col-sm-8" style="padding-left:0">
    <?php echo form_input('job_id',(!empty($query['id'])?$query['id']:""),'class=form-control');?>
  </div>
  </div>
  <div class="form-group col-sm-6 form-group-lg center-block" >
  <label class=" control-label col-sm-4" style="padding-right:0" >Job Category:</label>
  <div class="col-sm-8" style="padding-left:0">
    <?php echo form_dropdown('category',$categories,(!empty($query['category'])?$query['category']:""),'class=form-control');?>
  </div>
  </div>
  
  </div><!--close row-->



  
  
<div class="row">
  <div class="form-group col-sm-6 form-group-lg" >  
  <label class="control-label col-sm-3" style="padding-right:0" > Job Title:</label>
  <div class="col-sm-8" style="padding-left:0">
    <?php echo form_input('title',(!empty($query['title'])?$query['title']:""),'class=form-control');?>
  </div>
  </div>
 
  


  <div class="form-group col-sm-6 form-group-lg">
  <label class="control-label col-sm-4" style="padding-right:0" > Job Salary:</label>
  <div class="col-sm-8" style="padding-left:0">
    <?php echo form_dropdown('salary',$salaries,(!empty($query['salary'])?$query['salary']:""),'class=form-control');?>
  </div>
  </div>

    </div><!--close row-->
  <div class="row">
  <div class="form-group col-sm-6 form-group-lg center-block" >
  <label class=" control-label col-sm-3" style="padding-right:0" > Job Type:</label>
  <div class="col-sm-8" style="padding-left:0">
    <?php echo form_dropdown('job_type',$job_types,(!empty($query['job_type'])?$query['job_type']:""),'class=form-control');?>
  </div>
  </div>
  
  
  
 
  </div><!--close row-->
<br/>
<div  class="col-sm-12"style="text-align:center">
<button class="btn btn-primary" id="search_button">Search</button>
<br/><br/>

</div>

</form>

</div><!--close applied search div-->
<p style="text-align:center;color:blue">Note: Expired jobs are jobs which passed 'apply valid to' date.</p>
<div id="jobs_div">
<?php
$count = count($jobs);?>
<input type="hidden" id="limit" value="<?php echo $limit;?>">
<input type="hidden" id="count" value="<?php echo $count;?>">

<input type="hidden" id="q" value="<?php echo $q;?>">
<input type="hidden" id="sort_by" value="<?php echo $sort_by;?>">
<input type="hidden" id="sort_order" value="<?php echo $sort_order;?>">
<input type="hidden" id="offset" value="<?php echo $offset;?>">


<?php //echo "<h3 style='text-align:center;color:blue'>Search Results for: <span style=color:red>" .  $message. "</span></h3>";?>
<?php echo "<h3 style='text-align:center;color:blue'>Total Records Found: $total_records</h3>";?>

<div class="col-sm-11" id="refresh_div" style="margin-left:4%;">

<button class="btn btn-primary" id='refresh_button'  >Refresh Table</button>

</div><!--close refresh_div-->
<?php echo form_open('','method=get');?>
<?php $back_url=$this->uri->uri_string();?>
  <input type="hidden" id="back_url" name="back_url" value="<?php echo $back_url;?>">
  <div id="top_bar_div" class="col-sm-11"style=";padding:8px;margin-left:4%;border:1px solid blue;background:ivory">


<div class="col-sm-2" >
<label class="col-sm-12 text-center">Sort By:</label>
      <div class="col-sm-12">
      <select class="form-control" id="select_sort_by">
      <option <?php echo (($sort_by=='last_updated')?'selected':'');?> value='<?php echo "$q/last_updated";?>'>Last Updated</option>
       <option <?php echo (($sort_by=='opened_date')?'selected':'');?> value='<?php echo "$q/opened_date";?>'>Job Opened Date</option>
      <option <?php echo (($sort_by=='published_date')?'selected':'');?> value='<?php echo "$q/published_date";?>'>Published Date</option>
    
  
    <option <?php echo (($sort_by=='category')?'selected':'');?> value='<?php echo "$q/category";?>'>Job Category</option>
     <option <?php echo (($sort_by=='title')?'selected':'');?> value='<?php echo "$q/title";?>'>Job Title</option>
     
    <option <?php echo (($sort_by=='job_type')?'selected':'');?> value='<?php echo "$q/job_type";?>'>Job Type</option>
    <option <?php echo (($sort_by=='city')?'selected':'');?> value='<?php echo "$q/city";?>'>City(Address)</option>
     <option <?php echo (($sort_by=='valid_to')?'selected':'');?> value='<?php echo "$q/valid_to";?>'>Apply Valid To</option>
   
    
   </select>
      

</div>
</div>
<div class="col-sm-2">
<label class="col-sm-12 text-center">Sort Order:</label>
      <div class="col-sm-12">
      <select class="form-control" id="select_sort_order">
    <option <?php echo (($sort_order=='asc')?'selected':'');?> value="asc" selected>Ascending</option>
    <option <?php echo (($sort_order=='desc')?'selected':'');?> value="desc">Descending</option>
  </select>
</div>
</div>

<div class="col-sm-6" >
<div style="margin-left:10%;padding:4px" id="pagination">
  <?php echo $pagination;?>
  </div>
</div>
<div class="col-sm-2" >
<?php 
 
    if(in_array('check_all',$checked))
        {
             $found_all="checked";
        }
        else
        {
          $found_all="";
        }
        ?>
       
<input type="checkbox" id="check_all" <?php echo $found_all;?> class="form-control" style="width:50px;display:inline"    name="checked[]" value="check_all">
(Select/Deselect All)
<button class="btn btn-primary" id="delete">Delete</button> 
</div>
</div><!--close top_bar_div -->



<?php foreach($jobs as $value):?>

<div id="jobs_wrapper" class="col-sm-11"style=";padding:8px;margin-left:4%;border:1px solid blue">
  
  <div class="col-sm-2 text-center" style="">
  <?php $logo=$value['logo'];?>
    <img class="img-responsive img-thumbnail" id="logo"src="<?php echo(empty($logo)?base_url().'images/icons/no-image.jpg':$logo);?>"
     style="width:200px;height:140px" >
      <p>Job Id:<?php echo $value['id'];?></p>
      </div>
   <div class="col-sm-10" id="wrapper2">   
   <div class="row">
  
  
  <div class="col-sm-6 job_info">
  <label>Company Name:</label><span><?php echo $value['c_name'];?></span>
  </div>
   
  <div class="col-sm-6 job_info" >
    <label>Job Category:</label><span><?php echo $value['category'];?></span>
  </div>
  
  </div>
  <div class="row">
  
  <div class="col-sm-6 job_info" >
  <label>Job Title:</label><span><?php echo $value['title'];?></span>
  </div>
    
  <div class="col-sm-6 job_info" >
    <label>Job Salary:</label><span><?php echo $value['salary'];?></span>
  </div>
  
  </div>
  
  <div class="row">
 
  <div class="col-sm-6 job_info" >
  <label>Job Type:</label><span><?php echo $value['job_type'];?></span>
  </div>
    
  <div class="col-sm-6 job_info" >
    <label>City(Address):</label><span><?php echo $value['city'];?></span>
  </div>
  
  </div>

<div class="row">
  
  <div class="col-sm-6 job_info" >
  <label>Job Opened Date:</label><span><?php 
  
    $op=date_create($value['opened_date']);
  
  echo date_format($op,'d-M-Y');
    
  ?>
  </span></div>
  <div class="col-sm-6">
  <label>Job Status:</label><span><?php echo $value['job_status'];?></span>
  </div>
  </div>
  <div class="row">
  <div class="col-sm-6 job_info" >
  <label>Apply Valid To:</label><span><?php 
  
    $vt=date_create($value['valid_to']);
  
  echo date_format($vt,'d-M-Y');
    
  ?>
  </span>

  </div>
  <div class="col-sm-3 job_info" >
  <?php 
   
   $job_id=$value['id'];
   //$pu=$this->uri->uri_string();


   ?>
 
    <input type="submit" class="btn btn-primary" formaction='<?php echo base_url()."pages/expired_job_details/$job_id";?>' id="details" name="details" value="Details">
  </div>

   <div class="col-sm-3 text-center" style="overflow:initial">
     
<?php $job_id=$value['id'];
 if(in_array($job_id,$checked))
        {
             $found="checked";
        }
        else
        {
          $found="";
        }
        ?>


    <input class="form-control"type="checkbox" <?php echo $found;?> style="margin-left:20%;width:50px;display:inline-block"  name="checked[]" value="<?php echo $job_id;?>"><span>(Select To Delete)</span>
  </div>
  </div> 
  

  </div><!--close wrapper2-->
</div><!--close jobs_wrapper-->
<?php endforeach;?>
<?php echo form_close();?>


<div id="bottom_bar_div" class="col-sm-11"style="background:lightyellow;padding:8px;margin-left:4%;border:1px solid blue">
<div id="pagination" style="margin-left:40%">
<?php echo $pagination;?>
</div>
</div>

</div><!--close jobs_div-->
</div><!--close panel-body-->
</div><!--close panel-->
</div><!--close container-->
   




