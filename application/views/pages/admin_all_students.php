<?php
$is_logged_in = $this->session->userdata('is_logged_in');
$role=$this->session->userdata('role');
    
    if(!isset($is_logged_in) || $is_logged_in != true|| ($role!=='university'&& $role!=='administrator'))
    {
/*$this->load->view('templates/Cheader');
                $this->load->view('pages/members',$data);*/

                  // $this->load->view('templates/footer');
      echo "You don't have permission to access this page.";  
      echo anchor('admin_role','login');
             exit();
    }
?>    


<script>


$(document).ready(function(){

    $("#students_div").on('click','#pagination>a',function(e){
     e.preventDefault();
     var address= $(this).attr('href');
   
      
       $.ajax({
                url: address,
                type:"POST", 
              //  dataType: 'json',
                data:{is_ajax: 'true',address:address},
                success: function(result){
                  
            $("#students_div").html(result);
           // $("#applicants #previous_url").val(this.url);
           
        }
        });
    
    });
});


$(document).ready(function(){

    $("#students_div").on('click','th>a',function(e){
     e.preventDefault();
     var address= $(this).attr('href');
      
       $.ajax({
                type:"POST",
                url: address,
                
              //  dataType: 'json',
                data:{is_ajax: 'true'},
                 
            
                success: function(result){
            $("#students_div").html(result);
        }
        });
    
    });

});

$(document).ready(function(){
  

    $('#students_table').on('click','#refresh_submit',function(e){
      e.preventDefault();
                // alert($('#status').val());
                
 $.ajax({
                url: "<?php echo base_url();?>"+'admin_role/display_all_students',
                type:"POST", 
              //  dataType: 'json',
                data:{is_ajax:true},
                success: function(result){
            $("#students_div").html(result);
            $('#search_div input:text').val('');
            $('#search_div select').val('');
            $('#university_name').val('');

            //$('#company_name').val('');
             //$('job_applied_data').val('');
        }
        });
    
     

    });
});
$(document).ready(function(){

    $("#search_div").on('click','#search_submit',function(e){
     e.preventDefault();
   // alert('hi');
     
     var c = $('#search_div :input').serialize();
     
    c=btoa(c);
    // alert(c);
   
      //alert(address);
       $.ajax({
                type:"POST",
                url: "<?php echo base_url();?>" + "admin_role/display_all_students/"+c,
                
              //  dataType: 'json',
                data:{is_ajax: 'true'},
                 //complete : function(){
                // alert(this.url+"url")
            // },
                success: function(result){
            $('#students_div').html(result);
        }
        });
    
    });
});



</script>
<script>


function change_rows(here)
{
     
  document.forms['change_rows_form'].submit();
}

</script>
<style>

option[selected]
{
    display:none;
    color:gray;

}
input[type=submit].search{
  
  width:100px;
  text-align:center;
  

}
label{
    /*
margin-left:23%;
    float:left;
  width:200px;  
    text-align:right;*/
  


}
label.status
{   margin-left:0px;
    float:none;
  width:0px;  
  font-size:16px;
    text-align:center;
  

  color:blue;
}
.position
{

  /*float:left;*/
  vertical-align:middle;
  width:25px;
}
select{

  width:140px;
}

a
{
  color:red;
}
#search_div input[type=text]
{

  width:20%;
  border-radius:10px;
}
#search_div select
{

  width:20.2%;
  border-radius:10px;
}#search_div label{
    
  
    display:inline-block;
    width:180px;
    text-align:right; 
    margin-left:5%;
    
 

}
#search_div #inner_search
{
  margin-left:11%;
}


</style>

<?php $user =$this->session->userdata('username');
    $role=$this->session->userdata('role');
  echo "<h2>Welcome " . $user ."</h2>";
  
  ?>





<br/><br/>



<fieldset>
<h2 style="text-align:center"> All University Students</h2>
<div id="search_div" style="margin-left:0px;margin-bottom:40px;background-color:lightgreen">   
<?php echo form_open('admin_role/');?>
<?php echo validation_errors();?>


<br/><br/>
<div id="inner_search">
<?php echo form_label('Student Registration Number:','id');?>
<?php echo form_input('id',(isset($query)?$query['id']:""),'id="No"');?>




<?php echo form_label('National Registration Card No:','nrc');
echo form_input('nrc',(isset($query)?$query['nrc']:""),'id=nrc');?><br/><br/>




<?php echo form_label('Student Full Name:', ' Name');
echo form_input('name', (isset($query)?$query['name']:""),'id ="Name"');?>






<?php echo form_label('Major:','major');

echo form_input('major',(isset($query)?$query['industry']:""),'id="major"');?><br/><br/>




<?php echo form_label('University Name:');
echo form_dropdown('u_name',$university_names_dd,(isset($query)?$query['u_name']:""),'id=university_name');?>



<?php echo form_label('Status:','Status');
echo form_dropdown('status',array(''=>'','unchecked'=>'Unchecked',
'disapproved'=>'Disapproved','approved'=>'Approved'),(isset($query)?$query['status']:""), 'id="status"');?>
</div>
<br/><br/>
<div style="text-align:center">
<?php echo form_submit('action','Search','class=search id=search_submit');?>
</div>
<br/><br/>

<?php echo form_close();?>
</div>





<div id="students_div">
         
<?php

echo "<h3 style=text-align:center;color:red>Search Results: " . (empty($search_results)?"":$search_results) . "</h3>";
echo "<h3 style=color:blue;text-align:center>Total Records Found: " . $total_records . "</h3>";//echo $this->table->generate($all_students);

echo form_open('admin_role/all_students_cr','id=change_rows_form');?>

<div id="select_rows" style="text-align:center">
<input type="hidden" name="q" value="<?php echo $q;?>">
<input type="hidden" name="sort_by" value="<?php echo $sort_by;?>">
<input type="hidden" name="sort_order" value="<?php echo $sort_order;?>">

<?php
/*$list=array("disabled selected hidden"=>'show rows','2'=>'show 2 rows',
'10'=>'show 10 rows','20'=>'show 20 rows');*/

$selected= (empty($limit)?'':$limit);
//$selected = 5;
?>
<select name="rows" onchange="change_rows(this)">
    <option value="" disabled selected>Show Rows</option>
    <option value="2" <?php echo($selected==2?'selected':"");?>>show 2 rows</option>
    <option value="10" <?php echo ($selected==10?'selected':"");?>>show 10 rows</option>
    <option value="20" <?php echo ($selected==20?'selected':"");?>>show 20 rows</option>
</select>
<?php

echo form_close();


?>


</div>
</form>
<?php 

echo form_open('pages/change_status','id=change_status_form name=status_check');?>
<table id="students_table" style="margin-bottom:14px">
<?php echo "<tr class=refresh>";
echo "<td style=border:none>";
echo form_submit('refresh','Refresh Table','form=refresh_form id=refresh_submit');
echo "</td>
</tr>";
?>
<tr><th>UserName</th>
<th <?php if($sort_by =='id')echo "class=sort_$sort_order";?>><?php echo 
anchor("admin_role/display_all_students/$q/id/". (($sort_order == 'asc' && $sort_by == 'id')?'desc': 'asc')."/$limit"
,'Student Registration Number');?></th>
<th>Full Name</th><th>National Registration Card No</th> 
<th <?php if($sort_by =='u_name')echo "class=sort_$sort_order";?>><?php echo 
anchor("admin_role/display_all_students/$q/u_name/". (($sort_order == 'asc' && $sort_by == 'u_name')?'desc': 'asc')."/$limit"
,'University');?></th>

<th <?php if($sort_by =='industry')echo "class=sort_$sort_order";?>><?php echo 
anchor("admin_role/display_all_students/$q/industry/". (($sort_order == 'asc' && $sort_by == 'industry')?'desc': 'asc')."/$limit"
,'Major');?></th>
<th <?php if($sort_by =='gender')echo "class=sort_$sort_order";?>><?php echo 
anchor("admin_role/display_all_students/$q/gender/". (($sort_order == 'asc' && $sort_by == 'gender')?'desc': 'asc')."/$limit"
,'Gender');?></th>

<th <?php if($sort_by =='nationality')echo "class=sort_$sort_order";?>><?php echo 
anchor("admin_role/display_all_students/$q/nationality/". (($sort_order == 'asc' && $sort_by == 'nationality')?'desc': 'asc')."/$limit"
,'Nationality');?></th><!-- 
<th>Email</th> -->
<th <?php if($sort_by =='registered_date')echo "class=sort_$sort_order";?>><?php echo 
anchor("admin_role/display_all_students/$q/registered_date/". (($sort_order == 'asc' && $sort_by == 'registered_date')?'desc': 'asc')."/$limit"
,'Registered Date');?></th>
<th>View Details</th></tr>


<?php 


$this->load->library("pagination");
//$segment = $this->uri->segment(3);
if(empty($all_students))
{
    echo "<tr><td></td>";
    echo "<td></td>";
    echo "<td></td>";
    echo "<td></td>";
    echo "<td></td>";
    echo "<td></td>";
    echo "<td></td>";
    echo "<td></td>";
    echo "<td></td>";
    echo "<td></td>";
    echo "<td></td></tr>";
}
else
{



 foreach($all_students as $j)
{



    echo "<tr><td><div><span>" . $j['user']. "</span></div></td>";
  echo "<td><div><span>". $j['id']. "</span></div></td>";
  //echo "<td>". $j['user']."</td>";
            
  //echo "<input type='hidden' name='approve_user' value='$user'>";
  echo "<td><div><span>". $j['name']. "</span></div></td>";
  echo "<td><div><span>". $j['nrc']. "</div></span></td>";
  echo "<td><div><span>". $j['u_name']. "</span></div></td>";
  /*echo "<td>". $j['qualifications']. "</td>";*/
  echo "<td><div><span>". $j['industry']. "</span></div></td>";
  echo "<td><div><span>". $j['gender'] ."</span></div></td>";
  /*echo "<td>";$date_o_b = date_create($j['date_o_b']);
  echo date_format($date_o_b,'d-m-Y');echo "</td>";*/
  echo "<td><div><span>". $j['nationality']."</span></div></td>";
  //echo "<td>". $j['phone'] . "</td>";
  /*echo "<td>" . $j['email'] . "</td>";*/
  //echo "<td>". $j['address']."</td>";
  //echo "<td>". $j['township']. "</td>";
  //echo "<td>". $j['city']. "</td>";
  //echo "<td>" .$j['country']. "</td>";
  echo "<td><div><span>" ;$rd=date_create($j['registered_date']);
  echo date_format($rd,'d-m-Y');echo "</span></div></td>"; 
  

  
echo "<td style=max-width:140px><div><span>";
$user= $j['user']; 
echo"<input type='submit' id='details_submit' 
form='details_student' name=$user value='Details'/>";
echo "</span></div></td></tr>";
  

    
    
   
}
echo "</table>";

echo form_close();
}

echo form_open('admin_role/details_student',"id='details_student'");
$previous_url = base_url() .$this->uri->uri_string();?>
<input type="hidden" name='previous_url' id="previous_url"
 value="<?php echo $previous_url;?>">
 
 <?php





//
  
echo form_close();

echo "<span style=text-align:center>". $pagination . "</span>";?>
 </div>


 </fieldset>