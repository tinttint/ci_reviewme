<?php
$is_logged_in = $this->session->userdata('is_logged_in');
$role=$this->session->userdata('role');
    
    if(!isset($is_logged_in) || $is_logged_in != true|| ($role!=="admin"))
    {
/*$this->load->view('templates/Cheader');
                $this->load->view('pages/members',$data);*/

                  // $this->load->view('templates/footer');
      echo "You don't have permission to access this page.";  
      echo anchor('admin_role','login');
             exit();
    }
?>    


<script>

$(document).ready(function(){
    
  $('#admin_h_l_students').css('background','yellow');
  $('#admin_h_l_profile').css('background','#2874a6');
})

$(document).ready(function(){
  $('#ajax_students_div').on('click','#check_all',function(){
    //      e.preventDefault();
        var checked =$(this).is(':checked');
        
          if(checked==true)
          {
              
           $('input:checkbox').prop('checked','true');
          }
          else
          {
             $('input:checkbox').removeProp('checked');

          }
    });

});

$(document).ready(function(){
  

     $('#ajax_students_div').on('click','#refresh_button',function(e){
      e.preventDefault();
              window.location.href="<?php echo base_url();?>"+'admin_role/display_students';
 

    });
});

$(document).ready(function(){
     $('#ajax_students_div').on('change','#drop',function(e){
      e.preventDefault();
    
           var user=$(this).attr('name');
           var url=$('#url').val();
          var status = $(this).val();
          var result=false;
             if(status=="Approved")
             {
              
                result = confirm("Approve Student(s)?\n\n Approval confirmation email(s) will be sent to the student(s)");
             }
             if(status=="New")
             {

                result=confirm("Change Status");

             }
             if(status =="Disapproved")
             {
               // result = confirm('Disapprove Student(s)?\n\n Disapproved confirmation email(s) will be sent to the student(s)');
                  $('#myModal').modal();
                //  $('#message_modal').val('');
             var student_id=$(this).parents('tr').find('td:nth-of-type(1)').text();
                          $('#id_modal').val(student_id);
                          var nrc=$(this).parents('tr').find('td:nth-of-type(3)').text();
                          $('#nrc_modal').val(nrc);
                         // var message="something";
                          var message= 'You have been disapproved from Job Career Development Site as your student\n'
     +' id or national registration card no do not exist in our database.\n'
       +' Student Registration Number:'+student_id+'\n'
       + 'National Registration Card No:'+nrc;
                        
                        $('#message_modal').val(message);
                          $('#user_modal').val(user);
                  $('#drop').val('Disapproved');
                  return false;

             }
             
            
             if(result ==false)
             {

                $('#drop').val('Change_Status');
              
              return false;
             }
                $('#progress').css('display','block');
                
                $.ajax({
                  url:"<?php echo base_url() .'admin_role/change_status';?>",
                  type:"POST",
                  dataType:"json",
                  beforeSend:function(){
                   //// $('#loading_message').show();
                  }, 
                  
                  data:{status:status,user:user,url:url},
                // dataType:'json',
                  success:function(result)
                  {
                    $('#progress').hide();
                       //  var element=$.parseHTML(result);
                         //var element=$(element).filter('#some').html();
                          
                       if(result.message=="false")
                       {
                         $('#progress').hide();
                         alert("Can Not Change Status. Already "+ status);
                           $('#drop').val('Change_Status');

                       }
                     /*  else if(result.message=="email_error")
                       {
                            $('#progress').hide();
                          alert("Can Not Approve. Email Error."
                            +"\nCheck Internet Connection");


                       }*/
                    
                       else if(result.message=="true")
                       {
                        $('#progress').hide();
                        if(status=="Approved"||status=="Disapproved")
                        {
                        alert('Email Sent');
                      }
                    }
                    else
                    {


                    }
                           
                      $.ajax({

                        url:"<?php echo base_url();?>"+url,
                        type:"POST",
                        success:function(result)
                        {

                           $("#ajax_students_div").html(result);
                        }
                      });
                   
                   //$('#ajax_students_div').html(result);
                   //$('#drop').val('Change_Status');
                     
                  
                 
                
                       

                  }
               

                });

     });

});

$(document).ready(function(){

    $("#search_form").on('click','#search_button',function(e){
     e.preventDefault();
   // alert('hi');
     
     var c = $('#search_form :input').serialize();
   
  // c=encodeURIComponent(c);
     c=btoa(c);
     
      window.location.href="<?php echo base_url();?>"+'admin_role/display_students/'+c;
    
    
    });
});
$(document).ready(function(){

$('#send_modal').click(function(e){
  e.preventDefault();
  var answer=confirm("send email?");
  if(answer==false)
  {
    return;
  }

     
    var message=$('#message_modal').val();
   
    var user=$('#user_modal').val();
    var address=$('#url').val();
   
    

         $('#progress').css('display','block');
    
  //  var id=$('#id_modal').val();
   //var category="sport";
  
       $.ajax({

                  url:"<?php echo base_url() .'admin_role/disapprove_student';?>",
                  type:"POST",
                  
                  data:{message:message,user:user,url:address},
                  dataType:"json",
                  success:function(result)
                  { 
                    
                    if(result.message=="false")
                      {
                        $('#progress').css('display','none');
                        alert('Can Not Send Email. Already Been Disapproved.');
                       }
                       else if(result.message=="true")
                       {
                          $('#progress').css('display','none');
                          alert('Disapproved Email Sent');
                           $('#myModal').modal('hide');
                        }
                     /*   else if(result.message=="email_error")
                        {
                          $('#progress').css('display','none');
                          alert('Can not disapprove.Email Error'
                            +'\nCheck Internet Connection')
                         // window.location="<?php echo base_url().'admin_role/d_s_email_error';?>";
                        }*/
                    

                     $.ajax({

                        url:"<?php echo base_url();?>"+address,
                        type:"POST",
                        success:function(result)
                        {

                           $("#ajax_students_div").html(result);
                        }
                      });
                
                           
                       }

                   
                });

});


});
$(document).ready(function(){

  $('#cancel_modal').click(function(){
    $('#drop').val('Change_Status');
   // $('#progress').css('display','none');
   
  })
})

</script>
<script>


function change_rows(here)
{
     
  document.forms['change_rows_form'].submit();
}

</script>



<style>
.table-bordered
{
   
  border-top:1px solid blue;
  /*border-right:1px solid red;*/
 
 /* border-left:1px solid red;*/
}
.table > thead > tr >th{

  text-align:center;
 /*border:1px solid red;*/
  border-top:1px solid blue;
  border-right:1px solid blue;
  border-bottom:1px solid blue;
  border-left:1px solid blue;
  color:blue;
}
.table > thead > tr >th>a{


color:blue;

}
.table>tbody>tr>td
{
border-top:1px solid blue;
  border-right:1px solid blue;
  border-bottom:1px solid blue;
  border-left:1px solid blue;
  font-size:16px;

}

.table>tbody>tr>td>div
{

  max-height:60px;
  overflow:hidden;
}
.table
{

  border-collapse:collapse;
  border-spacing:0px;
}
#delete:hover
{

  cursor:pointer;
}

a
{
  color:red;
}
#details_link 
{
  color:blue;
  background-color:orange;
  width:180px;
  height:80px;
  padding:6px 10px;
}



#search_form label
{

color:blue;


}
#ajax_students_div #drop option[selected]
{
  display:none;
  color:red;
  background:red;
}
#rows option[selected]
{

  display:none;
  color:red;
}
#drop
{
  color:red;
}

.progress{
height:70px;


width:50%;
position:fixed;
top:60%;
left:25%;
z-index:1;
pointer-events:none;


}

.progress-bar {
  padding:15px;
}

</style>

<?php $user = $this->session->userdata('username');
$is_logged_in = $this->session->userdata('is_logged_in');
  

 
?>
<div class="progress" id="progress" style="display:none">
 
  <div  class="progress-bar progress-bar-striped active" role="progressbar"
  aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="font-size:24px;width:100%">
    Please Wait
  </div>
  
</div>
<div id="practice">



</div>

<div id="myModal" class="modal fade" role="dialog" style="">
  <div class="modal-dialog" style="">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header text-center">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Disapprove Student</h4>
        <p>(Add your own message or send this default message)</p>
        <div class="col-sm-12">
        <div class="col-sm-6">

        Student Registration Number:<input type="text" name="id_modal" id="id_modal" value="" style="padding-left:5px" readonly>
        </div>
        <div class="col-sm-6">
        National Registration Card No:
        <input type="text" name="nrc_modal" id="nrc_modal" value="" style="padding-left:5px" readonly>
        </div>
        </div>

      </div><!--close modal-header-->
      <div class="modal-body">
         <div class="form-group">
         <input type="hidden" name="user_modal" id="user_modal" value="">
        <?php $a="sentence";?>
      
        <textarea name="message_modal" rows="5" id="message_modal" class="form-control">
           
        </textarea>
      
       
        </div>
      </div>
      <div class="modal-footer">
       <input type="submit" class="btn btn-default" value="Send"  name="send_modal" id="send_modal">
        <button type="button" class="btn btn-default" name="cancel_modal" id="cancel_modal" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>
<div class="container-fluid" >
<div class="panel panel-default">
<div class="panel-heading" style="background:#5dade2">
<?php $f_name=$this->session->userdata('name');?>
<h2 style="color:white;margin:0">Welcome <?php echo $f_name;?></h2>
<h3 style="color:white">Students' Accounts</h3>



</div>
<div class="panel-body" style="background:">
<div id="search_div" class="col-sm-12"style="margin-bottom:40px;border:1px solid blue;background-color:ivory">   
<form id="search_form" class="form-horizontal" role="form">
<br/><br/>
<div class="row">
  <div class="form-group col-sm-6 form-group-lg">
  <label class="control-label col-sm-6" style="padding-right:4px" >Student Registration Number:</label>
  <div class="col-sm-6" style="padding-left:0">
    <?php echo form_input('id',(!empty($query['id'])?$query['id']:""),'class=form-control');?>
  </div>
  </div>
  <div class="form-group col-sm-6 form-group-lg" >
  <label class=" control-label col-sm-6" style="padding-right:0" >National Registration Card No:</label>
  <div class="col-sm-6" style="padding-left:0">
    <?php echo form_input('nrc',(!empty($query['nrc'])?$query['nrc']:""),'class=form-control');?>
  </div>
  </div>
  </div>
  
  
<div class="row">
  <div class="form-group col-sm-6 form-group-lg" >  
  <label class="control-label col-sm-6" style="padding-right:0" > Full Name:</label>
  <div class="col-sm-6" style="padding-left:0">
    <?php echo form_input('name',(!empty($query['name'])?$query['name']:""),'class=form-control');?>
  </div>
  </div>
 
  

  <div class="form-group col-sm-6 form-group-lg">
  <label class="control-label col-sm-6" style="padding-right:0" >Major:</label>
  <div class="col-sm-6" style="padding-left:0">
    <?php echo form_input('major',(!empty($query['major'])?$query['major']:""),'class=form-control');?>
  </div>
  </div>
  </div>
  <div class="row">
  <div class="form-group col-sm-6 form-group-lg" >
  <label class=" control-label col-sm-6" style="padding-right:0" >Nationality:</label>
  <div class="col-sm-6" style="padding-left:0">
    <?php echo form_input('nationality',(!empty($query['nationality'])?$query['nationality']:""),'class=form-control');?>
  </div>
  </div>
  
  
  
   
  <div class="form-group col-sm-6 form-group-lg" >  
  <label class="control-label col-sm-6" style="padding-right:0" > Status:</label>
  <div class="col-sm-6" style="padding-left:0">
    <?php echo form_dropdown('status',array(''=>'','New'=>'New',
'Disapproved'=>'Disapproved','Approved'=>'Approved'),(!empty($query['status'])?$query['status']:""),'class=form-control');?>
  </div>
  </div>
 
  </div><!--close row-->
<br/>
<div  class="col-sm-12"style="text-align:center">
<button class="btn btn-primary" style="font-size:18px" id="search_button">Search</button>
<br/><br/>
</div>

</form>

</div><!--close applied search div-->
<?php
echo form_open('admin_role/change_rows','id=change_rows_form');?>

<div id="select_rows" style="text-align:center">

<input type="hidden" name="q" value="<?php echo $q;?>">
<input type="hidden" name="sort_by" value="<?php echo $sort_by;?>">
<input type="hidden" name="sort_order" value="<?php echo $sort_order;?>">

<?php
/*$list=array("disabled selected hidden"=>'show rows','2'=>'show 2 rows',
'10'=>'show 10 rows','20'=>'show 20 rows');*/

$selected= (empty($limit)?'':$limit);
//$selected = 5;
?>
<select id="rows" name="rows" style="height:25px"onchange="change_rows(this)">
    <option value="" disabled selected>Show Rows</option>
    <option value="2" <?php echo($selected==2?'selected':"");?>>show 2 rows</option>
    <option value="10" <?php echo ($selected==10?'selected':"");?>>show 10 rows</option>
    <option value="20" <?php echo ($selected==20?'selected':"");?>>show 20 rows</option>
</select>
</div>
<?php

echo form_close();


?>

<div id="ajax_students_div">

<?php echo "<h3 style=text-align:center;color:blue>Total Records Found: " . $total_records . "</h3>";?>
<div style="text-align:center">
<?php echo $pagination;?>
</div>



<div class="table-responsive">
<table class="table text-center table-bordered">
    <thead class="text-center">
     <tr>
     
     <input type="button" id="refresh_button"  class=" btn btn-primary" value="Refresh Table">
     <div style="float:right">
    

<?php echo form_open('admin_role/change_students_page');?>
<?php $page=($offset/$limit)+1;?>
Page:<input style="width:100px;display:inline-block" type="text" name="page" id="page" value="<?php echo $page;?>" class="form-control">
<button type="submit" style="width:40px;display:inline-block;margin:0 auto" class="form-control">Go</button>
<input type="hidden" name="limit" id="limit" value="<?php echo $limit;?>">

 <input type="hidden" name="q" id="q" value="<?php echo $q;?>"> 
 <input type="hidden" name="sort_by" id="sort_by" value="<?php echo $sort_by?>"> 
 <input type="hidden" name="sort_order" id="sort_order" value="<?php echo $sort_order;?>"> 
</form>


     </div>
     
     </tr> 
      <tr class="text-center">
       
        <th>Student Registration Number</th>
        <th <?php if($sort_by =='name')echo "class=sort_$sort_order";?>><?php echo 
anchor("admin_role/display_students/$q/name/". (($sort_order == 'asc' && $sort_by == 'name')?'desc'
: 'asc')."/$limit",'Full Name');?></th>
        <th>National Registration Card No</th>
        <th <?php if($sort_by =='major')echo "class=sort_$sort_order";?>><?php echo 
anchor("admin_role/display_students/$q/major/". (($sort_order == 'asc' && $sort_by == 'major')?'desc'
: 'asc')."/$limit",'Major');?></th>
        <th <?php if($sort_by =='gender')echo "class=sort_$sort_order";?>><?php echo 
anchor("admin_role/display_students/$q/gender/". (($sort_order == 'asc' && $sort_by == 'gender')?'desc'
: 'asc')."/$limit",'Gender');?></th>
      
        <th <?php if($sort_by =='nationality')echo "class=sort_$sort_order";?>><?php echo 
anchor("admin_role/display_students/$q/nationality/". (($sort_order == 'asc' && $sort_by == 'nationality')?'desc'
: 'asc')."/$limit",'Nationality');?></th>
        <th <?php if($sort_by =='registered_date')echo "class=sort_$sort_order";?>><?php echo 
anchor("admin_role/display_students/$q/registered_date/". (($sort_order == 'asc' && $sort_by == 'registered_date')?'desc'
: 'asc')."/$limit",'Registered Date');?>
        <th>Details</th>
        <th>Status</th>

      </tr>
    </thead>
    <tbody>
    <?php foreach($students as $values):?>
      <tr>
        
        <td><div><?php echo $values['id'];?></div></td><!--change will affect in script-->
        <td><div><?php echo $values['name'];?></div></td>
        <td><div><?php echo $values['nrc'];?></div></td>
        <td><div><?php echo $values['major'];?></div></td>
         <td><div><?php echo $values['gender'];?></div></td>
          <td><div><?php echo $values['nationality'];?></div></td>
           <td><div><?php $rd=date_create($values['registered_date']);
           echo date_format($rd,'d-M-Y');?>
                
                   
          
          </div> </td>
         
           <td><div>
            <form method="get">
              <?php $student=$values['user'];
            
             $student=base64_encode($student);

              ?> 
              <input type="submit" class="btn btn-primary" id="details" name="details" formaction='<?php echo base_url()."admin_role/details_student/$student";?>' value="Details"></td>
          

           </div></td>
            
  
            <td><div>

         
<?php $back_url=$this->uri->uri_string();?>
 <input type="hidden" name="back_url" value="<?php echo $back_url;?>">
  <?php $count=count($students);?>
 <?php $url=$this->uri->uri_string();?>
              <input type="hidden" id="url" name="url" value="<?php echo $url;?>">

              
             <?php  $user_s=$values['user'];

              echo "<span style=color:blue>".$values['status']."</span><br>";
              echo form_dropdown($user_s,array('Change_Status'=>'Change Status','New'=>'New','Disapproved'=>'Disapproved','Approved'=>'Approved'),"Change_Status","style=height:34px;width:140px
     id=drop ");?>
       </form>
           </div></td>
         
      </tr>
     <?php endforeach;?>
    </tbody>
  </table>
  </div><!--close table div-->

<div style="text-align:center">
<?php echo $pagination;?>
</div>
</div><!--close ajax_students_div-->


 
</div>
</div><!--close panel body-->
</div><!--close panel-->
</div><!--close container fluid-->
<?php//////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////?>




<?php /////////////////////////////////////
///////////////////////////////////////////////
//////////////////////////////////////////
////////////////////////////////////////////?>











