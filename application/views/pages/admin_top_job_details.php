
<?php $is_logged_in = $this->session->userdata('is_logged_in');
       $role = $this->session->userdata('role');
       
    if(!isset($is_logged_in) || $is_logged_in != true || ($role !=="admin"))
    {
      //$this->load->view('templates/cheader');
      echo "<h3>You don't have permission to access this page.<h3>";  
      echo anchor('admin_role','Login');
      //$this->load->view('templates/footer');
      //<a href='../login'>Login</a>";
      exit();

    }?>
<script>


$(document).ready(function(){
    
  $('#admin_h_l_available_jobs').css('background','yellow');
  $('#admin_h_l_jobs').css('background','#2874a6')
 
})

$(document).ready(function()
{
   
   $('#abused').click(function(e)
   { 
      e.preventDefault();
      var user = $('#user').val();
      var job_id =$('#job_id').val() ;
      var user_c =$('#user_c').val();      
      var conf = confirm('Report Abuse?\nReport Abuse If Content Is Inappropriate.');
     
   if(conf==true)
      {
        
      $.ajax({
        url:"<?php echo base_url() . 'pages/report_abuse';?>",
        type:"POST",
        data:{user:user,job_id:job_id,user_c:user_c},
        dataType:'json',
        success:function(result)
        {     
            if(result.message)
            {
              alert(result.message);
            }
            else
            {
            alert('Abuse Reported');
          }
             
        }
     
      });}

   });

});

</script>
<style>



.form-group .form-control-static
{

  padding-left:0px;
  height:auto;
}
.control-label
{

  padding-right:0px;
}
.list-group-item
{

  background:ivory;
}
.panel .panel
{



  margin-left:5%;
  margin-right:5%;
}
hr
{



  border:1px solid lightgray;
  margin:0;
  margin-bottom:10px;
}


@media (min-width: 799px) {
   .panel .panel .information label,.panel .panel .contact label
{
  margin-left:40px;
}
   
}

</style>




<?php
$user = $this->session->userdata('username');
$job_id = $top_job['id'];
$user_c = $top_job['user_c'];



echo "<input id='user' type='hidden' name='user' value=$user>";
echo "<input id='job_id' type='hidden' value=$job_id>";
echo "<input id='user_c' type='hidden' value=$user_c>";
?>


<div class="container-fluid">

<div class="panel panel-default">
    <div class="panel-heading" style="background:#5dade2">
    <a style="" href="<?php echo base_url().'admin_role/display_jobs';?>"><img src="<?php echo base_url().'images/icons/back-arrow2.png';?>"
style="float:left;width:60px;height:40px"></a>
    <?php $f_name=$this->session->userdata('name');?>
    <h2 style="color:white;padding-right:60px;margin:0"> Welcome <?php  echo $f_name;?></h2>
    <h3 style="color:white">Top Job</h3></div>
    <div class="panel-body" style="background:ivory">
 
 <?php echo form_open('','class=form-horizontal role=form');?>
 
 <div class="panel panel-default">
 <div class="panel-heading text-center" style="background:lightblue">

    Job Information

 </div>
 <div class="panel-body">

 
<div style="text-align:right">
<button class="btn btn-primary" id="abused" name="abused"  style="font-size:18px">Report Abused</button>
</div>
     
 <div class="clearfix"></div>
 <br/>
<div class="row" style="margin-bottom:20px" >
<div class="col-sm-2 text-center" style="margin:0px;float:none;display:inline-block;vertical-align:bottom;" >
     
        <?php 
                 $logo=$top_job['logo'];         ?>
<img class="img-responsive img-thumbnail" src="<?php echo (empty($logo)?base_url().'images/icons/no-image.jpg':$logo);?>" style="width:200px;height:180px">
     (logo)  
      

      
     
   
     </div>





<div class="col-sm-5" style="margin:0px;float:none;display:inline-block;vertical-align:bottom;">
<div class="form-group form-group-lg" >

      <label class=" col-sm-4" style="">Job Id:</label>
      <div class="col-sm-8 info" style="">
        <p ><?php echo $top_job['id'];?> </p>
      </div>
    </div>
    <div class="form-group form-group-lg">
      <label class=" col-sm-4 " for="pwd">Company Name:</label>
      <div class="col-sm-8 info">          
        <p style="font-size:16px"><?php echo $top_job['c_name'];?></p>
      </div>
    </div>   
    <div class="form-group form-group-lg">
      <label class=" col-sm-4 " >Job Title:</label>
      <div class="col-sm-8 info">
        <p ><?php echo $top_job['title'];?> </p>
      </div>
    </div>
            <div class="form-group form-group-lg">
      <label class=" col-sm-4 " >Job Category:</label>
      <div class="col-sm-8 info">
        <p ><?php echo $top_job['category'];?> </p>
      </div>
    </div>






    </div>



     
    
<div class="col-sm-4" style="margin:0px;padding:0px;background:#e6f3f7;float:none;display:inline-block;vertical-align:bottom" >

 <div class="col-sm-12 text-center">
        <?php
$company_photo = $top_job['photo'];?>
<img class="img-responsive img-thumbnail" src="<?php echo (empty($company_photo)?base_url().'images/icons/no-image.jpg':$company_photo);?>" style="width:480px;height:240px">
      (company photo)
     </div>  

     </div>



     </div>
    


<hr style="border:1px solid lightgray">
    <div class="row" >
    <div class="col-sm-4">
<div class="form-group form-group-lg">
      <label class="control-label col-sm-4 " >Job Salary:</label>
      <div class="col-sm-7 info">
        <p class="form-control-static"><?php echo $top_job['salary'];?> </p>
      </div>
    </div>   
    </div> 

    <div class="col-sm-4">
<div class="form-group form-group-lg">
      <label class="control-label col-sm-4 " style="">Job Type:</label>
      <div class="col-sm-7 info" style="">
        <p class="form-control-static"><?php echo $top_job['job_type'];?> </p>
      </div>
    </div>
    </div>
    
<div class="col-sm-3">
 <div class="form-group form-group-lg">
      <label class="control-label col-sm-6 " >Published Date:</label>
      <div class="col-sm-6 info">
        <p class="form-control-static"><?php $pd=date_create($top_job['published_date']);
        echo date_format($pd,'d-M-Y');?> </p>
      </div>
    </div>
    </div>


    </div>
    <hr style="border:1px solid lightgray">
    <div class="row">
<div class="col-sm-4">
  <div class="form-group form-group-lg">
      <label class="control-label col-sm-4 " >Job Status:</label>
      <div class="col-sm-7 info">
        <p class="form-control-static"><?php echo $top_job['job_status'];?></p>
      </div>
    </div> 

    </div>
   

    
   <div class="col-sm-4">                  
    <div class="form-group form-group-lg">
      <label class="control-label col-sm-5">Apply Valid To:</label>
      <div class="col-sm-7 info">
        <p class="form-control-static"><?php $vt=date_create($top_job['valid_to']);
        echo date_format($vt,'d-M-Y');?> </p>
      </div>
    </div>
    </div>



    <div class="col-sm-4">
  <div class="form-group form-group-lg">
      <label class="control-label col-sm-5" >Job Opened Date:</label>
      <div class="col-sm-7 info">
        <p class="form-control-static"><?php $op=date_create($top_job['opened_date']);
        echo date_format($op,'d-M-Y');?> </p>
      </div>
    </div>
   
</div>
    </div>
<hr>
    







          
<div class="information">
<div class="form-group form-group-lg" style="">
      <label class=" col-sm-2" style="">Company Information:</label>
      <div class="col-sm-8 info" style="">
        <p class=""><?php echo $top_job['c_information'];?> </p>
      </div>
    </div>
   
   
    <div class="form-group form-group-lg">
      <label class="col-sm-2" for="pwd">Job Descriptions:</label>
      <div class="col-sm-8 info">          
        <p class=""><?php echo $top_job['descriptions'];?></p>
      </div>
    </div>
   
   
            <div class="form-group form-group-lg">
      <label class=" col-sm-2" >Job Responsibilities:</label>
      <div class="col-sm-8 info">
        <p class=""><?php echo $top_job['responsibilities'];?> </p>
      </div>
    </div>
    
   
      <div class="form-group form-group-lg">
      <label class="col-sm-2">Job Requirements:</label>
      <div class="col-sm-8 info">
        <p class=""><?php echo $top_job['requirements'];?> </p>
      </div>
    </div>



    </div>


 </div><!--close inner panel body-->



 </div><!--close inner panel-->



 <div class="panel panel-default">
<div class="panel-heading text-center" style="background:lightblue">

Contact Information

 </div>
<div class="panel-body contact">


<div class="row">
<div class="col-sm-6">


<div class="form-group form-group-lg">
      <label class=" col-sm-4 " >Phone:</label>
      <div class="col-sm-7 info">
        <p class=""><?php echo $top_job['phone'];?> </p>
      </div>
    </div>


 <div class="form-group form-group-lg">
      <label class=" col-sm-4 " >Email:</label>
      <div class="col-sm-7 info">
        <p class=""><?php echo $top_job['email'];?> </p>
      </div>
    </div>     
      <div class="form-group form-group-lg">
      <label class=" col-sm-4 " >Township(Address):</label>
      <div class="col-sm-7 info">
        <p class=""><?php echo $top_job['township'];?> </p>
      </div>
    </div>
     <div class="form-group form-group-lg">
      <label class=" col-sm-4 " >Country(Address):</label>
      <div class="col-sm-7 info">
        <p class=""><?php echo $top_job['country'];?> </p>
      </div>
    </div>


</div>
<div class="col-sm-6">

 <div class="form-group form-group-lg">
      <label class=" col-sm-3 " >Website:</label>
      <div class="col-sm-8 info">
        <p class=""><?php echo $top_job['website'];?> </p>
      </div>
    </div> <div class="form-group form-group-lg">
      <label class=" col-sm-3 " for="pwd">Address:</label>
      <div class="col-sm-8 info">          
        <p class=""><?php echo $top_job['address'];?></p>
      </div>
    </div>
    <div class="form-group form-group-lg">
      <label class=" col-sm-3 ">City(Address):</label>
      <div class="col-sm-8 info">
        <p class=""><?php echo $top_job['city'];?> </p>
      </div>
    </div>
</div>
</div>

 

 </div>
 </div>
</form>




  
  </div>
  </div>
</div><!--close container-->





