
<h3 style="color:blue">Total Records Found <?php echo $total_records;?></h3>
<table class="table text-center table-bordered">
    <thead class="text-center">
     <tr><input type="button" id="refresh_button" class="btn btn-primary" value="Refresh Table">
     </tr> 
      <tr class="text-center">
        <th>Order No</th>
        <th <?php if($sort_by =='category')echo "class=sort_$sort_order";?>><?php echo 
anchor("admin_role/display_categories/$q/category/". (($sort_order == 'asc' && $sort_by == 'category')?'desc'
: 'asc')."/".$limit,'Category');?></th>
      
        <th>Update</th>
        <th>Delete</th>
 
      </tr>
    </thead>
    <tbody>
    <?php $order_no=$offset+1;?>
    <?php foreach($categories as $values):?>
      <tr>
        <input type="hidden" id="<?php echo $values['category_id'];?>" value="<?php echo $values['category_id'];?>">
        <td><?php echo $order_no;?></td> 
        <td><?php echo $values['category'];?></td>
       
        
        <td>
        <?php $categories_url=$this->uri->uri_string();?>
        <input type="hidden" id="categories_url" value="<?php echo $categories_url;?>">
        <input type="button" class="btn btn-primary" id="edit" name="<?php echo $values['category_id'];?>" value="Edit"></td>
        <td>
         <?php $id=$values['category_id'];?>
          <button type='button' style="background-color:transparent" id="delete_button" name="<?php echo $id;?>" value="Delete"><img src="<?php echo base_url().'images/icons/trash.png';?>" width=35 height=35>
          </button>
        </td>
      </tr>
    <?php  $order_no++;
     endforeach;?>
    </tbody>
  </table>
  <div class="col-sm-12 text-center" >
   <?php echo $pagination;?>
   </div>