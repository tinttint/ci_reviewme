<?php

$is_logged_in = $this->session->userdata('is_logged_in');
$role=$this->session->userdata('role');
    
    if(!isset($is_logged_in) || $is_logged_in != true|| $role!=='admin')
    {
/*$this->load->view('templates/Cheader');
                $this->load->view('pages/members',$data);*/

                  // $this->load->view('templates/footer');
      echo "You must be logged in to access this page.";  
      echo anchor('admin_role','login');
             exit();
    }
   

?>
<script>

</script>
<script>


$(document).ready(function(){
    
  $('#admin_h_l_entries').css('background','yellow');
 $('#admin_h_l_jobs').css('background','#2874a6');
})




$(document).ready(function(){

    $('#jobs_div').on('change','#select_sort_by',function(e){
     e.preventDefault();
     
     var sort_by= $(this).val();
    
   
     var sort_order='asc';
    
     window.location.href="<?php echo base_url();?>"+'admin_role/display_job_entries/'+sort_by+'/'+sort_order;
  });
});


$(document).ready(function(){

    $('#jobs_div').on('change','#select_sort_order',function(e){
     e.preventDefault();
     
     var sort_by=$('#select_sort_by').val();
    
   
     var sort_order=$(this).val();
    
     window.location.href="<?php echo base_url();?>"+'admin_role/display_job_entries/'+sort_by+'/'+sort_order;
  });
});
$(document).ready(function(){
  
    $('#jobs_div').on('click','#refresh_button',function(e){
      e.preventDefault();
              window.location.href="<?php echo base_url();?>"+'admin_role/display_job_entries';
 

    });
});
$(document).ready(function(){

   $("#search_div").on('click','#search_button',function(e){
     e.preventDefault();
   
     
     var q = $('#search_div :input').serialize();
   // var q=encodeURIComponent(q);
    var q= btoa(q);
     
    
      window.location.href="<?php echo base_url();?>"+'admin_role/display_job_entries/'+q;
    
    });

});



$(document).ready(function(){

    $("#jobs_div").on('click','#open',function(e){
     e.preventDefault();  
        
       var id=this.name;
         
      var status=$(this).parent('div').find(":input:nth-of-type(2)").val();

      
       

       var specified=$(this).parent('div').find(":input:nth-of-type(3)").val();
       
       
       if(status=="opened")
       {

        alert('Can Not Open. Already Opened');
        return false;
        
       }
       if(specified=='true')
       {
          var check=confirm('Wait after one week,you maybe able to update your (city or cateogry) and open job\n'
          +'or open job now.');
       }
       if(check == false)
       {
          return;
       }
      
       var previous_url =$('#back_url').val();
     
     var answer = confirm('Open Job(s)');
    
     if(answer == false)
     {
      
      return false;
     }
   
      
       $.ajax({
               url: "<?php echo base_url() .'admin_role/open_jobs';?>",
                type:"POST", 
              //  dataType: 'json',
                data:{id:id,previous_url:previous_url},
                success: function(result){
                  $('#jobs_div').html(result);
               // window.location.href="<?php echo base_url();?>"+previous_url;
          
           }
        });    

    });

}); 




</script>


<style>

.job_info{

  max-height:55px;
  overflow:hidden;
  
}
.job_info label
{

  font-size:16px;
}
.job_info span
{
   font-size:16px;
 
}


</style>

<?php 
?>


<div class="container-fluid">
<div class="panel panel-default">
<div class="panel-heading" style="background:#5dade2">
<?php $f_name=$this->session->userdata('name');?>
<h2 style="color:white;margin:0">Welcome <?php echo $f_name;?></h2>
<h3 style="color:white">Job Entries</h3>


</div>
<div id="practice">



</div>
<div class="panel-body">
<div id="search_div" class="col-sm-12"style="margin-bottom:40px;background-color:ivory;border:1px solid blue">   
<form id="search_form" class="form-horizontal" role="form">
<br/><br/>
<div class="row">
  <div class="form-group col-sm-6 form-group-lg ">
  <label class="control-label col-sm-3" style="padding-right:0" >Job Id:</label>
  <div class="col-sm-8" style="padding-left:0">
    <?php echo form_input('job_id',(!empty($query['id'])?$query['id']:""),'class=form-control');?>
  </div>
  </div>
  <div class="form-group col-sm-6 form-group-lg center-block" >
  <label class=" control-label col-sm-4" style="padding-right:0" >Job Category:</label>
  <div class="col-sm-8" style="padding-left:0">
    <?php echo form_dropdown('category',$categories,(!empty($query['category'])?$query['category']:""),'class=form-control');?>
  </div>
  </div>
  
  </div><!--close row-->



  
  
<div class="row">
  <div class="form-group col-sm-6 form-group-lg" >  
  <label class="control-label col-sm-3" style="padding-right:0" > Job Title:</label>
  <div class="col-sm-8" style="padding-left:0">
    <?php echo form_input('title',(!empty($query['title'])?$query['title']:""),'class=form-control');?>
  </div>
  </div>
 
  


  <div class="form-group col-sm-6 form-group-lg">
  <label class="control-label col-sm-4" style="padding-right:0" > Job Salary:</label>
  <div class="col-sm-8" style="padding-left:0">
    <?php echo form_dropdown('salary',$salaries,(!empty($query['salary'])?$query['salary']:""),'class=form-control');?>
  </div>
  </div>

    </div><!--close row-->
  <div class="row">
  <div class="form-group col-sm-6 form-group-lg center-block" >
  <label class=" control-label col-sm-3" style="padding-right:0" > Job Type:</label>
  <div class="col-sm-8" style="padding-left:0">
    <?php echo form_dropdown('job_type',$job_types,(!empty($query['job_type'])?$query['job_type']:""),'class=form-control');?>
  </div>
  </div>
   <div class="form-group col-sm-6 form-group-lg center-block" >
  <label class=" control-label col-sm-4" style="padding-right:0" >Job Status:</label>
  <div class="col-sm-8" style="padding-left:0">
    <?php echo form_dropdown('job_status',array(''=>'','opened'=>'opened','closed'=>'closed'),(!empty($query['job_status'])?$query['job_status']:""),'class=form-control');?>
  </div>
  </div>
  
  
 
  </div><!--close row-->
<br/>
<div  class="col-sm-12"style="text-align:center">
<button class="btn btn-primary" id="search_button">Search</button>
<br/><br/>

</div>

</form>

</div><!--close applied search div-->

<div id="jobs_div">
<?php
$count = count($jobs);?>
<input type="hidden" id="limit" value="<?php echo $limit;?>">
<input type="hidden" id="count" value="<?php echo $count;?>">

<input type="hidden" id="q" value="<?php echo $q;?>">
<input type="hidden" id="sort_by" value="<?php echo $sort_by;?>">
<input type="hidden" id="sort_order" value="<?php echo $sort_order;?>">
<input type="hidden" id="offset" value="<?php echo $offset;?>">


<?php //echo "<h3 style='text-align:center;color:blue'>Search Results for: <span style=color:red>" .  $message. "</span></h3>";?>
<?php echo "<h3 style='text-align:center;color:blue'>Total Records Found: $total_records</h3>";?>

<div class="col-sm-11" id="refresh_div" style="margin-left:4%;">

<button class="btn btn-primary" id='refresh_button' style="">Refresh Table</button>

</div><!--close refresh_div-->


<div id="top_bar_div" class="col-sm-11"style=";padding:8px;margin-left:4%;border:1px solid blue;background:ivory">


<div class="col-sm-2" >
<label class="col-sm-12 text-center">Sort By:</label>
      <div class="col-sm-12">
      <select class="form-control" id="select_sort_by">
      <option <?php echo (($sort_by=='last_updated')?'selected':'');?> value='<?php echo "$q/last_updated";?>'>Last Updated</option>
        <option <?php echo (($sort_by=='opened_date')?'selected':'');?> value='<?php echo "$q/opened_date";?>'>Job Opened Date</option>
        <option <?php echo (($sort_by=='published_date')?'selected':'');?> value='<?php echo "$q/published_date";?>'>Published Date</option>
    
  
    <option <?php echo (($sort_by=='category')?'selected':'');?> value='<?php echo "$q/category";?>'>Job Category</option>
     <option <?php echo (($sort_by=='title')?'selected':'');?> value='<?php echo "$q/title";?>'>Job Title</option>
     
    <option <?php echo (($sort_by=='job_type')?'selected':'');?> value='<?php echo "$q/job_type";?>'>Job Type</option>
    <option <?php echo (($sort_by=='city')?'selected':'');?> value='<?php echo "$q/city";?>'>City(Address)</option>
     <option <?php echo (($sort_by=='valid_to')?'selected':'');?> value='<?php echo "$q/valid_to";?>'>Apply Valid To</option>
   
    
   </select>
      

</div>
</div>
<div class="col-sm-2">
<label class="col-sm-12 text-center">Sort Order:</label>
      <div class="col-sm-12">
      <select class="form-control" id="select_sort_order">
    <option <?php echo (($sort_order=='asc')?'selected':'');?> value="asc" selected>Ascending</option>
    <option <?php echo (($sort_order=='desc')?'selected':'');?> value="desc">Descending</option>
  </select>
</div>
</div>

<div class="col-sm-6" >
<div style="margin-left:10%;padding:4px" id="pagination">
  <?php echo $pagination;?>
  </div>
</div>
<div class="col-sm-2" style="" > 
 

 
</div>
</div><!--close top_bar_div -->



<?php foreach($jobs as $value):?>

<div id="jobs_wrapper" class="col-sm-11"style=";padding:8px;margin-left:4%;border:1px solid blue">
  
  <div class="col-sm-2 text-center" style="">
     <?php $logo=$value['logo'];?>
    <img class="img-responsive img-thumbnail" id="logo"src="<?php echo (empty($logo)?base_url().'images/icons/no-image.jpg':$logo);?>"
     style="width:200px;height:140px" >
      <p>Job Id:<?php echo $value['id'];?></p>
      </div>
   <div class="col-sm-10" id="wrapper2">   
    <div class="row">
  
  
  <div class="col-sm-6 job_info">
  <label>Company Name:</label><span><?php echo $value['c_name'];?></span>
  </div>
   
  <div class="col-sm-6 job_info" >
    <label>Job Category:</label><span><?php echo $value['category'];?></span>
  </div>
  
  </div>
  <div class="row">
  
  <div class="col-sm-6 job_info" >
  <label>Job Title:</label><span><?php echo $value['title'];?></span>
  </div>
    
  <div class="col-sm-6 job_info" >
    <label>Job Salary:</label><span><?php echo $value['salary'];?></span>
  </div>
  
  </div>
  
  <div class="row">
 
  <div class="col-sm-6 job_info" >
  <label>Job Type:</label><span><?php echo $value['job_type'];?></span>
  </div>
    
  <div class="col-sm-6 job_info" >
    <label>City(Address):</label><span><?php echo $value['city'];?></span>
  </div>
  
  </div>

<div class="row">
  <form method="get">
  <div class="col-sm-6 job_info" >
  <?php $back_url=$this->uri->uri_string();?>
  <input type="hidden" id="back_url" name="back_url" value="<?php echo $back_url;?>">


  <label>Job Opened Date:</label><span><?php 
  if($value['opened_date']=='0000-00-00')
  {
    echo '00-00-0000';
  }
  else
  {
    $op=date_create($value['opened_date']);
  
  echo date_format($op,'d-M-Y');
  }
  ?>
  </span><br/>
  <label>Job Status:</label><span><?php echo $value['job_status'];?></span>
 


  </div>
  <div class="col-sm-3 job_info" >
  <?php 
   
   $job_id=$value['id'];
   $job_status=$value['job_status'];
   //$pu=$this->uri->uri_string();


   ?>
 
    <input type="submit" class="btn btn-primary" formaction='<?php echo base_url()."admin_role/job_entries_details/$job_id";?>' id="details" name="details" value="Details/Update">
  </div>

   <div class="col-sm-3 text-center" style="overflow:initial">
   <input type="submit" class="btn btn-primary" formaction='<?php echo base_url()."admin_role/open_jobs/$job_id";?>'  name="<?php echo $job_id;?>" id="open" value="Open Job">
     <input type="hidden" name="job_status" id="job_status" value="<?php echo $job_status;?>">
  <input type="hidden" name="specified" id="specified" value="<?php echo $value['specified'];?>">
  <!--do not change the input order. Those are for jquery-->

  </div>
  </form>
  </div> 

  </div><!--close wrapper2-->
</div><!--close jobs_wrapper-->
<?php endforeach;?>
 


<div id="bottom_bar_div" class="col-sm-11"style="background:ivory;padding:8px;margin-left:4%;border:1px solid blue">
<div id="pagination" style="margin-left:40%">
<?php echo $pagination;?>
</div>
</div>

</div><!--close jobs_div-->
</div><!--close panel-body-->
</div><!--close panel-->
</div><!--close container-->
   




