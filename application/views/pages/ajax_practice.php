
<?php echo "<h3 style=text-align:center;color:blue>Total Records Found: " . $total_records . "</h3>";
echo $pagination;?>

<form method="post">

  <?php $count=count($students);?>


<div class="table-responsive">
<table class="table text-center table-bordered">
    <thead class="text-center">
     <tr><input type="button" id="refresh_button" class="btn btn-primary" value="Refresh Table">
     </tr> 
      <tr class="text-center">
       
        <th>Student Registration Number</th>
        <th <?php if($sort_by =='name')echo "class=sort_$sort_order";?>><?php echo 
anchor("admin_role/display_students/$q/name/". (($sort_order == 'asc' && $sort_by == 'name')?'desc'
: 'asc'),'Full Name');?></th>
        <th>National Registration Card No</th>
        <th <?php if($sort_by =='major')echo "class=sort_$sort_order";?>><?php echo 
anchor("admin_role/display_students/$q/major/". (($sort_order == 'asc' && $sort_by == 'major')?'desc'
: 'asc'),'Major');?></th>
        <th <?php if($sort_by =='gender')echo "class=sort_$sort_order";?>><?php echo 
anchor("admin_role/display_students/$q/gender/". (($sort_order == 'asc' && $sort_by == 'gender')?'desc'
: 'asc'),'Gender');?></th>
      
        <th <?php if($sort_by =='nationality')echo "class=sort_$sort_order";?>><?php echo 
anchor("admin_role/display_students/$q/nationality/". (($sort_order == 'asc' && $sort_by == 'nationality')?'desc'
: 'asc'),'Nationality');?></th>
        <th <?php if($sort_by =='registered_date')echo "class=sort_$sort_order";?>><?php echo 
anchor("admin_role/display_students/$q/registered_date/". (($sort_order == 'asc' && $sort_by == 'registered_date')?'desc'
: 'asc'),'Registered Date');?>
        <th>Details</th>
        <th><?php if(in_array('select_all_cb',$checked))
        {
             $found="checked";
        }
        else
        {
          $found="";
        }
        ?><input type="checkbox" <?php echo (empty($found)?'':$found);?> class="form-control" id="check_total"  name="select_all_cb" value="checkbox">
        (select/deselect all)<br/>
       <?php echo form_dropdown('drop',array('change_status'=>'change_status','Unchecked'=>'Unchecked',
'Disapproved'=>'Disapproved','Approved'=>'Approved'),"change_status"," id=drop style=height:25px;");?>
        
        </th>

      </tr>
    </thead>
    <tbody>
    <?php foreach($students as $values):?>
      <tr>
        
        <td><?php echo $values['id'];?></td>
        <td><?php echo $values['name'];?></td>
        <td><?php echo $values['nrc'];?></td>
        <td><?php echo $values['major'];?>iiiiiiiiiiiiiiiiiiii
        iiiiiiiiiiiiiii iiiiiiiiiiiiii iiiiiiiiiii iiiiiiiiiii
        iiiiiiiiiiiiiiii iiiiiiiiiii iiiiiiiiiii iiiiiiiiiiii
        iiiiiiiiiiiiii iiiiiiiiiiiii iiiiiiiiiiii iiiiiiiiii</td>
         <td><?php echo $values['gender'];?></td>
          <td><?php echo $values['nationality'];?></td>
           <td><?php $rd=date_create($values['registered_date']);
           echo date_format($rd,'d-M-Y');?>
                
                   
          
           </td>
           <td>
              <?php $user=$values['user'];
              $back_url=$this->uri->uri_string();?>
              <input type="submit" class="btn btn-primary" id="details" name="<?php echo $user;?>" formaction='<?php echo base_url()."admin_role/details_student/$user";?>' value="Details"></td>
           <input type="hidden" name="back_url" value="<?php echo $back_url;?>">

           </td>
            

            <td>
            <?php 
            $user=$values['user'];
            if(in_array($user,$checked))
            {
              $found="checked";
            }
            else
            {
              $found="";
            }
            ?>

            <?php echo $values['status'];
            $previous_url=$this->uri->uri_string();
            ?>
            <input type="hidden" id="previous_url" name="previous_url" value="<?php echo $previous_url;?>">
            <input type="checkbox" <?php echo (empty($found)?"":$found);?> class="form-control" id="<?php echo $user;?>" name="<?php echo $user;?>" value='checkbox'>
            </td>
      </tr>
     <?php endforeach;?>
    </tbody>
  </table>
  </div><!--close table div-->
  </form>

<?php echo $pagination;?>