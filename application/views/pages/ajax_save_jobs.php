<?php $previous_url=$this->uri->uri_string();?>
<input type="hidden" id="previous_url" value="<?php echo $previous_url;?>">
<input type="hidden" id="q" value="<?php echo $q;?>">
<input type="hidden" id="sort_by" value="<?php echo $sort_by;?>">
<input type= "hidden" id="sort_order" value="<?php echo $sort_order;?>">
<input type = "hidden" id="offset" value="<?php echo $offset;?>">
<input type= "hidden" id="limit" value="<?php echo $limit;?>">

<?php 
$count = count($jobs);

?>
<input type= "hidden" id="count" value="<?php echo $count;?>">
 





<?php echo "<h3 style=text-align:center;color:blue>Total Records Found: " . $total_records . "</h3>";?>








<?php echo form_open('pages/save_jobs_details','method=get id=save_jobs_form');
$back_url=$this->uri->uri_string();?>
   <input type="hidden" name="back_url" value="<?php echo $back_url;?>"> 
<div class="table-responsive">
<table class="table text-center table-bordered">
    <thead class="text-center">
     <tr><input type="button" id="refresh_button" name="refresh" class="btn btn-primary" value="Refresh Table">
     </tr> 
      <tr class="text-center">
        <th>Job Id</th>
        <th <?php if($sort_by =='c_name')echo "class=sort_$sort_order";?>><?php echo 
anchor("pages/display_save_jobs/$q/c_name/". (($sort_order == 'asc' && $sort_by == 'c_name')?'desc'
: 'asc'),'Company Name');?></th>
        <th <?php if($sort_by =='category')echo "class=sort_$sort_order";?>><?php echo 
anchor("pages/display_save_jobs/$q/category/". (($sort_order == 'asc' && $sort_by == 'category')?'desc'
: 'asc'),'Category');?></th>
        <th <?php if($sort_by =='title')echo "class=sort_$sort_order";?>><?php echo 
anchor("pages/display_save_jobs/$q/title/". (($sort_order == 'asc' && $sort_by == 'title')?'desc'
: 'asc'),'title');?></th>
        
        <th <?php if($sort_by =='job_type')echo "class=sort_$sort_order";?>><?php echo 
anchor("pages/display_save_jobs/$q/job_type/". (($sort_order == 'asc' && $sort_by == 'job_type')?'desc'
: 'asc'),'Job Type');?></th>
        <th <?php if($sort_by =='saved_date')echo "class=sort_$sort_order";?>><?php echo 
anchor("pages/display_save_jobs/$q/saved_date/". (($sort_order == 'asc' && $sort_by == 'saved_date')?'desc'
: 'asc'),'Job Saved Date');?>
        <th>Details</th>
        <th>
        <?php $found_all="";
        if(in_array('check_all',$checked))
        {
             $found_all="checked";
        }
        else
        {
          $found_all=="";
        }
        ?><input type="checkbox" <?php echo (empty($found_all)?'':$found_all);?> class="form-control" id="check_all"  name="checked[]" value="check_all">
        (select/deselect all)<br/>
        <label id="delete" class="label label-primary" style="font-size:16px">
        Delete</label></th>

      </tr>
    </thead>
    <tbody>
    <?php foreach($jobs as $values):?>
      <tr>
        <td><div><?php echo $values['id'];?></div></td>
        <td><div><?php echo $values['c_name'];?></div></td>
        <td><div><?php echo $values['category'];?></div></td>
        <td><div><?php echo $values['title'];?></div></td>
        
         <td><div><?php echo $values['job_type'];?></div></td>
          <td><div><?php $sd=date_create($values['saved_date']);
          echo date_format($sd,'d-M-Y');?></div></td>
           <td><div><?php $job_id=$values['id'];
           $save_job_id=$values['save_job_id'];?>
           
        
           <input type="submit" class="btn btn-primary" id="details" formaction='<?php echo base_url(). "pages/save_jobs_details/$save_job_id";?>' name="details" value="Details"></td>
           </div></td>
            <td><div><?php $save_job_id=$values['save_job_id'];?>
             <?php if(in_array($save_job_id,$checked))
             {
              $found="checked";
             }
             else
             {
              $found="";
             }
            ?>
            <input type="checkbox" <?php echo (empty($found)?"":$found);?> class="form-control" id="<?php echo $save_job_id;?>" name="checked[]" value="<?php echo $save_job_id;?>">
            </div></td>
      </tr>
     <?php endforeach;?>
    </tbody>
  </table>
  </div><!--close table div-->
  </form>

<?php echo $pagination;?>