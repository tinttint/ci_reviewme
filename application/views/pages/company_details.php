<?php
$is_logged_in = $this->session->userdata('is_logged_in');
$role=$this->session->userdata('role');
$user=$this->session->userdata('username');

    if(!isset($is_logged_in) || $is_logged_in != true ||($role!=='admin'))
    {
/*$this->load->view('templates/Cheader');
                $this->load->view('pages/members',$data);*/

                  // $this->load->view('templates/footer');
      echo "You don't have permission to access this page.";  
          echo anchor('admin_role','Login');       
      exit();

    }?>


<script>


$(document).ready(function(){
    
 $('#admin_h_l_companies').css('background','yellow');
  $('#admin_h_l_profile').css('background','#2874a6')
 
})

$(document).ready(function(){
     $('#status').on('change','#drop',function(e){
      e.preventDefault();
       
           var user=$(this).attr('name');
           var url=$('#url').val();
          var status = $(this).val();
             if(status=="Approved")
             {
              
                result = confirm("Approve Company(s)?\n\n Approval confirmation email(s) will be sent to the student(s)");
             }
             if(status=='New')
             {
                 result=confirm("Change Status?");
             }
              if(status =="Disapproved")
             {
                       $('#myModal').modal();
                //  $('#message_modal').val('');
           
                          $('#user_modal').val(user);
                          var c_name=$('#c_name').val();
                          $('#c_name_modal').val(c_name);
                         // var message="something";
                          var message= "You have been disapproved from Job Career Development Site as the contents you"
                          +" posted are not appropriate.";
                        
                        $('#message_modal').val(message);
                          $('#user_modal').val(user);
                  $('#drop').val('Disapproved');
                  return false;
             }
             
             
             if(result ==false)
             {
              $('#drop').val('Change_Status');
              
              return false;
             }
              
                $.ajax({
                  url:"<?php echo base_url() .'admin_role/status_company';?>",
                  type:"POST",
                  beforeSend:function(){
                    $('#progress').show();
                  }, 
                  
                  data:{status:status,user:user,url:url},
                  dataType:'json',
                  success:function(result)
                  {
                     
                       if(result.message=="false")
                       {
                         $('#progress').hide();
                         $('#drop').val('Change_Status');
                         alert("Can Not Change Status. Already "+ status);

                        

                       }
                         else if(result.message=='email_error')
                       {
                          $('#progress').hide();
                        alert('Can Not Change Status.Email Error.'
                          +'\nCheck Internet Connection');
                       }
                       else if(result.message=='true')
                       {
                        $('#progress').hide();
                        if(status=="Approved"||status=="Disapproved")
                        {
                        alert('Email Sent');
                        }
                        $('#drop').val('Change_Status');
                        window.location.href="<?php echo base_url();?>"+url;


                  }
                  else
                  {
                          window.location.href="<?php echo base_url();?>"+url;

                  }
                       

                  }
               

                });

     });

});

$(document).ready(function(){

$('#send_modal').click(function(e){
  e.preventDefault();
  var answer=confirm("send email?");
  if(answer==false)
  {
    return;
  }

     
    var message=$('#message_modal').val();

   
    var user=$('#user_modal').val();
    var address=$('#url').val();
    var status="Disapproved";

   
    

         $('#progress').css('display','block');
    
  //  var id=$('#id_modal').val();
   //var category="sport";
  
       $.ajax({

                  url:"<?php echo base_url() .'admin_role/status_company';?>",
                  type:"POST",
                  
                  data:{message:message,user:user,status:status,url:address},
                   dataType:"json",
                  success:function(result)
                  { 
                  
                       
                         if(result.message=="false")
                       {
                         $('#progress').css('display','none');
                         alert("Can Not Change Status. Already "+ status);
                         $('#myModal').modal('hide');
                        

                       }
                           else if(result.message=='email_error')
                       {
                          $('#progress').hide();
                        alert('Can Not Change Status.Email Error.'
                          +'\nCheck Internet Connection');
                          $('#myModal').modal('hide');
                       }
                       else if(result.message=="true")
                       {
                        $('#progress').css('display','none');
                        alert('Disapproved Email Sent');
                         $('#drop').val('Change_Status');
                         window.location.href="<?php echo base_url();?>"+address;
                       }
                       else
                       {
                          window.location.href="<?php echo base_url();?>"+address;

                       }

                  }
                });

});


});

$(document).ready(function(){

  $('#cancel_modal').click(function(){
    $('#drop').val('Change_Status');
   // $('#progress').css('display','none');
   
  })
})


</script>
    <style>


.control-label
{
  padding-right:0px;
}
.form-group .form-control-static
{
  padding-left:0px;
  height:auto;
}
#drop option[selected]
{
  display:none;
}
#drop
{
  color:red;
  font-size:16px;
}

.progress{
height:70px;


width:50%;
position:fixed;
top:60%;
left:25%;
z-index:1;
pointer-events:none;


}

.progress-bar {
  padding:15px;
}

    </style>



<div class="progress" id="progress" style="display:none">
 
  <div  class="progress-bar progress-bar-striped active" role="progressbar"
  aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="font-size:24px;width:100%">
    Please Wait
  </div>
  
</div>

<div id="myModal" class="modal fade" role="dialog" style="">
  <div class="modal-dialog" style="">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header text-center">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Disapprove Company</h4>
        <p>(Add your own message or send this default message)</p>
        <div class="col-sm-12 text-center">
        <div class="col-sm-6" style="">

        Username:<br/>
        <input type="text" name="user_modal" id="user_modal" value="" style="padding-left:5px" readonly>
        </div>
        <div class="col-sm-6" style="">
        Company Name:<input type="text" name="c_name_modal" id="c_name_modal" value="" style="padding-left:5px" readonly>
        </div>
        </div>

      </div><!--close modal-header-->
      <div class="modal-body">
         <div class="form-group">
         <input type="hidden" name="user_modal" id="user_modal" value="">
       
      
        <textarea name="message_modal" rows="5" id="message_modal" class="form-control">
           
        </textarea>
      
       
        </div>
      </div>
      <div class="modal-footer">
       <input type="submit" class="btn btn-default" value="Send"  name="send_modal" id="send_modal">
        <button type="button" class="btn btn-default" name="cancel_modal" id="cancel_modal" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>


<div class="container-fluid">

<div class="panel panel-default">
    <div class="panel-heading" style="background:#5dade2">
    <?php $details_url=(empty($details_url)?base_url().'admin_role/display_companies':base_url(). $details_url);?>
<a href="<?php echo $details_url;?>"><img src="<?php echo base_url().'images/icons/back-arrow2.png';?>" style="float:left;width:60px;height:40px"></a>
    <?php $f_name=$this->session->userdata('name');?>
    <h2 style="color:white;padding-right:60px;margin:0"> Welcome <?php  echo $f_name;?></h2>
    <h3 style="color:white">Company Account</h3>
    </div>
    <div class="panel-body" style="background:ivory">
  
 <?php echo form_open('pages/update_company','class=form-horizontal role=form');?>
       <div id="status" class="text-center">
              <label>Status:</label><span style="font-size:16px"><?php echo $account[0]['status']."<br>";?></span>
              <?php 
              $url=$this->uri->uri_string();

              $user_c=$account[0]['username'];echo form_dropdown($user_c,array('Change_Status'=>'Change Status','New'=>'New','Disapproved'=>'Disapproved','Approved'=>'Approved'),'Change_Status',"style=height:34px;width:140px
     id=drop ");?>
     <input type="hidden" name="url" id="url" value="<?php echo $url;?>">
     <input type="hidden" name="c_name" id="c_name" value="<?php echo $account[0]['c_name'];?>">
     </div>
     <br/><br/>
   <div class="panel panel-default inner" >
<div class="panel-heading text-center" style="background:lightblue">


Company Information

</div>

<div class="panel-body">

 

<div class=" form-group form-group-lg  text-left"   >
       
        <?php $user_c = $account[0]['username'];
$logo=  $this->news_model->get_logo($user_c);?>
<img class="img-responsive img-thumbnail" src="<?php echo (empty($logo)?base_url().'images/icons/no-image.jpg':$logo);?>" style="width:200px;height:180px">
     <br/>(logo)
      
      </div>

 
<div class="row">
<div class="col-sm-7">

     
<div class="form-group form-group-lg"   >
      <label class="control-label col-sm-5" style="">Username:</label>
      <div class="col-sm-7" style="">
        <p class="form-control-static"><?php echo $account[0]['username'];?> </p>
      </div>
    </div>
<div class="form-group form-group-lg">
      <label class="control-label col-sm-5" for="pwd">Full Name:</label>
      <div class="col-sm-7">          
        <p class="form-control-static"><?php echo $account[0]['name'];?></p>
      </div>
    </div>
             <div class="form-group form-group-lg">
      <label class="control-label col-sm-5" >Company Name:</label>
      <div class="col-sm-7">
        <p class="form-control-static"><?php echo $account[0]['c_name'];?></p>
      </div>
    </div>
     <div class="form-group form-group-lg">
      <label class="control-label col-sm-5" >Company Registration Number:</label>
      <div class="col-sm-7">
        <p class="form-control-static"><?php echo $account[0]['crn'];?></p>
      </div>
    </div>
     <div class="form-group form-group-lg">
      <label class="control-label col-sm-5" >Business Type:</label>
      <div class="col-sm-7">
        <p class="form-control-static"><?php echo $account[0]['b_type'];?> </p>
      </div>
    </div>
    <div class="form-group form-group-lg">
      <label class="control-label col-sm-4 col-sm-offset-1" >Registered Date:</label>
      <div class="col-sm-7">
        <p class="form-control-static"><?php $rd=date_create($account[0]['registered_date']);
        echo date_format($rd,'d-M-Y');?> </p>
      </div>
    </div>
    
      
    </div>
    
   





<div class="col-sm-5">     
   
    <!-- 
     <div class=" form-group form-group-lg  text-left"  >
       
        <?php $user_c = $account[0]['username'];
$logo=  $this->news_model->get_logo($user_c);?>
<img class="img-responsive img-thumbnail" src="<?php echo (empty($logo)?base_url().'images/icons/no-image.jpg':$logo);?>" style="width:200px;height:180px">
     <br/>(logo)
      
      </div>
               -->
   

    
      <div class="form-group form-group-lg  cols-sm-8 text-left">

        <?php $user_c = $account[0]['username'];
$company_photo = $this->news_model->get_company_photo($user_c);?>
<img class="img-responsive img-thumbnail" src="<?php echo (empty($company_photo)?base_url().'images/icons/no-image.jpg':$company_photo);?>" style="width:480px;height:240px">
      <br/> (company photo)
      </div> 
   


    </div>
    </div>




 <div class="form-group form-group-lg">
      <label class="control-label col-sm-3" >Company Information:</label>
      <div class="col-sm-9">
        <p class="form-control-static" style=""><?php echo $account[0]['c_information'];?>  </p>
      </div>
    </div>  

</div><!--close inner panel body-->


</div><!--close inner panel-->


<div class="panel panel-default inner">
<div class="panel-heading text-center" style="background:lightblue">


Contact Information

</div>
<div class="panel-body">

<div class="row">
<div class="col-sm-6">
  <div class="form-group form-group-lg">
      <label class="control-label col-sm-4 col-sm-offset-1" >Phone:</label>
      <div class="col-sm-7">
        <p class="form-control-static"><?php echo $account[0]['phone'];?> </p>
      </div>
    </div>
 

    <div class="form-group form-group-lg">
      <label class="control-label col-sm-4 col-sm-offset-1" >Website:</label>
      <div class="col-sm-7">
        <p class="form-control-static"><?php echo $account[0]['website'];?> </p>
      </div>
    </div>
<div class="form-group form-group-lg">
      <label class="control-label col-sm-4 col-sm-offset-1" >Township(Address):</label>
      <div class="col-sm-7">
        <p class="form-control-static"><?php echo $account[0]['township'];?> </p>
      </div>
    </div>
 <div class="form-group form-group-lg">
      <label class="control-label col-sm-4 col-sm-offset-1" >Country(Address):</label>
      <div class="col-sm-7">
        <p class="form-control-static"><?php echo $account[0]['country'];?> </p>
      </div>
    </div>


   </div>


   
    <div class="col-sm-6">

<div class="form-group form-group-lg">
      <label class="control-label col-sm-4 col-sm-offset-1" >Email:</label>
      <div class="col-sm-7">
        <p class="form-control-static"><?php echo $account[0]['email'];?> </p>
      </div>
    </div>


   

  
    <div class="form-group form-group-lg">
      <label class="control-label col-sm-4 col-sm-offset-1" >Address:</label>
      <div class="col-sm-7">
        <p class="form-control-static"><?php echo $account[0]['address'];?> </p>
      </div>
</div>


<div class="form-group form-group-lg">
      <label class="control-label col-sm-4 col-sm-offset-1" >City(Address):</label>
      <div class="col-sm-7">
        <p class="form-control-static"><?php echo $account[0]['city'];?> </p>
      </div>
    </div>

</div>
</div>




</div>
</div>

  
  </form>
  </div><!-- close panel body-->
  </div><!--close panel-->
</div><!-- close container-->







