<h3 style="color:blue">Total Records Found <?php echo $total_rows;?></h3>
<table class="table text-center table-bordered">
    <thead class="text-center">
     <tr><input type="button" id="refresh_button" class="btn btn-primary" value="Refresh Table">
     </tr> 
      <tr class="text-center">
        <th>Order No</th>
        <th <?php if($sort_by =='cities')echo "class=sort_$sort_order";?>><?php echo 
anchor("admin_role/display_cities/$q/cities/". (($sort_order == 'asc' && $sort_by == 'cities')?'desc'
: 'asc')."/".$limit,'City');?></th>
      
        <th>Update</th>
        <th>Delete</th>
 
      </tr>
    </thead>
    <tbody>
    <?php $order_no=$offset+1;?>
    <?php foreach($cities as $values):?>
      <tr>
        <input type="hidden" id="<?php echo $values['id'];?>" value="<?php echo $values['id'];?>">
        <td><?php echo $order_no;?></td> 
        <td><?php echo $values['cities'];?></td>
       
        
        <td>
        <?php $cities_url=$this->uri->uri_string();?>
        <input type="hidden" id="cities_url" value="<?php echo $cities_url;?>">
        <input type="button" class="btn btn-primary" formaction="<?php echo $values['cities'];?>"
        id="edit" name="<?php echo $values['id'];?>" value="Edit"></td>
        <td>
         <?php $id=$values['id'];?>
          <button style="background-color:transparent" type='button' id="delete_button" name="<?php echo $id;?>" value="Delete"><img src="<?php echo base_url().'images/icons/trash.png';?>" width=35 height=35>
          </button>
        </td>
      </tr>
    <?php  $order_no++;
     endforeach;?>
    </tbody>
  </table>
  <div class="col-sm-12 text-center">
   <?php echo $pagination;?>
   </div>