<?php
 

$is_logged_in = $this->session->userdata('is_logged_in');
$role=$this->session->userdata('role');
		
		if(!isset($is_logged_in) || $is_logged_in != true || $role!=='student')
		{
/*$this->load->view('templates/Cheader');
                $this->load->view('pages/members',$data);*/

                  // $this->load->view('templates/footer');
			echo "You don't have permission to access this page.";  
			echo anchor('login','Login');
			exit();

		}
		?>
    <script>


$(document).ready(function(){
    
  $('#header_l_profile').css('background',"#2874a6");
  $('#header_l_account').css('background',"yellow");
})

$(document).ready(function(){

    $('#photo_div').on('click','#upload_button',function(e){
      e.preventDefault();
     //  $('#photo_form').submit(function(){


       

   
    
    var formData = new FormData($('#update_form')[0]);
$.ajax({
       url:"<?php echo base_url(). 'pages/update_student_photo';?>",
       data: formData,
       async: false,
       contentType: false,
       processData: false,
       cache: false,
       type: 'POST',
       success: function(data)
       {
            
           $('#photo_div').html(data);
       },
    // })//return false;    
        });
    });

});


    </script>

<script>






$(document).ready(function(){
     $('#photo_div').on('click','#delete_photo',function(){
        var b =  $('#photo_server_path').val();
        var existing_path = $('#existing_server_path').val();
       
        $.ajax({
        	url:"<?php echo base_url() . 'pages/delete_u_student_photo';?>",
        	type: "POST",
        	data:{server_path:b,existing_path:existing_path},
            success:function(result)
           {
            
                 $('#photo').attr('src',"<?php echo base_url() . 'images/icons/Blank-photo.png';?>");
                 $('#delete_photo').css('visibility','hidden');
                
                 $('#photo_path').attr('value',"");
                 $('#photo_server_path').attr('value',"");
                 $('#previous_s_path').attr('value',"");
                

            }


        });

    });

})



</script>

<style>
.panel .panel-body input:focus
{
     border-color:blue;


  }
  .panel .panel-body input[type=text]
     {
        border-style:solid;
       /* background:transparent;*/
         
      border-width:0px 0px 1px 0px;
     
     border-color:gray;
      box-shadow:none;
      border-radius:0px;
     }
     .form-horizontal .form-group .form-control 
     {
       
      padding-bottom:0px;
      padding-top:16px;
      
      
      font-size:20px;
     }
     .form-horizontal .form-group .control-label
     {

      padding-top:18px;
      width:auto;
    /*  padding-left:8px;*/
     }
     .panel .panel
     {
         margin-left:5%;
         margin-right:5%;
         border-color:lightgray;

     }
      .form-horizontal .form-group select
      {

       /* padding-top:18px;*/
        text-align:center;
      }



</style>


<?php 

?>

<?php $user = $this->session->userdata('username');?>
<div id="practice">



</div>


<div class="container-fluid">
<div class="panel panel-default">
<div class="panel-heading" style="background:#5dade2">
<a  href=<?php echo base_url() . 'login/is_logged_in';?>><img src="<?php echo base_url().'images/icons/back-arrow2.png';?>" style="float:left;width:60px;height:40px;background:transparent">
</a>

<?php $name=$this->session->userdata('name');?>

<h2 style="color:white;padding-right:60px;margin:0">Welcome <?php echo $name;?></h2>
<h3 style="color:white">Update Account</h3>
 
</div>
<div class="panel-body" style="background:">
<h3 class="text-right"><?php echo anchor('pages/change_password','Change Password');?></h3>
<p style="color:red;text-align:center">* means required</p>
<h3 style="text-align:center;color:red"><?php echo (empty($message)?'':$message);?></h3> 
<h4 style="color:red"><?php echo (empty($message2)?"":$message2);?></h4>




    
<?php $ask= array('onsubmit'=>"return confirm('Update?')",'class'=>'form-horizontal'
,'role'=>'form','id'=>'update_form');?>

<?php echo form_open_multipart('pages/update_s',$ask);?>
 <div class="panel panel-default">
 <div class="panel-heading text-center" style="background:lightblue">

Personal Information

 </div>
 <div class="panel-body">
 <div class="form-group form-group-lg">
<label class="col-sm-1 control-label"  >* Username:</label>
<div class="col-sm-4">

<?php echo form_input('user',(empty($account)?set_value('user'):$account[0]['user']),'class=form-control');?>
 <?php echo form_error('user');?>
</div>

</div>
  
<div class="form-group form-group-lg ">
  <label class="control-label col-sm-1" >* Full Name:</label>
  <div class='col-sm-4'>
  <?php echo form_input('name',(empty($account)?set_value('name'):$account[0]['name']),'class=form-control');?>
  <?php echo form_error('name')?>
  </div>
  
  

    
  
  <label class="control-label col-sm-3" >* Student Registration Number:</label>
  <div class="col-sm-4">
    <?php echo form_input('id',(empty($account)?set_value('id'):$account[0]['id']),'class=form-control');?>
    <?php echo form_error('id');?>
    </div>
    
    </div>
  
    
<div class="form-group form-group-lg ">
    <label class="control-label col-sm-3"> * National Registration Card Number:</label>
   <div class="col-sm-4"> 
    <?php echo form_input('nrc',(empty($account)?set_value('nrc'):$account[0]['nrc']),'class=form-control');?>
     <?php echo form_error('nrc');?>
    </div>
  
    <label class="control-label col-sm-1 ">* Major:</label>
    <div class="col-sm-4">
    <?php echo form_input('major',(empty($account)?set_value('major'):$account[0]['major']),'class=form-control');?>
     <?php echo form_error('major');?>
    </div>
   
    </div>
    
 <br/>


   <div class="form-group form-group-lg ">
  <label class="control-label col-sm-3">* Qualifications/Degrees:</label>
  <div class="col-sm-8">
  <?php echo form_textarea('qualifications',(empty($account)?set_value('qualifications'):$account[0]['qualifications']),'id=qualifications class=form-control');?>
  <?php echo form_error('qualifications');?>
  </div> 
  </div>
  <br/>
    <div class="form-group form-group-lg ">
    <label class="control-label col-sm-2">* Gender:</label>
    <div class="col-sm-4">
    <?php $option=array(''=>'','female'=>'female','male'=>'male');
    echo form_dropdown('gender',$option,(empty($account)?set_value('gender'):$account[0]['gender']),'class=form-control');?>
     <?php echo form_error('gender');?>
    </div>
   
      <?php if(!empty($account))
{
    $date_o_b=$account[0]['date_o_b'];
    $date_o_b=explode('-',$date_o_b);
    $m_array=array('01'=>'January','02'=>'February','03'=>'March','04'=>'April','05'=>'May','06'=>'June',
  '07'=>'July','08'=>'August','09'=>'September','10'=>'October','11'=>'November','12'=>'December');
    $month=$m_array[$date_o_b[1]];
}
?>
    <label class="control-label col-sm-2">* Date Of Birth:</label>
    <div class="col-sm-4" id="date_o_b" >
    <?php $b_day = array('name'=>'b_day','id'=>'b_day');?>
    <div style="display:inline-block;vertical-align:top;margin-top:-15px;">
    <span style="width:70px;display:block;margin-bottom:0px">Day</span>
    <?php
    echo form_dropdown('b_day',$days,(empty($account)?set_value('b_day'):$date_o_b[2]),'style=width:70px;height:40px;');?>
   </div>
   <div style="display:inline-block;vertical-align:top;margin-top:-15px">
   <span style="display:block">Month</span>
   <?php echo form_dropdown('b_month',$months,(empty($month)?set_value('b_month'):$month),'style=width:140px;height:40px');?>
   </div>

    
     
     
  
   <div style="display:inline-block;vertical-align:top;margin-top:-15px">
   <span style="display:block">Year</span>
   <?php echo form_input('b_year',(empty($account)?set_value('b_year'):$date_o_b[0]),'style=border-width:1px;width:70px;height:40px;font-size:18px;text-align:center placeholder=yyyy id=b_year');?>
                    
   
   </div>
   <br/>
   <?php echo form_error('b_day');?>
    </div>
    </div>
   
    <div class="form-group form-group-lg ">
  <label class="control-label col-sm-2">* Nationality:</label>
  <div class="col-sm-4">
  <?php echo form_input('nationality',(empty($account)?set_value('nationality'):$account[0]['nationality']),'class=form-control');?>
   <?php  echo form_error('nationality');?>
   </div>
  </div>



  <input type="hidden" id="existing_server_path" name="existing_server_path" value="<?php echo(!isset($account)?$result['existing_server_path']:$account[0]['photo_server_path']);?>">
<div id="photo_div" class="form-group form-group-lg">
<br/>
    <label class="control-label col-sm-3">* My Photo:</label>

<input type="hidden" id="photo_path" name="photo_path" value="<?php echo(!isset($account)?$result['photo_path']:$account[0]['photo']);?>">
<input type="hidden" id="photo_server_path" name="photo_server_path" value="<?php echo (!isset($account[0]['photo_server_path'])?$result['photo_server_path']:$account[0]['photo_server_path']);?>">
<input type="hidden" id ="previous_s_path" name="previous_s_path" value="<?php echo (!isset($result['photo_server_path'])?"":$result['photo_server_path']);?>">

<div class="col-sm-8">
  <?php $result['photo_path']=(!isset($account[0]['photo'])?$result['photo_path']:$account[0]['photo']);?>



  <img id="photo" class="img-thumbnail" src="<?php echo (empty($result['photo_path'])?base_url() .'images/icons/Blank-photo.png':$result['photo_path']);?>" style="width:200px;height:180px">


 
<?php echo form_error('photo_path');?>

<?php
  $delete_photo_path =  base_url() . 'images/icons/delete.jpg';
 echo (!empty($account[0]['photo']) ||(!empty($result['photo_path']))? "<img id=delete_photo src=$delete_photo_path width=50 height=50>":"");?>
</div>

<div class="col-sm-9 col-sm-offset-1"  >
<br/>
      <input type="file" name="userfile" class="">
</div>

<div class="col-sm-9 col-sm-offset-1" style="margin-top:1%">
<br/>
      <input type="button" class="btn btn-primary" id ="upload_button" name="upload_button" value='Upload Photo'>
      <?php echo (empty($result['error'])?"":$result['error']);?>
      <br/> <span style="font-size:14px">(max-upload-size:100kb)
      <br/><a href="<?php echo base_url(). 'files/reduce_size.docx';?>">How To Reduce Image Size</a></span>
       
</div>
</div><!--close photo_div-->
</div><!--close inner panel body-->
  </div><!--close inner panel-->
   
  <div class="panel panel-default">
  <div class="panel-heading text-center" style="background:lightblue">
   Contact Information


  </div>
  <div class="panel-body">                                                   
  <div class="form-group form-group-lg">                          
  <label class="control-label col-sm-4">* Phone:</label>
  <div class="col-sm-8">
  <?php echo form_input('phone',(empty($account)?set_value('phone'):$account[0]['phone']),'class=form-control');?>
  <?php echo form_error('phone');?>
  </div>
  
  </div> 
  <div class="row">
 <div class="form-group form-group-lg col-sm-6">
  <label class="control-label col-sm-2 ">* Email: </label>
  <div class="col-sm-10">
    <?php echo form_input('email',(empty($account)?set_value('email'):$account[0]['email']),'class=form-control');?>
    <?php echo form_error('email');?>
    </div>
    </div>
    
   
          
      <div class="form-group form-group-lg col-sm-6">                             
    <label class="control-label col-sm-3">* Confirm Email: </label>
    <div class="col-sm-9">
    <?php echo form_input('c_email',(empty($account)?set_value('c_email'):$account[0]['email']),'class=form-control');?>
    <?php  echo form_error('c_email');?>
    </div>
    
    </div>
    </div><!--close row-->
<div class="row">
<div class="form-group form-group-lg col-sm-6">
  <label class="control-label col-sm-4">* Address:</label>
  <div class="col-sm-8">
    <?php echo form_input('address',(empty($account)?set_value('address'):$account[0]['address']),'class=form-control');?>
     <?php echo form_error('address');?>
    </div>
    </div>
   
   

  <div class="form-group form-group-lg col-sm-6">
  <label class="control-label col-sm-4">* Township(Address):</label>
  <div class="col-sm-8">
  <?php echo form_input('township',(empty($account)?set_value('township'):$account[0]['township']),'class=form-control');?>
  <?php echo form_error('township');?>
  </div>
  
  </div>
  </div><!--close row-->
  <div class="row">
<div class="form-group form-group-lg col-sm-6">
  <label class="control-label col-sm-4">* City(Address):</label>
  <div class="col-sm-8">
  <?php echo form_input('city',(empty($account)?set_value('city'):$account[0]['city']),'class=form-control');?>
  <?php echo form_error('city');?>
  </div>
  </div>

  

      <div class="form-group form-group-lg col-sm-6">                                                            
  <label class="control-label col-sm-4">* Country(Address):</label>
  <div class="col-sm-8">
  <?php echo form_input('country',(empty($account)?set_value('country'):$account[0]['country']),'class=form-control');?>
   <?php echo form_error('country');?>
  </div>
  </div>
  </div><!--close row-->
  </div><!--close inner panel body-->
  </div><!--close inner panel-->
  <div class="panel panel-default">
  <div class="panel-heading text-center" style="background:lightblue" >

  Work Information

  </div>
  <div class="panel-body">

<div class="form-group form-group-lg ">
  <label class="control-label col-sm-3 col-sm-offset-1"> Work Experience:</label>
  <div class="col-sm-8">
  <?php echo form_textarea('experience',(empty($account)?set_value('experience'):$account[0]['experience']),' class=form-control');?>
   <?php echo form_error('experience');?>
  </div>
 
  </div>

  </div><!--close inner panel body-->
  </div><!--close inner panel-->


<br/><br/>

  
  
 
  
    <div class="text-center" style="text-align:center">
    
  <input type="submit" class="btn btn-primary" id="update" name="update" value="Update Account">

  </div>
  
     <?php //echo form_close();?>
  
     </form>
</div><!--close panel body-->
</div><!--close panel-->     
</div><!--close container-->
