

<?php $is_logged_in = $this->session->userdata('is_logged_in');
     $role=$this->session->userdata('role');
    if(!isset($is_logged_in) || $is_logged_in != true||$role!=="company")
    {
/*$this->load->view('templates/Cheader');
                $this->load->view('pages/members',$data);*/

                  // $this->load->view('templates/footer');
      echo "You don't have permission to access this page.";
     
       
        
         
       
      echo anchor('login','Login');
    
      exit();

    }
    ?>



<style>



#vf_day,#vf_month,#vf_year,#vt_day,#vt_month,#vt_year
{

  text-align:center;
}
span
{

  color:red;
}




</style>
<script>


$(document).ready(function(){
    
  $('#header_l_j_registration').css('background','#2874a6');
  
})



$(document).ready(function(){
   $('#panel').on('change','#category',function(){
         var selected=this.value;
       
         if(selected=="Other(Specify)")
         {

          $('#s_category_div').css('display','block');
         }
         else
         {
          $('#s_category').val('');
          $('#s_category_div').css('display','none');
         }

   })

    $('#panel' ).on('change','#city',function(){
         var selected=this.value;
         
         if(selected=="Other(Specify)")
         {

          $('#s_city_div').css('display','block');
         }
         else
         {
          $('#s_city').val('');
          $('#s_city_div').css('display','none');
         }

   })


})
$(document).ready(function(){
  var category =$('#category').val();
  if(category=="Other(Specify)")
  {

    $('#s_category_div').css('display','block');
  }
  var city=$('#city').val();
  if(city=="Other(Specify)")
        {

          $('#s_city_div').css('display','block');


        }


})

</script>

<script>





</script>
<style>

.panel .panel-body input:focus
{
     border-color:blue;


  }
  .panel .panel-body input[type=text]
     {
        border-style:solid;
       /* background:transparent;*/
         
      border-width:0px 0px 1px 0px;
     
     border-color:gray;
      box-shadow:none;
      border-radius:0px;
     }
     .form-horizontal .form-group .form-control 
     {
       
      padding-bottom:0px;
      padding-top:16px;
      
      
      font-size:20px;
     }
     .form-horizontal .form-group .control-label
     {

      padding-top:18px;
      width:auto;
    /*  padding-left:8px;*/
     }
     .panel .panel
     {
         margin-left:5%;
         margin-right:5%;
         border-color:lightgray;

     }
      .form-horizontal .form-group select
      {

       /* padding-top:18px;*/

        text-align:center;
      }
      .valid-to span
      {

        color:black;
      }


</style>
<?php


 


 $user = $this->session->userdata('username');

?>
<div class="container-fluid">
<div class="panel panel-default">
<div class="panel-heading" style="background:#5dade2">
<?php $f_name=$this->session->userdata('name');?>
<h2 style="color:white;margin:0">Welcome <?php echo $f_name;?></h2>
<h3 style="color:white">Create Job</h3>



</div>
<div class="panel-body" style="background:" id="panel" >
<?php
$ask=array('onsubmit'=>"return confirm('Create Job?')",'class'=>'form-horizontal','role'=>'form'
  ,'id'=>'update_form'); 


echo form_open_multipart('pages/create_job',$ask);?>



<?php $photo=$this->news_model->get_company_photo($user);?>
    
<div class="form-group form-group-lg">
<div class="col-sm-2 text-center">
<?php $logo=$this->news_model->get_logo($user);?>
<img class="img-thumbnail" style="width:200px;height:150px"src="<?php echo(empty($logo)?base_url().'images/icons/no-image.jpg':$logo);?>">
<p>(Edit logo in account page.)</p>
</div>
</div>
<div>
<img class="img-responsive img-thumbnail center-block " src="<?php echo (empty($photo)?base_url().'images/icons/no-image.jpg':$photo);?>" style="width:500px;height:250px">
<p style="text-align:center">(Edit company photo in account page.)</p>
</div>
  
  <br/>
  <div class="panel panel-default">
  <div class="panel-heading text-center" style="background:lightblue">

     Job Information

  </div>
  <div class="panel-body">
  <div class="form-group form-group-lg" >
  <h3 style="color:red"><?php echo (empty($message)?'':$message);?></h3>
  <h4 style="color:red"><?php echo (empty($message2)?'':$message2);?></h4>
  
  <?php $company_name=$this->news_model->get_company($user);
  $company_name=$company_name[0]['c_name'];?>
  <label class="control-label col-sm-2" >* Company Name:</label>
  <div class="col-sm-5" style="">
  <?php echo form_input('c_name',set_value('c_name',$company_name),'class=form-control');?>
 
  <?php echo form_error('c_name');?>
  </div>
  </div>
  
 <div class="form-group form-group-lg">
 <br/>
  <label class="control-label col-sm-2" >* Job Category:</label>
  <div class='col-sm-3'>
  <?php echo form_dropdown('category',$categories,set_value('category'),'class=form-control id=category');?>
 
  <?php echo form_error('category')?>
  </div>
    
    <div id="s_category_div" style="display:none;">
  <label class="control-label col-sm-2" style="">* Specify Category:</label>
  <div class='col-sm-5'>
  <?php echo form_input('s_category',set_value('s_category'),'class=form-control id=s_category');?>
  <?php echo "If validated,you can update to this category after 1 week";?><br/>
  <?php echo form_error('s_category')?>
   </div>
   </div>
  
      

  </div>
                                         
<div class="form-group form-group-lg ">

  <label class="control-label col-sm-2" >* Job Title:</label>
  <div class="col-sm-8">
  <?php echo form_input('title',set_value('title'),'class=form-control');?>
 
  <?php echo form_error('title')?>
  </div>
  </div>
  <div class="form-group form-group-lg ">
  <br/>
  <label class="control-label col-sm-2" >* Salary:</label>
  <div class="col-sm-4">
  <?php echo form_dropdown('salary',$salaries,set_value('salary'),'class=form-control');?>
  
  <?php echo form_error('salary');?>
  </div>

  <label class="control-label col-sm-2" >* Job Type:</label>
  <div class="col-sm-4">
  <?php echo form_dropdown('job_type',$job_types,set_value('job_type'),'class=form-control');?>
  
    <?php  echo form_error('job_type');?>
    </div>
    </div>

<div class="form-group form-group-lg valid-to ">
<br/>
  <label class="control-label col-sm-2" >* Apply Valid To:</label>
  <div class="col-sm-6">
   <?php $b_day = array('name'=>'b_day','id'=>'b_day');?>
    <div style="display:inline-block;vertical-align:top;margin-top:-15px;">
    <span style="width:70px;display:block;margin-bottom:0px">Day</span>
    <?php
    echo form_dropdown('vt_day',$days,set_value('vt_day'),'style=width:70px;height:40px;');?>
   </div>
   <div style="display:inline-block;vertical-align:top;margin-top:-15px">
   <span style="display:block">Month</span>
   <?php echo form_dropdown('vt_month',$months,set_value('vt_month'),'style=width:140px;height:40px');?>
   </div>

    
     
     
   
   <div style="display:inline-block;vertical-align:top;margin-top:-15px">
   <span style="display:block">Year</span>
   <?php echo form_input('vt_year',set_value('vt_year'),'style=border-width:1px;width:70px;height:40px;font-size:18px;text-align:center placeholder=yyyy id=b_year');?>
                    
   
   </div>  
   <br/>
   <?php echo form_error('vt_day');?>
    
  </div>
    
  </div>
     <br/><br/>
  <div class="form-group form-group-lg ">
  <label class="control-label col-sm-2" style="width:18%" >* Company Information:</label>
  <div class="col-sm-9">
     <?php $user=$this->session->userdata('username');
     $c_information=$this->news_model->get_company($user);
     
     $c_information=$c_information[0]['c_information'];
    echo form_textarea('c_information',set_value('c_information',$c_information),'class=form-control');?>
    <p><?php echo form_error('c_information');?>
    </div>
   
    </div>
 <div class="form-group form-group-lg ">
  <label class="control-label col-sm-2" style="width:18%" >* Job Descriptions:</label>
  <div class="col-sm-9">
  <?php echo form_textarea('descriptions',set_value('descriptions'),'class=form-control');?>
   <p><?php echo form_error('descriptions');?></p>
  </div>
     
 
  </div>

  <div class="form-group form-group-lg ">
  <label class="control-label col-sm-2" style="width:18%" >* Job Responsibilities:</label>
  <div class="col-sm-9">
    <?php echo form_textarea('responsibilities',set_value('responsibilities'),'class=form-control');?>
     <p><?php echo form_error('responsibilities');?></p>
   
    
    </div>
    </div>

    <div class="form-group form-group-lg ">
  <label class="control-label col-sm-2" style="width:18%">* Requirements:</label>
  <div class="col-sm-9">
    <?php echo form_textarea('requirements',set_value('requirements'),'class=form-control');?>
    <p><?php echo form_error('requirements');?></p>
    </div>
      </div>




    </div><!--close inner panel-body-->
    </div><!-- close inner panel-->
    <div class="panel panel-default">
    <div class="panel-heading text-center" style="background:lightblue">

      Contact Information

    </div>
    <div class="panel-body" >



     <div class="row">
    <div class="form-group form-group-lg col-sm-6">
    <label class="control-label col-sm-2" >* Phone:</label>
    <div class="col-sm-10">
    <?php echo form_input('phone',set_value('phone'),'class=form-control');?>
    
    <?php echo form_error('phone');?>
    </div>
    </div>
    <div class="form-group form-group-lg col-sm-6">
<label class="control-label col-sm-2" >Website:</label>
    <div class="col-sm-10">
    <?php echo form_input('website',set_value('website'),'class=form-control');?>
                                  
   <?php echo form_error('website');?>
    </div>
    </div>
    </div><!--close row-->


    <div class="row">
    <div class="form-group form-group-lg col-sm-6">   
    <label class="control-label col-sm-2" >* Email:</label>
    <div class="col-sm-10">
  
    <?php echo form_input('email',set_value('email'),'class=form-control');?>
    
    <?php echo form_error('email');?>
    </div>
    </div>
    
     <div class="form-group form-group-lg col-sm-6">
    <label class="control-label col-sm-3" >* Confirm Email:</label>
    <div class="col-sm-9">
  
    <?php echo form_input('c_email',set_value('c_email'),'class=form-control');?>
    
    <?php echo form_error('c_email');?>
    </div>
    </div>
    </div><!--close row-->
      
    
  
    
 <div class="row">
 <div class="form-group form-group-lg col-sm-6">
  <label class="control-label col-sm-3" >* Address: </label>
  <div class="col-sm-9">
    <?php echo form_input('address',set_value('address'),'class=form-control');?>
    
    <?php echo form_error('address');?>
    </div>
    </div>
          
     <div class="form-group form-group-lg col-sm-6">           
    <label class="control-label col-sm-4" >* Township(Address): </label>
    <div class="col-sm-8">
    <?php echo form_input('township',set_value('township'),'class=form-control');?>
    
    <?php  echo form_error('township');?>
    </div>
    </div>
    </div><!--close row-->


  <div class="row">
 <div class="form-group form-group-lg col-sm-6">
 <br/>
  <label class="control-label col-sm-4" >* City(Address):</label>
  <div class="col-sm-8">
    <?php echo form_dropdown('city',$cities,set_value('city'),'class=form-control id=city');?>
    
    <?php echo form_error('city');?>

    </div>
    </div>
       <div class="form-group form-group-lg col-sm-6">
  <label class="control-label col-sm-4" >* Country(Address):</label>
  <div class="col-sm-8">
  <?php echo form_input('country',set_value('country'),'class=form-control');?>
  
  <?php echo form_error('country');?>
  </div>
  </div>
  </div><!--close row-->
       



  
  
    <div class="form-group form-group-lg" id="s_city_div" style="display:none">
  <label class="control-label col-sm-2" >* Specify City:</label>
  <div class='col-sm-4'>
  <?php echo form_input('s_city',set_value('s_city'),'class=form-control id=s_city');?>
  <?php echo "If validated,you can update to this city after 1 week";?>
  <br/>
  <?php echo form_error('s_city')?>
  </div>
  </div>  



  </div>
  </div>
 

 

 
 <div class="text-center">
 <?php echo form_submit('register','Register Job','class=btn btn-primary');?>
 </div>
 </form>
</div><!--close panel-body-->  
</div><!--close panel-->
</div><!--close container-->

<?php

//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
?>





<?php /////////////////////////////////////////////////
///////////////////////////////////////////////////////
///////////////////////////////////////////////////////
//////////////////////////////////////////////////////
?>
 

