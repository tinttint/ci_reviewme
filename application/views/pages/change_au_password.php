<script>
$(document).ready(function(){
    
  $('#admin_h_l_profile').css('background','#2874a6');
  $('#admin_h_l_account').css('background','yellow');
})
</script>



<?php
$is_logged_in = $this->session->userdata('is_logged_in');
$role = $this->session->userdata('role');
$user = $this->session->userdata('username');

		if(!isset($is_logged_in) || $is_logged_in != true ||($role!=='admin'))
		{
      echo "You don't have permission to access this page.";
        echo anchor('admin_role','Login');
			exit();
           
           }
		?>

<div class="clearfix"></div>

<div class="container-fluid">
<div class="panel panel-default">
<div class="panel-heading" style="background:#5dade2">
<?php   $path = base_url() . 'images/icons/back-arrow2.png';
     $reference = base_url() . 'admin_role/update_admin';?>
    <a style="float:left" href="<?php echo $reference;?>"><img src="<?php echo $path;?>" style="float:left;width:60px;height:40px"></a>
<h2 style="color:white;padding-right:60px;margin:0">Welcome <?php echo $user;?></h2>
<h3 style="color:white">Change Password</h3>
</div>
<div class="panel-body" style="background:ivory">
<?php echo "<h2 style=color:blue>" . (empty($message)?"":$message) . "</h2>";?>
<div id="login_form" class="col-sm-8 col-sm-offset-2" style="">

<?php $ask= array('onsubmit'=>"return confirm('Change Password?')",'class'=>'form-horizontal'
,'role'=>'form');?>
<?php echo form_open('admin_role/change_password',$ask);?>
<br/><br/>

<div class="form-group form-group-lg col-sm-11" >

<label class="control-label col-sm-3 col-sm-offset-1">New Password</label>
<div class="col-sm-6" >
<?php echo form_input('password',set_value('password'),'class=form-control');?>
<?php echo form_error('password');?>
</div>

</div>
<div class="form-group form-group-lg col-sm-11">

<label class="control-label col-sm-3 col-sm-offset-1">Confirm New Password</label>
<div class="col-sm-6">
<?php echo form_input('c_password',set_value('c_password'),'class=form-control');?>
<?php echo form_error('c_password');?>
</div>
</div>
<div class="col-sm-12" style="text-align:center;">
<?php echo form_submit('change_password','Change Password','class=btn btn-primary');?>
<br/><br/>
</div>

<?php echo form_close();
//echo anchor('login/signup','Create Account');
?>
</div>
</div><!--close panel body-->
</div><!-- close panel-->

</div><!--close container-->
<?php///////////////////////////////////////////////////
/////////////////////////////////////////////////////////
////////////////////////////////////////////////////
////////////////////////////////////////////////////////
?>