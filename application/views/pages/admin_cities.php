<script>

$(document).ready(function(){
    
  $('#admin_h_l_cities').css('background','yellow');
  $('#admin_h_l_category').css('background','#2874a6');
})

$(document).ready(function(){

    $("#ajax_other_cities").on('click','#pagination>a',function(e){
     e.preventDefault();
     var address= $(this).attr('href');
     
       $.ajax({
                url: address,
                type:"POST", 
              //  dataType: 'json',
                data:{is_ajax: 'true',address:address},
                success: function(result){
                  
            $("#ajax_other_cities").html(result);
            //$("#applied_jobs_div #previous_url").val(this.url);
           
        }
        });
    
    });

});

$(document).ready(function(){

    $("#ajax_cities").on('click','th>a',function(e){
     e.preventDefault();
     var address= $(this).attr('href');
     
       $.ajax({
                type:"POST",
                url: address,
                
              //  dataType: 'json',
                data:{is_ajax: 'true'},
                 
            
                success: function(result){
            $("#ajax_cities").html(result);
        }
        });
    
    });

});

$(document).ready(function(){

    $("#ajax_other_cities").on('click','th>a',function(e){
     e.preventDefault();
     var address= $(this).attr('href');
     
       $.ajax({
                type:"POST",
                url: address,
                
              //  dataType: 'json',
                data:{is_ajax: 'true'},
                 
            
                success: function(result){
            $("#ajax_other_cities").html(result);
        }
        });
    
    });

});
$(document).ready(function(){
  $("#search_form").on('click','#search_submit',function(e){
     e.preventDefault();
  
    
     var c = $('#search_form :input').serialize();

      c=btoa(c);
    
       
    
     
     
       $.ajax({
                type:"POST",
                url: "<?php echo base_url();?>" + 'admin_role/display_cities/'+c,
                
              //  dataType: 'json',
                data:{},
             
                success: function(result){
            $('#ajax_cities').html(result);
        }
        });
    
    });
});

$(document).ready(function(){

    $("#ajax_cities").on('click','#refresh_button',function(e){
     e.preventDefault();
     
     
       $.ajax({
                type:"POST",
                url:"<?php echo base_url();?>"+'admin_role/display_cities',
                
              //  dataType: 'json',
                data:{is_ajax: 'true'},
                 
            
                success: function(result){
                  $('#search_form :input[type=text]').val('');
                  $('#city_add').val('');
            $("#ajax_cities").html(result);
        }
        });
    
    });

});

$(document).ready(function(){

    $("#ajax_other_cities").on('click','#refresh_button',function(e){
     e.preventDefault();
     
     
       $.ajax({
                type:"POST",
                url:"<?php echo base_url();?>"+'admin_role/display_other_cities',
                
              //  dataType: 'json',
                data:{is_ajax: 'true'},
                 
            
                success: function(result){
            $("#ajax_other_cities").html(result);
        }
        });
    
    });

});
$(document).ready(function(){

    $("#ajax_cities").on('click','#pagination>a',function(e){
     e.preventDefault();
     var address= $(this).attr('href');
    
       $.ajax({
                url: address,
                type:"POST", 
              //  dataType: 'json',
                data:{is_ajax: 'true',address:address},
                success: function(result){
                  
            $("#ajax_cities").html(result);
            //$("#applied_jobs_div #previous_url").val(this.url);
           
        }
        });
    
    });

});

$(document).ready(function(){

    $("#ajax_cities").on('click','#delete_button',function(e){
     e.preventDefault();
      var id=this.name;
     
    
      var url=$('#cities_url').val();
     
      var answer= confirm('Delete?');
      if(answer==false)
      {
        return;
      }


    
       $.ajax({
                url:"<?php echo base_url() .'admin_role/delete_city';?>",
                type:"POST", 
              //  dataType: 'json',
                data:{is_ajax: 'true',id:id,url:url},
                success: function(result){
                 
            $("#ajax_cities").html(result);
           
           
        }
        });
    
    });

});

$(document).ready(function(){

    $('#ajax_other_cities').on('click','#delete_others',function(e){
     e.preventDefault();
      var id=this.name;
     
    
      var url=$('#status_url').val();
   
      var answer=confirm('Delete?');
      if(answer==false)
      {
          return;

      }
      

    
       $.ajax({
                url:"<?php echo base_url() .'admin_role/delete_other_cities';?>",
                type:"POST", 
              //  dataType: 'json',
                data:{id:id,url:url},
                success: function(result){
                 
            $('#ajax_other_cities').html(result);
           
           
        }
        });
    
    });

});

$(document).ready(function(){

    $("#add_form").on('click','#add_button',function(e){
     e.preventDefault();
    
     var city=$('#city_add').val();
     var url=$('#cities_url').val();
     if(city=="")
     {
      alert('The Add City Field Can Not Be Empty');
      return;
     }
    
  
    
       $.ajax({
                url:"<?php echo base_url() .'admin_role/add_city';?>",
                type:"POST", 
                dataType: 'json',
                data:{is_ajax: 'true',city:city,url:url},
                success: function(result){
                
                //  alert(result);
                alert(result.message);
                 $.ajax({
                          url:"<?php echo base_url();?>"+url,
                          type:"POST", 
                 
                          data:{},
                           success: function(result){
                            $('#city_add').val('');
                         $("#ajax_cities").html(result);
           
           
                }
                });
    
              // $("#ajax_categories").html(result);
           
           
        }
        });
    
    });

});
$(document).ready(function(){

$('#ajax_cities').on('click','#edit',function(e){
  e.preventDefault();
    
         
     var city=$(this).attr('formaction');
     ////$(this).parents('tr').find('td:nth-of-type(2)').text();
    
     
      
    
      var id=this.name;
   
    
       $('#myModal').modal();
     

      $('#id_modal').val(id);
      $('#city_modal').val(city);
     
})
})

$(document).ready(function(){

$('#edit_modal').click(function(e){
  e.preventDefault();
 
    var city=$('#city_modal').val();
    var id=$('#id_modal').val();
   //var category="sport";
  
       $.ajax({

                  url:"<?php echo base_url() .'admin_role/edit_city';?>",
                  type:"POST",
                  
                  data:{city:city,id:id},
                  dataType:"json",
                  success:function(result)
                  {
                    
                      alert(result.message);
                     var address="<?php echo base_url();?>"+$('#cities_url').val();
                   

                     $.ajax({

                        url:address,
                        type:"POST",
                        success:function(result)
                        {

                           $("#ajax_cities").html(result);
                        }
                      })
                      //alert('completed');
                   //   $('#practice').html(result);  
                       

                  }
                });

});


});

$(document).ready(function(){

    $("#ajax_other_cities").on('change','#status',function(e){
     e.preventDefault();
   
      
     var status=$('#status').val();
     var id=$('#status').attr('name');
    
     var url=$('#status_url').val();
    
    
       $.ajax({
                url: "<?php echo base_url();?>"+'admin_role/status_other_cities',
                type:"POST", 
              //  dataType: 'json',
                data:{status:status,id:id,url:url},
                success: function(result){
              
            $("#ajax_other_cities").html(result);
            
           
        }
        });
    
    });

});


</script>
 <style>
.table-bordered
{
   
  border-top:1px solid blue;
  /*border-right:1px solid red;*/
 
 /* border-left:1px solid red;*/
}
.table > thead > tr >th{

  text-align:center;

 /*border:1px solid red;*/
  border-top:1px solid blue;
  border-right:1px solid blue; 
  border-bottom:1px solid blue;
  border-left:1px solid blue;
  color:blue;
}
.table > thead > tr >th > a
{
    color:blue;

}
.table>tbody>tr>td
{
border-top:1px solid blue;
  border-right:1px solid blue;
  border-bottom:1px solid blue;
  border-left:1px solid blue;
  font-size:16px;

}

.table
{

  border-collapse:collapse;
  border-spacing:0px;
}



select option[selected]
{
  display:none;
}

select 
{
  text-align:center;
}


    </style>


<?php $is_logged_in = $this->session->userdata('is_logged_in');
     $role=$this->session->userdata('role');
     $user=$this->session->userdata('username');
    if(!isset($is_logged_in) || $is_logged_in != true || $role!=='admin')
    {

      echo "You don't have permission to access this page.";
     
      echo anchor('admin_role','Login');
    
      exit();

    }
    ?>
<div id="practice">



</div>
<?php
 
          ?>

<div class="container-fluid">
<div class="panel panel-default">
<div class="panel-heading" style="background:#5dade2">

<?php $f_name=$this->session->userdata('name');?>
<h2 style="color:white;margin:0">Welcome <?php echo $f_name;?></h2>
<h3 style="color:white">Add New City</h3>


</div>
<div class="panel-body" style="background:">

<div class="modal fade" id="myModal" style="position:absolute;top:40%" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
      <form method="post">
        <div class="modal-header">
         
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit City Name</h4>
        </div>
        <div class="modal-body">
           <input type="hidden" id="id_modal" value="">    
          <input class="form-control" type="text" value="" name="city_modal" id="city_modal">
        </div>
        <div class="modal-footer">
          <input type="submit" class="btn btn-default" value="Edit"  name="edit_modal" id="edit_modal">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </form>
      </div>
      
    </div>
  </div><!--close modal-->
  

<h4 style="text-align:center;color:red">Please check and update new city within 1 week since it's found</h4>
<div style="text-align:center">
<h4 style="text-align:center">Guideline</h4>
<ol style="text-align:left;display:inline-block;color:blue">
<li>Validate cities in 'city' column with status 'new' Table 1 to see if those are needed.</li>
<li>If Validated,search the city in 'search' field in Table 2 to check whether
existed or not.</li>
<li>If not exist in Table 2, add (or edit and add) the city in 'add city' field in Table 2.<br/>
If city exists in Table 2, not action is needed.</li>
<li>Then in 'status' column in Table 1, change the status to note it or delete the city.</li>
</ol>
</div>
<div class="row">
<div id="ajax_other_cities">

<form method="POST">
<div class="col-sm-5 table-responsive" style="border:1px solid blue">
<h3 style="color:blue">Table 1</h3>
<h3 style="color:blue">New Cities From Users</h3>
<h3 style="color:blue">Total Records Found <?php echo $other_cities_tr;?></h3>
<?php $others_url= "admin_role/display_other_categories";?>
<input type="hidden" id="others_url" value="<?php echo $others_url;?>">
<table class="table text-center table-bordered" style="text-align:center">
    <thead class="text-center">
     <tr><input type="button" id="refresh_button" class="btn btn-primary" value="Refresh Table">
     </tr> 
      <tr>
        <th <?php if($sort_by2 =='city')echo "class=sort_$sort_order2";?>><?php echo 
anchor("admin_role/display_other_cities/$q/city/". (($sort_order2 == 'asc' && $sort_by2 == 'city')?'desc'
: 'asc')."/".$limit,'City');?></th>
      
        <th <?php if($sort_by2 =='added_date')echo "class=sort_$sort_order2";?>><?php echo 
anchor("admin_role/display_other_cities/$q/added_date/". (($sort_order2 == 'asc' && $sort_by2 == 'added_date')?'desc'
: 'asc')."/".$limit,'Found Date');?></th>
      
        
         
        <th <?php if($sort_by2 =='status')echo "class=sort_$sort_order2";?>><?php echo 
anchor("admin_role/display_other_cities/$q/status/". (($sort_order2 == 'asc' && $sort_by2 == 'status')?'desc'
: 'asc')."/".$limit,'Status');?></th>
          <th>Delete</th>
      </tr>
    </thead>
    <tbody>
    <?php foreach($other_cities as $values):?>
      <tr>
        <input type="hidden" id="id" value="<?php echo $values['id'];?>">
        <td><?php echo $values['city'];?></td>

        
        <td><?php $ad=date_create($values['added_date']);
        echo date_format($ad,'d-M-Y');?></td>
        <td>
        
        <?php 
         echo "<span style=color:red>". $values['status']. "</span>";
         echo "<br>";
         $status_url="admin_role/display_other_cities";
       //  $id=$values['id'];
         echo form_dropdown($values['id'],array('change_status'=>'change status','added'=>'added','edited and added'=>'edited and added'
        ,'no need to add'=>'no need to add'),'change_status','id=status');?>
        <input type="hidden" id="status_url" name="status_url" value="<?php echo $status_url;?>"></td>
         <td>
        <?php $others_id=$values['id'];?>
            <button style="background-color:transparent" type='button' id="delete_others" name="<?php echo $others_id;?>" value="Delete"><img src="<?php echo base_url().'images/icons/trash.png';?>" width="35" height="35">
          </button>
        </td>
      </tr>
     <?php endforeach;?>
    </tbody>
  </table>
  <div class="col-sm-12 text-center">
   <?php echo $pagination2;?>
   </div>
</div><!--close column-->
</form>
</div><!--close ajax_other_categories-->
<div class="col-sm-6 col-sm-offset-1 table-responsive" style="border:1px solid blue">



<h3 style="color:blue">Table 2</h3>
<h3 style="color:blue">Existed Cities</h3>

<?php echo form_open('','class=form-horizontal role=form id=search_form');?>

<div class="form-group col-sm-8">
  <label class="control-label col-sm-5" style="padding-right:0" >City:</label>
  <div class="col-sm-7" style="padding-left:0">
    <?php echo form_input('city',set_value('city'),'class=form-control');?>
  </div>
  </div>
  
  <input type="button" class="btn btn-primary" id="search_submit" name="search"  value="Search">
     <br/><br/>
     </form>
     <form class="form-horizontal role=form" id="add_form">
<div class="form-group col-sm-8">
  <label class="control-label col-sm-5" style="padding-right:0" >Add City:</label>
  <div class="col-sm-7" style="padding-left:0">
    <?php echo form_input('city_add',set_value('city_add'),'id=city_add class=form-control');?>
  </div>
  </div>
  
  <input type="button" class="btn btn-primary" id="add_button" name="add_button"  value="Add City">
     <br/><br/>
     </form>


<div id="ajax_cities">
<h3 style="color:blue">Total Records Found <?php echo $total_rows;?></h3>
<table class="table text-center table-bordered">
    <thead class="text-center">
     <tr><input type="button" id="refresh_button" class="btn btn-primary" value="Refresh Table">
     </tr> 
      <tr class="text-center">
        <th>Order No</th>
        <th <?php if($sort_by =='cities')echo "class=sort_$sort_order";?>><?php echo 
anchor("admin_role/display_cities/$q/cities/". (($sort_order == 'asc' && $sort_by == 'cities')?'desc'
: 'asc')."/".$limit,'City');?></th>
      
        <th>Update</th>
        <th>Delete</th>
 
      </tr>
    </thead>
    <tbody>
    <?php $order_no=$offset+1;?>
    <?php foreach($cities as $values):?>
      <tr>
        <input type="hidden" id="<?php echo $values['id'];?>" value="<?php echo $values['id'];?>">
        <td><?php echo $order_no;?></td> 
        <td><?php echo $values['cities'];?></td>
       
        
        <td>
        <?php $cities_url="admin_role/display_cities";?>
        <input type="hidden" id="cities_url" value="<?php echo $cities_url;?>">
        <input type="button" class="btn btn-primary" formaction="<?php echo $values['cities'];?>"
        id="edit" name="<?php echo $values['id'];?>" value="Edit"></td>
        <td>
         <?php $id=$values['id'];?>
          <button style="background-color:transparent" type='button' id="delete_button" name="<?php echo $id;?>" value="Delete"><img src="<?php echo base_url().'images/icons/trash.png';?>" width=35 height=35>
          </button>
        </td>
      </tr>
    <?php  $order_no++;
     endforeach;?>
    </tbody>
  </table>
  <div class="col-sm-12 text-center">
   <?php echo $pagination;?>
   </div>
   </div><!--close ajax_categories-->
</div><!--close column-->




</div><!--close row-->


</div><!--close panel body-->
</div><!--close panel-->
</div><!--close container-fluid-->