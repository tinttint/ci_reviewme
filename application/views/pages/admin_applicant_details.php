<?php $is_logged_in = $this->session->userdata('is_logged_in');
     $role=$this->session->userdata('role');
    
    if(!isset($is_logged_in) || $is_logged_in != true || $role!=="admin")
    {
/*$this->load->view('templates/Cheader');
                $this->load->view('pages/members',$data);*/

                  // $this->load->view('templates/footer');
      echo "You don't have permission to access this page.";  
      echo anchor('admin_role','Login');
      exit();

    }
?>

<script>


$(document).ready(function(){
    
 $('#admin_h_l_applicants').css('background','yellow');
 $('#admin_h_l_profile').css('background','#2874a6');
})


</script>
<style>
.form-group .form-control-static
{

 padding-left:0px;
 height:auto;
}
.control-label
{

  padding-right:0px;
}


</style>
<?php
$details_url=(empty($details_url)?base_url().'admin_role/display_applicants':$details_url);

echo form_open($details_url,'method=get');?>


       <?php 
       if(isset($checked))
       {
       foreach($checked as $keys=>$value):?>

        <input type="hidden" name="checked[]" value="<?php echo $value;?>">
       <?php endforeach;
       }
     


$user = $this->session->userdata('username');?>

<div class="container-fluid">

<div class="panel panel-default">
    <div class="panel-heading" style="background:#5dade2">
     <input type="submit" value="" style='float:left;background-image:url("<?php echo base_url(). 'images/icons/back-arrow2.png';?>");background-size:60px 40px;width:60px;height:40px;border:none;background-color:transparent;background-repeat:no-repeat'>
       

<?php  echo form_close();
    $f_name=$this->session->userdata('name');?>
    <h2 style="color:white;padding-right:60px;margin:0"> Welcome <?php  echo $f_name;?></h2>
    <h3 style="color:white">Applicant Details</h3></div>
    <div class="panel-body" style="background:ivory">
   <?php $applicant_id=$applicant[0]['applicant_id'];?>
 <?php echo form_open("admin_role/delete_applicant",array('class'=>'form-horizontal','role'=>'form','onsubmit'=>"return confirm('Delete Applicant?')"));?>
  
<div class="panel panel-default">
<div class="panel-heading text-center" style="background:lightblue">

Personal Information

</div>   
<div class="panel-body">
   <div class="row">
   
   <div class="col-sm-8">
   
    <div class="form-group form-group-lg">
      <label class="control-label col-sm-4" style="">Username:</label>
      <div class="col-sm-8" style="">
        <p class="form-control-static" style=""><?php echo $applicant[0]['user'];?> </p>
      </div>
    </div>
    <hr>
   <div class="form-group form-group-lg">
      <label class="control-label col-sm-4" for="pwd">Full Name:</label>
      <div class="col-sm-7">          
        <p class="form-control-static"><?php echo $applicant[0]['name'];?></p>
      </div>
    </div>
    <hr>

   
      <div class="form-group form-group-lg">
      <label class="control-label col-sm-4"  >Student Registration Number:</label>
      <div class="col-sm-7">
        <p class="form-control-static"><?php echo $applicant[0]['id'];?> </p>
      </div>
    </div>
    <hr> 
            <div class="form-group form-group-lg">
      <label class="control-label col-sm-4" >National Registration Card No:</label>
      <div class="col-sm-7">
        <p class="form-control-static"><?php echo $applicant[0]['nrc'];?> </p>
      </div>
   
    </div>
    <hr>
   
 <div class="form-group form-group-lg">
      <label class="control-label col-sm-4" >Major:</label>
      <div class="col-sm-7">
        <p class="form-control-static"><?php echo $applicant[0]['major'];?> </p>
      </div>
    </div>
         
    </div>
  
     




                           
   
    <div class="col-sm-4" style="border:1px solid lightgray">
<div class="form-group form-group-lg" style="height:auto">
      <label class="control-label col-sm-4">Photo:</label>
      <div class="col-sm-7">
        <?php $student = $applicant[0]['user'];
$photo_path = $this->news_model->get_photo_path($student);?>
<img class="img-thumbnail" src="<?php echo (empty($photo_path)?base_url().'images/icons/Blank-photo.png':$photo_path);?>"
style="width:200px;height:180px">
      </div>
    </div>


 
     
     <div class="form-group form-group-lg">
      <label class="control-label col-sm-5" >Registered Date:</label>
      <div class="col-sm-7">
        <p class="form-control-static"><?php $registered_date=date_create($applicant[0]['registered_date']);
        echo date_format($registered_date,'d-M-Y');?> </p>
      </div>
    </div>
    <div class="form-group form-group-lg">
      <label class="control-label col-sm-5"  >Date Of Birth:</label>
      <div class="col-sm-7">
        <p class="form-control-static"><?php $date_o_b=date_create($applicant[0]['date_o_b']);
        echo date_format($date_o_b,'d-M-Y');?> </p>
      </div>
    </div>
    
   
         


    </div>
    </div><!--close row-->


         <hr>                       
 <div class="form-group form-group-lg">

      <label class="control-label col-sm-2" style="margin-left:4%">Qualifications/Degrees:</label>
      <div class="col-sm-8">
        <p class="form-control-static"><?php echo $applicant[0]['qualifications'];?> </p>
      </div>
    </div>
    <hr>
     
    <div class="row">
    <div class="col-sm-6">
 
 <div class="form-group form-group-lg">
      <label class="control-label col-sm-5" >Gender:</label>
      <div class="col-sm-7">
        <p class="form-control-static"><?php echo $applicant[0]['gender'];?> </p>
      </div>
    </div>
    <hr>


   
    <div class="form-group form-group-lg">
      <label class="control-label col-sm-5" >Nationality:</label>
      <div class="col-sm-7">
        <p class="form-control-static"><?php echo $applicant[0]['nationality'];?> </p>
      </div>
    </div>
    

    </div>
    <div class="col-sm-6">
            <div class="form-group form-group-lg">
      <label class="control-label col-sm-5" >Marital Status:</label>
      <div class="col-sm-7">
        <p class="form-control-static"><?php echo $applicant[0]['m_status'];?> </p>
      </div>
    </div>
    <hr>




    </div>
    

     
    </div>

    </div><!--close inner panel body-->
    </div><!--close panel body-->


    
       
      
     
   
    
    
   
    
   <div class="panel panel-default">
   <div class="panel-heading text-center" style="background:lightblue">

        Contact Information

   </div>
   <div class="panel-body">
    
    <div class="row">
    <div class="col-sm-6">
    <div class="form-group form-group-lg">
      <label class="control-label col-sm-4" >Phone:</label>
      <div class="col-sm-7">
        <p class="form-control-static"><?php echo $applicant[0]['phone'];?> </p>
      </div>
    </div>
    <hr>
  <div class="form-group form-group-lg">
      <label class="control-label col-sm-4" >Address:</label>
      <div class="col-sm-7">
        <p class="form-control-static"><?php echo $applicant[0]['address'];?> </p>
      </div>
    </div>
    <hr>

    <div class="form-group form-group-lg">
      <label class="control-label col-sm-4" >City(Address):</label>
      <div class="col-sm-7">
        <p class="form-control-static"><?php echo $applicant[0]['city'];?> </p>
      </div>
    </div>
    </div>                         
                                      
          
    
    <div class="col-sm-6">
   
<div class="form-group form-group-lg">
      <label class="control-label col-sm-4" >Email:</label>
      <div class="col-sm-7">
        <p class="form-control-static"><?php echo $applicant[0]['email'];?> </p>
      </div>
    </div>
    <hr>

    <div class="form-group form-group-lg">
      <label class="control-label col-sm-4" >Township(Address):</label>
      <div class="col-sm-7">
        <p class="form-control-static"><?php echo $applicant[0]['township'];?> </p>
      </div>
    </div>
    <hr>

   
    <div class="form-group form-group-lg">
      <label class="control-label col-sm-4" >Country(Address):</label>
      <div class="col-sm-7">
        <p class="form-control-static"><?php echo $applicant[0]['country'];?> </p>
      </div>
    </div>
    </div>
    </div>
   



    </div>
    </div>



    <div class="panel panel-default">
    <div class="panel-heading text-center" style="background:lightblue">

     Work Information          

    </div>
    <div class="panel-body">



     <div class="form-group form-group-lg">
      <label class="control-label col-sm-2">Work Experience:</label>
      <div class="col-sm-9">
        <p class="form-control-static"><?php echo $applicant[0]['experience'];?> </p>
      </div>
    </div>




    </div><!--close inner panel body-->
    </div><!--close inner panel-->
    <div class="text-center">
    <?php $previous_url=$this->uri->uri_string();
    $applicant_id=$applicant[0]['applicant_id'];?>
    <input type="hidden" name="applicant_id" id="applicant_id" value="<?php echo $applicant_id;?>">
    <input type="hidden" name="previous_url" id="previous_url" value="<?php echo $details_url;?>">

    <?php echo form_submit('action','Delete','class=btn btn-primary');?>
    </div>
   
  </form>
  </div>
  </div>
</div><!--close container-->


<?php//////////////////////////////////////////////////
//////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////?>
