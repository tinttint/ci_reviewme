<div class=" first-column text-left">
  <div class="row">
 
 
      <h4>Photo:</h4>
      <div class="center-block">
        <?php $student = $account[0]['user'];
$photo_path = $this->news_model->get_photo_path($student);?>
<img class="img-thumbnail" src="<?php echo (empty($photo_path)?base_url().'images/icons/Blank-photo.png':$photo_path);?>"
style="width:200px;height:180px">
      </div>
   
      
    </div>

    <div class="row">
    <div class="col-sm-5 col-sm-offset-1">

    
       <h4>Username</h4>
      
        <p><?php echo $account[0]['user'];?> </p>
      
     </div>
     <div class="col-sm-5 col-sm-offset-1">
    
    <h4>Full Name:</h4>
                
        <p><?php echo $account[0]['name'];?></p>
      </div>
    </div>
    <div class="row">
    <div class="col-sm-5 col-sm-offset-1">
      <h4>National Registration Card No:</h4>
      
        <p><?php echo $account[0]['nrc'];?> </p>
      
    </div>
      <div class="col-sm-5 col-sm-offset-1">
      <h4> Student Registration Number:</h4>
  <p><?php echo $account[0]['id'];?> </p>
      </div>
      </div>
     
      
      
     
      
      <div class="row">
      <div class="col-sm-5 col-sm-offset-1">
    
      <h4>Qualifications/Degrees:</h4>
      
        <p> <?php echo $account[0]['qualifications'];?> </p>
      
    </div>
    <div class="col-sm-5 col-sm-offset-1">
    
      <h4>Major:</h4>
      
        <p> <?php echo $account[0]['major'];?> </p>
      
    </div>
    </div>
     <div class="row">
     <div class="col-sm-5 col-sm-offset-1">
      <h4>Work Experience:</h4>
      
        <p> <?php echo $account[0]['experience'];?> </p>
      </div>
      <div class="col-sm-5 col-sm-offset-1">
    
    
    
    
      <h4>Gender:</h4>
      
        <p> <?php echo $account[0]['gender'];?> </p>
      </div>
    </div>
    
   
    
    
    <div class="row">
    <div class="col-sm-5 col-sm-offset-1">

      <h4>Date Of Birth:</h4>
     
        <p> <?php $date_o_b=date_create($account[0]['date_o_b']);
        echo date_format($date_o_b,'d-M-Y');?> </p>
     
    </div>
    <div class="col-sm-5 col-sm-offset-1">
      <h4>Nationality:</h4>
      
        <p> <?php echo $account[0]['nationality'];?> </p>
      </div>
    </div>
    <div class="row">
    <div class="col-sm-5 col-sm-offset-1">
      <h4>Phone:</h4>
      
        <p> <?php echo $account[0]['phone'];?> </p>
      </div>
    
      <div class="col-sm-5 col-sm-offset-1">
      <h4> Email:</h4>
      
        <p> <?php echo $account[0]['email'];?> </p>
      </div>
    </div>
      <div class="row">
      <div class="col-sm-5 col-sm-offset-1">
      <h4>Address:</h4>
      
        <p> <?php echo $account[0]['address'];?> </p>
      </div>
      <div class="col-sm-5 col-sm-offset-1">
    
    
      <h4>Township(Address):</h4>
      
        <p> <?php echo $account[0]['township'];?> </p>
      </div>
    </div>
     <div class="row">
     <div class="col-sm-5 col-sm-offset-1">
      <h4> City(Address):</h4>
      
        <p> <?php echo $account[0]['city'];?> </p>
      </div>
    
        <div class="col-sm-5 col-sm-offset-1">
      <h4>Country(Address):</h4>
      
        <p> <?php echo $account[0]['country'];?> </p>
      </div>
    </div>
     <div class="row">
     <div class="col-sm-5 col-sm-offset-1">
      <h4>Registered Date:</h4>
      
        <p> <?php $registered_date=date_create($account[0]['registered_date']);
        echo date_format($registered_date,'d-M-Y');?> </p>
      </div>
    </div>
    </div><!--close second column;-->
      
    <div style="text-align:center">
  <?php echo form_submit('update','Update','class=btn btn-primary');?>
  </div>