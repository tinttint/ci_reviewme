

 





<?php echo "<h3 style=text-align:center;color:blue>Total Records Found: " . $total_records . "</h3>";?>
<div style="text-align:center">
<?php echo $pagination;?>
</div>
<?php 


  $count=count($applicants);?>
                                          
<input type="hidden" id="q" name='q' value="<?php echo $q;?>">
<input type="hidden" id="sort_by" name="sort_by" value="<?php echo $sort_by;?>">
<input type= "hidden" id="sort_order" name="sort_order" value="<?php echo $sort_order;?>">
<input type = "hidden" id="offset" name="offset" value="<?php echo $offset;?>">
<input type= "hidden" id="limit" name="limit" value="<?php echo $limit;?>">
<input type= "hidden" id="count" name="count" value="<?php echo $count;?>">

<form method="get">
<?php $details_url=$this->uri->uri_string();?>
  <input type="hidden" name="details_url" value="<?php echo $details_url;?>">
  <?php $previous_url=$this->uri->uri_string();?>
        <input type="hidden" name="previous_url" id="previous_url" value="<?php echo $previous_url;?>">

<div class="table-responsive">
<table class="table text-center table-bordered">
    <thead class="text-center">
     <tr><input type="button" id="refresh_button" class="btn btn-primary" value="Refresh Table">
     </tr> 
      <tr class="text-center">
        <th <?php if($sort_by =='job_title')echo "class=sort_$sort_order";?>><?php echo 
anchor("admin_role/display_applicants/$q/job_title/". (($sort_order == 'asc' && $sort_by == 'job_title')?'desc'
: 'asc')."/$limit",'Job Title');?></th>
        <th>Job Id <br/>Applied For</th>
        <th>Student <br/>Registration No</th>
        <th> Applicant Id</th>
        <th <?php if($sort_by =='name')echo "class=sort_$sort_order";?>><?php echo 
anchor("admin_role/display_applicants/$q/name/". (($sort_order == 'asc' && $sort_by == 'name')?'desc'
: 'asc')."/$limit",'Applicant Name');?></th>
        <th>Applicant Resume</th>
      

        <th <?php if($sort_by =='gender')echo "class=sort_$sort_order";?>><?php echo 
anchor("admin_role/display_applicants/$q/gender/". (($sort_order == 'asc' && $sort_by == 'gender')?'desc'
: 'asc')."/$limit",'Gender');?></th>
      
        <th <?php if($sort_by =='date_applied')echo "class=sort_$sort_order";?>><?php echo 
anchor("admin_role/display_applicants/$q/date_applied/". (($sort_order == 'asc' && $sort_by == 'date_applied')?'desc'
: 'asc')."/$limit",'Date Applied');?></th>
        <th>Applicant Details</th>
        <th><?php if(in_array('check_all',$checked))
        {
             $found="checked";
        }
        else
        {
          $found="";
        }
        ?><input type="checkbox" <?php echo (empty($found)?'':$found);?> class="form-control" id="check_all"  name="checked[]" value="check_all">
        (select/deselect all)<br/>
        <input type="submit" id="delete" name="delete" formaction='<?php echo base_url()."admin_role/delete_applicant";?>' 
        class="btn btn-primary" style="font-size:16px" value="Delete">
      
        </th>

      </tr>
    </thead>
    <tbody>
    <?php foreach($applicants as $values):?>
      <tr>
        <td><div><?php echo $values['job_title'];?></div></td>

        
        <td><div><?php $job_id=$values['job_id'];
              $back_url2=$this->uri->uri_string(); 
              $back_url2=base64_encode($back_url2);
                   ?> 
          
           
        <a href='<?php echo base_url()."admin_role/details_job/$job_id/$back_url2";?>'>
        <?php echo $values['job_id'];?></a></div></td>
        <td><div><?php echo $values['id'];?></div></td>
        <td><div><?php echo $values['applicant_id'];?></div></td>
        <td><div><?php echo $values['name'];?></div></td>
        <td><div><?php $file_path=$values['file'];
        echo anchor($file_path,$values['file_orig_name']);?></div></td>
       
         <td><div><?php echo $values['gender'];?></div></td>
          <td><div><?php $date_ap=date_create($values['date_applied']);
          echo date_format($date_ap,'d-M-Y');?></div></td>
           <td><div><?php $applicant_id=$values['applicant_id'];
                 
                   ?> 
           <input type="submit" class="btn btn-primary" id="details" name="details" formaction='<?php echo base_url()."admin_role/view_applicant/$applicant_id";?>' value="Details"></td>
         
           </div></td>
            

            <td><div><?php $applicant_id=$values['applicant_id'];?>
            <?php if(in_array($applicant_id,$checked))
            {
              $found="checked";
            }
            else
            {
              $found="";
            }
            ?>
            <input type="checkbox" <?php echo (empty($found)?"":$found);?> class="form-control" id="<?php echo $applicant_id;?>" name="checked[]" value="<?php echo $applicant_id;?>">
            </div></td>
      </tr>
     <?php endforeach;?>
    </tbody>
  </table>
  </div><!--close table div-->
  </form>
<div style="text-align:center">
<?php echo $pagination;?>
</div>