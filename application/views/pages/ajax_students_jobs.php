<?php echo "<h3 style=text-align:center;color:blue>Total Records Found: " . $total_records . "</h3>";
?>
<div class="col-sm-11" style="margin-left:4%;">

<button class="btn btn-primary" id='refresh_button' style="">Refresh Table</button>

</div>

<?php
 
$count = count($my_jobs);
$previous_url=$this->uri->uri_string();
?>


<input type="hidden" id="previous_url" name="previous_url" value="<?php echo base_url() .$previous_url;?>">
<input type="hidden" id="limit" value="<?php echo $limit;?>">
<input type="hidden" id="count" value="<?php echo $count;?>">
<input type="hidden" id="address" value="<?php echo $address;?>">
<input type="hidden" id="q" value="<?php echo $q;?>">
<input type="hidden" id="sort_by" value="<?php echo $sort_by;?>">
<input type="hidden" id="sort_order" value="<?php echo $sort_order;?>">
<input type="hidden" id="offset" value="<?php echo $offset;?>">

<form id="jobs_form" method="get" >
<div id='top_bar' class="col-sm-11" style="background:#FFFFF0;padding:4px;margin-left:4%;border:1px solid blue">


<div class="col-sm-2" >
<label class="col-sm-12 text-center">Sort By:</label>
      <div class="col-sm-12">
      <select class="form-control" id="select_sort_by">
      <option <?php echo(($sort_by=='applied_date')?'selected':'');?> value='<?php echo "$q/applied_date";?>'>Job Applied Date</option>
     <option <?php echo(($sort_by=='category')?'selected':"");?> value='<?php echo "$q/category";?>'>Job Category</option>
    <option <?php echo(($sort_by=='c_name')?'selected':"");?> value='<?php echo "$q/c_name";?>'>Company Name</option>
    <option <?php echo(($sort_by=='title')?'selected':"");?> value='<?php echo "$q/title";?>'>Job Title</option>
     <option <?php echo(($sort_by=='job_type')?'selected':"");?> value='<?php echo "$q/job_type";?>'>Job Type</option>
      <option <?php echo(($sort_by=='valid_to')?'selected':"");?> value='<?php echo "$q/valid_to";?>'>Apply Valid To</option>

   </select>
      

</div>
</div>
<div class="col-sm-2">
<label class="col-sm-12 text-center">Sort Order:</label>
      <div class="col-sm-12">
      <select class="form-control" id="select_sort_order">
    <option <?php echo(($sort_order=='asc')?'selected':"");?> value="asc" >Ascending</option>
    <option <?php echo(($sort_order=='desc')?'selected':"");?> value="desc">Descending</option>
  </select>
</div>
</div>
     <div class="col-sm-6 " >
<div style="margin-left:8%"><?php echo $pagination;?></div>


</div>
<div class="col-sm-2  text-center center-block" >
<?php $found_all="";
if(in_array('select_all',$checked))
{
 $found_all='checked';
}
else
{
  $found_all='';
}
?>
<input type="checkbox" class="form-control" style="width:50px;display:inline" name="checked[]" id="select_all" <?php echo $found_all;?> value="select_all" onclick="check_all(this)">(Select/Deselect All)
<button class="btn btn-primary" id="delete">Delete</button> 
</div>
</div><!--closed top bar div-->

<?php     $back_url=$this->uri->uri_string();?> 
    
     <input type="hidden" name="back_url" value="<?php echo $back_url;?>">

<?php foreach($my_jobs as $m):?>
<div id="student_jobs" class="col-sm-11"style=";padding:8px;margin-left:4%;border:1px solid blue">
  
  <div class="col-sm-2">
   <?php $logo=$this->news_model->get_logo($m['user_c']);?>
    <img class="img-responsive img-thumbnail" id="logo"src="<?php echo (empty($logo)?base_url().'images/icons/no-image.jpg':$logo);?>"
      style="width:200px;height:150px">
      </div>
      
   <div class=" col-sm-10"><!--second column-->
   <div class="row">
  <div class="col-sm-6 job_info" >
  <label>Job Id:</label><span><?php echo $m['job_id'];?>
   </span>
  </div>
  
  <div class="col-sm-6 job_info" >
  <label>Company Name:</label><span><?php echo $m['c_name'];?>
 </span>
  </div>
   </div><!--close row-->
  <div class="row">    

   
  <div class="col-sm-6 job_info" >
  <label>Job Category:</label><span><?php echo $m['category'];?></span>
  </div>
  <div class="col-sm-6 job_info">
  <label>Job Title:</label><span><?php echo $m['title'];?></span>
  </div>
  </div><!--close row-->
  
   <div class="row">
  <div class="col-sm-6 job_info" >
  <label>Salary:</label><span><?php echo $m['salary'];?></span>
  </div>
  <div class="col-sm-6 job_info">
  <label>Job Type:</label><span><?php echo $m['job_type'];?></span>
  </div>
  </div><!--close row-->
    
   <div class="row">
  <div class="col-sm-6 job_info" >
  <label>Job Applied Date:</label><span><?php $ad=date_create($m['applied_date']);
  echo date_format($ad,'d-M-Y');?></span>
  </div>
  <form id="jobs_form" method="get" >
  <div class="col-sm-3 job_info"  >

 <?php $applied_id=$m['applied_id'];?>
   
    
     
     <input type="submit" class="btn btn-primary"  formaction='<?php echo base_url(). "pages/details_student_job/$applied_id";?>' id="details_submit" name="<?php echo $m['applied_id'];?>" value="Details"> 
  </div>
  
  
   <div class="col-sm-3 text-center" >
   <?php $applied_id=$m['applied_id'];
   

    $found='';
    if(in_array($applied_id,$checked))
    {
            $found='checked';
            
     }     
          else
          {
            $found=='';
          }

     
     ?>
     
     <input class="form-control"type="checkbox" style="margin-left:18%" id="<?php echo $applied_id;?>" <?php echo $found;?> name="checked[]" value="<?php echo $applied_id;?>">(Select To Delete)
      </div>
      
  </div><!--close row-->
 





</div><!--close second column-->
</div><!--close students_jobs div-->
<?php endforeach;?>
</form>

<div id="bottom_bar_div" class="col-sm-11"style="background:ivory;padding:8px;margin-left:4%;border:1px solid blue">
<div class="col-sm-6 col-sm-offset-4" id="pagination" >
<div style="margin-left:8%">
<?php echo $pagination;?>
</div>
</div>
</div>