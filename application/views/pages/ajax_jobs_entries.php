<?php
$count = count($jobs);?>
<input type="hidden" id="limit" value="<?php echo $limit;?>">
<input type="hidden" id="count" value="<?php echo $count;?>">

<input type="hidden" id="q" value="<?php echo $q;?>">
<input type="hidden" id="sort_by" value="<?php echo $sort_by;?>">
<input type="hidden" id="sort_order" value="<?php echo $sort_order;?>">
<input type="hidden" id="offset" value="<?php echo $offset;?>">


<?php //echo "<h3 style='text-align:center;color:blue'>Search Results for: <span style=color:red>" .  $message. "</span></h3>";?>
<?php echo "<h3 style='text-align:center;color:blue'>Total Records Found: $total_records</h3>";?>

<div class="col-sm-11" id="refresh_div" style="margin-left:4%;">

<button class="btn btn-primary" id='refresh_button'  >Refresh Table</button>

</div><!--close refresh_div-->


  <div id="top_bar_div" class="col-sm-11"style=";padding:8px;margin-left:4%;border:1px solid blue;background:ivory">


<div class="col-sm-2" >
<label class="col-sm-12 text-center">Sort By:</label>
      <div class="col-sm-12">
      <select class="form-control" id="select_sort_by">
      <option <?php echo (($sort_by=='last_updated')?'selected':'');?> value='<?php echo "$q/last_updated";?>'>Last Updated</option>
      <option <?php echo (($sort_by=='published_date')?'selected':'');?> value='<?php echo "$q/published_date";?>'>Published Date</option>
       <option <?php echo (($sort_by=='opened_date')?'selected':'');?> value='<?php echo "$q/opened_date";?>'>Job Opened Date</option>
    
  
    <option <?php echo (($sort_by=='category')?'selected':'');?> value='<?php echo "$q/category";?>'>Job Category</option>
     <option <?php echo (($sort_by=='title')?'selected':'');?> value='<?php echo "$q/title";?>'>Job Title</option>
     
    <option <?php echo (($sort_by=='job_type')?'selected':'');?> value='<?php echo "$q/job_type";?>'>Job Type</option>
    <option <?php echo (($sort_by=='city')?'selected':'');?> value='<?php echo "$q/city";?>'>City(Address)</option>
     <option <?php echo (($sort_by=='valid_to')?'selected':'');?> value='<?php echo "$q/valid_to";?>'>Apply Valid To</option>
   
    
   </select>
      

</div>
</div>
<div class="col-sm-2">
<label class="col-sm-12 text-center">Sort Order:</label>
      <div class="col-sm-12">
      <select class="form-control" id="select_sort_order">
    <option <?php echo (($sort_order=='asc')?'selected':'');?> value="asc" selected>Ascending</option>
    <option <?php echo (($sort_order=='desc')?'selected':'');?> value="desc">Descending</option>
  </select>
</div>
</div>

<div class="col-sm-6" >
<div style="margin-left:10%;padding:4px" id="pagination">
  <?php echo $pagination;?>
  </div>
</div>
<div class="col-sm-2" >

</div>
</div><!--close top_bar_div -->



<?php foreach($jobs as $value):?>

<div id="jobs_wrapper" class="col-sm-11"style=";padding:8px;margin-left:4%;border:1px solid blue">
  
  <div class="col-sm-2 text-center" style="">
  <?php $logo=$value['logo'];?>
    <img class="img-responsive img-thumbnail" id="logo"src="<?php echo(empty($logo)?base_url().'images/icons/no-image.jpg':$logo);?>"
     style="width:200px;height:140px" >
      <p>Job Id:<?php echo $value['id'];?></p>
      </div>
   <div class="col-sm-10" id="wrapper2">   
   <div class="row">
  
  
  <div class="col-sm-6 job_info">
  <label>Company Name:</label><span><?php echo $value['c_name'];?></span>
  </div>
   
  <div class="col-sm-6 job_info" >
    <label>Job Category:</label><span><?php echo $value['category'];?></span>
  </div>
  
  </div>
  <div class="row">
  
  <div class="col-sm-6 job_info" >
  <label>Job Title:</label><span><?php echo $value['title'];?></span>
  </div>
    
  <div class="col-sm-6 job_info" >
    <label>Job Salary:</label><span><?php echo $value['salary'];?></span>
  </div>
  
  </div>
  
  <div class="row">
 
  <div class="col-sm-6 job_info" >
  <label>Job Type:</label><span><?php echo $value['job_type'];?></span>
  </div>
    
  <div class="col-sm-6 job_info" >
    <label>City(Address):</label><span><?php echo $value['city'];?></span>
  </div>
  
  </div>

<div class="row">
  <form method="get">
  <div class="col-sm-6 job_info" >
  <?php $back_url=$this->uri->uri_string();?>
  <input type="hidden" id="back_url" name="back_url" value="<?php echo $back_url;?>">


  <label>Job Opened Date:</label><span><?php 
  if($value['opened_date']=='0000-00-00')
  {
    echo '00-00-0000';
  }
  else
  {
    $op=date_create($value['opened_date']);
  
  echo date_format($op,'d-M-Y');
  }
  ?>
  </span><br/>
  <label>Job Status:</label><span><?php echo $value['job_status'];?></span>
 


  </div>
  <div class="col-sm-3 job_info" >
  <?php 
   
   $job_id=$value['id'];
   $job_status=$value['job_status'];
   //$pu=$this->uri->uri_string();


   ?>
 
    <input type="submit" class="btn btn-primary" formaction='<?php echo base_url()."pages/job_entries_details/$job_id";?>' id="details" name="details" value="Details/Update">
  </div>

   <div class="col-sm-3 text-center" style="overflow:initial">
   <input type="submit" class="btn btn-primary" formaction='<?php echo base_url()."pages/open_jobs/$job_id";?>'  name="<?php echo $job_id;?>" id="open" value="Open Job">
     <input type="hidden" name="job_status" id="job_status" value="<?php echo $job_status;?>">
  <input type="hidden" name="specified" id="specified" value="<?php echo $value['specified'];?>">
  <!--do not change the input order. Those are for jquery-->

  </div>
  </form>
  </div> 
  

  </div><!--close wrapper2-->
</div><!--close jobs_wrapper-->
<?php endforeach;?>
 


<div id="bottom_bar_div" class="col-sm-11"style="background:lightyellow;padding:8px;margin-left:4%;border:1px solid blue">
<div id="pagination" style="margin-left:40%">
<?php echo $pagination;?>
</div>
</div>