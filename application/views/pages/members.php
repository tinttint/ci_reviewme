<script>


$(document).ready(function(){
    
  $('#header_l_profile').css('background','#2874a6');
  $('#header_l_account').css('background','yellow');
})



</script>
<style>

.control-label
{
  padding-right:0px;
  
}
.form-group .form-control-static
{

  padding-left:0px;
  height:auto;

}
.panel-body label
{
  color:blue;
}
.form-group .form-control-static
{

 height:auto;
}
.inner 
{



  margin-right:5%;
  margin-left:5%;
}

</style>
 

<div class="container-fluid">


<div class="panel panel-default">
    <div class="panel-heading" style="background:#5dade2">
    <?php $f_name=$this->session->userdata('name');?>
    <h2 style="color:white;margin:0"> Welcome <?php  echo $f_name;?></h2>
    <h3 style="color:white">My Account</h3></div>
    <div class="panel-body" style="background:">
  
 <?php echo form_open('pages/update_company','method=get class=form-horizontal role=form');?>
   
<h3 style="color:red"><?php echo (isset($_GET['message'])?$_GET['message']:"");?></h3>
   <div class="panel panel-default inner" >
<div class="panel-heading text-center" style="background:lightblue">


Company Information

</div>

<div class="panel-body">

 <div class="row">
 <div class="col-sm-2">
<div class=" form-group form-group-lg  text-left"  >
       
        <?php $user_c = $account[0]['username'];
$logo=  $this->news_model->get_logo($user_c);?>
<img class="img-responsive img-thumbnail" src="<?php echo (empty($logo)?base_url().'images/icons/no-image.jpg':$logo);?>" style="width:200px;height:180px">
     <br/>(logo)
      
      </div>
</div>
 

<div class="col-sm-6">

     
<div class="form-group form-group-lg"   >
      <label class="control-label col-sm-5" style="">Username:</label>
      <div class="col-sm-7" style="">
        <p class="form-control-static"><?php echo $account[0]['username'];?> </p>
      </div>
    </div>
<div class="form-group form-group-lg">
      <label class="control-label col-sm-5" for="pwd">Full Name:</label>
      <div class="col-sm-7">          
        <p class="form-control-static"><?php echo $account[0]['name'];?></p>
      </div>
    </div>
             <div class="form-group form-group-lg">
      <label class="control-label col-sm-5" >Company Name:</label>
      <div class="col-sm-7">
        <p class="form-control-static"><?php echo $account[0]['c_name'];?></p>
      </div>
    </div>
     <div class="form-group form-group-lg">
      <label class="control-label col-sm-5" >Company Registration Number:</label>
      <div class="col-sm-7">
        <p class="form-control-static"><?php echo $account[0]['crn'];?></p>
      </div>
    </div>
     <div class="form-group form-group-lg">
      <label class="control-label col-sm-5" >Business Type:</label>
      <div class="col-sm-7">
        <p class="form-control-static"><?php echo $account[0]['b_type'];?> </p>
      </div>
    </div>
    <div class="form-group form-group-lg">
      <label class="control-label col-sm-4 col-sm-offset-1" >Registered Date:</label>
      <div class="col-sm-7">
        <p class="form-control-static"><?php $rd=date_create($account[0]['registered_date']);
        echo date_format($rd,'d-M-Y');?> </p>
      </div>
    </div>
    
      
    </div>
    
   





<div class="col-sm-4">     
   
    
    <!--  <div class=" form-group form-group-lg  text-left"  >
       
        <?php $user_c = $account[0]['username'];
$logo=  $this->news_model->get_logo($user_c);?>
<img class="img-responsive img-thumbnail" src="<?php echo (empty($logo)?base_url().'images/icons/no-image.jpg':$logo);?>" style="width:200px;height:180px">
     <br/>(logo)
      
      </div> -->
              
   

    
      <div class="form-group form-group-lg  cols-sm-8 text-left">

        <?php $user_c = $account[0]['username'];
$company_photo = $this->news_model->get_company_photo($user_c);?>
<img class="img-responsive img-thumbnail" src="<?php echo (empty($company_photo)?base_url().'images/icons/no-image.jpg':$company_photo);?>" style="width:480px;height:240px">
      <br/> (company photo)
      </div> 
   


    </div>
    </div>




 <div class="form-group form-group-lg">
      <label class="control-label col-sm-3" >Company Information:</label>
      <div class="col-sm-9">
        <p class="form-control-static" style=""><?php echo $account[0]['c_information'];?>  </p>
      </div>
    </div>  

</div><!--close inner panel body-->


</div><!--close inner panel-->


<div class="panel panel-default inner">
<div class="panel-heading text-center" style="background:lightblue">


Contact Information

</div>
<div class="panel-body">

<div class="row">
<div class="col-sm-6">
  <div class="form-group form-group-lg">
      <label class="control-label col-sm-4 col-sm-offset-1" >Phone:</label>
      <div class="col-sm-7">
        <p class="form-control-static"><?php echo $account[0]['phone'];?> </p>
      </div>
    </div>
 

    <div class="form-group form-group-lg">
      <label class="control-label col-sm-4 col-sm-offset-1" >Website:</label>
      <div class="col-sm-7">
        <p class="form-control-static"><?php echo $account[0]['website'];?> </p>
      </div>
    </div>
<div class="form-group form-group-lg">
      <label class="control-label col-sm-4 col-sm-offset-1" >Township(Address):</label>
      <div class="col-sm-7">
        <p class="form-control-static"><?php echo $account[0]['township'];?> </p>
      </div>
    </div>
 <div class="form-group form-group-lg">
      <label class="control-label col-sm-4 col-sm-offset-1" >Country(Address):</label>
      <div class="col-sm-7">
        <p class="form-control-static"><?php echo $account[0]['country'];?> </p>
      </div>
    </div>


   </div>


   
    <div class="col-sm-6">

<div class="form-group form-group-lg">
      <label class="control-label col-sm-4 col-sm-offset-1" >Email:</label>
      <div class="col-sm-7">
        <p class="form-control-static"><?php echo $account[0]['email'];?> </p>
      </div>
    </div>


   

  
    <div class="form-group form-group-lg">
      <label class="control-label col-sm-4 col-sm-offset-1" >Address:</label>
      <div class="col-sm-7">
        <p class="form-control-static"><?php echo $account[0]['address'];?> </p>
      </div>
</div>


<div class="form-group form-group-lg">
      <label class="control-label col-sm-4 col-sm-offset-1" >City(Address):</label>
      <div class="col-sm-7">
        <p class="form-control-static"><?php echo $account[0]['city'];?> </p>
      </div>
    </div>

</div>
</div>




</div>
</div>

    
    <div style="text-align:center">
  <?php echo form_submit('update','Update','class=btn btn-primary');?>
  </div>
  
  </form>
  </div><!-- close panel body-->
  </div><!--close panel-->
</div><!-- close container-->
















