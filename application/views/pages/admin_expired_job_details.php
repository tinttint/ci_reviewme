<?php $is_logged_in = $this->session->userdata('is_logged_in');
$role = $this->session->userdata('role');
		
		if(!isset($is_logged_in) || $is_logged_in != true||($role!=='admin'))
		{
			echo "You don't have permission to access this page.".  
			anchor('admin_role','Login');
			exit();

		}
		?>
<script>




$(document).ready(function(){
    
  $('#admin_h_l_expired').css('background','yellow');
 $('#admin_h_l_jobs').css('background','#2874a6');
})




</script>
<style>



.form-group .form-control-static
{

  padding-left:0px;
  height:auto;
}
.control-label
{

  padding-right:0px;
}
.list-group-item
{

  background:ivory;
}
.panel .panel
{



  margin-left:4%;
  margin-right:4%;
}



hr
{



  border:1px solid lightgray;
  margin:0;
  margin-bottom:10px;
}


@media (min-width: 799px) {
   .panel .panel .information label,.panel .panel .contact label
{
  margin-left:40px;
}
   
}

</style>
<?php



$back_url=(empty($back_url)?base_url().'admin_role/display_expired_jobs':$back_url);

echo form_open($back_url,'method=get');
?>


       <?php 
       
       if(isset($checked))
       {
       foreach($checked as $keys=>$value):?>

        <input type="hidden" name="checked[]" value="<?php echo $value;?>">
       <?php endforeach;
       }

      
     
       

    
    ?>
<div class="container-fluid">
<div class="panel panel-default">

<div class="panel-heading" style="background:#5dade2">
<input type="submit" value="" style='float:left;background-image:url("<?php echo base_url(). 'images/icons/back-arrow2.png';?>");background-size:60px 40px;width:60px;height:40px;border:none;background-color:transparent;background-repeat:no-repeat'>
 <?php echo form_close();?>
<?php $user=$this->session->userdata('username');?>
<h2 style="color:white;padding-right:60px;margin:0">Welcome <?php echo $user;?></h2>
<h3 style="color:white">Expired Job Details</h3>


</div>
<div class="panel-body" style="background:ivory">
<?php $ask=array('onsubmit'=>"return confirm('Delete Job')",'class'=>'form-horizontal','role'=>'form');?>
<?php echo form_open('',$ask);?>
  
 
 <div class="panel panel-default">
 <div class="panel-heading text-center" style="background:lightblue">

    Job Information

 </div>
 <div class="panel-body">

 

 <br/>
<div class="row" style="margin-bottom:20px" >
<div class="col-sm-2 text-center" style="margin:0px;float:none;display:inline-block;vertical-align:bottom;" >
     
        <?php 
                 $logo=$job['logo'];         ?>
<img class="img-responsive img-thumbnail" src="<?php echo (empty($logo)?base_url().'images/icons/no-image.jpg':$logo);?>" style="width:200px;height:180px">
     (logo)  
      

      
     
   
     </div>





<div class="col-sm-5" style="margin:0px;float:none;display:inline-block;vertical-align:bottom;">
<div class="form-group form-group-lg" >

      <label class=" col-sm-4" style="">Job Id:</label>
      <div class="col-sm-8 info" style="">
        <p ><?php echo $job['id'];?> </p>
      </div>
    </div>
    <div class="form-group form-group-lg">
      <label class=" col-sm-4 " for="pwd">Company Name:</label>
      <div class="col-sm-8 info">          
        <p style="font-size:16px"><?php echo $job['c_name'];?></p>
      </div>
    </div>   
    <div class="form-group form-group-lg">
      <label class=" col-sm-4 " >Job Title:</label>
      <div class="col-sm-8 info">
        <p ><?php echo $job['title'];?> </p>
      </div>
    </div>
            <div class="form-group form-group-lg">
      <label class=" col-sm-4 " >Job Category:</label>
      <div class="col-sm-8 info">
        <p ><?php echo $job['category'];?> </p>
      </div>
    </div>






    </div>



     
    
<div class="col-sm-4" style="margin:0px;padding:0px;background:#e6f3f7;float:none;display:inline-block;vertical-align:bottom" >

 <div class="col-sm-12 text-center">
        <?php
$company_photo = $job['photo'];?>
<img class="img-responsive img-thumbnail" src="<?php echo (empty($company_photo)?base_url().'images/icons/no-image.jpg':$company_photo);?>" style="width:480px;height:240px">
      (company photo)
     </div>  

     </div>



     </div>
    


<hr style="border:1px solid lightgray">
    <div class="row" >
    <div class="col-sm-4">
<div class="form-group form-group-lg">
      <label class="control-label col-sm-4 " >Job Salary:</label>
      <div class="col-sm-7 info">
        <p class="form-control-static"><?php echo $job['salary'];?> </p>
      </div>
    </div>   
    </div> 

    <div class="col-sm-4">
<div class="form-group form-group-lg">
      <label class="control-label col-sm-4 " style="">Job Type:</label>
      <div class="col-sm-7 info" style="">
        <p class="form-control-static"><?php echo $job['job_type'];?> </p>
      </div>
    </div>
    </div>
    
<div class="col-sm-3">
 <div class="form-group form-group-lg">
      <label class="control-label col-sm-6 " >Published Date:</label>
      <div class="col-sm-6 info">
        <p class="form-control-static"><?php $pd=date_create($job['published_date']);
        echo date_format($pd,'d-M-Y');?> </p>
      </div>
    </div>
    </div>


    </div>
    <hr style="border:1px solid lightgray">
    <div class="row">
<div class="col-sm-4">
  <div class="form-group form-group-lg">
      <label class="control-label col-sm-4 " >Job Status:</label>
      <div class="col-sm-7 info">
        <p class="form-control-static"><?php echo $job['job_status'];?></p>
      </div>
    </div> 

    </div>
   

    
   <div class="col-sm-4">                  
    <div class="form-group form-group-lg">
      <label class="control-label col-sm-5">Apply Valid To:</label>
      <div class="col-sm-7 info">
        <p class="form-control-static"><?php $vt=date_create($job['valid_to']);
        echo date_format($vt,'d-M-Y');?> </p>
      </div>
    </div>
    </div>



    <div class="col-sm-4">
  <div class="form-group form-group-lg">
      <label class="control-label col-sm-5" >Job Opened Date:</label>
      <div class="col-sm-7 info">
        <p class="form-control-static"><?php $op=date_create($job['opened_date']);
        echo date_format($op,'d-M-Y');?> </p>
      </div>
    </div>
   
</div>
    </div>
<hr>
    







          
<div class="information">
<div class="form-group form-group-lg" style="">
      <label class=" col-sm-2" style="">Company Information:</label>
      <div class="col-sm-8 info" style="">
        <p class=""><?php echo $job['c_information'];?> </p>
      </div>
    </div>
   
   
    <div class="form-group form-group-lg">
      <label class="col-sm-2" for="pwd">Job Descriptions:</label>
      <div class="col-sm-8 info">          
        <p class=""><?php echo $job['descriptions'];?></p>
      </div>
    </div>
   
   
            <div class="form-group form-group-lg">
      <label class=" col-sm-2" >Job Responsibilities:</label>
      <div class="col-sm-8 info">
        <p class=""><?php echo $job['responsibilities'];?> </p>
      </div>
    </div>
    
   
      <div class="form-group form-group-lg">
      <label class="col-sm-2">Job Requirements:</label>
      <div class="col-sm-8 info">
        <p class=""><?php echo $job['requirements'];?> </p>
      </div>
    </div>



    </div>


 </div><!--close inner panel body-->



 </div><!--close inner panel-->



 <div class="panel panel-default">
<div class="panel-heading text-center" style="background:lightblue">

Contact Information

 </div>
<div class="panel-body contact">


<div class="row">
<div class="col-sm-6">


<div class="form-group form-group-lg">
      <label class=" col-sm-4 " >Phone:</label>
      <div class="col-sm-7 info">
        <p class=""><?php echo $job['phone'];?> </p>
      </div>
    </div>


 <div class="form-group form-group-lg">
      <label class=" col-sm-4 " >Email:</label>
      <div class="col-sm-7 info">
        <p class=""><?php echo $job['email'];?> </p>
      </div>
    </div>     
      <div class="form-group form-group-lg">
      <label class=" col-sm-4 " >Township(Address):</label>
      <div class="col-sm-7 info">
        <p class=""><?php echo $job['township'];?> </p>
      </div>
    </div>
     <div class="form-group form-group-lg">
      <label class=" col-sm-4 " >Country(Address):</label>
      <div class="col-sm-7 info">
        <p class=""><?php echo $job['country'];?> </p>
      </div>
    </div>


</div>
<div class="col-sm-6">

 <div class="form-group form-group-lg">
      <label class=" col-sm-3 " >Website:</label>
      <div class="col-sm-8 info">
        <p class=""><?php echo $job['website'];?> </p>
      </div>
    </div> <div class="form-group form-group-lg">
      <label class=" col-sm-3 " for="pwd">Address:</label>
      <div class="col-sm-8 info">          
        <p class=""><?php echo $job['address'];?></p>
      </div>
    </div>
    <div class="form-group form-group-lg">
      <label class=" col-sm-3 ">City(Address):</label>
      <div class="col-sm-8 info">
        <p class=""><?php echo $job['city'];?> </p>
      </div>
    </div>
</div>
</div>

 

 </div>
 </div>
 
<?php 
   
   $job_id=$job['id'];
   
       
 // $back_url=(empty($back_url)?base_url().'pages/display_job_entries':$back_url);
    if(isset($checked))
       {
       foreach($checked as $keys=>$value):?>

        <input type="hidden" name="checked[]" value="<?php echo $value;?>">
       <?php endforeach;
       };?>
  <input type="hidden" id="previous_url" name="previous_url" value="<?php echo $back_url;?>">
    <div style="text-align:center">
    <input type="submit" class="btn btn-primary" formaction='<?php echo base_url()."admin_role/delete_expired_jobs";?>' id="delete" name="delete" value="Delete Job">
    <input type="hidden" name="job_id" value="<?php echo $job_id;?>">
    </div>
    </form> 
</div><!--close panel body-->
</div><!--close panel-->
</div><!--close container-->
  

