<form method="POST">
<div class="col-sm-5 table-responsive" style="border:1px solid blue">
<h3 style="color:blue">Table 1</h3>
<h3 style="color:blue">New Category From Users</h3>

<h3 style="color:blue">Total Records Found <?php echo $other_categories_tr;?></h3>
<?php $others_url= "admin_role/display_other_categories";?>
<input type="hidden" id="others_url" value="<?php echo $others_url;?>">
<table class="table text-center table-bordered" style="text-align:center">
    <thead class="text-center">
     <tr><input type="button" id="refresh_button" class="btn btn-primary" value="Refresh Table">
     </tr> 
      <tr>
        <th <?php if($sort_by2 =='category')echo "class=sort_$sort_order2";?>><?php echo 
anchor("admin_role/display_other_categories/$q/category/". (($sort_order2 == 'asc' && $sort_by2 == 'category')?'desc'
: 'asc')."/".$limit,'Category');?></th>
      
        <th <?php if($sort_by2 =='added_date')echo "class=sort_$sort_order2";?>><?php echo 
anchor("admin_role/display_other_categories/$q/added_date/". (($sort_order2 == 'asc' && $sort_by2 == 'added_date')?'desc'
: 'asc')."/".$limit,'Found Date');?></th>
      
        
        <th <?php if($sort_by2 =='status')echo "class=sort_$sort_order2";?>><?php echo 
anchor("admin_role/display_other_categories/$q/status/". (($sort_order2 == 'asc' && $sort_by2 == 'status')?'desc'
: 'asc')."/".$limit,'Status');?></th>
         <th>Delete</th>  
 
      </tr>
    </thead>
    <tbody>
    <?php foreach($other_categories as $values):?>
      <tr>
        <input type="hidden" id="id" value="<?php echo $values['id'];?>">
        <td><?php echo $values['category'];?></td>

        
        <td><?php $ad=date_create($values['added_date']);
        echo date_format($ad,'d-M-Y');?></td>
        <td>
        
        <?php 
         echo "<span style=color:red>".$values['status'] . "</span>";
         echo "<br>";
         $status_url=$this->uri->uri_string();
       //  $id=$values['id'];
         echo form_dropdown($values['id'],array('change_status'=>'change status','added'=>'added','edited and added'=>'edited and added'
        ,'no need to add'=>'no need to add'),'change_status','id=status');?>
        <input type="hidden" id="status_url" name="status_url" value="<?php echo $status_url;?>"></td>
          <td>
        <?php $others_id=$values['id'];?>
            <button type='button' style="background-color:transparent" id="delete_others" name="<?php echo $others_id;?>" value="Delete"><img src="<?php echo base_url().'images/icons/trash.png';?>" width="35" height="35">
          </button>
        </td>
      </tr>
     <?php endforeach;?>
    </tbody>
  </table>
  <div class="col-sm-12 text-center">
   <?php echo $pagination2;?>
   </div>

</div><!--close column-->
</form>