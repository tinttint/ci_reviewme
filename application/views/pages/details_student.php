<?php
$is_logged_in = $this->session->userdata('is_logged_in');
$role=$this->session->userdata('role');
    
    if(!isset($is_logged_in) || $is_logged_in != true|| ($role!=="admin"))
    {
/*$this->load->view('templates/Cheader');
                $this->load->view('pages/members',$data);*/

                  // $this->load->view('templates/footer');
      echo "You don't have permission to access this page.";  
      echo anchor('admin_role','login');
             exit();
    }
?>    


<script>


$(document).ready(function(){
    
  $('#admin_h_l_profile').css('background','#2874a6');
  $('#admin_h_l_account').css('background','yellow');
})
$(document).ready(function(){
     $('#status').on('change','#drop',function(e){
      e.preventDefault();
    
           var user=$(this).attr('name');
        
           var url=$('#url').val();
          var status = $(this).val();

         
             if(status=="Approved")
             {
              
                result = confirm("Approve Company(s)?\n\n Approval confirmation email(s) will be sent to the student(s)");
             }
              if(status =="Disapproved")
             {
                 $('#myModal').modal();
                //  $('#message_modal').val('');
             var student_id=$('#student_id').val();
                          $('#id_modal').val(student_id);
                          var nrc=$('#nrc').val();
                          $('#nrc_modal').val(nrc)
                         // var message="something";
                          var message= 'You have been disapproved from Job Career Development Site as your student\n'
     +' id or national registration card no do not exist in our database.\n'
       +' Student Registration Number:'+student_id+'\n'
       + 'National Registration Card No:'+nrc;
                        
                        $('#message_modal').val(message);
                          $('#user_modal').val(user);
                  $('#drop').val('Disapproved');
                  return false;
             }
             else if(status=="New")
             {

              result=confirm('Change Status?');
             }
             
             
             if(result ==false)
             {
                 $('#drop').val('Change_Status');
              
              return false;
             }
                    $('#progress').css('display','block');
                $.ajax({
                  url:"<?php echo base_url() .'admin_role/change_status';?>",
                  type:"POST",
                  beforeSend:function(){
                   //// $('#loading_message').show();
                  }, 
                  
                  data:{status:status,user:user,url:url},
                  dataType:'json',
                  success:function(result)
                  {

                       //  var element=$.parseHTML(result);
                        // var element=$(element).filter('#some').html();
                    
                       if(result.message=="false")
                       {
                         $('#progress').hide();
                         $('#drop').val('Change_Status');
                         alert("Can Not Change Status. Already "+ status);

                        

                       }
                        else if(result.message=="email_error")
                       {
                            $('#progress').hide();
                          alert("Can Not Approve. Email Error."
                            +"\nCheck Internet Connection");


                       }

                       else if(result.message=="true")
                       {

                        $('#progress').hide();
                        if(status=="Approved"||status=="Disapproved")
                             {
                                  alert('Email Sent');
                             }
                          }
                         else
                         {


                         }


                      
                        $('#drop').val('Change_Status');
                        window.location.href="<?php echo base_url();?>"+url;


                  }
                  
                     ////$('#practice').html(result); 

                  
               

                });

     });

});
$(document).ready(function(){

$('#send_modal').click(function(e){
  e.preventDefault();
  var answer=confirm("send email?");
  if(answer==false)
  {
    return;
  }

     
    var message=$('#message_modal').val();
   
    var user=$('#user_modal').val();
  
    var url=$('#url').val();

    

        $('#progress').css('display','block');
    
  //  var id=$('#id_modal').val();
   //var category="sport";
  
       $.ajax({

                  url:"<?php echo base_url() .'admin_role/disapprove_student';?>",
                  type:"POST",
                  
                  data:{message:message,user:user,url:url},
                  dataType:"json",
                  success:function(result)
                  { 
                    
                    $('#practice').html(result);
                   
                    if(result.message=="false")
                      {
                        $('#progress').css('display','none');
                        alert('Can Not Send Email. Already Been Disapproved.');
                       }
                         else if(result.message=="email_error")
                        {
                          $('#progress').css('display','none');
                          alert('Can not disapprove.Email Error'
                            +'\nCheck Internet Connection')
                         // window.location="<?php echo base_url().'admin_role/d_s_email_error';?>";
                        }
                       else if(result.message=="true")
                       {
                          $('#progress').css('display','none');
                          alert('Disapproved Email Sent');
                        
                    

                
                           
                       }
                        window.location.href="<?php echo base_url();?>"+url;

                  }
                });

});


});
$(document).ready(function(){

  $('#cancel_modal').click(function(){
    $('#drop').val('Change_Status');
   // $('#progress').css('display','none');
   
  })
})

</script>
<style>

.control-label
{
  padding-right:0px;
}
.form-group .form-control-static
{
  padding-left:0px;  
  height:auto;

}



#drop option[selected]
{
    display:none;
    color:gray;

}
#drop{

  color:red;
  font-size:16px;
}

.progress{
height:70px;


width:50%;
position:fixed;
top:60%;
left:25%;
z-index:1;
pointer-events:none;


}

.progress-bar {
  padding:15px;
}
hr
{

  border-color:lightgray;
  margin:0px;
  margin-bottom:8px;
}
@media (min-width: 799px) {
   .panel .panel .form-group label
{
  margin-left:25px;
}
   
}

/*.panel .panel .row.row1
{
  width:100%;
 height:auto;
 display:-moz-flex;
 display:flex;
 flex-wrap:wrap;
 

}
.panel .panel .row1 .column1
{

  -moz-flex:1;
  flex:1;
  flex-basis:75%;
  
}
.panel .panel .row1 .column2
{

  -moz-flex:1;
  flex:1;
  flex-basis:25%;
}*/

</style>

 

    
<div id="practice">



</div>


<div class="progress" id="progress" style="display:none">
 
  <div  class="progress-bar progress-bar-striped active" role="progressbar"
  aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="font-size:24px;width:100%">
    Please Wait
  </div>
  
</div>

<div id="myModal" class="modal fade" role="dialog" style="">
  <div class="modal-dialog" style="">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header text-center">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Disapprove Student</h4>
        <p>(Add your own message or send this default message)</p>
        <div class="col-sm-12">
        <div class="col-sm-6">

        Student Registration Number:<input type="text" name="id_modal" id="id_modal" value="" style="padding-left:5px" readonly>
        </div>
        <div class="col-sm-6">
        National Registration Card No:
        <input type="text" name="nrc_modal" id="nrc_modal" value="" style="padding-left:5px" readonly>
        </div>
        </div>

      </div><!--close modal-header-->
      <div class="modal-body">
         <div class="form-group">
         <input type="hidden" name="user_modal" id="user_modal" value="">
        
      
        <textarea name="message_modal" rows="5" id="message_modal" class="form-control">
           
        </textarea>
      
       
        </div>
      </div>
      <div class="modal-footer">
       <input type="submit" class="btn btn-default" value="Send"  name="send_modal" id="send_modal">
        <button type="button" class="btn btn-default" name="cancel_modal" id="cancel_modal" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>
<?php
$back_url=(empty($back_url)?base_url().'admin_role/display_students':base_url().$back_url);

echo form_open($back_url,'method=get');?>


      

<div class="container-fluid">
<div class="panel panel-default">
<div class="panel-heading" style="background:#5dade2">
  <input type="submit" value="" style='float:left;background-image:url("<?php echo base_url(). 'images/icons/back-arrow2.png';?>");background-size:60px 40px;width:60px;height:40px;border:none;background-color:transparent;background-repeat:no-repeat'>
      

<?php  echo form_close();?>
<?php $f_name=$this->session->userdata('name');?>

<h2 style="color:white;padding-right:60px;margin:0">Welcome <?php echo $f_name;?></h2>
<h3 style="color:white">Student Details</h3>


</div>
<div class="panel-body" style="background:ivory">
<?php echo form_open('','class=form-horizontal role=form');?>
                <div id="status" class="text-center">
              <label>Status:</label><span style="font-size:16px"><?php echo $account[0]['status']."<br>";?></span>
             <?php 
              $url=$this->uri->uri_string();

              $student=$account[0]['user'];echo form_dropdown($student,array('Change_Status'=>'Change Status','New'=>'New','Disapproved'=>'Disapproved','Approved'=>'Approved'),'Change_Status',"style=height:34px;width:140px
     id=drop ");?>
     <input type="hidden" name="url" id="url" value="<?php echo $url;?>">

     </div>
     <br/><br/>

     
    
<div class="panel panel-default">
<div class="panel-heading text-center" style="background:lightblue">

Personal Information

</div>   
<div class="panel-body">
   <div class="row row1">
   <div class="col-sm-8 column1">
   <div class="row">
   <div class="col-sm-6">
   
    <div class="form-group form-group-lg">
      <label class=" col-sm-4">Username:</label>
      <div class="col-sm-5">
        <p class="" style=""><?php echo $account[0]['user'];?> </p>
      </div>
    </div>

    </div>
    

<div class="col-sm-6">
   <div class="form-group form-group-lg">
      <label class="col-sm-4" for="pwd">Full Name:</label>
      <div class="col-sm-7">          
        <p class=""><?php echo $account[0]['name'];?></p>
      </div>
    </div>
    

   </div>
   </div>
   <hr>

    




   
      <div class="form-group form-group-lg">
      <label class=" col-sm-4"  >Student Registration Number:</label>
      <div class="col-sm-7" >
        <p class=""><?php echo $account[0]['id'];?> 
        </p>
      </div>
    </div>
    
    <hr > 
    
            <div class="form-group form-group-lg">
      <label class=" col-sm-4 " >National Registration Card No:</label>
      <div class="col-sm-7">
        <p class=""><?php echo $account[0]['nrc'];?> </p>
      </div>
   
    </div>
    
    <hr>
   
 <div class="form-group form-group-lg">
      <label class=" col-sm-4" >Major:</label>
      <div class="col-sm-7">
        <p class=""><?php echo $account[0]['major'];?> </p>
      </div>
    </div>
    <hr>     
    <div class="form-group form-group-lg">

      <label class=" col-sm-4 " style="">Qualifications/ Degrees:</label>
      <div class="col-sm-7 ">
        <p class=""><?php echo $account[0]['qualifications'];?> 
       </p>
      </div>
    </div>
 <hr>
 <div class="row">
 <div class="col-sm-6">
 <div class="form-group form-group-lg">
      <label class=" col-sm-4">Gender:</label>
      <div class="col-sm-7">
        <p class=""><?php echo $account[0]['gender'];?> </p>
      </div>
    </div>
    </div>


<div class="col-sm-6">
    <div class="form-group form-group-lg">
      <label class="col-sm-4" style="">Nationality:</label>
      <div class="col-sm-7">
        <p class=""><?php echo $account[0]['nationality'];?> </p>
      </div>
    </div>
</div>
</div>
<hr>
<div class="row">
 <div class="col-sm-6">
  <div class="form-group form-group-lg">
      <label class=" col-sm-4"  >Date Of Birth:</label>
      <div class="col-sm-7">
        <p class=""><?php $date_o_b=date_create($account[0]['date_o_b']);
        echo date_format($date_o_b,'d-M-Y');?> </p>
      </div>
    </div>
    </div>


<div class="col-sm-6">
     <div class="form-group form-group-lg">
      <label class="col-sm-4" >Marital Status:</label>
      <div class="col-sm-7">
        <p class=""><?php echo $account[0]['m_status'];?> </p>
      </div>
    </div>
</div>
</div>
<hr>
  </div>

  
     




                           
   
    <div class="col-sm-4 column2" style="height:100%;border:1px solid lightgray">
<div class="form-group form-group-lg" style="height:auto">
      <label class="control-label col-sm-4">Photo:</label>
      <div class="col-sm-7">
        <?php $student = $account[0]['user'];
$photo_path = $this->news_model->get_photo_path($student);?>
<img class="img-thumbnail" src="<?php echo (empty($photo_path)?base_url().'images/icons/Blank-photo.png':$photo_path);?>"
style="width:200px;height:180px">
      </div>
    </div>


 
     
     <div class="form-group form-group-lg">
      <label class="control-label col-sm-5" >Registered Date:</label>
      <div class="col-sm-6">
        <p class="form-control-static"><?php $registered_date=date_create($account[0]['registered_date']);
        echo date_format($registered_date,'d-M-Y');?> </p>
      </div>
    </div>
   
          
         


    </div>
    </div><!--close row-->


     
    
    
 

    
             
    


   

    
   

    

    </div><!--close inner panel body-->
    </div><!--close panel body-->


    
       
      
     
   
    
    
   
    
   <div class="panel panel-default">
   <div class="panel-heading text-center" style="background:lightblue">

        Contact Information

   </div>
   <div class="panel-body">
    
    <div class="row">
    <div class="col-sm-6">
    <div class="form-group form-group-lg" >
      <label class=" col-sm-2" style="" >Phone:</label>
      <div class="col-sm-9">
        <p class=""><?php echo $account[0]['phone'];?> </p>
      </div>
    </div>
    <hr>
  <div class="form-group form-group-lg">
      <label class=" col-sm-2"style="" >Address:</label>
      <div class="col-sm-9">
        <p class=""><?php echo $account[0]['address'];?> </p>
      </div>
    </div>
    <hr>

    <div class="form-group form-group-lg">
      <label class=" col-sm-2"style="" >City(Address):</label>
      <div class="col-sm-9">
        <p class=""><?php echo $account[0]['city'];?> </p>
      </div>
    </div>
    </div>                         
                                      
          
    
    <div class="col-sm-6">
   
<div class="form-group form-group-lg">
      <label class="col-sm-3" >Email:</label>
      <div class="col-sm-8">
        <p class=""><?php echo $account[0]['email'];?> </p>
      </div>
    </div>
    <hr>

    <div class="form-group form-group-lg">
      <label class=" col-sm-3" >Township(Address):</label>
      <div class="col-sm-8">
        <p class=""><?php echo $account[0]['township'];?> </p>
      </div>
    </div>
    <hr>

   
    <div class="form-group form-group-lg">
      <label class=" col-sm-3" >Country(Address):</label>
      <div class="col-sm-8">
        <p class=""><?php echo $account[0]['country'];?> </p>
      </div>
    </div>
    </div>
    </div>
   



    </div>
    </div>



    <div class="panel panel-default">
    <div class="panel-heading text-center" style="background:lightblue">

     Work Information          

    </div>
    <div class="panel-body">



     <div class="form-group form-group-lg">
      <label class=" col-sm-2">Work Experience:</label>
      <div class="col-sm-8">
        <p class=""><?php echo $account[0]['experience'];?> </p>
      </div>
    </div>




    </div><!--close inner panel body-->
    </div><!--close inner panel-->

   
  </form>
  </div><!--close panel body-->
  </div><!--close panel-->
  </div><!--close container-->






