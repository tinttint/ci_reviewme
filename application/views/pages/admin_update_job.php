<?php $is_logged_in = $this->session->userdata('is_logged_in');
     $role=$this->session->userdata('role');
    if(!isset($is_logged_in) || $is_logged_in != true || $role!=='admin')
    {
/*$this->load->view('templates/Cheader');
                $this->load->view('pages/members',$data);*/

                  // $this->load->view('templates/footer');
      echo "You don't have permission to access this page.";  
      echo anchor('admin_role','Login');
      exit();

    }
    ?>



<style>



#vf_day,#vf_month,#vf_year,#vt_day,#vt_month,#vt_year
{

  text-align:center;
}
span
{

  color:red;
}



.panel .panel-body input:focus
{
     border-color:blue;


  }
  .panel .panel-body input[type=text]
     {
        border-style:solid;
       /* background:transparent;*/
         
      border-width:0px 0px 1px 0px;
     
     border-color:gray;
      box-shadow:none;
      border-radius:0px;
     }
     .form-horizontal .form-group .form-control 
     {
       
      padding-bottom:0px;
      padding-top:16px;
      
      
      font-size:20px;
     }
     .form-horizontal .form-group .control-label
     {

      padding-top:18px;
      width:auto;
    /*  padding-left:8px;*/
     }
     .panel .panel
     {
         margin-left:5%;
         margin-right:5%;
         border-color:lightgray;

     }
      .form-horizontal .form-group select
      {

       /* padding-top:18px;*/

        text-align:center;
      }
      .valid-to span
      {

        color:black;
      }



</style>
<script>

$(document).ready(function(){
    
$('#admin_h_l_entries').css('background','yellow');
 $('#admin_h_l_jobs').css('background','#2874a6');
})
$(document).ready(function(){
   $('#panel').on('change','#category',function(){
         var selected=this.value;
      
         if(selected=="Other(Specify)")
         {

          $('#s_category_div').css('display','block');
         }
         else
         {
          $('#s_category').val('');
          $('#s_category_div').css('display','none');
         }

   })

    $('#panel').on('change','#city',function(){
         var selected=this.value;
         
         if(selected=="Other(Specify)")
         {

          $('#s_city_div').css('display','block');
         }
         else
         {
          $('#s_city').val('');
          $('#s_city_div').css('display','none');
         }

   })


})
$(document).ready(function(){
  var category =$('#category').val();
  if(category=="Other(Specify)")
  {

    $('#s_category_div').css('display','block');
  }
  var city=$('#city').val();
  if(city=="Other(Specify)")
        {

          $('#s_city_div').css('display','block');


        }


$(document).ready(function(){
   var applicants=$('#applicants').val();
   if(applicants>=1)
   {
  $(':input:not(#vt_day,#vt_month,#vt_year)').attr('readonly',true);
    $("#category_div").hide();
    $("#category_text_div").show();
   // $("#category_text_div").prop('disabled',false);
    $("#salary_div").hide();
    $("#salary_text_div").show();
    //$("#salary_text_div").prop('disabled',false);
    $("#job_type_div").hide();
    $("#job_type_text_div").show();
    //$("#job_type_text_div").prop('disabled',false);
    $("#city_div").hide();

    $("#city_text_div").show();
    //$("#city_text_div").prop('disabled',false);
   }
   else
   {
      
 $("#category_text_div input").prop('disabled',true);
  $("#salary_text_div input").prop('disabled',true);
  $("#job_type_text_div input").prop('disabled',true);
  $("#city_text_div input").prop('disabled',true);

   }


})

})
</script>

<script>




</script>
<?php



$back_url=(empty($back_url)?base_url().'admin_role/display_job_entries':$back_url);

echo form_open($back_url,'method=get');
?>


       <?php 
       if(isset($checked))
       {
       foreach($checked as $keys=>$value):?>

        <input type="hidden" name="checked[]" value="<?php echo $value;?>">
       <?php endforeach;
       }

        
    
 


 $user = $this->session->userdata('username');

?>


</style>

<div class="container-fluid">
<div class="panel panel-default">
<div class="panel-heading" style="background: #5dade2">
 <input type="submit" value="" style='float:left;background-image:url("<?php echo base_url(). 'images/icons/back-arrow2.png';?>");background-size:60px 40px;width:60px;height:40px;border:none;background-color:transparent;background-repeat:no-repeat'>
      

<?php echo form_close();
    
  $f_name=$this->session->userdata('name');?>
<h2 style="color:white;padding-right:60px;margin:0">Welcome <?php echo $f_name;?></h2>
<h3 style="color:white">Update Job</h3>



</div>
<div class="panel-body" style="background:ivory" id="panel">
<?php

$ask=array('onsubmit'=>"return confirm('Update Job?')",'class'=>'form-horizontal','role'=>'form'
  ,'id'=>'update_form'); 
$job_id=(empty($job['id'])?$job_id:$job['id']);
echo form_open_multipart("admin_role/update_job/$job_id",$ask);?>
<input type="hidden" name='applicants' id="applicants" value="<?php echo $applicants;?>">
 <?php if(isset($checked))
       {
       foreach($checked as $keys=>$value):?>

        <input type="hidden" name="checked[]" value="<?php echo $value;?>">
       <?php endforeach;
       }

       ?>

<input type="hidden" name="back_url" value="<?php echo $back_url;?>">
<div class="form-group form-group-lg">
<div class="col-sm-2 text-center">
<?php $logo=(empty($job['logo'])?$logo:$job['logo']);?>
<input type="hidden" name="logo" value="<?php echo $logo;?>">
<img class="img-thumbnail" style="width:200px;height:150px"src="<?php echo(empty($logo)?base_url().'images/icons/no-image.jpg':$logo);?>">
<p>(edit logo in account page.)</p>
</div>
</div>
<div class="text-center" style="">
<?php 
$photo=(!isset($job['photo'])?$photo:$job['photo']);?>
<input type="hidden" name="photo" value="<?php echo $photo;?>">
<img class="img-responsive img-thumbnail center-block" src="<?php echo (empty($photo)?base_url().'images/icons/no-image.jpg':$photo);?>" style="width:500px;height:250px">
<p>(edit university photo in account page.)</p>
  </div>
  
  <h3 style="color:red"><?php echo (empty($message)?'':$message);?></h3>
  <h4 style="color:red"><?php echo (empty($message2)?'':$message2);?></h4>
  <h3><?php echo "Job Id: " .(empty($job['id'])?$job_id:$job['id']);?></h3>
  <input type="hidden" name="job_id" value="<?php echo (empty($job['id'])?$job_id:$job['id']);?>">
  
 <div class="panel panel-default">
   <div class="panel-heading" style="background:lightblue">

    <h4 style="color:blue">Job Information</h4>

   </div>
   <div class="panel-body">


    <div class="form-group form-group-lg" >
   <label class="control-label col-sm-2" >* Company Name</label>
  <div class="col-sm-5" style="">
  <?php echo form_input('c_name',(empty($job)?set_value('c_name') :$job['c_name']),'id=c_name class=form-control');?>
  
  <?php echo form_error('c_name');?>
  </div>
  </div>
  
<div class="form-group form-group-lg"    id="category_div">
<br/>
  <label class="control-label col-sm-2">* Job Category:</label>
  <div class='col-sm-4'>
  <?php echo form_dropdown('category',$categories,(empty($job)?set_value('category'):$job['category']),'class=form-control id=category');?>
  
  <?php echo form_error('category')?>
  </div>
   <div id="s_category_div" style="display:none">
  <label class="control-label col-sm-2">* Specify Category:</label>
  <div class='col-sm-4'>
  <?php echo form_input('s_category',set_value('s_category'),'class=form-control id=s_category');?>
  <?php echo "If validated,you can update to this category after 1 week";?>
  <br/>

  <?php echo form_error('s_category')?>
  
  
  
  </div>
  </div>
  </div>  

   
 <div class="form-group form-group-lg" id="category_text_div" style="display:none">
  <label class="control-label col-sm-2">* Job Category:</label>
  <div class='col-sm-2'>
  <?php echo form_input('category',(empty($job)?set_value('category'):$job['category']),'class=form-control id=category_text');?>
  
  <?php echo form_error('category')?>
  

  </div>
  </div>
  
   

  

 
   
   
<div class="form-group form-group-lg ">
  <label class="control-label col-sm-2">* Job Title:</label>
  <div class="col-sm-7">
  <?php echo form_input('title',(empty($job)?set_value('title'):$job['title']),'class=form-control');?>
  
  <?php echo form_error('title')?>
  </div>
  </div>
  <br/>
  <div class="form-group form-group-lg " id="salary_div">
  <label class="control-label col-sm-2">* Salary:</label>
  <div class="col-sm-4">
  <?php echo form_dropdown('salary',$salaries,(empty($job)?set_value('salary'):$job['salary']),'class=form-control');?>

  <?php echo form_error('salary');?>
  </div>
  <div id="job_type_div">
  <label class="control-label col-sm-2">* Job Type:</label>
  <div class="col-sm-4">
  <?php echo form_dropdown('job_type',$job_types,(empty($job)?set_value('job_type'):$job['job_type']),'class=form-control');?>
  
    <?php  echo form_error('job_type');?>
    </div>
    </div>
  </div>

  <div class="form-group form-group-lg" id="salary_text_div" style="display:none">
  <label class="control-label col-sm-2">* Salary:</label>
  <div class="col-sm-4">
  <?php echo form_input('salary',(empty($job)?set_value('salary'):$job['salary']),'class=form-control');?>
  
  <?php echo form_error('salary');?>
  </div>

    <div id="job_type_text_div" style="display:none">
  <label class="control-label col-sm-2">* Job Type:</label>
  <div class="col-sm-4">
  <?php echo form_input('job_type',(empty($job)?set_value('job_type'):$job['job_type']),'class=form-control ');?>
  
    <?php  echo form_error('job_type');?>
    </div>
      
    </div>
    </div>
<br/>
 <div class="form-group form-group-lg valid-to">
 <br/>
  <label class="control-label col-sm-2">* Apply Valid To:
  </label>
  <div class="col-sm-6">
 
  <?php if(!empty($job))
{
    $valid_to=$job['valid_to'];
    $valid_to=explode('-',$valid_to);
    $m_array=array('01'=>'January','02'=>'February','03'=>'March','04'=>'April','05'=>'May','06'=>'June',
  '07'=>'July','08'=>'August','09'=>'September','10'=>'October','11'=>'November','12'=>'December');
    $month=$m_array[$valid_to[1]];
}
?>


  

    <div style="display:inline-block;vertical-align:top;margin-top:-15px;">
    <span style="width:70px;display:block;margin-bottom:0px">Day</span>
    <?php
    echo form_dropdown('vt_day',$days,(empty($valid_to[2])?set_value('vt_day'):$valid_to[2]),'style=width:70px;height:40px; id=vt_day');?>
   </div>
   <div style="display:inline-block;vertical-align:top;margin-top:-15px">
   <span style="display:block">Month</span>
   <?php echo form_dropdown('vt_month',$months,(empty($month)?set_value('vt_month'):$month),'style=width:140px;height:40px; id=vt_month');?>
   </div>

    
     
     
   
   <div style="display:inline-block;vertical-align:top;margin-top:-15px">
   <span style="display:block">Year</span>
   <?php echo form_input('vt_year',(empty($valid_to[0])?set_value('vt_year'):$valid_to[0]),'style=border-width:1px;width:70px;height:40px;font-size:18px;text-align:center placeholder=yyyy id=vt_year');?>
                    
   
   </div>  
   <br/>
   <?php echo form_error('vt_day');?>
    
   
  </div>

  </div>
  <div class="form-group form-group-lg ">
  <br/><br/>
  <label class="control-label col-sm-2" style="width:18%">* Company Information:</label>
  <div class="col-sm-9">
    <?php echo form_textarea('c_information',(empty($job)?set_value('c_information'):$job['c_information']),'class=form-control');?>
     <?php echo form_error('c_information');?>
    </div>
   
    </div>
 <div class="form-group form-group-lg ">
  <label class="control-label col-sm-2" style="width:18%">* Job Descriptions:</label>
  <div class="col-sm-9">
  <?php echo form_textarea('descriptions',(empty($job)?set_value('descriptions'):$job['descriptions']),'class=form-control');?>
  <?php echo form_error('descriptions');?>
  </div>
  
  </div>

  <div class="form-group form-group-lg ">
  <label class="control-label col-sm-2" style="width:18%">* Job Responsibilities:</label>
  <div class="col-sm-9">
    <?php echo form_textarea('responsibilities',(empty($job)?set_value('responsibilities'):$job['responsibilities']),'class=form-control');?>
     <?php echo form_error('responsibilities');?>
    </div>
   
    </div>

    <div class="form-group form-group-lg ">
  <label class="control-label col-sm-2" style="width:18%">* Requirements:</label>
  <div class="col-sm-9">
    <?php echo form_textarea('requirements',(empty($job)?set_value('requirements'):$job['requirements']),'class=form-control');?>
    <?php echo form_error('requirements');?>
    </div>
   
    </div> 


    </div><!--close inner panel-body-->
    </div><!-- close inner panel-->
    <div class="panel panel-default">
    <div class="panel-heading" style="background:lightblue">

    <h4 style="color:blue">Contact Information</h4>

    </div>
    <div class="panel-body">
   <div class="row">
    <div class="form-group form-group-lg col-sm-6">
    <label class="control-label col-sm-4">* Phone:</label>
    <div class="col-sm-8">
    <?php echo form_input('phone',(empty($job)?set_value('phone'):$job['phone']),'class=form-control');?>
    
    <?php echo form_error('phone');?>
    </div> 
    </div>
    <div class="col-sm-6 form-group form-group-lg">
    <label class="control-label col-sm-2">Website:</label>
    <div class="col-sm-8">
    <?php echo form_input('website',(empty($job)?set_value('website'):$job['website']),'class=form-control');?>
    
   <?php echo form_error('website');?>
    </div>
    </div>
    </div>
    <div class="row">

   <div class="form-group form-group-lg col-sm-6">
    <label class="control-label col-sm-4">* Email:</label>
    <div class="col-sm-8">
  
    <?php echo form_input('email',(empty($job)?set_value('email'):$job['email']),'class=form-control');?>
    
    <?php echo form_error('email');?>
    </div>
    </div>

    <div class="col-sm-6 form-group form-group-lg"> 
    <label class="control-label col-sm-2">* Confirm Email:</label>
    <div class="col-sm-8">
  
    <?php echo form_input('c_email',(empty($job)?set_value('c_email'):$job['email']),'class=form-control');?>
    
    <?php echo form_error('c_email');?>
    </div> 
    </div>
    </div>
    
   
  
    
 <div class="row">
 <div class="form-group form-group-lg col-sm-6">
  <label class="control-label col-sm-4">* Address: </label>
  <div class="col-sm-8">
    <?php echo form_input('address',(empty($job)?set_value('address'):$job['address']),'class=form-control');?>
    
    <?php echo form_error('address');?>
    </div>
    </div>

          
          <div class="col-sm-6 form-group form-group-lg">                              
    <label class="control-label col-sm-4">* Township(Address): </label>
    <div class="col-sm-8">
    <?php echo form_input('township',(empty($job)?set_value('township'):$job['township']),'class=form-control');?>
    
    <?php  echo form_error('township');?>
    </div>
    </div>
    </div>

  <div class="row">
  <br/>
 <div class="form-group form-group-lg col-sm-6" id="city_div">
  <label class="control-label col-sm-4">* City(Address):</label>
  <div class="col-sm-8">
    <?php echo form_dropdown('city',$cities,(empty($job)?set_value('city'):$job['city']),'class=form-control id=city');?>
    
    <?php echo form_error('city');?>
    </div>
    </div>
    
<div class="form-group form-group-lg col-sm-6" id="city_text_div" style="display:none">
  <label class="control-label col-sm-4">* City(Address):</label>
  <div class="col-sm-8">
    <?php echo form_input('city',(empty($job)?set_value('city'):$job['city']),'class=form-control');?>
    
    <?php echo form_error('city');?>

    </div>
    </div>
   
  <div class="col-sm-6 form-group form-group-lg">    
  <label class="control-label col-sm-4">* Country(Address):</label>
  <div class="col-sm-8">
  <?php echo form_input('country',(empty($job)?set_value('country'):$job['country']),'class=form-control');?>
  
  <?php echo form_error('country');?>
  </div>
  </div>
  </div>
    <div  class="form-group form-group-lg" id="s_city_div" style="display:none">
  <label class="control-label col-sm-2">* Specify City:</label>
  <div class='col-sm-4'>
  <?php echo form_input('s_city',set_value('s_city'),'class=form-control id=s_city');?>
  <?php echo "If validated,you can update to this category after 1 week";?>
  <br/>
  <?php echo form_error('s_city')?>

  
  
  </div>
  </div>

            
 
 

  </div><!--close inner panel-body-->
  </div><!--close inner panel-->
 
 <div class="text-center">
 <?php echo form_submit('update_job','Update','class=btn btn-primary');?>
 
 
 
      

 

 </div>
</form>
 
</div><!--close panel-body-->  
</div><!--close panel-->
</div><!--close container-->

<?php

//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
?>

