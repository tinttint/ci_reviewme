<?php $is_logged_in = $this->session->userdata('is_logged_in');
       $role = $this->session->userdata('role');
       
    if(!isset($is_logged_in) || $is_logged_in != true || ($role !== "student" && $role!=="admin"))
    {
      //$this->load->view('templates/cheader');
      echo "<h3>You don't have permission to access this page. 
      You must be registered as student to access this page<h3>";  
      echo anchor('login','Login');
      //$this->load->view('templates/footer');
      //<a href='../login'>Login</a>";
      exit();

    }?>
<script>


$(document).ready(function(){
    
  $('#header_l_jobs').css('background','#2874a6');
 
})



$(document).ready(function(){

    $("#search_jobs_div").on('click','#search_button',function(e){
     e.preventDefault();
   
     
     var q = $('#search_jobs_div :input').serialize();
    // var q= encodeURIComponent(q);
    
     
     q=btoa(q);
               
     // q=q.replace(/\)/gi,'%29');
      //var q=encodeURIComponent(q);
    // alert(q);
     
      window.location.href="<?php echo base_url();?>"+'pages/display_jobs/'+q+'/none/asc/';
    
    });
});

$(document).ready(function(){
  

    $('#jobs_div').on('click','#refresh_button',function(e){
      e.preventDefault();
      window.location.href="<?php echo base_url();?>"+'pages/display_jobs';
                
 
     

    });
});
$(document).ready(function(){

    $('#top_bar_div').on('change','#select_sort_by',function(e){
     e.preventDefault();
     var sort_order='asc';
     var sort_by= $(this).val();
     /*var id=$(this).find('option:selected').attr('id');
      if(id=='1')
      {
        sort_order='none'
      }*/
    
//     var sort_order='asc';
     
     window.location.href="<?php echo base_url();?>"+'pages/display_jobs/'+sort_by+'/'+sort_order;
  });
});

$(document).ready(function(){

    $("#top_bar_div").on('change','#select_sort_order',function(e){
     e.preventDefault();
     var sort_by= $('#select_sort_by').val();
     
     var sort_order=$(this).val();
     
     window.location.href="<?php echo base_url();?>"+'pages/display_jobs/'+sort_by+'/'+sort_order;
    
    });

});
$(document).ready(function(){
  $('#options').click(function(){


    $('#advanced_search').slideToggle();
  });


});
</script>

<style>

#jobs_wrapper .job_info 
{
  
  font-size:16px;
  
  max-height:55px;
  overflow:hidden;
}
#jobs_wrapper label
{

  
}
.search_input
{
  padding-left:0px;

}


</style>
<div class="container-fluid">

<h2 style="color:blue;margin:0px">Jobs</h2>
<br/>
<br/>


 



<style>
  
  .carousel-inner > .item div p
  {
   max-height:20px;overflow:hidden;width:240px;margin:0;white-space: nowrap; text-overflow:ellipsis;
  }
  .carousel-inner >.item div span
  {

    color:blue;
  }
  .item  p
  {

    font-size:14px;
  }
 
  .carousel-indicators li,.carousel-indicators .active
  {
    background:blue;
  }

   .carousel-control.right, .carousel-control.left {
  background:none;
}

  .recommended{

           max-height:240px;
           overflow-y:scroll;
       }
       .recommended label
       {
        color:blue;
        font-size:14px;
       }
       .recommended  a div > div:not(:nth-of-type(4))
       {
 white-space: nowrap; 
       max-width:260px;
    overflow: hidden;
    text-overflow: ellipsis; 
    vertical-align:top;
   /* border:1px solid red;*/
    
    
     
       }
       .recommended a div  > div:nth-of-type(4)
       {
        vertical-align:top;
        overflow:hidden;
        max-height:40px;
        
        width:270px;
       }


/*
.carousel-control.right
{
    border:1px solid blue;
  border-left:none;

  }
 .carousel-control.left {

border:1px solid blue;
  border-right:none;
    }*/
 
  </style>
  <div class="panel panel-default" style="border:2px solid lightgray">



  <div class="panel-heading" style="background:lightblue">
<h4 style="color:blue;margin:0px"> Top 20 Jobs</h4>
</div>



<div class="panel-body">
<div id="myCarousel" class="carousel slide" data-ride="carousel" style="">
  <!-- Indicators -->
  <ol class="carousel-indicators" style="">
   <?php if(isset($top_jobs[0]['id'])):?>
    <li  data-target="#myCarousel" data-slide-to="0" class="active"></li>
  <?php endif;?>
   <?php if(isset($top_jobs[4]['id'])):?>
    <li  data-target="#myCarousel" data-slide-to="1"></li>
  <?php endif;?>
<?php if(isset($top_jobs[8]['id'])):?>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  <?php endif;?>
 <?php if(isset($top_jobs[12]['id'])):?>
     <li data-target="#myCarousel" data-slide-to="3"></li>
   <?php endif;?>
    <?php if(isset($top_jobs[16]['id'])):?>
     <li data-target="#myCarousel" data-slide-to="4"></li>
   <?php endif;?>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox" style="border:2px solid lightgray">
  
      <?php if(isset($top_jobs[0]['id'])):?>
    <div class="item active">
      <div class="col-sm-12 text-center" id="container" style="margin-top:1%">
     
             <div id="img-wrapper" style="display:inline-block;overflow:hidden;height:auto;vertical-align:top">
             <?php $logo=$this->news_model->get_job_logo($top_jobs[0]['id']);$id=$top_jobs[0]['id'];?>
      <a href='<?php echo base_url()."pages/top_job_details/$id";?>'><img class="img-thumbnail" src="<?php echo $logo;?>" alt="No Image" style="width:220px;height:150px">
      </a>
        <p>Company:<span><?php echo $top_jobs[0]['c_name'];?></span></p>
      <p>Category:<span><?php echo $top_jobs[0]['category'];?>
     </span></p>
      <p>Title:<span><?php echo $top_jobs[0]['title'];?>
     </span></p>

      </div>
    
     

    <?php if(isset($top_jobs[1]['id'])):?>
      <div style="display:inline-block;overflow:hidden;height:auto;vertical-align:top">
             <?php $logo=$this->news_model->get_job_logo($top_jobs[1]['id']);$id=$top_jobs[1]['id'];?>
      <a href='<?php echo base_url()."pages/top_job_details/$id";?>'><img class="img-thumbnail img-responsive" src="<?php echo $logo;?>" alt="No Image" style="width:220px;height:150px">
      </a>  <p>Company:<span><?php echo $top_jobs[1]['c_name'];?></span></p>
         <p >Category:<span><?php echo $top_jobs[1]['category'];?></span></p>
      <p >Title:<span><?php echo $top_jobs[1]['title'];?></span></p>
      </div>
    <?php endif;?>
     <?php if(isset($top_jobs[2]['id'])):?>
        <div style="display:inline-block;overflow:hidden;height:auto;vertical-align:top">
             <?php $logo=$this->news_model->get_job_logo($top_jobs[2]['id']);$id=$top_jobs[2]['id'];?>
      <a href='<?php echo base_url()."pages/top_job_details/$id";?>'><img class="img-thumbnail img-responsive" src="<?php echo $logo;?>" alt="No Image" style="width:220px;height:150px">
      </a>  <p>Company:<span><?php echo $top_jobs[2]['c_name'];?></span></p>
        <p >Category:<span><?php echo $top_jobs[2]['category'];?></span></p>
      <p >Title:<span><?php echo $top_jobs[2]['title'];?></span></p>
          </div>
        <?php endif;?>
         <?php if(isset($top_jobs[3]['id'])):?>
        <div style="display:inline-block;overflow:hidden;height:auto;vertical-align:top">
             <?php $logo=$this->news_model->get_job_logo($top_jobs[3]['id']);$id=$top_jobs[3]['id'];?>
      <a href='<?php echo base_url()."pages/top_job_details/$id";?>'>
      <img class="img-thumbnail" src="<?php echo $logo;?>" alt="No Image" style="width:220px;height:150px">
      </a>  <p>Company:<span><?php echo $top_jobs[3]['c_name'];?></span></p>
        <p >Category:<span><?php echo $top_jobs[3]['category'];?></span></p>
      <p >Title:<span><?php echo $top_jobs[3]['title'];?></span></p>
      </div>
    <?php endif;?>
       
       </div><!--close container-->
    </div><!--close item-->
  <?php else:?>
      <div class="item active">
      <div class="col-sm-12 text-center" style="margin-top:1%">
     
             <div style="text-align:center">
               <h3>Top Jobs Not Available Yet</h3>              
                
                              
    
      </div>
      </div><!--close div-->
      </div><!--close item-->
    <?php endif;?>
    <?php if(isset($top_jobs[4]['id'])):?>
    <div class="item">
  <div class="col-sm-12 text-center" style="margin-top:1%">
        <div style="display:inline-block;overflow:hidden;height:auto;vertical-align:top">
             <?php $logo=$this->news_model->get_job_logo($top_jobs[4]['id']);$id=$top_jobs[4]['id'];?>
      <a href='<?php echo base_url()."pages/top_job_details/$id";?>'>
      <img class="img-thumbnail" src="<?php echo $logo;?>" alt="No Image" style="width:220px;height:150px">
      </a>  <p>Company:<span><?php echo $top_jobs[4]['c_name'];?></span></p>
       <p >Category:<span><?php echo $top_jobs[4]['category'];?></span></p>
      <p >Title:<span><?php echo $top_jobs[4]['title'];?></span></p>
      </div>
       
 <?php if(isset($top_jobs[5]['id'])):?>
      <div style="display:inline-block;overflow:hidden;height:auto;vertical-align:top">
             <?php $logo=$this->news_model->get_job_logo($top_jobs[5]['id']);$id=$top_jobs[5]['id'];?>
      <a href='<?php echo base_url()."pages/top_job_details/$id";?>'>
      <img class="img-thumbnail" src="<?php echo $logo;?>" alt="No Image" style="width:220px;height:150px">
      </a>  <p>Company:<span><?php echo $top_jobs[5]['c_name'];?></span></p>
        <p >Category:<span><?php echo $top_jobs[5]['category'];?></span></p>
      <p >Title:<span><?php echo $top_jobs[5]['title'];?></span></p>

      </div>
    <?php endif;?>
      <?php if(isset($top_jobs[6]['id'])):?>
        <div style="display:inline-block;overflow:hidden;height:auto;vertical-align:top">
             <?php $logo=$this->news_model->get_job_logo($top_jobs[6]['id']);$id=$top_jobs[6]['id'];?>
      <a href='<?php echo base_url()."pages/top_job_details/$id";?>'>
      <img class="img-thumbnail" src="<?php echo $logo;?>" alt="No Image" style="width:220px;height:150px">
      </a>  <p>Company:<span><?php echo $top_jobs[6]['c_name'];?></span></p>
        <p >Category:<span><?php echo $top_jobs[6]['category'];?></span></p>
      <p >Title:<span><?php echo $top_jobs[6]['title'];?></span></p>

      </div>
    <?php endif;?>
    <?php if(isset($top_jobs[7]['id'])):?>
        <div style="display:inline-block;overflow:hidden;height:auto;vertical-align:top">
             <?php $logo=$this->news_model->get_job_logo($top_jobs[7]['id']);$id=$top_jobs[7]['id'];?>
      <a href='<?php echo base_url()."pages/top_job_details/$id";?>'>
      <img class="img-thumbnail" src="<?php echo $logo;?>" alt="No Image" style="width:220px;height:150px">
      </a>  <p>Company:<span><?php echo $top_jobs[7]['c_name'];?></span></p>
        <p >Category:<span><?php echo $top_jobs[7]['category'];?></span></p>
      <p >Title:<span><?php echo $top_jobs[7]['title'];?></span></p>

      </div>
    <?php endif;?>
       </div>

      
    </div><!--close item-->
  <?php endif;?>
    <?php if(isset($top_jobs[8]['id'])):?>
    <div class="item">
 <div class="col-sm-12 text-center" style="margin-top:1%">
 
             <div style="display:inline-block;overflow:hidden;height:auto;vertical-align:top">
             <?php $logo=$this->news_model->get_job_logo($top_jobs[8]['id']);$id=$top_jobs[8]['id'];?>
      <a href='<?php echo base_url()."pages/top_job_details/$id";?>'>
      <img class="img-thumbnail" src="<?php echo $logo;?>" alt="No Image" style="width:220px;height:150px">
      </a>  <p>Company:<span><?php echo $top_jobs[8]['c_name'];?></span></p>
       <p >Category:<span><?php echo $top_jobs[8]['category'];?></span></p>
      <p >Title:<span><?php echo $top_jobs[8]['title'];?></span></p>

      </div>
    
    <?php if(isset($top_jobs[9]['id'])):?>
      <div style="display:inline-block;overflow:hidden;height:auto;vertical-align:top">
             <?php $logo=$this->news_model->get_job_logo($top_jobs[9]['id']);$id=$top_jobs[9]['id'];?>
      <a href='<?php echo base_url()."pages/top_job_details/$id";?>'>
      <img class="img-thumbnail" src="<?php echo $logo;?>" alt="No Image" style="width:220px;height:150px">
      </a>  <p>Company:<span><?php echo $top_jobs[9]['c_name'];?></span></p>
         <p >Category:<span><?php echo $top_jobs[9]['category'];?></span></p>
      <p >Title:<span><?php echo $top_jobs[9]['title'];?></span></p>

      </div>
    <?php endif;?>
    <?php if(isset($top_jobs[10]['id'])):?>
        <div style="display:inline-block;overflow:hidden;height:auto;vertical-align:top">
             <?php $logo=$this->news_model->get_job_logo($top_jobs[10]['id']);$id=$top_jobs[10]['id'];?>
      <a href='<?php echo base_url()."pages/top_job_details/$id";?>'>
      <img class="img-thumbnail" src="<?php echo $logo;?>" alt="No Image" style="width:220px;height:150px">
      </a>
  <p>Company:<span><?php echo $top_jobs[10]['c_name'];?></span></p>
       <p >Category:<span><?php echo $top_jobs[10]['category'];?></span></p>
      <p >Title:<span><?php echo $top_jobs[10]['title'];?></span></p>

      </div>
    <?php endif;?>
    <?php if(isset($top_jobs[11]['id'])):?>
        <div style="display:inline-block;overflow:hidden;height:auto;vertical-align:top">
             <?php $logo=$this->news_model->get_job_logo($top_jobs[11]['id']);$id=$top_jobs[11]['id'];?>
      <a href='<?php echo base_url()."pages/top_job_details/$id";?>'>
      <img class="img-thumbnail" src="<?php echo $logo;?>" alt="No Image" style="width:220px;height:150px">
      </a>  <p>Company:<span><?php echo $top_jobs[11]['c_name'];?></span></p>
       <p >Category:<span><?php echo $top_jobs[11]['category'];?></span></p>
      <p >Title:<span><?php echo $top_jobs[11]['title'];?></span></p>
      </div>
     
       <?php endif;?>
       </div>
    </div><!--close item-->
  <?php endif;?>
    <?php if(isset($top_jobs[12]['id'])):?>
    <div class="item">
<div class="col-sm-12 text-center" style="margin-top:1%">

             <div style="display:inline-block;overflow:hidden;height:auto;vertical-align:top">
             <?php $logo=$this->news_model->get_job_logo($top_jobs[12]['id']);$id=$top_jobs[12]['id'];?>
      <a href='<?php echo base_url()."pages/top_job_details/$id";?>'>
      <img class="img-thumbnail" src="<?php echo $logo;?>" alt="No Image" style="width:220px;height:150px">
      </a>  <p>Company:<span><?php echo $top_jobs[12]['c_name'];?></span></p>
        <p >Category:<span><?php echo $top_jobs[12]['category'];?></span></p>
      <p >Title:<span><?php echo $top_jobs[12]['title'];?></span></p>
      </div>
    
    <?php if(isset($top_jobs[13]['id'])):?>
      <div style="display:inline-block;overflow:hidden;height:auto;vertical-align:top">
             <?php $logo=$this->news_model->get_job_logo($top_jobs[13]['id']);$id=$top_jobs[13]['id'];?>
      <a href='<?php echo base_url()."pages/top_job_details/$id";?>'>
      <img class="img-thumbnail" src="<?php echo $logo;?>" alt="No Image" style="width:220px;height:150px">
      </a>  <p>Company:<span><?php echo $top_jobs[13]['c_name'];?></span></p>
       <p >Category:<span><?php echo $top_jobs[13]['category'];?></span></p>
      <p >Title:<span><?php echo $top_jobs[13]['title'];?></span></p>

      </div>
    <?php endif;?>
    <?php if(isset($top_jobs[14]['id'])):?>
        <div style="display:inline-block;overflow:hidden;height:auto;vertical-align:top">
             <?php $logo=$this->news_model->get_job_logo($top_jobs[14]['id']);$id=$top_jobs[14]['id'];?>
      <a href='<?php echo base_url()."pages/top_job_details/$id";?>'>
      <img class="img-thumbnail" src="<?php echo $logo;?>" alt="No Image" style="width:220px;height:150px">
      </a>  <p>Company:<span><?php echo $top_jobs[14]['c_name'];?></span></p>
         <p >Category:<span><?php echo $top_jobs[14]['category'];?></span></p>
      <p >Title:<span><?php echo $top_jobs[14]['title'];?></span></p>

      </div>
    <?php endif;?>
    <?php if(isset($top_jobs[15]['id'])):?>
        <div style="display:inline-block;overflow:hidden;height:auto;vertical-align:top">
             <?php $logo=$this->news_model->get_job_logo($top_jobs[15]['id']);$id=$top_jobs[15]['id'];?>
      <a href='<?php echo base_url()."pages/top_job_details/$id";?>'>
      <img class="img-thumbnail" src="<?php echo $logo;?>" alt="No Image" style="width:220px;height:150px">
      </a>  <p>Company:<span><?php echo $top_jobs[15]['c_name'];?></span></p>
       <p >Category:<span><?php echo $top_jobs[15]['category'];?></span></p>
      <p >Title:<span><?php echo $top_jobs[15]['title'];?></span></p>

      </div>
     <?php endif;?>
       
       </div>
    </div><!--close item-->
    <?php endif;?>
      <?php if(isset($top_jobs[16]['id'])):?>
<div class="item">
    <div class="col-sm-12 text-center" style="margin-top:1%">
  
             <div style="display:inline-block;overflow:hidden;height:auto;vertical-align:top">
             <?php $logo=$this->news_model->get_job_logo($top_jobs[16]['id']);$id=$top_jobs[16]['id'];?>
      <a href='<?php echo base_url()."pages/top_job_details/$id";?>'>
      <img class="img-thumbnail" src="<?php echo $logo;?>" alt="No Image" style="width:220px;height:150px">
      </a>  <p>Company:<span><?php echo $top_jobs[16]['c_name'];?></span></p>
       <p >Category:<span><?php echo $top_jobs[16]['category'];?></span></p>
      <p >Title:<span><?php echo $top_jobs[16]['title'];?></span></p>

      </div>
   
    <?php if(isset($top_jobs[17]['id'])):?>
      <div style="display:inline-block;overflow:hidden;height:auto;vertical-align:top">
             <?php $logo=$this->news_model->get_job_logo($top_jobs[17]['id']);$id=$top_jobs[17]['id'];?>
      <a href='<?php echo base_url()."pages/top_job_details/$id";?>'>
      <img class="img-thumbnail" src="<?php echo $logo;?>" alt="No Image" style="width:220px;height:150px">
      </a>  <p>Company:<span><?php echo $top_jobs[17]['c_name'];?></span></p>
        <p >Category:<span><?php echo $top_jobs[17]['category'];?></span></p>
      <p >Title:<span><?php echo $top_jobs[17]['title'];?></span></p>

      </div>
    <?php endif;?>
    <?php if(isset($top_jobs[18]['id'])):?>
        <div style="display:inline-block;overflow:hidden;height:auto;vertical-align:top">
             <?php $logo=$this->news_model->get_job_logo($top_jobs[18]['id']);$id=$top_jobs[18]['id'];?>
      <a href='<?php echo base_url()."pages/top_job_details/$id";?>'>
      <img class="img-thumbnail" src="<?php echo $logo;?>" alt="No Image" style="width:220px;height:150px">
      </a>  <p>Company:<span><?php echo $top_jobs[18]['c_name'];?></span></p>
        <p >Category:<span><?php echo $top_jobs[18]['category'];?></span></p>
      <p >Title:<span><?php echo $top_jobs[18]['title'];?></span></p>

      </div>
    <?php endif;?>
    <?php if(isset($top_jobs[19]['id'])):?>
        <div style="display:inline-block;overflow:hidden;height:auto;vertical-align:top">
             <?php $logo=$this->news_model->get_job_logo($top_jobs[19]['id']);$id=$top_jobs[19]['id'];?>
      <a href='<?php echo base_url()."pages/top_job_details/$id";?>'>
      <img class="img-thumbnail" src="<?php echo $logo;?>" alt="No Image" style="width:220px;height:150px">
      </a>  <p>Company:<span><?php echo $top_jobs[19]['c_name'];?></span></p>
      <p >Category:<span><?php echo $top_jobs[19]['category'];?></span></p>
      <p >Title:<span><?php echo $top_jobs[19]['title'];?></span></p>

      </div>
    <?php endif;?>
     
       
       </div>
    </div><!--close item-->
   <?php endif;?>
  </div><!--close carousel inner-->

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" style="color:red" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
  <span class="glyphicon glyphicon-chevron-right" style="color:red" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div><!--close carousel-->



</div>
</div>
<br/><br/>


<div class="panel panel-default" style="border:2px solid lightgray">
<div class="panel-heading" style="background:lightblue">
<h4 style="margin:0px">Recommended Jobs</h4>
</div>
<div class="panel-body">
<div class="recommended">
      
     
         <?php if(!empty($recommended)):
                  if(count($recommended)<=2):?>
                  <div class="" style="text-align:center">

                       <?php foreach($recommended as $values):
        $job_id=$values['id'];?>

 <a href='<?php echo base_url()."pages/recommended/$job_id";?>'> <div style="vertical-align:bottom;width:400px;margin-right:5%;display:inline-block;padding:10px;border-bottom:1px solid blue;text-align:left">
      
        <label> Job Id:</label> <div style="display:inline-block"><?php echo $values['id'];?>
       </div><br/>
         <label>Company:</label> <div style="display:inline-block"><?php echo $values['c_name'];?></div><br/>
         <label>Category:</label> <div style="display:inline-block"><?php echo $values['category'];?></div><br/>
         <label>Title:</label> <div style="display:inline-block"><?php echo $values['title'];?></div>



        </div></a>
        <?php endforeach;?>
        </div>
              
      <?php else:?>
       <div class="col-sm-12">
       <?php foreach($recommended as $values):
        $job_id=$values['id'];?>


       <a href='<?php echo base_url()."pages/recommended/$job_id";?>'> 
       <div class="col-sm-3 " style="float:none;display:inline-block;margin:0 4%;padding:5px;border-bottom:1px solid blue;text-align:left;vertical-align:bottom">
     
        <label> Job Id:</label> <div style="display:inline-block"><?php echo $values['id'];?>
       </div><br/>
         <label>Company:</label> <div style="display:inline-block"><?php echo $values['c_name'];?></div><br/>
         <label>Category:</label> <div style="display:inline-block"><?php echo $values['category'];?></div><br/>
         <label >Title:</label> <div style="display:inline-block"><?php echo $values['title'];?>
        </div>



        </div></a>

      <?php endforeach;?>
      </div>
      <?php endif;
      
      else:?>
           <div class="text-center">
           <h3 style="padding:10px;background:ivory;display:inline-block">Recommended Not Available Yet</h3>
          <!--  <img class="text-center" src="<?php echo base_url() .'images/icons/recommended2.png';?>" style="width:550px;height:150px"> -->
          </div>
      <?php endif;?>

      

      </div>



      </div>
      </div>

     <br/><br/>  
<div class="panel panel-default" style="border:2px solid lightgray">
<div class="panel-heading" style="background:lightblue">

<h4 style="color:blue;margin:0px">Search</h4>

</div>
<div class="panel-body" style="background:ivory">

<form id="search_form" class="form-horizontal" role="form"> 
<div class="col-sm-12" id="search_jobs_div">


<div class="row">

<div class="col-sm-12 form-group form-group-lg">
<label class="col-sm-2 control-label " style="margin-left:8%">Title</label>
<div class="col-sm-7 search_input">
<?php echo form_input('title',(!empty($query['title'])?$query['title']:""),'class=form-control');?>
</div>
</div>


</div><!--close row-->
<br/>
 <div id='advanced_search' style="display:none">
<div class="row">
<div class="col-sm-4 form-group form-group-lg">
<label class="col-sm-5 control-label">Job Id</label>
<div class="col-sm-7 search_input">
<?php echo form_input('job_id',(!empty($query['id'])?$query['id']:""),'class=form-control');?>
</div>
</div>
<div class="col-sm-4 form-group form-group-lg" >
<label class="col-sm-4 control-label">Company Name</label>
<div class="col-sm-8 search_input">
<?php echo form_input('c_name',(!empty($query['c_name'])?$query['c_name']:""),'class=form-control');?>
</div>
</div>
<div class="col-sm-4 form-group form-group-lg">
<label class="col-sm-5 control-label">Category</label>
<div class="col-sm-7 search_input">
<?php echo form_dropdown('category',$categories,(!empty($query['category'])?$query['category']:""),'class=form-control');?>
</div>
</div>

</div><!--close row-->

<div class="row">
<div class="col-sm-4 form-group form-group-lg">
<label class="col-sm-5 control-label">Job Type</label>
<div class="col-sm-7 search_input">
<?php echo form_dropdown('job_type',$job_types,(!empty($query['job_type'])?$query['job_type']:""),'class=form-control');?>
<?php if(!empty($query['job_type'])||!empty($query['salary'])||!empty($query['city'])
||!empty($query['id'])||!empty($query['c_name'])||!empty($query['category'])):?>
  <script>
  $(document).ready(function(){
  $('#advanced_search').show();
});
  </script>
<?php endif;?>
</div>
</div>
<div class="col-sm-4 form-group form-group-lg" >
<label class="col-sm-4 control-label">Salary</label>
<div class="col-sm-8 search_input">
<?php echo form_dropdown('salary',$salaries,(!empty($query['salary'])?$query['salary']:""),'class=form-control');?>
</div>
</div>

<div class="col-sm-4 form-group form-group-lg">
<label class="col-sm-5 control-label">City(Address)</label>
<div class="col-sm-7 search_input">
<?php echo form_dropdown('city',$cities,(!empty($query['city'])?$query['city']:""),'class=form-control');?>
</div>
</div>
</div><!--close row-->

</div><!--close advance_search-->
<div class="text-center">
<label id="options" style="font-size:14px;margin-right:2%">More Options</label>
<?php echo form_button('search','Search','id=search_button style=color:white class=btn btn-primary');?>
<br/><br/>
</div>



</div>
</div>
   



</div><!--close search_jobs_div-->
</form>

<br/><br/>
<h2 style="color:blue;">Available Jobs</h2>

<div id="jobs_div">



<?php echo "<h4 style='text-align:center;color:blue'>Search Results for: <span style=color:red>" .  $message. "</span></h4>";?>
<?php echo "<h3 style='text-align:center;color:blue'>Total Records Found: $total_records</h3>";?>

<div class="col-sm-11" style="margin-left:4%;">

<button class="btn btn-primary" id='refresh_button'>Refresh Table</button>

</div>
<div id="top_bar_div" class="col-sm-11"style=";padding:8px;margin-left:4%;border:1px solid blue;background: #B3E5FC">


<div class="col-sm-2" >
<label class="col-sm-12 text-center" style="">Sort By:</label>
      <div class="col-sm-12">
      <select class="form-control" id="select_sort_by">
      <option <?php echo (($sort_by=='none')?'selected':'');?> id='1' value='<?php echo "$q/none";?>'>None</option>
      <option <?php echo (($sort_by=='last_updated')?'selected':'');?> id='2' value='<?php echo "$q/last_updated";?>'>Last Updated</option>
    
    <option <?php echo (($sort_by=='c_name')?'selected':'');?> id='3' value='<?php echo "$q/c_name";?>'>Company Name</option>
    <option <?php echo (($sort_by=='category')?'selected':'');?> id ='4' value='<?php echo "$q/category";?>'>Job Category</option>
     <option <?php echo (($sort_by=='title')?'selected':'');?> id= '5' value='<?php echo "$q/title";?>'>Job Title</option>
    
    <option <?php echo (($sort_by=='job_type')?'selected':'');?> id='6' value='<?php echo "$q/job_type";?>'>Job Type</option>
    <option <?php echo (($sort_by=='city')?'selected':'');?> id='7' value='<?php echo "$q/city";?>'>City(Address)</option>
     <option <?php echo (($sort_by=='valid_to')?'selected':'');?> id='8' value='<?php echo "$q/valid_to";?>'>Apply Valid To</option>
   
    
   </select>
      

</div>
</div>
 
<div class="col-sm-2">
<label class="col-sm-12 text-center" style="">Sort Order:</label>
      <div class="col-sm-12">
      <select class="form-control" id="select_sort_order">
      
    <option <?php echo (($sort_order=='asc')?'selected':'');?> value="asc">Ascending</option>
    <option <?php echo (($sort_order=='desc')?'selected':'');?> value="desc">Descending</option>
  </select>
</div>
</div>

<div class="col-sm-6" >
<div>
<?php echo $pagination;?>
</div>
</div>
<div class="col-sm-2">

<?php echo form_open('pages/change_page');?>
<?php $page=($offset/$limit)+1;?>
Page:<input style="width:100px;display:inline-block" type="text" name="page" id="page" value="<?php echo $page;?>" class="form-control">
<button type="submit" style="width:40px;display:inline-block;margin:0 auto" class="form-control">Go</button>
<input type="hidden" name="limit" id="limit" value="<?php echo $limit;?>">

 <input type="hidden" name="q" id="q" value="<?php echo $q;?>"> 
 <input type="hidden" name="sort_by" id="sort_by" value="<?php echo $sort_by?>"> 
 <input type="hidden" name="sort_order" id="sort_order" value="<?php echo $sort_order;?>"> 
</form>

</div>
</div><!--close top_bar_div -->
<?php foreach($jobs as $value):?>
<div id="jobs_wrapper" class="col-sm-11"style=";padding-top:10px;margin-left:4%;border:1px solid blue">
  
  <div class="col-sm-2 text-center">
    <?php $logo=$value['logo'];?>
    <img class="img-responsive img-thumbnail" id="logo"src="<?php echo (empty($logo)?base_url().'images/icons/no-image.jpg':$logo);?>"
      style="width:200px;height:140px;">
      <p>Job Id:<?php echo $value['id'];?></p>
      </div>
   <div class="col-sm-10" id="wrapper2">   
   <div class="row">
  
  <div class="col-sm-6 job_info" >
    <label>Company Name:</label><span><?php echo $value['c_name'];?></span>
  </div>
  
  <div class="col-sm-6 job_info">
  <label>Job Category:</label><span><?php echo $value['category'];?></span>
  </div>
   
  </div>
  <div class="row">
  
  <div class="col-sm-6 job_info" >
   <label>Job Title:</label><span><?php echo $value['title'];?></span>
  </div>
  
  <div class="col-sm-6 job_info" >
   <label>Job Salary:</label><span><?php echo $value['salary'];?></span>
  </div>
   
  </div>
  
  <div class="row">
  
  <div class="col-sm-6 job_info" >
   <label>Job Type:</label><span><?php echo $value['job_type'];?></span>
  </div>
  
  <div class="col-sm-6 job_info" >
   <label>City(Address):</label><span><?php echo $value['city'];?></span>
  </div>
   
  </div>

<div class="row">
  
  <div class="col-sm-6 job_info" >
    <label>Apply Valid To:</label><span><?php $vt=date_create($value['valid_to']);
    echo date_format($vt,'d-M-Y');?></span>
  </div>
  
  <div class="col-sm-6 job_info" >
  <form method="get">
  <?php 
   $back_url=$this->uri->uri_string();
   $job_id=$value['id'];
   ?>

   
  <input type="hidden" name="back_url" id="back_url" value="<?php echo $back_url;?>">
  <input type="submit" class="btn btn-primary" name="details" formaction='<?php echo base_url()."pages/view_job/$job_id";?>' id="details" value="Details/Apply">
             
  </form>
  </div>
   
  </div><!--close row--> 

  </div><!--close wrapper2-->
</div><!--close jobs_wrapper-->
<?php endforeach;?>
<div id="bottom_bar_div text-center" class="col-sm-11"style=";padding:15px 0px;background:#B3E5FC;margin-left:4%;border:1px solid blue">
<div class="col-sm-8 col-sm-offset-4">
<?php echo $pagination;?>
</div>
</div>
   














</div><!--close jobs_div-->

</div><!--close container-->