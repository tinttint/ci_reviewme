
<div id="ajax_companies_div">

<?php echo "<h3 style=text-align:center;color:blue>Total Records Found: " . $total_records . "</h3>";?>
<div style="text-align:center">
<?php echo $pagination;?>
</div>



  

<div class="table-responsive">
<table class="table text-center table-bordered">
    <thead class="text-center">
     <tr><input type="button" id="refresh_button" class="btn btn-primary" value="Refresh Table">
     <div style="float:right">
    

<?php echo form_open('admin_role/change_company_page');?>
<?php $page=($offset/$limit)+1;?>
Page:<input style="width:100px;display:inline-block" type="text" name="page" id="page" value="<?php echo $page;?>" class="form-control">
<button type="submit" name="c_page" style="width:40px;display:inline-block;margin:0 auto" class="form-control">Go</button>
<input type="hidden" name="limit" id="limit" value="<?php echo $limit;?>">

 <input type="hidden" name="q" id="q" value="<?php echo $q;?>"> 
 <input type="hidden" name="sort_by" id="sort_by" value="<?php echo $sort_by?>"> 
 <input type="hidden" name="sort_order" id="sort_order" value="<?php echo $sort_order;?>"> 
</form>


     </div>



     </tr> 
      <tr class="text-center">
       
        <th>Username<br/>
        (for login)</th>
        <th <?php if($sort_by =='name')echo "class=sort_$sort_order";?>><?php echo 
anchor("admin_role/display_companies/$q/name/". (($sort_order == 'asc' && $sort_by == 'name')?'desc'
: 'asc')."/$limit",'Full Name');?></th>
        <th <?php if($sort_by =='c_name')echo "class=sort_$sort_order";?>><?php echo 
anchor("admin_role/display_companies/$q/c_name/". (($sort_order == 'asc' && $sort_by == 'c_name')?'desc'
: 'asc')."/$limit",'Company Name');?></th>
        <th> Email</th>
        <th <?php if($sort_by =='registered_date')echo "class=sort_$sort_order";?>><?php echo 
anchor("admin_role/display_companies/$q/registered_date/". (($sort_order == 'asc' && $sort_by == 'registered_date')?'desc'
: 'asc')."/$limit",'Registered Date');?></th>
      
        <th <?php if($sort_by =='abused')echo "class=sort_$sort_order";?>><?php echo 
anchor("admin_role/display_companies/$q/abused/". (($sort_order == 'asc' && $sort_by == 'abused')?'desc'
: 'asc')."/$limit",'Abused Reported');?></th>
       <th>Details</th>
       <th <?php if($sort_by =='status')echo "class=sort_$sort_order";?>><?php echo 
anchor("admin_role/display_companies/$q/status/". (($sort_order == 'asc' && $sort_by == 'status')?'desc'
: 'asc')."/$limit",'Status');?></th>
      
    

    
    

      </tr>
    </thead>
    <tbody>
    <?php foreach($companies as $values):?>
      <tr>
        <td><div><?php echo $values['username'];?></div></td>
        <td><div><?php echo $values['name'];?></div></td>
        <td><div><?php echo $values['c_name'];?></div></td>
        <td><div><?php echo $values['email'];?></div></td>
        <td><div><?php $rd=date_create($values['registered_date']);
        echo date_format($rd,'d-M-Y');?></div></td>
         <td><div><?php echo $values['abused'];?></div></td>
      

         <td><div>    
                  <form method="get"> 
<?php $details_url=$this->uri->uri_string();?>
         <input type="hidden" name="details_url" value="<?php echo $details_url;?>">
  <?php $url=$this->uri->uri_string();?>
              <input type="hidden" id="url" name="url" value="<?php echo $url;?>">
         <?php $user_c=$values['username'];
          $user_c=base64_encode($user_c);?>
        
         <input type="submit" class="btn btn-primary" name="details" 
         value="Details" formaction='<?php echo base_url()."admin_role/company_details/$user_c";?>'>
          </div></td>
          <td><div>
            
             <?php  $user_c=$values['username'];
              echo $values['status']."<br>";
              echo form_dropdown($user_c,array('Change_Status'=>'Change Status','New'=>'New','Disapproved'=>'Disapproved','Approved'=>'Approved'),'Change_Status',"style=height:34px;width:140px
     id=drop ");?>
            
 </form>
          </div> </td>
         
            

          
      </tr>
     <?php endforeach;?>
    </tbody>
  </table>
  </div><!--close table div-->
   
<div style="text-align:center">
<?php echo $pagination;?>
</div>