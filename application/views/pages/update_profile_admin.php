<?php 

$is_logged_in = $this->session->userdata('is_logged_in');
$role=$this->session->userdata('role');
		
		if(!isset($is_logged_in) || $is_logged_in != true || $role!== 'admin')
		{

			echo "You don't have permission to access this page.";  
			echo anchor('admin_role','Login');
			exit();

		}
		

$user=$this->session->userdata('username');?>
<script>

$(document).ready(function(){
    
  $('#admin_h_l_profile').css('background','#2874a6');
  $('#admin_h_l_account').css('background','yellow');
})
$(document).ready(function(){

    $('#logo_div').on('click','#upload_logo',function(e){
      e.preventDefault();

     //  $('#photo_form').submit(function(){


       

   
    
    var formData = new FormData($('#update_form')[0]);
$.ajax({
       url:"<?php echo base_url() . 'admin_role/update_logo';?>",
       data: formData,
       async: false,
       contentType: false,
       processData: false,
       cache: false,
       type: 'POST',
       success: function(data)
       {
           
           $('#logo_div').html(data);
       },
    // })//return false;    
        });
    });

});



$(document).ready(function(){
     $('#logo_div').on('click','#delete_logo',function(){
        var b =  $('#logo_server_path').val();
        var existing_path = $('#logo_e_server_path').val();
     
        $.ajax({
          url:"<?php echo base_url() . 'pages/delete_company_logo';?>",
          type: "POST",
          data:{server_path:b,existing_path:existing_path},
            success:function(result)
           {
              
                 $('#logo').attr('src',"<?php echo base_url() .'images/icons/no-image.jpg';?>");
                 $('#delete').css('visibility','hidden');
                // var b = $('#server_path').attr('value');
                 //$('#before_delete').attr('value',b);
                 $('#logo_path').attr('value',"");
                 //var previous_photos{};
                // previous_photos[]=('#server_path').attr('value');
                 


                 $('#logo_server_path').attr('value',"");
                 $('#logo_previous_s_path').attr('value',"");             
            }


        });

    });

})


$(document).ready(function(){

    $('#photo_div').on('click','#upload_button',function(e){
      e.preventDefault();

     //  $('#photo_form').submit(function(){


       

   
    
    var formData = new FormData($('#update_form')[0]);
$.ajax({
       url:"<?php echo base_url() . 'admin_role/update_photo';?>",
       data: formData,
       async: false,
       contentType: false,
       processData: false,
       cache: false,
       type: 'POST',
       success: function(data)
       {
           
           $('#photo_div').html(data);
       },
    // })//return false;    
        });
    });

});


$(document).ready(function(){
     $('#photo_div').on('click','#delete_photo',function(){
        var b =  $('#photo_server_path').val();
        var existing_path = $('#e_server_path').val();
     
        $.ajax({
          url:"<?php echo base_url() . 'pages/delete_company_photo';?>",
          type: "POST",
          data:{server_path:b,existing_path:existing_path},
            success:function(result)
           {
              
                 $('#photo').attr('src',"<?php echo base_url() .'images/icons/no-image.jpg';?>");
                 $('#delete').css('visibility','hidden');
                // var b = $('#server_path').attr('value');
                 //$('#before_delete').attr('value',b);
                 $('#photo_path').attr('value',"");
                 //var previous_photos{};
                // previous_photos[]=('#server_path').attr('value');
                 


                 $('#photo_server_path').attr('value',"");
                 $('#previous_s_path').attr('value',"");             
            }


        });

    });

})

</script>
<style>


.panel .panel-body input:focus
{
     border-color:blue;


  }
  .panel .panel-body input[type=text]
     {
        border-style:solid;
       /* background:transparent;*/
         
      border-width:0px 0px 1px 0px;
     
     border-color:gray;
      box-shadow:none;
      border-radius:0px;
     }
     .form-horizontal .form-group .form-control 
     {
       
      padding-bottom:0px;
      padding-top:16px;
      
      
      font-size:20px;
     }
     .form-horizontal .form-group .control-label
     {

      padding-top:18px;
      width:auto;
    /*  padding-left:8px;*/
     }
     .panel .panel
     {
         margin-left:5%;
         margin-right:5%;
         border-color:lightgray;

     }
      .form-horizontal .form-group select
      {

       /* padding-top:18px;*/
        text-align:center;
      }

</style>


<div class="container-fluid">



<div class="panel panel-default" style="clear:both">

<div class="panel-heading" style="background:#5dade2">
<a style="float:left" href="<?php echo base_url().'admin_role/is_logged_in';?>"><img src="<?php echo base_url().'images/icons/back-arrow2.png';?>" style="float:left;width:60px;height:40px"></a>
<?php $f_name=$this->session->userdata('name');?>
<h2 style="color:white;padding-right:60px;margin:0">Welcome <?php echo $f_name;?></h2>
<h3 style="color:white">Update Account</h3>
</div>
<div class="panel-body" style="background:">
<h3 style='text-align:right'><?php echo anchor('admin_role/change_password/','Change Password');?></h3>

<?php $ask= array('onsubmit'=>"return confirm('Update?')",'class'=>'form-horizontal'
,'role'=>'form','id'=>'update_form');?>

<?php echo form_open_multipart('admin_role/update_admin',$ask);?>
<h3 style="color:red"><?php echo (empty($message)?"":$message);?></h3> 
 <h4 style="text-align:center;color:red"> * means required</h4>
<div class="panel panel-default">
<div class="panel-heading text-center" style="background:lightblue">

Personal Information

</div>
<div class="panel-body">
  <div class="row">

  <div class="form-group form-group-lg col-sm-6">
  <label class="control-label col-sm-3" >* User Name:<br/>
  (for login)</label>
  <div class="col-sm-8" style="">
  <?php echo form_input('user',(empty($account)?set_value('user') :$account[0]['user']),'class=form-control');?>
  
  <?php echo form_error('user');?>
  </div>
  </div>
  


  
<div class="form-group form-group-lg col-sm-6">
  <label class="control-label col-sm-3">* Full Name:</label>
  <div class='col-sm-8'>
  <?php echo form_input('name',(empty($account)?set_value('name'):$account[0]['name']),'class=form-control');?>
  
  <?php echo form_error('name')?>
  </div>
  </div>
  </div><!--close row-->
  
  <div class="row">  
  <div class="form-group form-group-lg col-sm-6">
  <label class="control-label col-sm-3">* Email:</label>
  <div class="col-sm-8">
  <?php echo form_input('email',(empty($account)?set_value('email'):$account[0]['email']),'class=form-control');?>
  
  <?php echo form_error('email');?>
  </div>
  </div>

  <div class="form-group form-group-lg col-sm-6">
  <label class="control-label col-sm-3">* Confirm Email:</label>
  <div class="col-sm-8">
  <?php echo form_input('c_email',(empty($account)?set_value('c_email'):$account[0]['email']),'class=form-control');?>
  
  <?php echo form_error('c_email');?>
  </div>
  </div>
  </div><!--close row-->

<input type="hidden" id="e_server_path" name="e_server_path" value="<?php echo (!isset($account[0]['photo_server_path'])?$result['e_server_path']:$account[0]['photo_server_path']);?>">
  <div id="photo_div" class="form-group">
  <br/>

    <label class="control-label col-sm-3">University Photo:</label>
    <div class="col-sm-8">

    <input type="hidden" id="photo_path" name="photo_path" value="<?php echo(!isset($account)?$result['photo_path']:$account[0]['photo']);?>">
    <input type="hidden" id="photo_server_path" name="photo_server_path" value="<?php echo (!isset($account[0]['photo_server_path'])?$result['photo_server_path']:$account[0]['photo_server_path']);?>">
     <input type="hidden" id ="previous_s_path" name="previous_s_path" value="<?php echo (!isset($result['photo_server_path'])?"":$result['photo_server_path']);?>">
          <?php $result['photo_path']=(!isset($account[0]['photo'])?$result['photo_path']:$account[0]['photo']);?>
          <img class="img-responsive img-thumbnail" id="photo" src="<?php echo (empty($result['photo_path'])?base_url() .'images/icons/no-image.jpg':$result['photo_path']);?>" style="width:480px;height:250px">
<?php

  $delete_photo_path =  base_url() . 'images/icons/delete.jpg';
 echo (!empty($result['photo_path'])? "<img id=delete_photo src=$delete_photo_path width=50 height=50>":"");?>
 
 </div>
 
       <div class="col-sm-8 col-sm-offset-2" >
       <br/>
             <input type="file" name="userfile" class="">
        </div>   
        <div class="col-sm-8 col-sm-offset-2">
        <br/>  
             <input type="button" class="btn btn-primary" style="margin-top:2%"  
             id="upload_button" name="upload_button" value='Upload Photo'>
              <?php echo (empty($result['error'])?"":$result['error']);?>
              <br/><span style="font-size:14px">(max-upload-size:100kb)
      <br/><a href="<?php echo base_url(). 'files/reduce_size.docx';?>">How To Reduce Image Size</a></span>
            
             <br/><br/>
      </div>
</div><!--close photo div-->


<input type="hidden" id="logo_e_server_path" name="logo_e_server_path" value="<?php echo (!isset($account[0]['logo_server_path'])?$logo['logo_e_server_path']:$account[0]['logo_server_path']);?>">
<div class="form-group" id="logo_div">
<br/>
     <label class="control-label col-sm-3">* University Logo:</label>
    <div class="col-sm-8">

    <input type="hidden" id="logo_path" name="logo_path" value="<?php echo(!isset($account[0]['logo'])?$logo['photo_path']:$account[0]['logo']);?>">
    <input type="hidden" id="logo_server_path" name="logo_server_path" value="<?php echo (!isset($account[0]['logo_server_path'])?$logo['photo_server_path']:$account[0]['logo_server_path']);?>">
     <input type="hidden" id ="logo_previous_s_path" name="logo_previous_s_path" value="<?php echo (!isset($logo['photo_server_path'])?"":$logo['photo_server_path']);?>">
 <?php $logo['photo_path']=(!isset($account[0]['logo'])?$logo['photo_path']:$account[0]['logo']);?>
          <img id="logo" class="img-thumbnail" src="<?php echo (empty($logo['photo_path'])?base_url() .'images/icons/no-image.jpg':$logo['photo_path']);?>" style="width:240px;height:190px">
<?php
  echo form_error('logo_path');
  $delete_photo_path =  base_url() . 'images/icons/delete.jpg';
 echo (!empty($logo['photo_path'])? "<img id=delete_logo src=$delete_photo_path width=50 height=50>":"");?>
 
 </div>
 
       <div class="col-sm-8 col-sm-offset-2" >
       <br/>
             <input type="file" name="logo" class="">
        </div>   
        <div class="col-sm-8 col-sm-offset-2">
        <br/>  
             <input type="button" class="btn btn-primary" style="margin-top:2%"  
             id="upload_logo" name="upload_logo" value='Upload Logo'>
              <?php echo (empty($logo['error'])?"":$logo['error']);?>
              <br/><span style="font-size:14px">(max-upload-size:100kb)
      <br/><a href="<?php echo base_url(). 'files/reduce_size.docx';?>">How To Reduce Image Size</a></span>
            
             <br/><br/>
      </div>
</div><!--logo div-->



</div><!--close inner panel body-->
</div><!--close inner panel-->

   <div class="text-center" style="text-align:center">
    
  <input type="submit" class="btn btn-primary" name="update_admin" value="Update Account">

  </div>
  </div><!--close panel-body-->
  </div><!--close panel-->
</div><!--close container-fluid-->




<?php///////////////////////////////////////////////////
//////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
////////////////////////////////////////////////////////?>
