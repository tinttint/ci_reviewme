
<?php echo "<h3 style=text-align:center;color:blue>Total Records Found: " . $total_records . "</h3>";?>
<div style="text-align:center">
<?php echo $pagination;?>
</div>



<div class="table-responsive">
<table class="table text-center table-bordered">
    <thead class="text-center">
     <tr>
     
     <input type="button" id="refresh_button"  class=" btn btn-primary" value="Refresh Table">
     <div style="float:right">
    

<?php echo form_open('admin_role/change_students_page');?>
<?php $page=($offset/$limit)+1;?>
Page:<input style="width:100px;display:inline-block" type="text" name="page" id="page" value="<?php echo $page;?>" class="form-control">
<button type="submit" style="width:40px;display:inline-block;margin:0 auto" class="form-control">Go</button>
<input type="hidden" name="limit" id="limit" value="<?php echo $limit;?>">

 <input type="hidden" name="q" id="q" value="<?php echo $q;?>"> 
 <input type="hidden" name="sort_by" id="sort_by" value="<?php echo $sort_by?>"> 
 <input type="hidden" name="sort_order" id="sort_order" value="<?php echo $sort_order;?>"> 
</form>


     </div>
     
     </tr> 
      <tr class="text-center">
       
        <th>Student Registration Number</th>
        <th <?php if($sort_by =='name')echo "class=sort_$sort_order";?>><?php echo 
anchor("admin_role/display_students/$q/name/". (($sort_order == 'asc' && $sort_by == 'name')?'desc'
: 'asc')."/$limit",'Full Name');?></th>
        <th>National Registration Card No</th>
        <th <?php if($sort_by =='major')echo "class=sort_$sort_order";?>><?php echo 
anchor("admin_role/display_students/$q/major/". (($sort_order == 'asc' && $sort_by == 'major')?'desc'
: 'asc')."/$limit",'Major');?></th>
        <th <?php if($sort_by =='gender')echo "class=sort_$sort_order";?>><?php echo 
anchor("admin_role/display_students/$q/gender/". (($sort_order == 'asc' && $sort_by == 'gender')?'desc'
: 'asc')."/$limit",'Gender');?></th>
      
        <th <?php if($sort_by =='nationality')echo "class=sort_$sort_order";?>><?php echo 
anchor("admin_role/display_students/$q/nationality/". (($sort_order == 'asc' && $sort_by == 'nationality')?'desc'
: 'asc')."/$limit",'Nationality');?></th>
        <th <?php if($sort_by =='registered_date')echo "class=sort_$sort_order";?>><?php echo 
anchor("admin_role/display_students/$q/registered_date/". (($sort_order == 'asc' && $sort_by == 'registered_date')?'desc'
: 'asc')."/$limit",'Registered Date');?>
        <th>Details</th>
        <th>Status</th>

      </tr>
    </thead>
    <tbody>
    <?php foreach($students as $values):?>
      <tr>
        
        <td><div><?php echo $values['id'];?></div></td><!--change will affect in script-->
        <td><div><?php echo $values['name'];?></div></td>
        <td><div><?php echo $values['nrc'];?></div></td>
        <td><div><?php echo $values['major'];?></div></td>
         <td><div><?php echo $values['gender'];?></div></td>
          <td><div><?php echo $values['nationality'];?></div></td>
           <td><div><?php $rd=date_create($values['registered_date']);
           echo date_format($rd,'d-M-Y');?>
                
                   
          
          </div> </td>
         
           <td><div>
            <form method="get">
              <?php $student=$values['user'];
            
             $student=base64_encode($student);

              ?> 
              <input type="submit" class="btn btn-primary" id="details" name="details" formaction='<?php echo base_url()."admin_role/details_student/$student";?>' value="Details"></td>
          

           </div></td>
            
  
            <td><div>

         
<?php $back_url=$this->uri->uri_string();?>
 <input type="hidden" name="back_url" value="<?php echo $back_url;?>">
  <?php $count=count($students);?>
 <?php $url=$this->uri->uri_string();?>
              <input type="hidden" id="url" name="url" value="<?php echo $url;?>">

              
             <?php  $user_s=$values['user'];

              echo "<span style=color:blue>".$values['status']."</span><br>";
              echo form_dropdown($user_s,array('Change_Status'=>'Change Status','New'=>'New','Disapproved'=>'Disapproved','Approved'=>'Approved'),"Change_Status","style=height:34px;width:140px
     id=drop ");?>
      </form>
           </div></td>
          
      </tr>
     <?php endforeach;?>
    </tbody>
  </table>
  </div><!--close table div-->

<div style="text-align:center">
<?php echo $pagination;?>
</div>