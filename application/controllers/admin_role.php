<?php

class Admin_role extends CI_Controller {

//  var $file_content=$data['file']['file_content'];
	public function __construct()
	{
		parent::__construct();
  date_default_timezone_set('Asia/Rangoon');
   
    //////////////////////////////////////////////////////
   /* $this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
$this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
$this->output->set_header('Pragma: no-cache');*/
////////////////////////////////////////////////////////
///////////////////////////////////////////////////////
header("cache-Control: no-store, no-cache, must-revalidate");
header("cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");


		$this->load->model('news_model');
         $this->load->library('pagination');
         $this->load->helper('form');
         $this->load->helper('url');
         
        
	$this->load->library('form_validation');   
	$this->load->library('session'); 
  $this->form_validation->set_error_delimiters('<span style="text-align:center;
   color:red"; >','</span>');  
  $this->load->library('javascript');
  $this->load->library('javascript/jquery',FALSE);         
	}


 function index()
    {
      
            $this->load->view('templates/admin_header');
      $this->load->view('pages/admin_login');
             $this->load->view('templates/footer');
      
    }


public function _remap($method, $params = array())
    {

        // Check if the requested route exists
        if (method_exists($this, $method))
        {
            // Method exists - so just continue as normal
            return call_user_func_array(array($this, $method), $params);
        }
        else
        {
        redirect('admin_role/is_logged_in');
      }
        }

    public function validate()
    {
 // $result=  $this->news_model->cvalidate();
    //     $result_student = $this->news_model->validate_student();
        
      $result_admin = $this->news_model->validate_admin();
     
         
          

         if($result_admin)
          {
            $user=$this->input->post('username');
            $name=$this->news_model->get_admin_name($user);
            $data = array('username'=> $this->input->post('username'),'name'=>$name,'role'
                 =>'admin','is_logged_in' => true);
                 $this->session->set_userdata($data); 

            redirect('admin_role/is_logged_in');
          }
           else
           {
                $data['message']="Login Failed";
            $this->load->view('templates/admin_header');
            $this->load->view('pages/admin_login',$data);
            $this->load->view('templates/footer');
           }
              

           


    

    }
    public function recover()
{
      $data['message']="";
     $this->load->view('templates/admin_header');
     $this->load->view('pages/admin_recovery_page',$data);
     $this->load->view('templates/footer');

}
public function validate_recovery_email()
{

      if(isset($_POST['action']))
      {
    
      $this->form_validation->set_rules('email','Email','trim|required');
      $this->form_validation->set_rules('c_email','Confirm Email','trim|required|matches[email]');

      if($this->form_validation->run()=== FALSE)
      {   $data['message']="";
          $this->load->view('templates/admin_header');
          $this->load->view('pages/admin_recovery_page',$data);
          $this->load->view('templates/footer');
      }
      else
      {          

              


        $email = $this->input->post('email');
                 $result = $this->news_model->check_admin_recovery_email($email);
                 if($result)
                 {
                       $this->news_model->send_admin_recovery_email($email);

                 }
                 else
                 {
                    $data['message']="The email does not exist";
                    $this->load->view('templates/admin_header');
                    $this->load->view('pages/admin_recovery_page',$data);
                    $this->load->view('templates/footer');

                 }




      }
    }
    else
    {
                    $data['message']="";
                    $this->load->view('templates/admin_header');
                    $this->load->view('pages/admin_recovery_page',$data);
                    $this->load->view('templates/footer');

    }

}

public function email_error()
{

  $this->load->view('templates/admin_header');
  $this->load->view('pages/email_error');
  $this->load->view('templates/footer');
}

public function recover_u_p($email,$number)
{
         $result = $this->news_model->recover_u_p($email,$number);
       if(empty($email)||empty($number))
       {

          redirect('admin_role');

       }
       if(isset($_POST['change_password']))
       {
$data['user'] = $this->input->post('user');
  
  
$this->form_validation->set_rules('password','Password','trim|required|min_length[5]');
$this->form_validation->set_rules('c_password','Confirm Password','trim|required|matches[password]|min_length[5]');
if ($this->form_validation->run() === FALSE)
{
      $data['email']=$email;
      $data['number']=$number;
       $data['message']="";
       $data['user']=$this->input->post('user');
    $this->load->view('templates/admin_header');
  $this->load->view('pages/admin_renew_u_p',$data);
  $this->load->view('templates/footer');

     

}
else{
          
      $user = $this->input->post('user');
    $result = $this->news_model->renew_password($user);
     if($result)
     {
     
          $data['message']="Password Update Completed";
          $this->news_model->erase_random_number($user);
          $data['email']="";
          $data['number']="";

    $this->load->view('templates/admin_header');
  $this->load->view('pages/admin_renew_success', $data);
  $this->load->view('templates/footer');

     }

}

       }
       else
       {
       $result = $this->news_model->recover_u_p($email,$number);
       $data['email']=$email;
       $data['number']=$number;
       $data['profile']=$result['profile'];
       $data['expired']=$result['expired'];
       $data['message']="";
         $profile=$result['profile'];
       if(!empty($profile))
       {
            if(isset($profile[0]['username']))
            {
           $user=$profile[0]['username'];
             }
             else
             {
              $user=$profile[0]['user'];
             }
       }
       else
       {
        $user='';
       }
       $this->news_model->erase_random_number($user);
       if($result['expired']==false)
       {

        $this->load->view('templates/admin_header');
        $this->load->view('pages/admin_renew_u_p',$data);
        $this->load->view('templates/footer');


       }
       else
       {
            $data['message']='Link for recover username and password has expired.
            Please apply for username/password recovery again.';

        $this->load->view('templates/admin_header');
        $this->load->view('pages/renew_u_p_expired',$data);
        $this->load->view('templates/footer');
       }

}

}



   public function is_logged_in()
   {

$is_logged_in = $this->session->userdata('is_logged_in');
$role= $this->session->userdata('role');
    
    if(!isset($is_logged_in) || $is_logged_in != true ||($role!=='admin'))
    {
/*$this->load->view('templates/Cheader');
                $this->load->view('pages/members',$data);*/

                  // $this->load->view('templates/footer');
      echo "You don't have permission to access this page.";  
      echo anchor('admin_role','Login');
      exit();

    }

    else
    {             
               
               
            
               
            
               if($role=="admin")
               {
             
             
                $user= $this->session->userdata('username');
                $data['user']=$user;
                 $data['account']=$this->news_model->get_admin_account($user);
                
                      $data1 = array('job_status'=>'expired');
         $today = date("Y-m-d");
         $this->db->where('valid_to <',$today);
        $result =$this->db->update('jobs',$data1);
        
            
                $this->load->view('templates/admin_header_logged');
                $this->load->view('pages/member_administrator',$data);
                $this->load->view('templates/footer');  


               }

               
                
           }


   }


public function update_university()
{

  $is_logged_in = $this->session->userdata('is_logged_in');
   $role = $this->session->userdata('role');
    /*if(!isset($is_logged_in) || $is_logged_in !== true || $role!=='university')
    {
      exit();
           
    }*/
    $user = $this->session->userdata('username');
    
  

  $data['my_profile'] = $this->news_model->get_university_account($user); 
  $data['message'] = "";
     $this->load->view('templates/admin_header_logged');
  $this->load->view('pages/update_profile_u',$data);
  $this->load->view('templates/footer');
    

}


public function update_admin()
{

   if(isset($_POST['update_admin']))
   {

  $this->form_validation->set_rules('user','Username','trim|required|min_length[4]|callback_check_update_name|callback_check_space|callback_check_characters');
   $this->form_validation->set_rules('name','Full Name','trim|required');
  
  $this->form_validation->set_rules('email','Email','trim|required|valid_emails|min_length[4]|callback_check_update_email');
  $this->form_validation->set_rules('c_email','Confirm Email','trim|required|valid_emails|min_length[4]|matches[email]');
  $this->form_validation->set_rules('logo_path','Logo','trim|required');
  if($this->form_validation->run()===FALSE)
  {
 $data['message']="Update Unsuccessful";  

    $data['result']['photo_path']=$this->input->post('photo_path');
                       $data['result']['photo_server_path']= $this->input->post('photo_server_path');
                        $data['result']['e_server_path']=$this->input->post('e_server_path');

    $data['logo']['photo_path']=$this->input->post('logo_path');
                       $data['logo']['photo_server_path']= $this->input->post('logo_server_path');
                        $data['logo']['logo_e_server_path']=$this->input->post('logo_e_server_path');
                $data['message']="Update Incompleted";  
     
    $this->load->view('templates/admin_header_logged');
    $this->load->view('pages/update_profile_admin',$data);
    $this->load->view('templates/footer');

  }
  else
  {
       //$original_user = $this->input->post('original_user');
     // $original_user =$this->session->userdata('username');
       $user = $this->input->post('user');
       $name=$this->input->post('name');
       
       
       $email = $this->input->post('email');

           $photo= $this->input->post('photo_path');
           //   echo "photo : " . $photo . "<br>";
              $photo_server_path = $this->input->post('photo_server_path');
             // echo "photo server_path is : " . $photo_server_path . "<br>";
              $existing_path = $this->input->post('e_server_path');
             if(!empty($photo_server_path)&&$photo_server_path!==$existing_path)
             {
               
                            
                                if(!empty($existing_path))
                                {
                              @unlink($existing_path);
                                }
                          
                            
              }
       $logo=$this->input->post('logo_path');
       $logo_server_path=$this->input->post('logo_server_path');
  $logo_existing_path = $this->input->post('logo_e_server_path');
          if(!empty($logo_server_path)&&$logo_server_path!==$logo_existing_path)
             {
               
                            
                                if(!empty($logo_existing_path))
                                {
                              @unlink($logo_existing_path);
                                }
                          
                            
              }
   $original_user=$this->session->userdata('username');
     $data = array('user'=>$user,'name'=>$name,'email'=>$email,'logo'=>$logo,'logo_server_path'=>$logo_server_path
      ,'photo'=>$photo,'photo_server_path'=>$photo_server_path);
    $this->db->where('user', $original_user);
    $result =$this->db->update('administrator',$data);
     $this->db->where('user_c',$original_user);
                  $this->db->update('abused_reports',array('user_c'=>$user));
     $this->db->where('user_c',$original_user);
              $this->db->update('applicants',array('user_c'=>$user));
                $this->db->where('user_c',$original_user);
              $this->db->update('jobs',array('user_c'=>$user,'logo'=>$logo,'photo'=>$photo));
               $this->db->where('user_c',$original_user);
              $this->db->update('students_jobs',array('user_c'=>$user));
    if($result)
    {

      
       $data = array('username'=> $user,'name'=>$name,
                
                 'role'=>'admin','is_logged_in' => true);
                 $this->session->set_userdata($data); 
                $user = $this->session->userdata('username'); 
           $data['account'] = $this->news_model
                ->get_admin_account($user);

           // $data['message'] = "Update Profile Successful.";
            redirect('admin_role/is_logged_in?message=Update Successful');
    }
   
}
}//end first if
else
{
   $user =$this->session->userdata('username');
   $is_logged_in=$this->session->userdata('is_logged_in');
  
   
  
   $data['account']=$this->news_model->get_admin_account($user);
   $data['message']=(isset($_GET['message'])?$_GET['message']:"");
    $this->load->view('templates/admin_header_logged');
  $this->load->view('pages/update_profile_admin',$data);
    $this->load->view('templates/footer');
  }
}



public function update_logo()
{

  if(empty($_POST))
  {
     redirect('admin_role/update_admin');
  }
 $data['logo']=$this->news_model->upload_logo();
  $this->load->view('pages/ajax_admin_logo',$data);

}
public function update_photo()
{


 $data['result'] = $this->news_model->company_photo();

    $this->load->view('pages/ajax_admin_u_photo',$data);


}
public function  update_u()
{
  $is_logged_in = $this->session->userdata('is_logged_in');
  $role = $this->session->userdata('role');

    if(!isset($is_logged_in) || $is_logged_in != true || $role != 'university')
    {
      exit();
           
           }
  if(isset($_POST['update']))
  {

  $this->form_validation->set_rules('user','User','trim|required|callback_check_update_name|callback_check_space|callback_check_characters');
 
  
  $this->form_validation->set_rules('name', 'Name', 'trim|required');
  
  $this->form_validation->set_rules('u_name','University Name','trim|required');
  $this->form_validation->set_rules('website','Website','trim');
  $this->form_validation->set_rules('phone','Phone','trim|required');
  $this->form_validation->set_rules('email','Email','trim|required|callback_check_update_email');
  $this->form_validation->set_rules('c_email','Confirm Email','trim|required|matches[email]');
  $this->form_validation->set_rules('address','Address','trim|required');
  $this->form_validation->set_rules('township','Township','trim|required');
  $this->form_validation->set_rules('city','City','trim|required');
  $this->form_validation->set_rules('country','Country','trim|required');
  
  if ($this->form_validation->run() === FALSE)
  {
    
       
      $data['message']="Update Unsuccessful";
      $name= $this->input->post('dropdown'); 
      $data['u_name']=$name; 
      //$data['dropdown']=array($name=>$name);
     // $data['current_user'] = $original_user;
    $this->load->view('templates/admin_header_logged');
    $this->load->view('pages/update_profile_u',$data);
    $this->load->view('templates/footer');
    
    }
    else{


$original_user = $this->session->userdata('username');
 $user = $this->input->post('user');

 
    $name = $this->input->post('name');
    $u_name = $this->input->post('u_name');
    $phone = $this->input->post('phone');
    $email = $this->input->post('email');
    $website=$this->input->post('website');
    $address = $this->input->post('address');
    $township = $this->input->post('township');
    $city = $this->input->post('city');
    $country =$this->input->post('country');
    

     $data = array('user'=>$user,'name'=>$name,'u_name'=>$u_name,
      'phone'=>$phone, 'email'=>$email,'website'=>$website,'address'=>$address,
       'township'=>$township,'city'=>$city, 'country'=>$country);
    $this->db->where('user', $original_user);
    $result =$this->db->update('university',$data);
    
   
    if($result)
    {

      
       $data = array('username'=> $user,
        'role'=>'university','is_logged_in' => true);
                 $this->session->set_userdata($data); 
                $user = $this->session->userdata('username'); 
           $data['my_profile'] = $this->news_model
                ->get_university_account($user);

            $data['message'] ="Update Profile Completed.";
      $this->load->view('templates/admin_header_logged');
      $this->load->view('pages/update_profile_u', $data);
      $this->load->view('templates/footer');
    }
   
}
}
else
{
      $user = $this->session->userdata('username');
      $data['my_profile'] = $this->news_model->get_university_account($user);
$data['message'] = " ";
      $this->load->view('templates/Cheader');
      $this->load->view('pages/update_profile_u', $data);
      $this->load->view('templates/footer');

}
//echo "the result is " . $result;

}
public function university_details()
    {
         $data['previous_url'] =$this->input->post('previous_url');
         
         
         $array =$_POST;
         $user =array_search('Details',$array);
if($user=="")
       {
         $user = $this->session->userdata('university_user');
          if(empty($user))
          {
              exit();

          }

       }
       else
       {
            $this->session->set_userdata(array('university_user'=>$user));
            
       }

         $data['u_account']= $this->news_model->get_university_account($user);
         $this->load->view('templates/admin_header_logged');
         $this->load->view('pages/university_details',$data);
         $this->load->view('templates/footer');


    }
public function status_university()
 {

  $url = $this->input->post('url');
$status = $this->input->post('status');




/*$array = $_POST;
$user = array_search('Change Status',$array);*/

//$status = $user ."d";
$user = $this->input->post('user');
$is_ajax = $this->input->post('is_ajax');
/*echo $url ."<br>";
echo $status. "<br>";
echo $user . "<br>";

//$student_no = $this->input->post('student_no');*/

/*$this->db->select('email');
                              $result = $this->db->where_in('user',$user)->get('university');
                              $result = $result->result_array();
 
                            foreach($result as $values)
                               {

                                     $email=$values['email'];
                                    // $this->send_confirm_email($email);
                                }*/

             
    

    $data = array('status'=>$status);
       $this->db->where('user',$user);
      $result = $this->db->update('university',$data);
               
               if($result)
               {
           $this->session->set_flashdata('message', "You have changed status of
            student id: $status");
          redirect($url);                
                } 
              

 }


public function update_admin_profile()
{
$original_user = $this->input->post('original_user');
  $this->form_validation->set_rules('user','User','trim|required|callback_check_update_name|callback_check_space|callback_check_characters');
  $this->form_validation->set_rules('u_name','University name','trim|required');
  $this->form_validation->set_rules('email','Email','trim|required|min_length[4]|callback_check_update_email');
  $this->form_validation->set_rules('c_email','Confirm Email','trim|required|min_length[4]|matches[email]');
  if($this->form_validation->run()===FALSE)
  {
 $data['message']="Update Unsuccessful";  
  $data['university_names_dd'] = $this->news_model->university_names();
      $data['current_user'] = $original_user;
    $this->load->view('templates/admin_header_logged');
    $this->load->view('pages/update_profile_admin',$data);
    $this->load->view('templates/footer');

  }
  else
  {
       //$original_user = $this->input->post('original_user');
       $original_user =$this->session->userdata('username');
       $user = $this->input->post('user');
       $u_name =$this->input->post('u_name');

       $email = $this->input->post('email');
   
     $data = array('user'=>$user,'u_name'=>$u_name,'email'=>$email);
    $this->db->where('user', $original_user);
    $result =$this->db->update('administrator',$data);
    
   
    if($result)
    {

      
       $data = array('username'=> $user,
                
                 'role'=>'administrator','is_logged_in' => true);
                 $this->session->set_userdata($data); 
                $user = $this->session->userdata('username'); 
           $data['account'] = $this->news_model
                ->get_administrator_account($user);

            $data['message'] = "Update Profile Successful.";
            $data['university_names_dd'] = $this->news_model->university_names();
      $this->load->view('templates/admin_header_logged');
      $this->load->view('pages/update_profile_admin', $data);
      $this->load->view('templates/footer');
    }
   
}

}
public function check_update_email($stri)
{     
      $result = $this->news_model->check_duplicate_email($stri);
    $result2 = $this->news_model->check_update_email($stri);
      if( $result2==true||$result== true)
    {
        return true;


      }
      else
      {
         $this->form_validation->set_message('check_update_email','Choose Another Email');
         return false;

      }

}  
public function check_update_name($str)
{
  $user = $this->session->userdata('username');
  
  $result = $this->news_model->check_user($str);
     if(strcasecmp($str,$user) == 0|| $result== TRUE)
     {
        return TRUE;

     }
     else
     {
       $this->form_validation->set_message('check_update_name', 'Choose another user name');
      return FALSE;
     }
  
}





  public function display_all_students($q=0,$sort_by ='registered_date',$sort_order='desc',$limit = 2,$offset=0,$ajax='false')
  {
 //$limit = 4;
        //$results =
    

         $checked = $_POST;
         $data['checked']= array_keys($checked,'true');
        
          if(!empty($q))
          {   
             
             $q=base64_decode($q);
          }

        $is_ajax =$this->input->post('is_ajax');
       
        if(empty($is_ajax))
        {
          $is_ajax=$this->input->is_ajax_request();
        }
       
        parse_str($q, $_POST);
        $this->load->library('form_validation');

        // $this->input->loadquery($query_id);
        

         
          $query_array = array(
        'id' => $this->input->post('id'),
        'nrc'=> $this->input->post('nrc'),
        'u_name'=>$this->input->post('u_name'),
        'name' => $this->input->post('name'),
        'industry'=> $this->input->post('major'),
        'status' => $this->input->post('status'));
          $data['query']=$query_array;
         // print_r($query_array);
         //$data['search']=$query_array['category']." ,".$query_array['title'];*/

             
          $user = $this->session->userdata('username');

         //$limit;
        $result = $this->news_model->search_all_students($query_array,$limit,$offset,
            $sort_by,$sort_order);
       
        $data['all_students'] = $result['rows'];
        $data['search_results']= $result['search_results'];
        $data['total_records']= $result['num_rows'];
         $data['university_profile'] = $this->news_model
                ->get_university_account($user);

        $q=base64_encode($q);
        $this->load->library('pagination');
        $config = array();
        $config['base_url'] = base_url() ."admin_role/display_all_students/$q/$sort_by/$sort_order/$limit/";
        $config['total_rows']= $data['total_records'];
        $config['per_page'] = $limit; 
        $config['uri_segment'] = 7;
         $config['full_tag_open']='<div id="pagination">';
         $config['full_tag_close']='</div>';
        $config['num_links']= 14;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
         $data['sort_by'] = $sort_by ;
         $data['sort_order'] = $sort_order;  
         $data['q']=$q;
         $data['limit']=$limit;
         $data['segment']=$this->uri->uri_string();
         $data['university_names_dd'] = $this->news_model->university_names();
              //$data['segment'] = "" ;
        //print_r($data);
       $data['user'] = $this->session->userdata('username');
$is_logged_in = $this->session->userdata('is_logged_in');
           if($is_ajax =='true')
           {

                $this->load->view('pages/ajax_admin_all_students',$data);
           }
           else
           {
$this->load->view('templates/admin_header_logged');
          $this->load->view('pages/admin_all_students',$data);
          $this->load->view('templates/footer');
            } 

  }


public function display_students($q=0,$sort_by ='registered_date',$sort_order='desc',$limit=2,$offset=0)
  {
 //$limit = 4;
        //$results =
       
         if(isset($_POST['checked']))
      {
      $data['checked']=$_POST['checked'];
      //print_r($_POST['checked']);
    }
      else
      {
        $data['checked']=array();
      }

        
  
             
             if(!empty($q))
             {
             $q=base64_decode($q);
           }
          
     $is_ajax=$this->input->is_ajax_request();
       
        parse_str($q, $_POST);
       

          $query_array = array(
        'id' => $this->input->post('id'),
        'nrc'=> $this->input->post('nrc'),
        'name' => $this->input->post('name'),
        'major'=> $this->input->post('major'),
        'status' => $this->input->post('status'),
        'nationality'=>$this->input->post('nationality'));
         $data['query']=$query_array;
        
$sort_column=array('name','major','gender','nationality','registered_date');
          if(in_array($sort_by,$sort_column))
          {
         
          }
          else
          {
           
            $sort_by='registered_date';//change this to time format
            redirect('admin_role/display_students');
            
       
          }

          if($offset<0)
          {
            redirect('admin_role/display_students');
          }
               if($limit<0)
          {
            redirect('admin_role/display_students');
          }
              
             
          $user = $this->session->userdata('username');

         //$limit;
        $result = $this->news_model->search_students($query_array,$limit,$offset,
            $sort_by,$sort_order);
       
        $data['students'] = $result['rows'];
        $data['search_results']= $result['search_results'];
        $data['total_records']= $result['total_rows'];
         $data['university_profile'] = $this->news_model
                ->get_university_account($user);

        $q=base64_encode($q);
        $this->load->library('pagination');
        $config = array();
        $config['base_url'] = base_url() ."admin_role/display_students/$q/$sort_by/$sort_order/$limit";
        $config['total_rows']= $data['total_records'];
        $config['per_page'] = $limit; 
        $config['uri_segment'] =7;
        $config['full_tag_open']='<p style="word-wrap:break-word;line-height:30px" id="pagination">';
         $config['full_tag_close']='</p>';
        $config['num_links']= 7;
        $config['first_link'] = '<&nbspFirst';
        $config['last_link']='Last&nbsp>';
        
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
         $data['sort_by'] = $sort_by ;
         $data['sort_order'] = $sort_order;  
         $data['q']=$q;
         $data['limit']=$limit;
         $data['offset']=$offset;
         $data['segment']=$this->uri->uri_string();
              //$data['segment'] = "" ;
        //print_r($data);
       $data['user'] = $this->session->userdata('username');
$is_logged_in = $this->session->userdata('is_logged_in');
          if($is_ajax)
          {

            $this->load->view('pages/ajax_admin_students',$data);


          }  
          else
          {
          
           
$this->load->view('templates/admin_header_logged');
          $this->load->view('pages/admin_students',$data);
          $this->load->view('templates/footer');
          }     

  }
public function all_students_cr()
{

 
  
  $q = $this->input->post('q');
  $q = (empty($q)?0:$q);
  $sort_by = $this->input->post('sort_by');
  $sort_by = (empty($sort_by)?'registered_date':$sort_by);
  $sort_order = $this->input->post('sort_order');
  $sort_order = (empty($sort_order)?'desc':$sort_order);
  /*$offset = $this->input->post('offset2');
$offset = (empty($sort_order)?0:$offset); */
 $limit = $this->input->post('rows');
 $data['select']=$limit;

 // print_r($_POST);
 // echo "q:" . $q. "\nsort_by: " . $sort_by. "\nsort_order : ". $sort_order;
redirect("admin_role/display_all_students/".$q."/".$sort_by."/".$sort_order."/".$limit."/0");
}
public function change_rows()
{

  
  $q = $this->input->post('q');
  $q = (empty($q)?0:$q);
  $sort_by = $this->input->post('sort_by');
  $sort_by = (empty($sort_by)?'registered_date':$sort_by);
  $sort_order = $this->input->post('sort_order');
  $sort_order = (empty($sort_order)?'desc':$sort_order);
  /*$offset = $this->input->post('offset2');
$offset = (empty($sort_order)?0:$offset); */
 $limit = $this->input->post('rows');

 $data['select']=$limit;
  
 //redirect("admin_role/display_students/".$q."/".$sort_by."/".$sort_order."/".$limit."/".$offset);
//  echo "q:" . $q. "\nsort_by: " . $sort_by. "\nsort_order : ". $sort_order;
redirect("admin_role/display_students/".$q."/".$sort_by."/".$sort_order."/".$limit."/0");
}
public function change_students_page()
{

       $page=$this->input->post('page');

       $limit=$this->input->post('limit');
       if(empty($page)||empty($limit))
       {
        redirect('admin_role/display_students');
        exit();
       }
        $q=$this->input->post('q');
         $sort_by=$this->input->post('sort_by');
          $sort_order=$this->input->post('sort_order');
       $offset=($page*$limit)-$limit;
       $url="admin_role/display_students"."/$q/$sort_by/$sort_order/$limit/$offset";

       
       redirect($url);



}
public function change_company_page()
{

 $page=$this->input->post('page');

       $limit=$this->input->post('limit');
       if(empty($page)||empty($limit))
       {
        redirect('admin_role/display_companies');
        exit();
       }
        $q=$this->input->post('q');
         $sort_by=$this->input->post('sort_by');
          $sort_order=$this->input->post('sort_order');
       $offset=($page*$limit)-$limit;
       $url="admin_role/display_companies"."/$q/$sort_by/$sort_order/$limit/$offset";
           
       
       redirect($url);


}
public function change_rows_company()
{

$q = $this->input->post('q');
  $q = (empty($q)?0:$q);
  $sort_by = $this->input->post('sort_by');
  $sort_by = (empty($sort_by)?'registered_date':$sort_by);
  $sort_order = $this->input->post('sort_order');
  $sort_order = (empty($sort_order)?'desc':$sort_order);
  /*$offset = $this->input->post('offset2');
$offset = (empty($sort_order)?0:$offset); */
 $limit = $this->input->post('rows');

 
  
 //redirect("admin_role/display_students/".$q."/".$sort_by."/".$sort_order."/".$limit."/".$offset);
//  echo "q:" . $q. "\nsort_by: " . $sort_by. "\nsort_order : ". $sort_order;
redirect("admin_role/display_companies/".$q."/".$sort_by."/".$sort_order."/".$limit."/0");


}

public function details_student($student="")
 {
  $student=base64_decode($student);
 $student=$this->news_model->get_each_student($student);
  if($student=="" || empty($student))
  {

    redirect('admin_role/display_students');
    exit();
  }
 
 
  if(isset($_GET['checked']))
  {
    $data['checked']=$_GET['checked'];
  }
  
  
  

  
  
  
 
  $data['back_url'] = $this->input->get('back_url');
  $data['account']= $student;
  $data['message']="";

     $this->load->view('templates/admin_header_logged');
  $this->load->view('pages/details_student',$data);
  $this->load->view('templates/footer');


 }


public function change_status()
 {    
   
  


       

$url = $this->input->post('url');
$status = $this->input->post('status');

$user = $this->input->post('user');

if(empty($status)||empty($user))
{

  redirect('admin_role/display_students');
  exit();
}


     $result2=$this->db->select('status')->from('students')->where('user',$user)
     ->get()->row_array();
     $e_status=$result2['status'];
     if($e_status==$status)
     {
          echo json_encode(array('message'=>"false")); 
         // echo "<p id='some'>false</p>";
          

           return;
     }

   if($status=='Approved')
   {


                     $this->db->select('email');
                      $result = $this->db->where('user',$user)->get('students');
                      $result = $result->row_array();
                           
                      $email=$result['email'];

    /*  $sent=$this->send_approve_email($email);
   if($sent)
   {
*/

    $data = array('status'=>$status);
       $this->db->where('user',$user);
      $result = $this->db->update('students',$data);
      //redirect($url);
      echo json_encode(array('message'=>"true")); 


  /* }
   else
   {


    $data = array('status'=>$status);
       $this->db->where('user',$user);
      $result = $this->db->update('students',$data);

         echo json_encode(array('message'=>"email_error")); 
       //echo "<p id='some'>email_error</p>";
        

   }*/
 }
   else
   {
$data = array('status'=>$status);
       $this->db->where('user',$user);
      $result = $this->db->update('students',$data);
       echo json_encode(array('message'=>"true")); 
     // redirect($url);
   
   }

  

             
  
  
   

   
  

 }
 public function disapprove_student()
 {
     $message=$this->input->post('message');
       if(!isset($_POST['user']))
     {
      redirect('admin_role/display_students');
     }
     $user=$this->input->post('user');
     $url=$this->input->post('url');



     $result2=$this->db->select('status')->from('students')->where('user',$user)
     ->get()->row_array();

     if($result2['status']=='Disapproved')
     {
        echo json_encode(array('message'=>"false"));   
        return;
     }



       $this->db->select('email');
                              $result4= $this->db->where('user',$user)->get('students');
                              $result4= $result4->row_array();
                              $email=$result4['email'];
                              /*  $sent=$this->send_disapprove_email($email,$message);
                                if($sent)
                                {*/
                                   
                                 $data = array('status'=>'Disapproved');
                                 $this->db->where('user',$user);
                                  $result = $this->db->update('students',$data);

                                   echo json_encode(array('message'=>'true'));   
                               /* }
                                else
                                {
                                  echo json_encode(array('message'=>'email_error')); 
                                }*/ 


 }
 public function send_disapprove_email($email,$message)
 {


$config = Array( 'protocol'=>'smtp',
    'smtp_host'=>'ssl://smtp.googlemail.com',
    'smtp_port'=> 465,
    'smtp_user'=> 'jobmatchingmm@gmail.com',
    'smtp_pass'=> '12february'
   );
$this->load->library('email',$config);
/*$config['protocol'] = 'sendmail';
$config['mailpath'] = '/usr/sbin/sendmail';
$config['charset'] = 'iso-8859-1';
$config['wordwrap'] = TRUE;

$this->email->initialize($config);
*/$this->email->set_newline("\r\n");
$this->email->from('jobmatchingmm@gmail.coom','Job Career Development Site');
$this->email->to($email);
$this->email->subject('Job Career Development Site,Disapproved');
$this->email->message($message);

if(@$this->email->send())
{

    return true; 



}
else
{
//  show_error($this->email->print_debugger());
  return false;
 
   
}


 }
 public function d_s_email_error()
 {

   $this->load->view('templates/admin_header_logged');
   $this->load->view('pages/d_s_email_error');
   $this->load->view('templates/footer');
 }
public function send_approve_email($email)
{
$config = Array( 'protocol'=>'smtp',
    'smtp_host'=>'ssl://smtp.googlemail.com',
    'smtp_port'=> 465,
    'smtp_user'=> 'jobmatchingmm@gmail.com',
    'smtp_pass'=> '12february'
   );
$this->load->library('email',$config);
/*$config['protocol'] = 'sendmail';
$config['mailpath'] = '/usr/sbin/sendmail';
$config['charset'] = 'iso-8859-1';
$config['wordwrap'] = TRUE;

$this->email->initialize($config);
*/$this->email->set_newline("\r\n");
$this->email->from('jobmatchingmm@gmail.coom','Job Career Development Site');
$this->email->to($email);
$this->email->subject('Job Career Development Site,Approved');
$this->email->message("You have been approved.\n
 You can now log in at the Job Career Development Site.");

if(@$this->email->send())
{

  return true;



}
else
{
     
   return false;
  //show_error($this->email->print_debugger());
}


}

   public function display_universities($q=0,$sort_by ='registered_date',$sort_order='asc',$offset = 0,$ajax='false')
    {
$limit = 2;
      // $is_ajax=$this->input->is_ajax_request();
       $is_ajax =$this->input->post('is_ajax');
       if(empty($is_ajax))
       {
           $is_ajax=$this->input->is_ajax_request();
       }

          if(!empty($q))
          {   
             
             $q=base64_decode($q);

            
          }

        parse_str($q, $_POST);
       // $this->load->library('form_validation');

          $query_array = array(
        'user' => $this->input->post('user'),
        'name' => $this->input->post('name'),
        'email'=> $this->input->post('email'),
        'u_name'=>$this->input->post('u_name'),
        'status' => $this->input->post('status'));
        
        $data['query']=$query_array;
          $user = $this->session->userdata('username');

         
        $result = $this->news_model->search_universities($query_array,$limit,$offset,
            $sort_by,$sort_order);
        
        $data['all_universities'] = $result['rows'];
       
        $data['total_rows']= $result['total_rows'];
       
     
        $q=base64_encode($q);
        $this->load->library('pagination');
        $config = array();
        $config['base_url'] = base_url() ."admin_role/display_universities/$q/$sort_by/$sort_order/";
        $config['total_rows']= $data['total_rows'];
        $config['per_page'] = $limit; 
        $config['uri_segment'] = 6;
         $config['full_tag_open']='<div style="text-align:center" id="pagination">';
         $config['full_tag_close']='</div>';
        //$config['num_links']= 10;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
         $data['sort_by'] = $sort_by ;
         $data['sort_order'] = $sort_order;  
         $data['q']=$q;
         $data['url']=$this->uri->uri_string();
             $data['account']=$this->news_model->get_administrator_account($user);
             $data['university_names_dd'] = $this->news_model->university_names();
       $data['user'] = $this->session->userdata('username');
$is_logged_in = $this->session->userdata('is_logged_in');
         if($is_ajax=='true'||$is_ajax=='1')
         {
              $this->load->view('pages/ajax_admin_universities',$data);
         }
         else
         {
$this->load->view('templates/admin_header_logged');
          $this->load->view('pages/admin_universities',$data);
          $this->load->view('templates/footer');
         }

    }



    public function company_details($user_c="")
 {    
  $user_c=base64_decode($user_c);
  $user_c=$this->news_model->get_company($user_c);
  
 if($user_c=="" || empty($user_c))
  {

    redirect('admin_role/display_companies');
    exit();
  }
     
      $array = $_POST;
      $data['details_url']=$this->input->get('details_url');

      $data['account'] = $user_c;
      $this->load->view('templates/admin_header_logged');
      $this->load->view('pages/company_details',$data);
      $this->load->view('templates/footer');

 }
public function status_company()
 {

 
$url = $this->input->post('url');

$status = $this->input->post('status');
$message=$this->input->post('message');

$user = $this->input->post('user');
if(empty($status)||empty($user))
{

  redirect('admin_role/display_companies');
}


     $result2=$this->db->select('status')->from('caccounts')->where('username',$user)
     ->get()->row_array();
     $e_status=$result2['status'];
     if($e_status==$status)
     {
          echo json_encode(array('message'=>'false'));
          // echo "<p id='some'>false</p>";
          

           return;
     }

             

               
                   if($status=="Disapproved")
                   {
                          
                            
                             $this->db->select('email');
                              $result4= $this->db->where('username',$user)->get('caccounts');
                              $result4= $result4->row_array();
                           
 
                           
                               

                                     $email=$result4['email'];
                                 /*  $sent=$this->send_disapprove_email($email,$message);
                                   if($sent)
                                   {

*/
                                     $data = array('status'=>$status);
                                     $this->db->where('username',$user);
                                     $result = $this->db->update('caccounts',$data);
                                     echo json_encode(array('message'=>'true'));
                                    // echo "<p id='some'>true</p>";
                                    // redirect($url);
              

                                 //  }
                                /*   else
                                   { 

                                    //need to remove this when changed to live server
                                     $data = array('status'=>$status);
                                     $this->db->where('username',$user);
                                     $result = $this->db->update('caccounts',$data);

                                    echo json_encode(array('message'=>'email_error'));
                                     // echo "<p id='some'>email_error</p>";

                                   }*/

                   }
                   elseif($status=="Approved")
                   {
                             $this->db->select('email');
                              $result4= $this->db->where('username',$user)->get('caccounts');
                              $result4= $result4->row_array();
                           
 
                           
                               

                                     $email=$result4['email'];
                                 /*  $sent=$this->send_approve_email($email);
                                   if($sent)
                                   {

*/
                                     $data = array('status'=>$status);
                                     $this->db->where('username',$user);
                                     $result = $this->db->update('caccounts',$data);
                                     echo json_encode(array('message'=>'true'));

                                    // echo "<p id='some'>true</p>";
                                    // redirect($url);
              

                                 //  }
                                //   else
                                  /* { 



                                     //Need to remove this
                                     $data = array('status'=>$status);
                                     $this->db->where('username',$user);
                                     $result = $this->db->update('caccounts',$data);

                                    echo json_encode(array('message'=>'email_error'));

                                      //echo "<p id='some'>email_error</p>";

                                   }*/

                               

                   }
                   elseif($status=='New')
                   {
                                  $data = array('status'=>$status);
                                     $this->db->where('username',$user);
                                     $result = $this->db->update('caccounts',$data);
                                    echo json_encode(array('message'=>'true'));

                                   // echo "<p id='some'>true</p>";
                                    // redirect($url);



                   }

         /*  $this->session->set_flashdata('message', "You have changed status of
            student id: $status");*/
                    
               
              

 }
    public function display_companies($q=0,$sort_by ='registered_date',$sort_order='desc',$limit=2,$offset = 0)
{




           $is_ajax=$this->input->is_ajax_request();
          
        if(!empty($q))
        {
          $q=base64_decode($q);
        }
         
        parse_str($q, $_POST);
       
         
          $query_array = array(
        'c_name'=>$this->input->post('c_name'),
        'name' => $this->input->post('name'),
        'email'=> $this->input->post('email'),
        'status' => $this->input->post('status'));

         $data['query']=$query_array;
          $sort_column=array('name','c_name','registered_date','abused');
          if(in_array($sort_by,$sort_column))
          {
         
          }
          else
          {
           
            $sort_by='registered_date';//change this to time format
            redirect('admin_role/display_companies');
            
       
          }

          if($offset<0)
          {
            redirect('admin_role/display_companies');
          }

                    if($limit<0)
          {
            redirect('admin_role/display_companies');
          }
              
         
          $user = $this->session->userdata('username');

        $result = $this->news_model->search_companies($query_array,$limit,$offset,
            $sort_by,$sort_order);
        
        $data['companies'] = $result['rows'];
       
        $data['total_records']= $result['total_rows'];
        

        $q =base64_encode($q);
        $this->load->library('pagination');
        $config = array();
        $config['base_url'] = base_url()."admin_role/display_companies/$q/$sort_by/$sort_order/$limit/";
        $config['total_rows']= $data['total_records'];
        $config['per_page'] = $limit; 
        $config['uri_segment'] = 7;
         $config['full_tag_open']='<p style="word-wrap:break-word;line-height:30px" id="pagination">';
         $config['full_tag_close']='</p>';
        $config['num_links']= 7;
        $config['first_link'] = '<&nbspFirst';
        $config['last_link']='Last&nbsp>';
      
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
         $data['sort_by'] = $sort_by ;
         $data['sort_order'] = $sort_order;  
         $data['q']=$q;
         $data['limit']=$limit;
         $data['url']=$this->uri->uri_string();
             $data['offset']=$offset;
           
       $data['user'] = $this->session->userdata('username');
$is_logged_in = $this->session->userdata('is_logged_in');
          if($is_ajax)
          {
            $this->load->view('pages/ajax_admin_companies',$data);
          }
          else
          {
$this->load->view('templates/admin_header_logged');
          $this->load->view('pages/admin_companies',$data);
          $this->load->view('templates/footer');
           } 

}



public function view($page = 'home')
{
 /* 
  if ( ! file_exists(APPPATH.'/views/pages/'.$page.'.php'))
  {
    // Whoops, we don't have a page for that!
    show_404();
  }


  $data['title'] = ucfirst($page); // Capitalize the first letter

          $data['students'] = $this->news_model->get_students();
           $data['jobs']= $this->news_model->get_all_jobs(); 
         
       

         $data['message']= "";

        
$is_logged_in = $this->session->userdata('is_logged_in');

   if($is_logged_in==true)
   {
           
      $this->load->view('templates/admin_header_logged');
  $this->load->view('pages/'.$page, $data);
            $this->load->view('templates/footer');
    }
    else
    {

      $this->load->view('templates/admin_header');
  $this->load->view('pages/'.$page, $data);
            $this->load->view('templates/footer');
    }
*/
   


}

public function display_jobs($q=0,$sort_by = 'last_updated',$sort_order= 'desc',$offset = 0)
  {   

        ////$this->load->library('form_validation');
  ////$this->load->library('form_validation');
      
        $is_ajax = $this->input->post('is_ajax');
        $limit = 4;
        $data = array('job_status'=>'expired');
         $today = date("Y-m-d");
         $this->db->where('valid_to <',$today);
        $result =$this->db->update('jobs',$data);

         $data4 = array('job_status'=>'opened');
         $this->db->where('valid_to >=',$today);
         $result = $this->db->update('jobs',$data4 );

             // echo str_replace('\(','%28',$q );         
          
          if(!empty($q))
          {   
             
             $q=base64_decode($q);
            
          }

     
          parse_str($q,$_POST);
        
         $query_array = array(
            'c_name'=>$this->input->post('c_name'),
            'title' => $this->input->post('title'),
          'id'=>$this->input->post('job_id'),
        'category' =>$this->input->post('category'),
        
        'job_type'=> $this->input->post('job_type'),
        'city'=> $this->input->post('city'),
        'salary'=> $this->input->post('salary')
        //'salary' => $this->input->post('salary'),
        
        );

 $data['query']=$query_array;

         
     $sort_column=array('none','last_updated','c_name','category','title','job_type','city','valid_to');
          if(in_array($sort_by,$sort_column))
          {
         
          }
          else
          {
           
            $sort_by='last_updated';//change this to time format
            redirect('admin_role/display_jobs');
            
       
          }

          if($offset<0)
          {
            redirect('admin_role/display_jobs');
          }
             


        $result = $this->news_model->search($query_array,$limit,$offset,
            $sort_by,$sort_order);
       
        $data['jobs'] = $result['rows'];
        $data['total_records']= $result['total_rows'];
        $data['message']= $result['message'];
         $q=base64_encode($q);
        $this->load->library('pagination');
        $config = array();
        $config['base_url'] = base_url() . "admin_role/display_jobs/$q/$sort_by/$sort_order/";
        $config['total_rows']= $data['total_records'];
        $config['per_page'] = $limit; 
        $config['uri_segment'] = 6;
        $config['full_tag_open']='<p style="word-wrap:break-word;line-height:30px" id="pagination">';
         $config['full_tag_close']='</p>';
        $config['num_links']= 7;
        $config['first_link'] = '<&nbspFirst';
        $config['last_link']='Last&nbsp>';
       
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
         $data['sort_by'] = $sort_by ;
         $data['sort_order'] = $sort_order;    
          $data['q'] = $q;   
          $data['offset']=$offset;
          $data['limit']=$limit;
          $data['top_jobs'] = $this->news_model->top_jobs();
      
         
        
          
        
       
        /////  $data['top_ten_jobs'] = $top_ten_jobs;
         
       
         $data['categories']= $this->news_model->get_categories();
       
         $data['job_types'] = $this->news_model->get_job_types();
       
         $data['cities'] = $this->news_model->get_cities();
         $data['salaries']=$this->news_model->get_salaries();
        

$is_logged_in = $this->session->userdata('is_logged_in');
$role =$this->session->userdata('role');
    if($is_ajax=='true')
    {
      $this->load->view('pages/ajax_jobs',$data);
    }
    else
    { 
       $this->load->view('templates/admin_header_logged');
          $this->load->view('pages/admin_jobs',$data);
          $this->load->view('templates/footer');
     } 

  }

  public function top_job_details($job_id)
  {

  $top_job = $this->news_model->get_available_job($job_id);
  if($job_id==""||empty($top_job))
  {

    redirect('admin_role/display_jobs');
  }
  $data['top_job']=$top_job;
  $is_logged_in = $this->session->userdata('is_logged_in');
  $role=$this->session->userdata('role');
  
  

     
  
  
  $this->load->view('templates/admin_header_logged');
  $this->load->view('pages/admin_top_job_details',$data);
  $this->load->view('templates/footer');
     

  }

public function change_page()
{
  $page=$this->input->post('page');

       $limit=$this->input->post('limit');
       if(empty($page)||empty($limit))
       {
        redirect('admin_role/display_jobs');
        exit();
       }
        $q=$this->input->post('q');
         $sort_by=$this->input->post('sort_by');
          $sort_order=$this->input->post('sort_order');
       $offset=($page*$limit)-$limit;
       $url="admin_role/display_jobs"."/$q/$sort_by/$sort_order/$offset";

       
       redirect($url);
}
  public function view_job($job_id="")
  {
     $jobs= $this->news_model->get_available_job($job_id);
    if($job_id=="" || empty($jobs))
   {
     redirect('admin_role/display_jobs');
     exit();
   }
  $is_logged_in = $this->session->userdata('is_logged_in');
  $role=$this->session->userdata('role');
  $data['job'] = $jobs;
  
  $data['job_no'] = $job_id;
  $user = $this->session->userdata('username');
  


  
    


               
               $this->load->view('templates/admin_header_logged');
               $this->load->view('pages/admin_job_details',$data);
               $this->load->view('templates/footer');
          

       
               



  }
public function display_job_entries($q=0,$sort_by ='last_updated',$sort_order='desc',$offset = 0,$ajax='false')
  
  {

      $data1 = array('job_status'=>'expired');
         $today = date("Y-m-d");
         $this->db->where('valid_to <',$today);
         $this->db->update('jobs',$data1);

if(isset($_GET['checked']))
      {
      $data['checked']=$_GET['checked'];
      //print_r($_POST['checked']);
    }
      else
      {
        $data['checked']=array();
      }
        if(!empty($q))
        {
          $q=base64_decode($q);

        }
    
$limit = 2;
         $is_ajax=$this->input->is_ajax_request();

      
        parse_str($q, $_POST);
       
       
       
        

           $query_array = array(
        'id' => $this->input->post('job_id'),
        'category' => $this->input->post('category'),
        'title'=> $this->input->post('title'),
        'salary'=>$this->input->post('salary'),
        'job_type'=>$this->input->post('job_type'),
        'job_status'=>$this->input->post('job_status')

       // 'status' => $this->input->post('status')
        );
        
         $data['query']=$query_array;

           $sort_column=array('last_updated','opened_date','published_date','category','title','job_type','city','valid_to');
          if(in_array($sort_by,$sort_column))
          {
         
          }
          else
          {
           
            $sort_by='last_updated';//change this to time format
            redirect('admin_role/display_job_entries');
            
       
          }

          if($offset<0)
          {
            redirect('admin_role/display_job_entries');
          }
             
          $user = $this->session->userdata('username');

         //$limit;
        $result = $this->news_model->search_job_entries($query_array,$limit,$offset,
            $sort_by,$sort_order);
       // print_r($result);
        $data['jobs'] = $result['rows'];
       
        $data['total_records']= $result['num_rows'];
        
              


        $q=base64_encode($q);
        $this->load->library('pagination');
        $config = array();
        $config['base_url'] = base_url() .  "admin_role/display_job_entries/$q/$sort_by/$sort_order/";
        $config['total_rows']= $data['total_records'];
        $config['per_page'] = $limit; 
        $config['uri_segment'] = 6;
       $config['full_tag_open']='<p style="word-wrap:break-word;line-height:30px" id="pagination">';
         $config['full_tag_close']='</p>';
        $config['num_links']= 5;
        $config['first_link'] = '<&nbspFirst';
        $config['last_link']='Last&nbsp>';
         
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
         $data['sort_by'] = $sort_by ;
         $data['sort_order'] = $sort_order;  
         $data['q']=$q;
         $data['limit']=$limit;
         $data['segment']=$this->uri->uri_string();
         $data['total_records']=$data['total_records'];
         $data['offset']=$offset;
         $data['message'] = "";

         $data['categories']= $this->news_model->get_categories();
       
         $data['job_types'] = $this->news_model->get_job_types();
       
         $data['cities'] = $this->news_model->get_cities();
         $data['salaries']=$this->news_model->get_salaries(true);

              //$data['segment'] = "" ;
        //print_r($data);
       $data['user'] = $this->session->userdata('username');
$is_logged_in = $this->session->userdata('is_logged_in');
    
       if($is_ajax)
       {
          $this->load->view('pages/ajax_admin_job_entries',$data);
       }
       else
       {
$this->load->view('templates/admin_header_logged');
          $this->load->view('pages/admin_job_entries',$data);
          $this->load->view('templates/footer');
        }


  }
  public function display_expired_jobs($q=0,$sort_by ='valid_to',$sort_order='desc',$offset = 0)
  
  {


      $data1 = array('job_status'=>'expired');
         $today = date("Y-m-d");
         $this->db->where('valid_to <',$today);
        $result =$this->db->update('jobs',$data1);


    
if(isset($_GET['checked']))
      {
      $data['checked']=$_GET['checked'];
      //print_r($_POST['checked']);
    }
      else
      {
        $data['checked']=array();
      }

          $is_ajax=$this->input->is_ajax_request();
             
             if(!empty($q))
             {
                $q=base64_decode($q);
              }
          
    
$limit = 2;
  
        
       parse_str($q,$_POST);

        
          $query_array = array(
        'id' => $this->input->post('job_id'),
        'category' => $this->input->post('category'),
        'title'=> $this->input->post('title'),
        'salary'=>$this->input->post('salary'),
        'job_type'=>$this->input->post('job_type')

       // 'status' => $this->input->post('status')
        );
        $sort_column=array('closed_date','opened_date','last_updated','published_date','category','title','job_type','city','valid_to');
          if(in_array($sort_by,$sort_column))
          {
         
          }
          else
          {
           
            $sort_by='valid_to';//change this to time format
            redirect('admin_role/display_expired_jobs');
            
       
          }

          if($offset<0)
          {
            redirect('admin_role/display_expired_jobs');
          }
             
          $data['query']=$query_array;
             
          $user = $this->session->userdata('username');

        $is_ajax=$this->input->is_ajax_request();
        $result = $this->news_model->search_expired_jobs($query_array,$limit,$offset,
            $sort_by,$sort_order);
       // print_r($result);
        $data['jobs'] = $result['rows'];
       
        $data['total_records']= $result['num_rows'];
        
               // $data['myaccount'] = $this->news_model->get_account($user);

        $q=base64_encode($q);
        
        $this->load->library('pagination');
        $config = array();
        $config['base_url'] = base_url() .  "/admin_role/display_expired_jobs/$q/$sort_by/$sort_order/";
        $config['total_rows']= $data['total_records'];
        $config['per_page'] = $limit; 
        $config['uri_segment'] = 6;
        $config['full_tag_open']='<p style="word-wrap:break-word;line-height:30px" id="pagination">';
         $config['full_tag_close']='</p>';
        $config['num_links']= 5;
        $config['first_link'] = '<&nbspFirst';
        $config['last_link']='Last&nbsp>';
       
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
         $data['sort_by'] = $sort_by ;
         $data['sort_order'] = $sort_order;  
         $data['q']=$q;
         $data['limit']=$limit;
         $data['segment']=$this->uri->uri_string();
         $data['total_records']=$data['total_records'];
         $data['offset']=$offset;
         $data['message'] = "";


         $data['categories']= $this->news_model->get_categories();
       
         $data['job_types'] = $this->news_model->get_job_types();
       
         $data['cities'] = $this->news_model->get_cities();
         $data['salaries']=$this->news_model->get_salaries(true);

              //$data['segment'] = "" ;
        //print_r($data);
       $data['user'] = $this->session->userdata('username');
$is_logged_in = $this->session->userdata('is_logged_in');
    
       if($is_ajax)
       {
          $this->load->view('pages/ajax_admin_expired_jobs',$data);
       }
       else
       {
$this->load->view('templates/admin_header_logged');
          $this->load->view('pages/admin_expired_jobs',$data);
          $this->load->view('templates/footer');
        }


  }

public function delete_job_entry()
{
$user =$this->session->userdata('username');

if(!empty($_POST['job_id']))
{
$job_id=$this->input->post('job_id');
}
if(!empty($_POST['selected']))
{

 $job_id=$_POST['selected'];
}
if(empty($job_id))
{
  redirect('admin_role/display_job_entries');
}
 
 $previous_url= $this->input->post('previous_url');
   

 
 
 
 
 
 

    $data = array('delete_status'=>'true');
       $this->db->where('user_c',$user);
       $this->db->where_in('id',$job_id);
      $result = $this->db->update('jobs',$data);
          if($result)
          {     
    redirect($previous_url);
  }






}


public function open_jobs($job_id='')
{


$user =$this->session->userdata('username');

if(!empty($_POST['id']))
{
$job_id=$this->input->post('id');
}
if(!empty($_POST['selected']))
{

 $job_id=$_POST['selected'];
}
//$id=$this->db->select('id')->from('jobs')->where(array('user_c'=>$user,'job_status'=>'opened'))->get()->result_array();
if(empty($job_id))
{
  redirect('admin_role/display_job_entries');
}

 
 $previous_url= $this->input->post('previous_url');
   

 
 
 
 
 
 

    $data = array('job_status'=>'opened','opened_date'=>date('Y-m-d'));
       $this->db->where('user_c',$user);
       $this->db->where_in('id',$job_id);
      $result = $this->db->update('jobs',$data);
          if($result)
          {     
    redirect($previous_url);
  }


}




public function check_valid_to($date,$date2)
{
if(preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])-([a-zA-Z]+)-[0-9]{4}$/",$date2)) 
//(preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])-(0[1-9]|1[0-2])-[0-9]{4}$/",$date2))
{ 
  
  return TRUE;
}
else{

  $this->form_validation->set_message('check_valid_to', 'The date format is day-month-yyyy');
   return FALSE;  
}

}



public function valid_to_extend($date,$vari)
{
if($vari=="incorrect")
{
  
                                 
  $this->form_validation->set_message('valid_to_extend', 'The date can only be extended');
   return FALSE;  
}
else{

   return true;  
   
  
}

}



  public function update_job($job_id="")
{ 

  $job=$this->news_model->get_detail_job($job_id);
if($job_id=="" || empty($job))
  {

    redirect('admin_role/display_job_entries');
    exit();
  }

  
  if(isset($_POST['update_job']))
  {

   
             
      $vt_day=$this->input->post('vt_day');
             $vt_month=$this->input->post('vt_month');
             $vt_year = $this->input->post('vt_year');
             $valid_to = $vt_day."-".$vt_month ."-". $vt_year;
              $vt = date_create($valid_to);
       $vt = date_format($vt,"Y-m-d");
                $orig_valid_to=$this->db->select('valid_to')->from('jobs')->where('id',$job_id)->get()->row_array();
             
             $a='';

             if($vt<$orig_valid_to)
             {
               $a='incorrect';
             
             }
       if($this->input->post('category')=='Other(Specify)')
       {
         $this->form_validation->set_rules('s_category','Specify Category'
         ,'trim|required');
                }
        $this->form_validation->set_rules('category','Job Category','trim|required');
          //$this->form_validation->set_rules('user', 'User', 'required');
          //$this->form_validation->set_rules('password', 'Password', 'trim|required');
          //$this->form_validation->set_rules('cpassword', 'Confirm Password', 'required|matches[password]');
          $this->form_validation->set_rules('c_name','University Name','trim|required');
          $this->form_validation->set_rules('c_information','University Information','trim|required');
          $this->form_validation->set_rules('title', 'Job Title', 'trim|required');
          $this->form_validation->set_rules('salary', 'Job Salary', 'trim|required');
          $this->form_validation->set_rules('descriptions','Job Descriptions','trim|required');
          $this->form_validation->set_rules('responsibilities','Responsibilities','trim|required');
          $this->form_validation->set_rules('requirements','Requirements','trim|required');
                    $this->form_validation->set_rules('job_type','Job Type','trim|required');
          $this->form_validation->set_rules('phone','Phone','trim|required');
          $this->form_validation->set_rules('email','Email','trim|required');
          $this->form_validation->set_rules('c_email','Confirm Email','trim|required|matches[email]');
          $this->form_validation->set_rules('website','Website','trim');
          $this->form_validation->set_rules('address','Address','trim|required');
          $this->form_validation->set_rules('township','Township','trim|required');
        if($this->input->post('city')=='Other(Specify)')
          {
          $this->form_validation->set_rules('s_city','Specify City','trim|required');
        }
          $this->form_validation->set_rules('city','City','trim|required');
          $this->form_validation->set_rules('country','Country','trim|required');
             if($this->input->post('applicants')>=1)
        {

          $this->form_validation->set_rules('vt_day','Apply Valid To',"trim|required|callback_check_valid_to[$valid_to]|callback_valid_to_extend[$a]");

        }
        else
        {
          
          $this->form_validation->set_rules('vt_day','Apply Valid To',"trim|required|callback_check_valid_to[$valid_to]");
        }
          
          
          $this->form_validation->set_rules('vt_month','Date Of Birth','trim|required');
          $this->form_validation->set_rules('vt_year','Date Of Birth','trim|required');
      
          //$this->form_validation->set_rules('email','Email','trim|required');
                if ($this->form_validation->run() === FALSE)
                {
                  
                    $data['back_url']=$this->input->post('back_url');
                    $data['message']="Update Incomplete";
                    $data['message2']="Please Check The Input Fields";
                    if(isset($_POST['checked']))
                    {
                    $data['checked']=$this->input->post('checked');
                    }  
                    $data['job_id'] = $this->input->post('job_id');
                    $data['logo']=$this->input->post('logo');
                    $data['photo']=$this->input->post('photo');
                     $data['applicants']=$this->input->post('applicants');
                             $data['days']=$this->news_model->get_days();
        $data['months']=$this->news_model->get_months();
              $data['categories']= $this->news_model->get_categories(true);
                  
                     
                       $data['job_types'] = $this->news_model->get_job_types(true);
                       

                       $data['cities'] = $this->news_model->get_cities(true);
                       $data['salaries']=$this->news_model->get_salaries();
              
                
                  $this->load->view('templates/admin_header_logged');
                  $this->load->view('pages/admin_update_job',$data);
                  $this->load->view('templates/footer');
                  
                  }
                else{
               if($this->input->post('s_category'))
           {
              $this->news_model->insert_category();
           }
           if($this->input->post('s_city'))
           { 
               


               $result = $this->news_model->insert_city();
         
           }   
           
  
         $type="";
         $s_category="";
         $s_city="";
         if($this->input->post('s_category'))
         {
            $s_category = "Others";
           
         }
        if($this->input->post('s_city'))
         {
             $s_city = "Others";
         }
         $job_id=$this->input->post('job_id');
       
       
        $valid_to = date_create($valid_to);
        $valid_to = date_format($valid_to,"Y-m-d"); 
//$no =$this->input->post('no');
 $c_name = $this->input->post('c_name');
 $c_information = $this->input->post('c_information');
 $category=(!empty($s_category)?$s_category:$this->input->post('category'));
    $title = $this->input->post('title');
    $salary = $this->input->post('salary');
    $descriptions = $this->input->post('descriptions');
    $phone = $this->input->post('phone');
    $email = $this->input->post('email');
    $website = $this->input->post('website');
    $responsibilities = $this->input->post('responsibilities');
    $requirements = $this->input->post('requirements');
    $job_type = $this->input->post('job_type');
    $address = $this->input->post('address');
    $township = $this->input->post('township');
    $city=(!empty($s_city)?$s_city:$this->input->post('city'));
    $country = $this->input->post('country');
   
    $valid_to = $valid_to;
     date_default_timezone_set('Asia/Rangoon');
     
    $lu_date =date('Y-m-d H:i:s');

     if(($category && $city)!==('Others'||'Other(Specify)'))
    {
        $specified='';

    }

    
     $data = array('c_name'=>$c_name,'c_information'=>$c_information,'category'=>$category,
        'title'=>$title,'salary'=>$salary, 'descriptions'=>$descriptions, 'phone'=>$phone,
        'email'=>$email,'website'=>$website, 'responsibilities'=>$responsibilities,'requirements'=>$requirements,
         'job_type'=>$job_type,'address'=>$address,'township'=>$township,'city'=>$city,'country'=>$country
        ,'valid_to'=>$valid_to,'last_updated'=>$lu_date,'specified'=>$specified);
    $this->db->where('id', $job_id);
    $result =$this->db->update('jobs',$data);
      $this->db->where('job_id',$job_id);
    $this->db->update('applicants',array('job_valid_to'=>$valid_to));
   // $this->db->where('id', $no);
   // $this->db->update('companies_jobs',$data);
  

   
    if($result)
    {

      
            $data['job'] = $job;
         $data['categories']= $this->news_model->get_categories(true);



         $data['job_types']= $this->news_model->get_job_types(true);
         $data['cities'] = $this->news_model->get_cities(true);
         $data['salaries']=$this->news_model->get_salaries();
       
           //$data['back_url']=$this->input->post('back_url');
           $back_url=$this->input->post('back_url');
            $back_url=urlencode($back_url);
            $data['job_id']=$job_id;
            if(isset($_POST['checked']))
            {
            $data['checked']=$this->input->post('checked');
          }
        //    $data['message'] = "Update Completed.";
            redirect("admin_role/update_job/$job_id?message=Update Completed&back_url=$back_url");
         
    }
  }
}
else
{

       




  
  if(isset($_GET['checked']))
  {
  $data['checked']=$_GET['checked'];
  }

  
  
   
  $data['back_url']=(isset($_GET['back_url'])?$_GET['back_url']:"");

  $data['message']=(isset($_GET['message'])?$_GET['message']:"");
  $data['job']= $this->news_model->get_detail_job($job_id);
$data['applicants']=$this->db->select('*')->from('students_jobs')->where('job_id',$job_id)->get()->num_rows();
        $data['days']=$this->news_model->get_days();
        $data['months']=$this->news_model->get_months();
$data['categories']= $this->news_model->get_categories(true);
         $data['job_types'] = $this->news_model->get_job_types(true);
         $data['cities'] = $this->news_model->get_cities(true);
          $data['salaries']=$this->news_model->get_salaries();
         
 

      $this->load->view('templates/admin_header_logged');
  $this->load->view('pages/admin_update_job',$data);
$this->load->view('templates/footer');


}


}


public function job_entries_details($job_id="")
{


        
$job= $this->news_model->get_detail_job($job_id,true);
if($job_id=="" || empty($job))
  {

    redirect('admin_role/display_job_entries');
    exit();
  }
  

  
  if(isset($_GET['checked'])&&!empty($_GET['checked']))
  {
  $data['checked']=$_GET['checked'];
  
     
  }

  
  

 $data['back_url']=(isset($_GET['back_url'])?$_GET['back_url']:"");

  $data['job']= $job;

         
         
 

      $this->load->view('templates/admin_header_logged');
  $this->load->view('pages/admin_job_entries_details',$data);
$this->load->view('templates/footer');



}


public function expired_job_details($job_id='')
{


        
$job= $this->news_model->get_expired_job($job_id);
if($job_id=="" || empty($job))
  {

    redirect('admin_role/display_expired_jobs');
    exit();
  }
  

  
  if(isset($_GET['checked'])&&!empty($_GET['checked']))
  {
  $data['checked']=$_GET['checked'];
  
     
  }

  
  

 $data['back_url']=(isset($_GET['back_url'])?$_GET['back_url']:"");

  $data['job']= $job;

         
         
 

      $this->load->view('templates/admin_header_logged');
  $this->load->view('pages/admin_expired_job_details',$data);
$this->load->view('templates/footer');



}

public function delete_expired_jobs()
{
$user =$this->session->userdata('username');

if(!empty($_POST['job_id']))
{
$job_id=$this->input->post('job_id');
}
if(!empty($_POST['selected']))
{

 $job_id=$_POST['selected'];
}
if(empty($job_id))
{
  redirect('admin_role/display_expired_jobs');
}
 

 
 
 $previous_url= $this->input->post('previous_url');
   

 
 
 
 
 
 

    $data = array('delete_status'=>'true');
       $this->db->where('user_c',$user);
       $this->db->where_in('id',$job_id);
      $result = $this->db->update('jobs',$data);
          if($result)
          {     
    redirect($previous_url);
  }


}


public function create_job()

 {   

   if(isset($_POST['register']))
       {
     
             


             $vt_day=$this->input->post('vt_day');
             $vt_month=$this->input->post('vt_month');
             $vt_year = $this->input->post('vt_year');
             $valid_to = $vt_day."-".$vt_month ."-". $vt_year;

 
 
  $this->form_validation->set_rules('c_name','University Name','trim|required|max_length[200]');
  
  if($this->input->post('category')=='Other(Specify)')
  {
    $this->form_validation->set_rules('s_category','Specify Category'
    ,'trim|required');
  }

  $this->form_validation->set_rules('category','Job Category','trim|required');

  $this->form_validation->set_rules('c_information','University Information','trim|required');
  $this->form_validation->set_rules('title','Job Title','trim|required|');
  $this->form_validation->set_rules('salary','Job Salary','trim|required');
  $this->form_validation->set_rules('descriptions','Job Description','trim|required');
  $this->form_validation->set_rules('responsibilities','Responsibilities','trim|required');
  $this->form_validation->set_rules('requirements','Requirements','trim|required');
  
  $this->form_validation->set_rules('job_type','Job Type','trim|required');
  $this->form_validation->set_rules('phone','Phone','trim|required');
  $this->form_validation->set_rules('email','Email','trim|required|valid_emails');
  $this->form_validation->set_rules('c_email','Confirm Email','trim|required|valid_emails|matches[email]');
  $this->form_validation->set_rules('website','Website','trim');
  $this->form_validation->set_rules('address','Address','trim|required');
  $this->form_validation->set_rules('township','Township','trim|required');
  if($this->input->post('city')=='Other(Specify)')
  {
  $this->form_validation->set_rules('s_city','Specify City','trim|required');
}
  $this->form_validation->set_rules('city','City','trim|required');
  $this->form_validation->set_rules('country','Country','trim|required');
 
  
  //$this->form_validation->set_rules('valid_to','Valid To','trim|required|callback_check_date');
  
   $this->form_validation->set_rules('vt_day', 'Valid To', "trim|required|callback_check_valid_to[$valid_to]");
  $this->form_validation->set_rules('vt_month','Valid To','trim|required');
  $this->form_validation->set_rules('vt_year',"Valid To",'trim|required');
  

  

    
     
     if($this->form_validation->run()== FALSE)
     {



                $data['days']=$this->news_model->get_days();
        $data['months']=$this->news_model->get_months();


     $data['categories']= $this->news_model->get_categories(true);
         $data['job_types'] = $this->news_model->get_job_types(true);
         $data['cities'] = $this->news_model->get_cities(true);
          $data['salaries']=$this->news_model->get_salaries();
         $data['message']="Job Registration Unsuccessful";
         $data['message2']="Please Check The Input Fields";
       $this->load->view('templates/admin_header_logged');
      $this->load->view('pages/admin_jobr',$data);
      $this->load->view('templates/footer');
     }
     else
     {     
           if($this->input->post('s_category'))
           { 
              //$s_category=$this->input->post('s_category');
              $this->news_model->insert_category();
           }
           if($this->input->post('s_city'))
           { 
               


             // $s_city=$this->input->post('s_city');
              $this->news_model->insert_city();
         
           }   
          


           $this->news_model->job_create();
           redirect('admin_role/j_r_success');
       
     }
  }//end first if
   else
   {
 
          $data['days']=$this->news_model->get_days();
        $data['months']=$this->news_model->get_months();
$data['categories']= $this->news_model->get_categories(true);
         $data['job_types'] = $this->news_model->get_job_types(true);
         $data['cities'] = $this->news_model->get_cities(true);
         $data['salaries']=$this->news_model->get_salaries();
            $this->load->view('templates/admin_header_logged');
            $this->load->view('pages/admin_jobr',$data);
            $this->load->view('templates/footer');
            

   }



 }
 public function j_r_success()
 {
     $this->load->view('templates/admin_header_logged');
            $this->load->view('pages/JSuccess');
            $this->load->view('templates/footer');
            


 }

 public function display_applicants($q=0,$sort_by ='date_applied',$sort_order='desc',$limit=2,$offset = 0)
  {

        
if(isset($_GET['checked']))
      {
      $data['checked']=$_GET['checked'];
      //print_r($_POST['checked']);
    }
      else
      {
        $data['checked']=array();
      }
      if(!empty($q))
      {
          $q=base64_decode($q);

      }



         $is_ajax = $this->input->is_ajax_request();
             

        //$results = 
        parse_str($q, $_POST);
        $this->load->library('form_validation');

        // $this->input->loadquery($query_id);
        
          $this->input->post('some');
         
          $query_array = array(
        'name' => $this->input->post('name'),
        'job_id'=>$this->input->post('job_id'),
        'title'=>$this->input->post('title'),
        'id'=>$this->input->post('id'), 
        'applicant_id'=>$this->input->post('applicant_id')        
      
        );
         
         $data['query'] = $query_array;

        
     $sort_column=array('job_title','name','gender','date_applied');
          if(in_array($sort_by,$sort_column))
          {
         
          }
          else
          {
           
            $sort_by='date_applied';//change this to time format
            redirect('admin_role/display_applicants');
            
       
          }

          if($offset<0)
          {
            redirect('admin_role/display_applicants');
          }
          if($limit<0 ||empty($limit))
          {
            redirect('admin_role/display_applicants');
          }
        
             
          $user = $this->session->userdata('username');

         //$limit;
        $result = $this->news_model->search_applicants($query_array,$limit,$offset,
            $sort_by,$sort_order);
        
        $data['applicants'] = $result['rows'];
       
        $data['total_records']= $result['total_rows'];
        
                $data['myaccount'] = $this->news_model->get_company($user);


        $q=base64_encode($q);
        $this->load->library('pagination');
        $config = array();
        $config['base_url'] = base_url() . "admin_role/display_applicants/$q/$sort_by/$sort_order/$limit/";
        $config['total_rows']= $data['total_records'];
        $config['per_page'] = $limit; 
      $config['uri_segment'] = 7;
            $config['full_tag_open']='<p style="word-wrap:break-word;line-height:30px" id="pagination">';
         $config['full_tag_close']='</p>';
        $config['num_links']= 5;
        $config['first_link'] = '<&nbspFirst';
        $config['last_link']='Last&nbsp>';
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
         $data['sort_by'] = $sort_by ;
         $data['sort_order'] = $sort_order;  
         $data['q']=$q;
         $data['limit'] = $limit;
         $data['offset']=$offset;
         $data['segment']=$this->uri->uri_string();
         
              //$data['segment'] = "" ;
        //print_r($data);
       $data['user'] = $this->session->userdata('username');
$is_logged_in = $this->session->userdata('is_logged_in');

          if($is_ajax)
          {
            $this->load->view('pages/ajax_admin_applicants',$data);
          }
          else
          {
$this->load->view('templates/admin_header_logged');
          $this->load->view('pages/admin_applicants',$data);
          $this->load->view('templates/footer');
      }


  }



public function details_job($job_id='',$url='')
{

  $job=$this->news_model->get_detail_job($job_id,false);
  
   
     
  $url=$this->uri->segment(4);
  if($job_id=='' || empty($job)||empty($url))
  {

    redirect('admin_role/display_applicants');
    exit();
  }  
  
    

$data['job'] = $job;
  
     $this->load->view('templates/admin_header_logged');
  $this->load->view('pages/admin_details_job',$data);
  $this->load->view('templates/footer');


}

  public function applicants_rows()
  {
$q = $this->input->post('q');
  $q = (empty($q)?0:$q);
  $sort_by = $this->input->post('sort_by');
  $sort_by = (empty($sort_by)?'date_applied':$sort_by);
  $sort_order = $this->input->post('sort_order');
  $sort_order = (empty($sort_order)?'desc':$sort_order);
  /*$offset = $this->input->post('offset2');
$offset = (empty($sort_order)?0:$offset); */
 $limit = $this->input->post('rows');

 $data['select']=$limit;
  
 //redirect("admin_role/display_students/".$q."/".$sort_by."/".$sort_order."/".$limit."/".$offset);
//  echo "q:" . $q. "\nsort_by: " . $sort_by. "\nsort_order : ". $sort_order;
redirect("admin_role/display_applicants/".$q."/".$sort_by."/".$sort_order."/".$limit."/0");  



  }
  public function view_applicant($applicant="")
  {


         

      $applicant=$this->news_model->get_applicant_details($applicant);
      if($applicant=="" || empty($applicant))
  {

   redirect('admin_role/display_applicants');
    exit();
  }
  
  


          

  if(isset($_GET['checked']))
  {
  $data['checked']=$_GET['checked'];
}
  
  
  $data['details_url'] = $this->input->get('details_url');
  $data['applicant']= $applicant;
  
  
      $this->load->view('templates/admin_header_logged');
  $this->load->view('pages/admin_applicant_details',$data);
$this->load->view('templates/footer');

}


public function delete_applicant()
{


 


$user =$this->session->userdata('username');

if(!empty($_POST['applicant_id']))
{
$applicant_id=$this->input->post('applicant_id');
}
if(!empty($_POST['selected']))
{

 $applicant_id=$_POST['selected'];
}
if(empty($applicant_id))
{
  redirect('admin_role/display_applicants');
}
 
 
 $previous_url=$this->input->post('previous_url');
 
 
  
    $data = array('delete'=>'true');
       $this->db->where('user_c',$user);
       $this->db->where_in('applicant_id',$applicant_id);
      $result = $this->db->update('applicants',$data);
     // echo "update result is: " .$result;
      
               
    if($result)
      {
          /*  if(!empty($applicant))
            {
              redirect('admin_role/display_applicants');
            }*/
           // else
            //{
             redirect($previous_url);
           
              //   }
            
            
            }


  



}






public function add_university_name()
{
             $result = $this->db->select("*")->from('university_name')
             ->get();

               $data['university_names'] = $result->result_array();
            /*   $university_names = array(''=>'');
               foreach( $result as $row)
               {
                       $university_names[$row['university_name']]=$row['university_name'];

               }
*/
             
              // $data['university_names'] = $university_names;
      $this->load->view('templates/admin_header_logged');
      $this->load->view('pages/add_university_name',$data);
      $this->load->view('templates/footer');

}



public function add_category_name()
{     $data['category_names']=$this->news_model->get_category_options();
      $this->load->view('templates/admin_header_logged');
      $this->load->view('pages/add_category_name',$data);
      $this->load->view('templates/footer');

}
public function display_categories($q=0,$sort_by="category",$sort_order="asc",$limit=2,$offset=0)
{

        $is_ajax =$this->input->is_ajax_request();
       
    
         if(!empty($q))
         {
            $q=base64_decode($q);
         }        
               parse_str($q, $_POST);
      

        
        

         
          $query = array(
        'category' => $this->input->post('category'));


             
          $user = $this->session->userdata('username');

         $sort_by2="added_date";
         $sort_order2="desc";
        $result = $this->news_model->search_category($query,$sort_by,$sort_order,$limit,$offset);
        $result2=$this->news_model->search_other_categories($q,$sort_by2,$sort_order2,$limit,$offset);

        $data['categories'] = $result['rows'];
       
        $data['total_records']= $result['total_rows'];
        $data['other_categories']=$result2['rows'];
        $data['other_categories_tr']=$result2['total_rows'];


        $q=base64_encode($q);
        $config = array();
        $config['base_url'] = base_url() ."admin_role/display_categories/$q/$sort_by/$sort_order/$limit/";
        $config['total_rows']= $data['total_records'];
        $config['per_page'] = $limit; 
        $config['uri_segment'] = 7;
          $config['full_tag_open']='<p style="word-wrap:break-word;line-height:30px" id="pagination">';
         $config['full_tag_close']='</p>';
        $config['num_links']= 5;
        $config['first_link'] = '<&nbspFirst';
        $config['last_link']='Last&nbsp>';
       
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
         $data['sort_by'] = $sort_by ;
         $data['sort_order'] = $sort_order;  
         $data['q']=$q;
         $data['limit']=$limit;
         $data['segment']=$this->uri->uri_string();
         $data['status']=$this->session->flashdata('status');
         $data['category_name']=$this->session->flashdata('category_name');
         $data['id']=$this->session->flashdata('id');
         $data['offset']=$offset;
              //$data['segment'] = "" ;
         $sort_by2="added_date";
          $config2 = array();
        $config2['base_url'] = base_url() ."admin_role/display_other_categories/$q/$sort_by2/$sort_order2/$limit/";
        $config2['total_rows']= $data['other_categories_tr'];
        $config2['per_page'] = $limit; 
        $config2['uri_segment'] = 7;
         $config2['full_tag_open']='<p style="word-wrap:break-word;line-height:30px" id="pagination">';
         $config2['full_tag_close']='</p>';
        $config2['num_links']= 5;
        $config2['first_link'] = '<&nbspFirst';
        $config2['last_link']='Last&nbsp>';
       
        $this->pagination->initialize($config2);
        $data['pagination2'] = $this->pagination->create_links();
        //print_r($data);
        $data['sort_by2']="added_date";
        $data['sort_order2']="desc";
       $data['user'] = $this->session->userdata('username');
$is_logged_in = $this->session->userdata('is_logged_in');
           if($is_ajax)
           {

               $this->load->view('pages/ajax_categories',$data);
           }
           else
           {
$this->load->view('templates/admin_header_logged');
          $this->load->view('pages/admin_categories',$data);
          $this->load->view('templates/footer');
            } 

  

}

public function display_cities($q=0,$sort_by="cities",$sort_order="asc",$limit=2,$offset=0)
{

        $is_ajax =$this->input->is_ajax_request();
      
    
         if(!empty($q))
         {
            $q=base64_decode($q);
         }        
               parse_str($q, $_POST);
      

        
        

         
          $query = array(
        'city' => $this->input->post('city'));


             
          $user = $this->session->userdata('username');

         $sort_by2="added_date";
         $sort_order2="desc";
        $result = $this->news_model->search_city($query,$sort_by,$sort_order,$limit,$offset);
        $result2=$this->news_model->search_other_cities($q,$sort_by2,$sort_order2,$limit,$offset);

        $data['cities'] = $result['rows'];
       
        $data['total_rows']= $result['total_rows'];
        $data['other_cities']=$result2['rows'];
        $data['other_cities_tr']=$result2['total_rows'];


        $q=base64_encode($q);
        $config = array();
        $config['base_url'] = base_url() ."admin_role/display_cities/$q/$sort_by/$sort_order/$limit/";
        $config['total_rows']= $data['total_rows'];
        $config['per_page'] = $limit; 
        $config['uri_segment'] = 7;
           $config['full_tag_open']='<p style="word-wrap:break-word;line-height:30px" id="pagination">';
         $config['full_tag_close']='</p>';
        $config['num_links']= 5;
        $config['first_link'] = '<&nbspFirst';
        $config['last_link']='Last&nbsp>';
            
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
         $data['sort_by'] = $sort_by ;
         $data['sort_order'] = $sort_order;  
         $data['q']=$q;
         $data['limit']=$limit;
         $data['segment']=$this->uri->uri_string();
         $data['status']=$this->session->flashdata('status');
         $data['category_name']=$this->session->flashdata('category_name');
         $data['id']=$this->session->flashdata('id');
         $data['offset']=$offset;
              //$data['segment'] = "" ;
         $sort_by2="added_date";
          $config2 = array();
        $config2['base_url'] = base_url() ."admin_role/display_other_cities/$q/$sort_by2/$sort_order2/$limit/";
        $config2['total_rows']= $data['other_cities_tr'];
        $config2['per_page'] = $limit; 
        $config2['uri_segment'] = 7;
         $config['full_tag_open']='<p style="word-wrap:break-word;line-height:30px" id="pagination">';
         $config['full_tag_close']='</p>';
        $config['num_links']= 5;
        $config['first_link'] = '<&nbspFirst';
        $config['last_link']='Last&nbsp>';
       
        $this->pagination->initialize($config2);
        $data['pagination2'] = $this->pagination->create_links();
        //print_r($data);
        $data['sort_by2']="added_date";
        $data['sort_order2']="desc";
       $data['user'] = $this->session->userdata('username');
$is_logged_in = $this->session->userdata('is_logged_in');
           if($is_ajax)
           {

               $this->load->view('pages/ajax_admin_cities',$data);
           }
           else
           {
$this->load->view('templates/admin_header_logged');
          $this->load->view('pages/admin_cities',$data);
          $this->load->view('templates/footer');
            } 

  

}
public function display_other_categories($q=0,$sort_by2="added_date",$sort_order2="desc",$limit=2,$offset=0)
{

 $result2=$this->news_model->search_other_categories($q,$sort_by2,$sort_order2,$limit,$offset);

        
       
        $is_ajax=$this->input->is_ajax_request();
        $data['other_categories']=$result2['rows'];
        $data['other_categories_tr']=$result2['total_rows'];
          $config2 = array();
        $config2['base_url'] = base_url() ."admin_role/display_other_categories/$q/$sort_by2/$sort_order2/$limit/";
        $config2['total_rows']= $data['other_categories_tr'];
        $config2['per_page'] = $limit; 
        $config2['uri_segment'] = 7;
          $config2['full_tag_open']='<p style="word-wrap:break-word;line-height:30px" id="pagination">';
         $config2['full_tag_close']='</p>';
        $config2['num_links']= 5;
        $config2['first_link'] = '<&nbspFirst';
        $config2['last_link']='Last&nbsp>';
      
        $this->pagination->initialize($config2);
        $data['pagination2'] = $this->pagination->create_links();
        $data['sort_by2']=$sort_by2;
        $data['sort_order2']=$sort_order2;
        $data['q']=$q;
        $data['limit']=$limit;
   if($is_ajax)
           {

               $this->load->view('pages/ajax_admin_other_ca',$data);
           }
           else
           {
$this->load->view('templates/admin_header_logged');
          $this->load->view('pages/about',$data);
          $this->load->view('templates/footer');
            } 


}


public function display_other_cities($q=0,$sort_by2="added_date",$sort_order2="desc",$limit=2,$offset=0)
{

 $result2=$this->news_model->search_other_cities($q,$sort_by2,$sort_order2,$limit,$offset);

        
       
        $is_ajax=$this->input->is_ajax_request();
        $data['other_cities']=$result2['rows'];
        $data['other_cities_tr']=$result2['total_rows'];
          $config2 = array();
        $config2['base_url'] = base_url() ."admin_role/display_other_cities/$q/$sort_by2/$sort_order2/$limit/";
        $config2['total_rows']= $data['other_cities_tr'];
        $config2['per_page'] = $limit; 
        $config2['uri_segment'] = 7;
         $config2['full_tag_open']='<p style="word-wrap:break-word;line-height:30px" id="pagination">';
         $config2['full_tag_close']='</p>';
        $config2['num_links']= 5;
        $config2['first_link'] = '<&nbspFirst';
        $config2['last_link']='Last&nbsp>';
       
        $this->pagination->initialize($config2);
        $data['pagination2'] = $this->pagination->create_links();
        $data['sort_by2']=$sort_by2;
        $data['sort_order2']=$sort_order2;
        $data['q']=$q;
        $data['limit']=$limit;
   if($is_ajax)
           {

               $this->load->view('pages/ajax_admin_other_cities',$data);
           }
           else
           {
$this->load->view('templates/admin_header_logged');
          $this->load->view('pages/about',$data);
          $this->load->view('templates/footer');
            } 


}
public function add_category()
{ 


         $category=$this->input->post('category');
         if(empty($category))
         {
          redirect('admin_role/display_categories');
          exit();

         }
         $url=$this->input->post('url');
            $result=$this->db->select('category')->where(array('category'=>$category,'delete_status !='=>'true'))
          ->from('job_categories')->get()->num_rows();
          
          if($result>0)
          {
           $message=array('message'=>'Can Not Add. Category Existed Already');
           echo json_encode($message);
          // echo "Can Not Add.Category Existed Already";
          // redirect($url);

          }

            else
          {
                    $result2=$this->db->select('category,category_id')->where(array('category'=>$category,'delete_status '=>'true'))
                  ->from('job_categories')->get();
                   if($result2->num_rows()>0)
                   {
                     $id=$result2->result_array();
                     $id=$id[0]['category_id'];
                    $this->db->where('category_id',$id);
                    $this->db->update('job_categories',array('delete_status'=>''));
                   }
                   else
                   {

                     $this->db->insert('job_categories',array('category'=>$category));
                   }
                 $message=array('message'=>'Added Completed');
                echo json_encode($message);

           
             //redirect($url);
        
          }
 

}
public function add_city()
{ 

        
         $city=$this->input->post('city');
          if(empty($city))
      {
        redirect('admin_role/display_cities');
        exit();
      }
         $url=$this->input->post('url');
            $result=$this->db->select('cities')->where(array('cities'=>$city,'delete_status !='=>'true'))
          ->from('cities')->get()->num_rows();
          
          if($result>0)
          {
           $message=array('message'=>'Can Not Add. City Existed Already');
           echo json_encode($message);
          // echo "Can Not Add.Category Existed Already";
          // redirect($url);

          }

            else
          {
                    $result2=$this->db->select('cities,id')->where(array('cities'=>$city,'delete_status '=>'true'))
                  ->from('cities')->get();
                   if($result2->num_rows()>0)
                   {
                     $id=$result2->result_array();
                     $id=$id[0]['id'];
                    $this->db->where('id',$id);
                    $this->db->update('cities',array('delete_status'=>''));
                   }
                   else
                   {

                     $this->db->insert('cities',array('cities'=>$city));
                   }
                 $message=array('message'=>'Added Completed');
                echo json_encode($message);

           
             //redirect($url);
        
          }
 

}
public function update_category_name()
{
  
  
  
  $status=$this->input->post('status');
  $this->session->set_flashdata('status',$status);
  $url =$this->input->post('url');
  ////$id = $this->input->post('id');
  $array =$_POST;
  $id = array_search('Update',$array);
  $this->session->set_flashdata('id',$id);
 $category_name= $this->db->select("category")->from('job_categories')
 ->where('category_id',$id)->get()->result_array();
   

   $category_name=$category_name[0]['category'];
   $this->session->set_flashdata('category_name',$category_name);
  // $data['id']=$id;
   /*return;

               $data['all_categories'];
             $result2 = $this->db->get_where('university_name',array('id'=>$id));
             $data['result2']=$result2->result_array();*/

       
       //// $this->load->view('pages/ajax_update_category',$data);
   redirect($url);     



}
public function edit_category()
{

 $category=$this->input->post('category');
 $id=$this->input->post('id');
 if(empty($category)||empty($id))
 {

  redirect('admin_role/display_categories');
  exit();
 }
 $result2=$this->db->select('*')->from('job_categories')->where(array('category'=>$category,'delete_status !='=>'true'))
 ->get();
 $result4=$this->db->select('category')->from('job_categories')->where('category_id',$id)->get()->row_array();
 if($result2->num_rows()>0 && $result4['category'] !==$category)
 {
    echo json_encode(array('message'=>'Can Not Update. Category Already Existed'));
 }
 else
 {

 
  $this->db->where('category_id',$id);
      $result =   $this->db->update('job_categories',array('category'=>$category));
     echo json_encode(array('message'=>'Edit Completed'));
    }
}




public function edit_city()
{

 $city=$this->input->post('city');
 $id=$this->input->post('id');
  if(empty($city)||empty($id))
      {
        redirect('admin_role/display_cities');
        exit();
      }
 $result2=$this->db->select('*')->from('cities')->where(array('cities'=>$city,'delete_status !='=>'true'))
 ->get();
 $result4=$this->db->select('cities')->from('cities')->where('id',$id)->get()->row_array();
 if($result2->num_rows()>0 && $result4['cities'] !== $city)
 {
    echo json_encode(array('message'=>'Can Not Update. Category Already Existed'));
 }
 else
 {
    echo json_encode(array('message'=>'Edit Completed'));
  $this->db->where('id',$id);
      $result =   $this->db->update('cities',array('cities'=>$city));
    
    }
}
public function status_other_categories()
{
      $status=$this->input->post('status');
      $id=$this->input->post('id');
      if(empty($status)||empty($id))
      {
        redirect('admin_role/display_categories');
        exit();
      }
      $url=$this->input->post('url');
      $this->db->where('id',$id);
      $this->db->update('other_categories',array('status'=>$status));
      redirect($url);


}

public function status_other_cities()
{
      $status=$this->input->post('status');
      $id=$this->input->post('id');
       if(empty($status)||empty($id))
      {
        redirect('admin_role/display_cities');
        exit();
      }
      $url=$this->input->post('url');
      $this->db->where('id',$id);
      $this->db->update('other_cities',array('status'=>$status));
      redirect($url);


}
public function update_category_n()
{
if(isset($_POST['update_submit']))
    {  
      

    


    $status=$this->input->post('status');
    $this->session->set_flashdata('status',$status);
    $url =$this->input->post('url');
    $id = $this->input->post('id');
    $this->session->set_flashdata('id',$id);

    $name = $this->input->post('category_name');
    
    $this->db->where('category_id',$id);
      $result =   $this->db->update('job_categories',array('category'=>$name));
      if($result)
      {

        $this->session->set_flashdata('category_name',$name);
        $this->session->set_flashdata('message','Update Category Name Successful');
      redirect($url);
    }
    
     }
     else
     {

      

     }

}
public function cancel_category()
{
   $this->session->set_flashdata('status',false);
   $url=$this->input->post('url');
   redirect($url);

}
public function delete_category()
{
     //print_r ($_POST);
      
      $array=$_POST;
      $id=$this->input->post('id');
      $url=$this->input->post('url');
      if(empty($id))
      {
        redirect('admin_role/display_categories');
        exit();

      }
       



       $this->db->where('category_id',$id);
      $this->db->update('job_categories',array('delete_status'=>'true'));
     redirect($url);




}
public function delete_other_categories()
{

 $id=$this->input->post('id');

 
 if(empty($id))
 {
  redirect('admin_role/display_categories');
  exit();
 }
      $url=$this->input->post('url');
       $this->db->where('id',$id);
      $this->db->update('other_categories',array('delete_status'=>'true'));
     redirect($url);


}

public function delete_city()
{
     //print_r ($_POST);
      
      $array=$_POST;
      $id=$this->input->post('id');
       if(empty($id))
      {
        redirect('admin_role/display_cities');
        exit();
      }
      $url=$this->input->post('url');
       $this->db->where('id',$id);
      $this->db->update('cities',array('delete_status'=>'true'));
     redirect($url);




}
public function delete_other_cities()
{

 $id=$this->input->post('id');
  if(empty($id))
      {
        redirect('admin_role/display_cities');
        exit();
      }
      $url=$this->input->post('url');
       $this->db->where('id',$id);
      $this->db->update('other_cities',array('delete_status'=>'true'));
     redirect($url);


}
public function delete_category_name()
{

   print_r($_POST);

   $id=$this->input->post('delete_hidden');
   $name=$this->input->post('delete_cn');
   $result=array('category'=>$name);
   
   $this->db->delete('job_categories',array('category_id'=>$id));
   $data['message_delete']="Delete Successful";
   $data['update']=true;
   $data['delete_cn']=$name;
   $data['category_names']=$this->news_model->get_category_options();
   $this->load->view('templates/header');
   $this->load->view('pages/add_category_name',$data);
   $this->load->view('templates/footer');


}


public function update_university_name()
{


  $data['update'] = $this->input->post('update_u');
  
  $array = $_POST;
  
  
  
  $id = array_search('Update',$array);
  
 $result = $this->db->select("*")->from('university_name')
             ->get();

               $data['university_names'] = $result->result_array();
             $result2 = $this->db->get_where('university_name',array('id'=>$id));
             $data['result2']=$result2->result_array();

        $this->load->view('templates/admin_header_logged');
        $this->load->view('pages/add_university_name',$data);
        $this->load->view('templates/footer');


}

public function update_u_name()
{

    if(isset($_POST['update_submit']))
    {   
    $id = $this->input->post('id');

    $name = $this->input->post('u_name');
    $this->db->where('id',$id);
      $result =   $this->db->update('university_name',array('u_name'=>$name));
    //  echo $result;
      $result4 = $this->db->select("*")->from('university_name')
             ->get();

               $data['university_names'] = $result4->result_array();
                $result2 = $this->db->get_where('university_name',array('id'=>$id));
             $data['result2']=$result2->result_array();
             $data['update']= 'true';

$data['message'] = "Update University Name Successful";
     $this->load->view('templates/admin_header_logged');
  $this->load->view('pages/add_university_name',$data);
  $this->load->view('templates/footer');
     }
     else
     {
     $data['update']= 'true';
 $result4 = $this->db->select("*")->from('university_name')
             ->get();

               $data['university_names'] = $result4->result_array();
     $this->load->view('templates/admin_header_logged');
  $this->load->view('pages/add_university_name',$data);
  $this->load->view('templates/footer');


     }
}
public function register_university()
{
  $data['university_names_dd'] = $this->news_model->university_names();

    $this->load->view('templates/admin_header_logged');
    $this->load->view('pages/university_r',$data);
    $this->load->view('templates/footer');

}

public function delete_university_name()
{
      $array = $_POST;
      $id = array_search('Delete',$array);
      $this->db->where('id',$id);
      $this->db->delete('university_name');
$result = $this->db->select("*")->from('university_name')
             ->get();

               $data['university_names'] = $result->result_array();


        $this->load->view('templates/admin_header_logged');
        $this->load->view('pages/add_university_name',$data);
        $this->load->view('templates/footer');

}





public function add_new_university_name()
{
       $university_name = $this->input->post('university_name');
       if(empty($university_name))
       {  
        echo "Empty University Name";
        exit();


       }
       $entry = array('u_name'=>$university_name);
       $result = $this->db->insert('university_name',$entry);
       if($result)
       {
         $answer = $this->db->select("*")->from('university_name')
             ->get();

               $data['university_names'] = $answer->result_array();
         $data['message']="University Added Successfully";
        $this->load->view('templates/admin_header_logged');
        $this->load->view('pages/add_university_name',$data);
        $this->load->view('templates/footer');
       }
       else{
        return false;
       }



}

public function change_password()
{ 
    $user = $this->session->userdata('username');

         if(isset($_POST['change_password']))
         { 
        
$this->form_validation->set_rules('password','Password','trim|required|min_length[4]');
$this->form_validation->set_rules('c_password','Confirm Password','trim|required|min_length[4]|matches[password]');
if ($this->form_validation->run() === FALSE)
{
       $data['message']="Password Update Incompleted";
      
       //$data['user']=$this->input->post('user');
    $this->load->view('templates/admin_header_logged');
  $this->load->view('pages/change_au_password',$data);
  $this->load->view('templates/footer');

     

}
else{
          
     // $user = $this->input->post('user');
    $result = $this->news_model->change_password($user);
     if($result)
     {
         
          $data['message']="Password has been updated";
    $this->load->view('templates/admin_header_logged');
  $this->load->view('pages/change_au_password', $data);
  $this->load->view('templates/footer');

     }

}
}
else
{

 
             
    $this->load->view('templates/admin_header_logged');
  $this->load->view('pages/change_au_password');
  $this->load->view('templates/footer');

}
}
public function check_name($str)
  {
        $query = $this->news_model->check_user($str);
        
    if ($query)
    {
       return TRUE;
    }
    else
    {$this->form_validation->set_message('check_name', 'Choose another user name');
      return FALSE;
      
    }      
  

      
  }

  public function check_space($string)
{
   if(preg_match("/[\s]+/",$string))
   {
    
    $this->form_validation->set_message('check_space','white space is not allowed');
    return false;
   }
   else
   {
    return true;
   }

}

public function check_characters($string)
{
           /////^[a-z0-9:_!#@$%^&*(){}+|\\\<>\/\[\]\-]+$/i";   
if(! preg_match("/^[a-z0-9:_!@#$%^&*()<>{}+=\/-]+$/i", $string))
{
    
    $this->form_validation->set_message('check_characters','The username have disallowed characters');
    return false;
   }
   else
   {
    return true;
   }

}


public function check_email($str)
{
  $result= $this->news_model->check_duplicate_email($str);
  if($result)
  {
     return true;

  } 
  else
  {
     $this->form_validation->set_message('check_email', 'Choose Another Email');
      return FALSE;

  }

}


public function create_university()
{
$this->load->helper('form');
  $this->load->library('form_validation');
     
  $this->form_validation->set_rules('user','User','trim|required|callback_check_name|callback_check_space|callback_check_characters|max_length[200]');
  
  $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|max_length[200]');
  $this->form_validation->set_rules('cpassword', 'Confirm Password', 'trim|required|matches[password]|min_length[4]|max_length[200]');
  $this->form_validation->set_rules('name','Name','trim|required|max_length[200]');
  
  $this->form_validation->set_rules('phone','Phone','trim|required|max_length[200]');
  $this->form_validation->set_rules('email','Email','trim|required|callback_check_email|max_length[200]');
  $this->form_validation->set_rules('c_email','Confirm Email','trim|required|matches[email]|max_length[200]');
  $this->form_validation->set_rules('website',"Website",'trim|max_length[200]');
  $this->form_validation->set_rules('address','Address','trim|required|max_length[200]');
  $this->form_validation->set_rules('township','Township','trim|required|max_length[200]');
  $this->form_validation->set_rules('city','City','trim|required|max_length[200]');
  $this->form_validation->set_rules('country','Country','trim|required|max_length[200]');
  $this->form_validation->set_rules('u_name','University Name','trim|required|max_length[200]');
  //$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
  
  

  
  if ($this->form_validation->run() === FALSE)
  { 
    $is_logged_in = $this->session->userdata('is_logged_in');
    
    
    
    $data['university_names_dd'] = $this->news_model->university_names();
    
    $data['message']= 'Registration Unsuccessful';
    $this->load->view('templates/admin_header_logged');
    $this->load->view('pages/university_r',$data);
    $this->load->view('templates/footer');
    

  }
  else
  {
    $data['message']="University Registration Completed";
    $this->news_model->set_university();
    $data['university_names_dd']=$this->news_model->university_names();
    $this->load->view('templates/admin_header_logged');
    $this->load->view('pages/university_r',$data);
    $this->load->view('templates/footer');

  }



}

public function signout()
{

             $this->session->sess_destroy();
           /*  header("cache-Control: no-store, no-cache, must-revalidate");
header("cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");*/
           // $this->cache->clean();
            $this->db->cache_delete_all();
            redirect(base_url() .'admin_role');

  

}



}