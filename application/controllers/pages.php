<?php

class Pages extends CI_Controller {
    
//  var $file_content=$data['file']['file_content'];
	public function __construct()
	{
		parent::__construct();
/*
 $this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
$this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
$this->output->set_header('Pragma: no-cache');*/
        
   header("cache-Control: no-store, no-cache, must-revalidate");
header("cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
date_default_timezone_set('Asia/Rangoon');
   
  header("Expires: Thu, 19 Nov 1981 08:52:00 GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");

    
		$this->load->model('news_model');
         $this->load->library('pagination');
         $this->load->helper('form');
         $this->load->helper('url');
         
        
	$this->load->library('form_validation');   
	$this->load->library('session'); 
  $this->form_validation->set_error_delimiters('<span class="error" 
    style="color:red;">','</span>');  
  $this->load->library('javascript');
  $this->load->library('javascript/jquery',FALSE);         
	}
  public function calendar($year=null,$month=null)
  {
     $prefs = array (
               'start_day'=>"monday",
               'show_next_prev'  => TRUE,
               'next_prev_url'   => base_url() .'pages/calendar'
             );

         $this->load->library('calendar',$prefs);
             echo $this->calendar->generate($year,$month);

  }
  public function disk_free()
  {
       $name = $this->input->post('name');
       echo $name;
//    $this->load->view('pages/ajax_practice');
  }
public function display_jobs($q=0,$sort_by = 'last_updated',$sort_order= 'desc',$offset = 0)
	{   

        ////$this->load->library('form_validation');
      // $q=urldecode($q);
       
        $is_ajax = $this->input->post('is_ajax');
        $limit = 4;
        $data1 = array('job_status'=>'expired');
         $today = date("Y-m-d");
         $this->db->where('valid_to <',$today);
        $result =$this->db->update('jobs',$data1);

         $data4 = array('job_status'=>'opened');
         $this->db->where('valid_to >=',$today);
         $result = $this->db->update('jobs',$data4 );

        
       
             // echo str_replace('\(','%28',$q );         
          
          if(!empty($q))
          {   
             
             $q=base64_decode($q);
            
          }

     
          parse_str($q,$_POST);
        
         $query_array = array(
            'c_name'=>$this->input->post('c_name'),
            'title' => $this->input->post('title'),
          'id'=>$this->input->post('job_id'),
        'category' =>$this->input->post('category'),
        
        'job_type'=> $this->input->post('job_type'),
        'city'=> $this->input->post('city'),
        'salary'=> $this->input->post('salary')
        //'salary' => $this->input->post('salary'),
        
        );
  $sort_column=array('none','last_updated','c_name','category','title','job_type','city','valid_to');
          if(in_array($sort_by,$sort_column))
          {
         
          }
          else
          {
           
            $sort_by='last_updated';//change this to time format
            redirect('pages/display_jobs');
            
       
          }

          if($offset<0)
          {
            redirect('pages/display_jobs');
          }
             
              
         

          
         


         $data['query']=$query_array;
     
        $result = $this->news_model->search($query_array,$limit,$offset,
            $sort_by,$sort_order);
       
        $data['jobs'] = $result['rows'];
        $data['total_records']= $result['total_rows'];
        $data['message']= $result['message'];
        $q=base64_encode($q);
        $this->load->library('pagination');
        $config = array();
        $config['base_url'] = base_url() . "pages/display_jobs/$q/$sort_by/$sort_order/";
        $config['total_rows']= $data['total_records'];
        $config['per_page'] = $limit; 
        $config['uri_segment'] = 6;
        $config['full_tag_open']='<div style="word-wrap:break-word;line-height:30px" id="pagination">';
         $config['full_tag_close']='</div>';
         $config['first_link'] = '<&nbspFirst';

        $config['last_link'] = 'Last&nbsp>';
       /* $config['last_tag_open'] = "<span style='background:red;color:black'>";
        $config['last_tag_close'] = '</span>';*/
        $config['num_links']= 7;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
         $data['sort_by'] = $sort_by ;
         $data['sort_order'] = $sort_order;    
          $data['q'] = $q;   
          $data['offset']=$offset;
          $data['limit']=$limit;

          $data['top_jobs'] = $this->news_model->top_jobs();
          $data['recommended']=$this->news_model->recommended();
      
         
        
          
        
       
        /////  $data['top_ten_jobs'] = $top_ten_jobs;
         
       
         $data['categories']= $this->news_model->get_categories();
       
         $data['job_types'] = $this->news_model->get_job_types();
       
         $data['cities'] = $this->news_model->get_cities();
         $data['salaries']=$this->news_model->get_salaries(true);
        

       
$is_logged_in = $this->session->userdata('is_logged_in');
$role =$this->session->userdata('role');
		
    
     
       $this->load->view('templates/cheader');
          $this->load->view('pages/job',$data);
          $this->load->view('templates/footer');
      

	}
  public function change_page()
  {
     

       $page=$this->input->post('page');

       $limit=$this->input->post('limit');
       if(empty($page)||empty($limit))
       {
        redirect('pages/display_jobs');
        exit();
       }
        $q=$this->input->post('q');
         $sort_by=$this->input->post('sort_by');
          $sort_order=$this->input->post('sort_order');
       $offset=($page*$limit)-$limit;
       $url="pages/display_jobs"."/$q/$sort_by/$sort_order/$offset";

       
       redirect($url);




  }

  public function display_job_entries($q=0,$sort_by ='last_updated',$sort_order='desc',$offset = 0)
  
  {


      $data1 = array('job_status'=>'expired');
         $today = date("Y-m-d");
         $this->db->where('valid_to <',$today);
        $result =$this->db->update('jobs',$data1);
    
    
if(isset($_GET['checked']))
      {
      $data['checked']=$_GET['checked'];
      //print_r($_POST['checked']);
    }
      else
      {
        $data['checked']=array();
      }

          $is_ajax=$this->input->is_ajax_request();
             
             if(!empty($q))
             {
                $q=base64_decode($q);
              }
          
    
$limit = 2;
  
        
       parse_str($q,$_POST);

        
          $query_array = array(
        'id' => $this->input->post('job_id'),
        'category' => $this->input->post('category'),
        'title'=> $this->input->post('title'),
        'salary'=>$this->input->post('salary'),
        'job_type'=>$this->input->post('job_type'),
        'job_status'=>$this->input->post('job_status')

       // 'status' => $this->input->post('status')
        );
        $sort_column=array('last_updated','opened_date','published_date','category','title','job_type','city','valid_to');
          if(in_array($sort_by,$sort_column))
          {
         
          }
          else
          {
           
            $sort_by='last_updated';//change this to time format
            redirect('pages/display_job_entries');
            
       
          }

          if($offset<0)
          {
            redirect('pages/display_job_entries');
          }
             
          $data['query']=$query_array;
             
          $user = $this->session->userdata('username');

        $is_ajax=$this->input->is_ajax_request();
        $result = $this->news_model->search_job_entries($query_array,$limit,$offset,
            $sort_by,$sort_order);
       // print_r($result);
        $data['jobs'] = $result['rows'];
       
        $data['total_records']= $result['num_rows'];
        
               // $data['myaccount'] = $this->news_model->get_account($user);

        $q=base64_encode($q);
        
        $this->load->library('pagination');
        $config = array();
        $config['base_url'] = base_url() .  "/pages/display_job_entries/$q/$sort_by/$sort_order/";
        $config['total_rows']= $data['total_records'];
        $config['per_page'] = $limit; 
        $config['uri_segment'] = 6;
        $config['full_tag_open']='<p style="word-wrap:break-word;line-height:30px" id="pagination">';
         $config['full_tag_close']='</p>';
        $config['num_links']= 5;
        $config['first_link'] = '<&nbspFirst';
        $config['last_link']='Last&nbsp>';
       
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
         $data['sort_by'] = $sort_by ;
         $data['sort_order'] = $sort_order;  
         $data['q']=$q;
         $data['limit']=$limit;
         $data['segment']=$this->uri->uri_string();
         $data['total_records']=$data['total_records'];
         $data['offset']=$offset;
         $data['message'] = "";


         $data['categories']= $this->news_model->get_categories();
       
         $data['job_types'] = $this->news_model->get_job_types();
       
         $data['cities'] = $this->news_model->get_cities();
         $data['salaries']=$this->news_model->get_salaries(true);

              //$data['segment'] = "" ;
        //print_r($data);
       $data['user'] = $this->session->userdata('username');
$is_logged_in = $this->session->userdata('is_logged_in');
    
       if($is_ajax)
       {
          $this->load->view('pages/ajax_jobs_entries',$data);
       }
       else
       {
$this->load->view('templates/Cheader');
          $this->load->view('pages/jobs_entries',$data);
          $this->load->view('templates/footer');
        }


  }



  public function display_expired_jobs($q=0,$sort_by ='valid_to',$sort_order='desc',$offset = 0)
  
  {
     $data1 = array('job_status'=>'expired');
         $today = date("Y-m-d");
         $this->db->where('valid_to <',$today);
        $result =$this->db->update('jobs',$data1);
if(isset($_GET['checked']))
      {
      $data['checked']=$_GET['checked'];
      //print_r($_POST['checked']);
    }
      else
      {
        $data['checked']=array();
      }

          $is_ajax=$this->input->is_ajax_request();
             
             if(!empty($q))
             {
                $q=base64_decode($q);
              }
          
    
$limit = 2;
  
        
       parse_str($q,$_POST);

        
          $query_array = array(
        'id' => $this->input->post('job_id'),
        'category' => $this->input->post('category'),
        'title'=> $this->input->post('title'),
        'salary'=>$this->input->post('salary'),
        'job_type'=>$this->input->post('job_type')

       // 'status' => $this->input->post('status')
        );
        $sort_column=array('closed_date','opened_date','last_updated','published_date','category','title','job_type','city','valid_to');
          if(in_array($sort_by,$sort_column))
          {
         
          }
          else
          {
           
            $sort_by='valid_to';//change this to time format
            redirect('pages/display_expired_jobs');
            
       
          }

          if($offset<0)
          {
            redirect('pages/display_expired_jobs');
          }
             
          $data['query']=$query_array;
             
          $user = $this->session->userdata('username');

        $is_ajax=$this->input->is_ajax_request();
        $result = $this->news_model->search_expired_jobs($query_array,$limit,$offset,
            $sort_by,$sort_order);
       // print_r($result);
        $data['jobs'] = $result['rows'];
       
        $data['total_records']= $result['num_rows'];
        
               // $data['myaccount'] = $this->news_model->get_account($user);

        $q=base64_encode($q);
        
        $this->load->library('pagination');
        $config = array();
        $config['base_url'] = base_url() .  "/pages/display_expired_jobs/$q/$sort_by/$sort_order/";
        $config['total_rows']= $data['total_records'];
        $config['per_page'] = $limit; 
        $config['uri_segment'] = 6;
        $config['full_tag_open']='<p style="word-wrap:break-word;line-height:30px" id="pagination">';
         $config['full_tag_close']='</p>';
        $config['num_links']= 5;
        $config['first_link'] = '<&nbspFirst';
        $config['last_link']='Last&nbsp>';
       
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
         $data['sort_by'] = $sort_by ;
         $data['sort_order'] = $sort_order;  
         $data['q']=$q;
         $data['limit']=$limit;
         $data['segment']=$this->uri->uri_string();
         $data['total_records']=$data['total_records'];
         $data['offset']=$offset;
         $data['message'] = "";


         $data['categories']= $this->news_model->get_categories();
       
         $data['job_types'] = $this->news_model->get_job_types();
       
         $data['cities'] = $this->news_model->get_cities();
         $data['salaries']=$this->news_model->get_salaries(true);

              //$data['segment'] = "" ;
        //print_r($data);
       $data['user'] = $this->session->userdata('username');
$is_logged_in = $this->session->userdata('is_logged_in');
    
       if($is_ajax)
       {
          $this->load->view('pages/ajax_expired_jobs',$data);
       }
       else
       {
$this->load->view('templates/Cheader');
          $this->load->view('pages/closed_jobs',$data);
          $this->load->view('templates/footer');
        }


  }

  public function search_jobs()
    {
      $user = $this->session->userdata('username');
$query_array = array(
        'id' => $this->input->post('No'),
         'category'=> $this->input->post('category'),
        'title'=> $this->input->post('title')
        
       // 'status' => $this->input->post('Status')
        );
       

     
    
      
     $q =  http_build_query($query_array);
     
    
     
   redirect('/pages/display_jobs/'. $q);

    }


  public function display_applicants($q=0,$sort_by ='date_applied',$sort_order='desc',$limit=2,$offset = 0)
  {

     
if(isset($_GET['checked']))
      {
      $data['checked']=$_GET['checked'];
      //print_r($_POST['checked']);
    }
      else
      {
        $data['checked']=array();
      }

      if(!empty($q))
      {
             
             $q=base64_decode($q);
             
      }   
         
           $is_ajax =$this->input->is_ajax_request();
         

      
       parse_str($q, $_POST);
     
       

        // $this->input->loadquery($query_id);
        
         
         
          $query_array = array(
        'name' => $this->input->post('name'),
        'job_id'=>$this->input->post('job_id'),
        'title'=>$this->input->post('title'),
        'id'=>$this->input->post('id'), 
        'applicant_id'=>$this->input->post('applicant_id')        
      
        );
           $sort_column=array('job_title','name','gender','date_applied');
          if(in_array($sort_by,$sort_column))
          {
         
          }
          else
          {
           
            $sort_by='date_applied';//change this to time format
            redirect('pages/display_applicants');
            
       
          }

          if($offset<0)
          {
            redirect('pages/display_applicants');
          }
          if($limit<0 ||empty($limit))
          {
            redirect('pages/display_applicants');
          }
         $data['query'] = $query_array;
        

             
          $user = $this->session->userdata('username');

         //$limit;
        $result = $this->news_model->search_applicants($query_array,$limit,$offset,
            $sort_by,$sort_order);
        
        $data['applicants'] = $result['rows'];
       
        $data['total_records']= $result['total_rows'];
        
               

         $q=base64_encode($q);

     
        $config = array();
        $config['base_url'] = base_url() . "pages/display_applicants/$q/$sort_by/$sort_order/$limit";
        $config['total_rows']= $data['total_records'];
        $config['per_page'] = $limit; 
      $config['uri_segment'] = 7;
        $config['num_links'] = 14;
        //$config['use_page_numbers'] = TRUE;
         $config['full_tag_open']='<p style=text-align:center id="pagination">';
         $config['full_tag_close']='</p>';
        $config['num_links']= 10;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
         $data['sort_by'] = $sort_by ;
         $data['sort_order'] = $sort_order;  
         $data['q']=$q;
         $data['limit'] = $limit;
         $data['offset']=$offset;
         $data['segment']=$this->uri->uri_string();
         
       $data['user'] = $this->session->userdata('username');
$is_logged_in = $this->session->userdata('is_logged_in');

          if($is_ajax)
          {
            $this->load->view('pages/ajax_applicants',$data);
          }
          else
          {
$this->load->view('templates/Cheader');
          $this->load->view('pages/applicants',$data);
          $this->load->view('templates/footer');
      }


  }

public function applicants_rows()
{
 $q = $this->input->post('q');
  $q = (empty($q)?0:$q);
  $sort_by = $this->input->post('sort_by');
  $sort_by = (empty($sort_by)?'date_applied':$sort_by);
  $sort_order = $this->input->post('sort_order');
  $sort_order = (empty($sort_order)?'desc':$sort_order);
  /*$offset = $this->input->post('offset2');
$offset = (empty($sort_order)?0:$offset); */
 $limit = $this->input->post('rows');

 $data['select']=$limit;
  
 //redirect("admin_role/display_students/".$q."/".$sort_by."/".$sort_order."/".$limit."/".$offset);
//  echo "q:" . $q. "\nsort_by: " . $sort_by. "\nsort_order : ". $sort_order;
redirect("pages/display_applicants/".$q."/".$sort_by."/".$sort_order."/".$limit."/0");  
}

  public function create_job()
  {
       

       if(isset($_POST['register']))
       {
     
             


             $vt_day=$this->input->post('vt_day');
             $vt_month=$this->input->post('vt_month');
             $vt_year = $this->input->post('vt_year');
             $valid_to = $vt_day."-".$vt_month ."-". $vt_year;
             $vt_date=date_create($valid_to);
             $vt_date=date_format($vt_date,'Y-m-d');

 
 
  $this->form_validation->set_rules('c_name','Company Name','trim|required|max_length[200]');
  
  if($this->input->post('category')=='Other(Specify)')
  {
    $this->form_validation->set_rules('s_category','Specify Category'
    ,'trim|required');
  }

  $this->form_validation->set_rules('category','Job Category','trim|required');

  $this->form_validation->set_rules('c_information','Company Information','trim|required');
  $this->form_validation->set_rules('title','Job Title','trim|required|');
  $this->form_validation->set_rules('salary','Job Salary','trim|required');
  $this->form_validation->set_rules('descriptions','Job Description','trim|required');
  $this->form_validation->set_rules('responsibilities','Responsibilities','trim|required');
  $this->form_validation->set_rules('requirements','Requirements','trim|required');
  
  $this->form_validation->set_rules('job_type','Job Type','trim|required');
  $this->form_validation->set_rules('phone','Phone','trim|required');
  $this->form_validation->set_rules('email','Email','trim|required|valid_emails');
  $this->form_validation->set_rules('c_email','Confirm Email','trim|required|valid_emails|matches[email]');
  $this->form_validation->set_rules('website','Website','trim');
  $this->form_validation->set_rules('address','Address','trim|required');
  $this->form_validation->set_rules('township','Township','trim|required');
  if($this->input->post('city')=='Other(Specify)')
  {
  $this->form_validation->set_rules('s_city','Specify City','trim|required');
}
  $this->form_validation->set_rules('city','City','trim|required');
  $this->form_validation->set_rules('country','Country','trim|required');
 
  
  //$this->form_validation->set_rules('valid_to','Valid To','trim|required|callback_check_date');
  
   $this->form_validation->set_rules('vt_day', 'Valid To', "trim|required|callback_check_valid_to[$valid_to]|callback_valid_to_date[$vt_date]");
  $this->form_validation->set_rules('vt_month','Valid To','trim|required');
  $this->form_validation->set_rules('vt_year',"Valid To",'trim|required');
  

  

    
     
     if($this->form_validation->run()== FALSE)
     {


        $data['days']=$this->news_model->get_days();
        $data['months']=$this->news_model->get_months();

     $data['categories']= $this->news_model->get_categories(true);
         $data['job_types'] = $this->news_model->get_job_types();
         $data['cities'] = $this->news_model->get_cities(true);
         $data['salaries']=$this->news_model->get_salaries();
         $data['message']="Job Registration Unsuccessful";
         $data['message2']="Please Check The Input Fields";
      
       $this->load->view('templates/Cheader');
      $this->load->view('pages/jobr',$data);
      $this->load->view('templates/footer');
     }
     else
     {     
           if($this->input->post('s_category'))
           {
              $this->news_model->insert_category();
           }
           if($this->input->post('s_city'))
           { 
               


               $this->news_model->insert_city();
         
           }   
          


           $this->news_model->job_create();
           redirect('pages/j_r_success');

     }
  }//end first if

else{

 $data['days']=$this->news_model->get_days();
 $data['months']=$this->news_model->get_months();
$data['categories']= $this->news_model->get_categories(true);
         $data['job_types'] = $this->news_model->get_job_types(true);
         $data['cities'] = $this->news_model->get_cities(true);
         $data['salaries']=$this->news_model->get_salaries();
          $this->load->view('templates/Cheader');
          $this->load->view('pages/jobr',$data);
          $this->load->view('templates/footer');
        }

  }
  public function j_r_success()
  {
         $this->load->view('templates/cheader');
         $this->load->view('pages/jsuccess');
         $this->load->view('templates/footer');

  }
  public function search_applicants()
  {
    $user=$this->session->userdata('username');
     $query_array = array(
        'name' => $this->input->post('name'),
        );
       

     
    
      
     $q =  http_build_query($query_array);
     
    
     
   redirect('/pages/display_applicants/'. $q);


    
    

  }
  public function display_students_jobs($q=0,$sort_by ='applied_date',$sort_order='desc',$offset = 0)
  {

     


 if(isset($_GET['checked']))
      {
      $data['checked']=$_GET['checked'];
      //print_r($_POST['checked']);
    }
      else
      {
        $data['checked']=array();
      }

 if(!empty($q))
          {   
             
             $q=base64_decode($q);
          }

          $is_ajax =$this->input->is_ajax_request();
        
         
$limit = 2;

      
        
 $data['address'] = $this->input->post('address');
  
  
  parse_str($q,$_POST);

  
          $query_array = array(
        'job_id' => $this->input->post('job_id'),
        'c_name' => $this->input->post('c_name'),
        'category'=> $this->input->post('category'),
        'title'=>$this->input->post('title'),
        'salary'=>$this->input->post('salary'),
        'job_type'=>$this->input->post('job_type')

      
        );
$sort_column=array('c_name','category','title','job_type','valid_to','applied_date');
          if(in_array($sort_by,$sort_column))
          {
         
          }
          else
          {
           
            $sort_by='applied_date';//change this to time format
            redirect('pages/display_students_jobs');
            
       
          }

          if($offset<0)
          {
            redirect('pages/display_students_jobs');
          }

            $data['query'] = $query_array;

        $user = $this->session->userdata('username');

        
        $result = $this->news_model->search_students_jobs($query_array,$limit,$offset,
            $sort_by,$sort_order);
     
        $data['my_jobs'] = $result['rows'];
       
       
        
                $data['student_profile'] = $this->news_model->get_each_student($user);


          $q=base64_encode($q);
        $is_ajax=$this->input->is_ajax_request();
        $this->load->library('pagination');
        $config = array();
        $config['base_url'] = base_url() ."pages/display_students_jobs/$q/$sort_by/$sort_order/";
        $config['total_rows']= $result['total_rows'];
        $config['per_page'] = $limit; 
        $config['uri_segment'] = 6;
         $config['full_tag_open']='<p style="word-wrap:break-word;line-height:30px" id="pagination">';
         $config['full_tag_close']='</p>';
        $config['num_links']= 5;
        $config['first_link'] = '<&nbspFirst';
        $config['last_link']='Last&nbsp>';
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
         $data['sort_by'] = $sort_by ;
         $data['sort_order'] = $sort_order;  
         $data['q']=$q;
         $data['offset']=$offset;
         $data['segment']=$this->uri->uri_string();
         $data['limit'] = $limit;
         $data['total_records']=$result['total_rows'];
     

         $data['categories']= $this->news_model->get_categories();
       
         $data['job_types'] = $this->news_model->get_job_types();
       
         $data['cities'] = $this->news_model->get_cities();
         $data['salaries']=$this->news_model->get_salaries(true);
     
          
          if($is_ajax)
          {
             $this->load->view('pages/ajax_students_jobs',$data);
           }
           else{
     $this->load->view('templates/cheader');
      $this->load->view('pages/student_jobs',$data);
      $this->load->view('templates/footer');
                  }
     
    }





    public function display_save_jobs($q=0,$sort_by ='saved_date',$sort_order='asc',$offset = 0)
    {
     
      if(isset($_GET['checked']))
      {
      $data['checked']=$_GET['checked'];

      //print_r($_POST['checked']);
    }
      else
      {
        $data['checked']=array();
      }
$limit = 2;
         $data['sort_by'] = $sort_by ;
         $data['sort_order'] = $sort_order;  
  $data['q']=$q;       
  $data['offset']= $offset; 
  $data['limit'] = $limit;
 
 
  
     $is_ajax=$this->input->is_ajax_request();

  
      /*  parse_str($q, $_POST);
        $this->load->library('form_validation');
   
          $query = array(
        'job_id' => $this->input->post('job_id'),
        'name' => $this->input->post('name'),
        'date_applied'=> $this->input->post('date_applied'),
       // 'status' => $this->input->post('status')
        );*/

$sort_column=array('c_name','category','title','job_type','saved_date');
          if(in_array($sort_by,$sort_column))
          {
         
          }
          else
          {
              
            $sort_by='saved_date';//change this to time format
            redirect('pages/display_save_jobs');
            
       
          }

          if($offset<0)
          {
            redirect('pages/display_save_jobs');
          }
             
          $user = $this->session->userdata('username');
       
        
        $result = $this->news_model->search_save_jobs($q,$limit,$offset,
            $sort_by,$sort_order);
                                  
      $data['jobs'] = $result['rows'];


       $data['total_records']= $result['total_rows'];
      //  $q=base64_encode($q);
       $is_ajax=$this->input->is_ajax_request();
        $this->load->library('pagination');
        $config = array();
        $config['base_url'] = base_url() ."pages/display_save_jobs/$q/$sort_by/$sort_order/";
        $config['total_rows']= $result['total_rows'];
        $config['per_page'] = $limit; 
        $config['uri_segment'] = 6;
         $config['full_tag_open']='<p style=text-align:center id="pagination">';
         $config['full_tag_close']='</p>';
        //$config['num_links']= 10;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        
     
         $data['user']= $user;
         if($is_ajax)
         {
           $this->load->view('pages/ajax_save_jobs',$data);

         }
         else
         {
         $this->load->view('templates/cheader');
         $this->load->view('pages/save_jobs',$data);
         $this->load->view('templates/footer');
         }

    }

    
    
    public function search_university()
    {
 $query_array = array(
          'user'=>$this->input->post('user'),
        'name' => $this->input->post('name'),
        'email' => $this->input->post('email'),
        'status' => $this->input->post('status')
        );

  $q =  http_build_query($query_array);
     
    
     
   redirect('/pages/display_universities/'. $q);
        
    }



public function get_abused_reports()
{
/*$result = $this->db->select('user,job_id,user_c, Count("user") as user_total')->from('abused_reports')
  ->group_by('user')->where(array('user_c'=>'company'))->order_by('user_total','desc');
   $result = $result->get()->num_rows();
   
/*   $result2 = $this->db->get('students');
   $result['total_students'] = $result2->num_rows();
  return $result;*/

}

public function search_company()
    {
 $query_array = array(
          'username'=>$this->input->post('user'),
         'c_name'=>$this->input->post('c_name'),
        'name' => $this->input->post('name'),
        'email' => $this->input->post('email'),
        'status' => $this->input->post('status')
        );

  $q =  http_build_query($query_array);
     
    
     
   redirect('/pages/display_companies/'. $q);
        
    }




    public function search()

    {     

      //  $q = $this->input->post('category'); 
        $query_array = array(
          'c_name'=>$this->input->post('c_name'),
          'id'=>$this->input->post('job_id'),
        'category' => $this->input->post('category'),
        'title' => $this->input->post('title'),
        'type'=> $this->input->post('job_type'),
        'salary'=> $this->input->post('salary'),
        //'salary' => $this->input->post('length')
              'city'=> $this->input->post('city')
        );
       

     
    
      
     $q =  http_build_query($query_array);
     
    
     
   redirect('/pages/display/'. $q);
     

         //$this->input->post('title');

    }
    public function search_students()
    {
                $user=$this->session->userdata('username');
     $query_array = array(
        'id' => $this->input->post('no'),
        'nrc'=> $this->input->post('nrc'),
         'name'=> $this->input->post('name'),
        'email'=> $this->input->post('email'),
        'status' => $this->input->post('status')
        );
       

     
    
      
     $q =  http_build_query($query_array);
     
    
     
   redirect('/pages/display_students/'. $q);


    }
    



	public function paginate()
	{

		/*$this->load->library('pagination');
		$this->load->library('table');
		$this->table->set_heading('Job Catergory', 'Job Title','Job Salary','Job Description','Detail/Apply');
		$config['base_url']='http://localhost:81/ci1/index.php/pages/paginate';
		$config['total_rows']=  $this->db->get('jobs')->num_rows();
		$config['per_page']= 2;
		$config['num_links']= 20;    
         $config['full_tag_open']='<div id="pagination">';
         $config['full_tag_close']='</div>';
                    
                              
		$this->pagination->initialize($config);
		//echo $this->pagination->create_links();
        //$records= $this->db->get('jobs');
       // $records= $records->result_array();
        
		//print_r($info);
		//$data['records']=$info;
        //$data['records']=($info,$config['per_page'],$this->uri->segment(3));
		$records= $this->db->get('jobs',$config['per_page'],$this->uri->segment(3));
		$records = $records->result_array();
		//print_r($records);
		foreach($records as $re)

		{ 
                  
                  $info[]=array(anchor('pages',$re['JobC']),$re['JobT'],$re['JobS'],$re['JobD'],anchor('pages/view','Detials/Apply'));  
             
			 //$records= array($re['JobC'],$re['JobT'],$re['JobS'],$re['JobD']);


		}
		//print_r($info);
		$data['records']= $info;
                                        
        $this->load->view('templates/header');
		$this->load->view('pages/job',$data);

		

*/
	}
  public function company_ap()
  {
 $data['user'] = $this->session->userdata('username');
                $user = $this->session->userdata('username'); 
                $password= $this->session->userdata('password'); 
               // $data['jobs'] = $this->news_model->get_jobs($user);
                //$total_records = $this->news_model->get_jobs_rows($user);
                //print_r($total_rows);                
              //  $data['myaccount'] = $this->news_model->get_account($user);
               $total_rows_ap = $this->news_model->get_applicants_rows($user);
              // $data['my_applicants'] = $this->news_model->get_applicants($user);
        
 
 $config = array();


        $config2 = array();
        $config2['base_url'] = base_url()."pages/company_ap/";
        $config2['total_rows']= $total_rows_ap;
        $config2['per_page'] = 5; 
       
    
        $config2['uri_segment']=3;
      
         $config2['full_tag_open']='<div id="pagination">';
         $config2['full_tag_close']='</div>';
$this->pagination->initialize($config2);
        $data['pagination'] = $this->pagination->create_links();


 
              
              
$query2 = $this->db->select('*')->from('applicants')->where('user_c',$user)
      ->limit(5,$this->uri->segment(3));
      $data['my_applicants'] = $query2->get()->result_array();

    
      
    
               $segment4=$this->uri->segment(4); 

 $data['total_records']= $total_rows_ap;
        $data['segment'] = $this->uri->uri_string();
         $data['sort_by'] = "id" ;
         $data['sort_order'] = "asc";  
         $data['q']=0;  
 $this->load->view('templates/Cheader');
                $this->load->view('pages/member_comp_ap',$data);
                $this->load->view('templates/footer');
               
    
  }
	public function index()
	{

		
      
	$this->load->view('templates/header');
	$this->load->view('pages/login_form');
	$this->load->view('templates/footer');
        /*$this->load->view('templates/header');
	$this->load->view('pages/home');
	$this->load->view('templates/footer');
*/
	}
  public function student_registration()
  {
   
                    $days=$this->news_model->get_days();
                           $data['days']=$days;
                    $months=$this->news_model->get_months();
                    $data['months']=$months;       
   
         
    $this->load->view('templates/header');
  $this->load->view('pages/studentr',$data);
  $this->load->view('templates/footer');


  }

   public function company_registration()
  {

   
         
    $this->load->view('templates/header');
  $this->load->view('pages/companyr');
  $this->load->view('templates/footer');


  }
	public function contact()
  {
         $is_logged_in=$this->session->userdata('is_logged_in');
         if($is_logged_in==true)
         {
    $this->load->view('templates/cheader');
  $this->load->view('pages/about');
  $this->load->view('templates/footer');
         }
         else
         {
 
    $this->load->view('templates/header');
  $this->load->view('pages/about');
  $this->load->view('templates/footer');

         }


  }


public function change_password()
{ 
    $user = $this->session->userdata('username');
         if(isset($_POST['change_password']))
         { 
        
 // $user= $this->input->post('user');
  
  $this->load->library('form_validation');
$this->form_validation->set_rules('password','Password','trim|required|min_length[5]');
$this->form_validation->set_rules('c_password','Confirm Password','trim|required|min_length[5]|matches[password]');
if ($this->form_validation->run() === FALSE)
{
       $data['message']="Password Update Incompleted";
       
       //$data['user']=$this->input->post('user');
    $this->load->view('templates/Cheader');
  $this->load->view('pages/change_password',$data);
  $this->load->view('templates/footer');

     

}
else{
          
     // $user = $this->input->post('user');
    $result = $this->news_model->change_password($user);
     if($result)
     {
         // $data['user']=$user; 
          redirect('pages/change_password?message=Password Updated');
/*          $data['message']="Password has be updated";
    $this->load->view('templates/Cheader');
  $this->load->view('pages/change_password', $data);
  $this->load->view('templates/footer');
*/
     }

}
}
else
{
   
$data['user']=$user;  
          $data['message']=(isset($_GET['message'])?$_GET['message']:"");
    $this->load->view('templates/Cheader');
  $this->load->view('pages/change_password', $data);
  $this->load->view('templates/footer');

}
}


public function delete_applicant($q=0,$sort_by="applied_date",$sort_order="asc",$offset=0)
{
    // print_r($_POST);
     
$user =$this->session->userdata('username');


if(!empty($_POST['applicant_id']))
{
$applicant_id=$this->input->post('applicant_id');
}
if(!empty($_POST['selected']))
{

 $applicant_id=$_POST['selected'];
}
if(empty($applicant_id))
{
  redirect('pages/display_applicants');
}
 

 
 $previous_url=$this->input->post('previous_url');
  
    $data = array('delete'=>'true');
       $this->db->where('user_c',$user);
       $this->db->where_in('applicant_id',$applicant_id);
      $result = $this->db->update('applicants',$data);
     // echo "update result is: " .$result;
      
               
    if($result)
      {
 
             redirect($previous_url);
           
                 }


   
 

 /*$checked= array_keys($_POST['checked'],'checkbox');


 $size= sizeof($checked);
 

   
  $q= $this->input->post('q');
 $sort_by =$this->input->post('sort_by');
 $sort_order= $this->input->post('sort_order');
 $offset= $this->input->post('offset');
 $limit= $this->input->post('limit');
 $count=$this->input->post('count');
  $previous_url= $this->input->post('previous_url');
       $count= $count-$size;
       if($count==0 && $offset !=0)
       {
         $offset = $offset - $limit;
         $previous_url= base_url() .'pages/display_applicants/'.
         $q.'/'.$sort_by.'/'.$sort_order.'/'.$offset;

     }




 
 $applicant_id=$checked;
 
    $data = array('delete'=>'true');
       $this->db->where('user_c',$user);
       $this->db->where_in('applicant_id',$applicant_id);
      $result = $this->db->update('applicants',$data);
     // echo "update result is: " .$result;
      
               
    if($result)
      {
 
             redirect($previous_url);
           
                 }


   
  */

   
}
public function delete_students_jobs()
{
     $user =$this->session->userdata('username');
if(!empty($_POST['applied_id']))
{
 $applied_id=$this->input->post('applied_id');
}
if(!empty($_POST['selected']))
{

 $applied_id=$_POST['selected'];
}
if(empty($applied_id))
{
  redirect('pages/display_students_jobs');
  exit();
}
 

  

 

 $previous_url= $this->input->post('previous_url');



// $applied_id=array_keys($data,'checkbox');
 
 
 
 

    $data = array('delete_status'=>'true');
       $this->db->where('user',$user);
       $this->db->where_in('applied_id',$applied_id);
      $result = $this->db->update('students_jobs',$data);
               
    if($result)
      {
 
             redirect($previous_url);
           
                 }

}
public function delete_expired_jobs()
{
$user =$this->session->userdata('username');

if(!empty($_POST['job_id']))
{
$job_id=$this->input->post('job_id');
}
if(!empty($_POST['selected']))
{

 $job_id=$_POST['selected'];
}
if(empty($job_id))
{
  redirect('pages/display_expired_jobs');
}
 

 
 
 $previous_url= $this->input->post('previous_url');
   

 
 
 
 
 
 

    $data = array('delete_status'=>'true');
       $this->db->where('user_c',$user);
       $this->db->where_in('id',$job_id);
      $result = $this->db->update('jobs',$data);
          if($result)
          {     
    redirect($previous_url);
  }


}

public function open_jobs($job_id='')
{


$user =$this->session->userdata('username');

if(!empty($_POST['id']))
{
$job_id=$this->input->post('id');
}
if(!empty($_POST['selected']))
{

 $job_id=$_POST['selected'];
}
//$id=$this->db->select('id')->from('jobs')->where(array('user_c'=>$user,'job_status'=>'opened'))->get()->result_array();
if(empty($job_id))
{
  redirect('pages/display_job_entries');
}
 

 
 
 $previous_url= $this->input->post('previous_url');
   

 
 
 
 
 
 

    $data = array('job_status'=>'opened','opened_date'=>date('Y-m-d'));
       $this->db->where('user_c',$user);
       $this->db->where_in('id',$job_id);
      $result = $this->db->update('jobs',$data);
          if($result)
          {     
    redirect($previous_url);
  }


}

public function expired_job_details($job_id='')
{


        
$job= $this->news_model->get_expired_job($job_id);
if($job_id=="" || empty($job))
  {

    redirect('pages/display_expired_jobs');
    exit();
  }
  

  
  if(isset($_GET['checked'])&&!empty($_GET['checked']))
  {
  $data['checked']=$_GET['checked'];
  
     
  }

  
  

 $data['back_url']=(isset($_GET['back_url'])?$_GET['back_url']:"");

  $data['job']= $job;

         
         
 

      $this->load->view('templates/Cheader');
  $this->load->view('pages/expired_job_details',$data);
$this->load->view('templates/footer');



}

public function check_update_name($str)
{
	$user = $this->session->userdata('username');
  
	$result = $this->news_model->check_user($str);
     if(strcasecmp($str,$user) == 0|| $result== TRUE)
     {
        return TRUE;

     }
     else
     {
       $this->form_validation->set_message('check_update_name', 'Choose another user name');
     	return FALSE;
     }
	
}
public function save_jobs($q=0,$sort_by ='applied_date',$sort_order='desc',$offset = 0)
{
    $user = $this->session->userdata('username');
  $data['save_jobs_info'] = $this->news_model->get_save_jobs($user);
               
               $data['save_jobs_total_records'] = $data['save_jobs_info']['total_records'];
  $config2 = array();
        $config2['base_url'] = base_url() . "pages/display_save_jobs/$q/$sort_by/$sort_order/";
        ////"http://localhost:81/ci1/index.php/login/is_logged_in/";
        $config2['total_rows']= $data['save_jobs_total_records'];
        $config2['per_page'] = 2; 
        $config2['uri_segment'] = 6;
         $config2['full_tag_open']='<div id="pagination">';
         $config2['full_tag_close']='</div>';
         $config2['num_links'] = 8;
     $user = $this->session->userdata('username');
$this->db->select('*')->from('save_jobs')->limit(2,0)->where(array('user'=>$user,'delete !='=>'false'));
    $this->db->join('jobs','save_jobs.job_id = jobs.id');
    $this->db->order_by('published_date','desc');
    ////($sort_by2,$sort_order);    
    $q = $this->db->get();
    $data['save_jobs']=$q->result_array();
    //$data['segment'] = echo base_url().;
        $data['sort_by'] = $sort_by ;
         $data['sort_order'] = $sort_order; 

         $data['q']= $q;
        $this->pagination->initialize($config2);
       $data['pagination2'] = $this->pagination->create_links();
        $this->load->view('templates/cheader');
        $this->load->view('pages/save_jobs',$data);
        $this->load->view('templates/footer');


}
public function applied_jobs()
{
$this->load->library('pagination');
                       $this->load->library('table');
       
                $config = array();
        $config['base_url'] = base_url() . "/pages/display_students_jobs/$q/$sort_by/$sort_order/";
        ////"http://localhost:81/ci1/index.php/login/is_logged_in/";
        $config['total_rows']= $total_records;
        $config['per_page'] = 2; 
        $config['uri_segment'] = 3;
         $config['full_tag_open']='<div id="pagination">';
         $config['full_tag_close']='</div>';
         $config['num_links']=5;
        // $data['all_students'] = $this->news_model->
               //get_students_by_u($university);
          $query= $this->db->select('*')->from('students_jobs')->where(array('user'=>
          $user,'delete !='=>'false'))->order_by('applied_date','desc')->
      limit(2,$this->uri->segment(3));
      $data['my_jobs'] = $query->get()->result_array();

        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['segment'] = $this->uri->uri_string();
         $data['sort_by'] = "applied_date" ;
         $data['sort_order'] = "desc";  
         $data['q']=0;  
         $data['total_records']= $total_records;

         $this->load->view('templates/cheader');
         $this->load->view('pages/students_jobs');
         $this->load->view('templates/footer');

}

public function upload_company_photo()
{
//echo " from controller method: " ;print_r($_FILES);

       

        
    $data['result'] = $this->news_model->company_photo();
    $this->load->view('pages/ajax_company_photo',$data);

}
public function update_company_photo()
{

 $data['result'] = $this->news_model->company_photo();

    $this->load->view('pages/ajax_company_photo',$data);




}


public function upload_logo()
{

  $data['logo']=$this->news_model->upload_logo();
  $this->load->view('pages/ajax_logo_photo',$data);
}
public function update_logo()
{
 $data['logo']=$this->news_model->upload_logo();
  $this->load->view('pages/ajax_logo_photo',$data);

}


public function update_student_photo()
{

 $data['result'] = $this->news_model->update_student_photo();

    $this->load->view('pages/ajax_update_photo',$data);




}
public function upload_student_photo()
{
    
  
    $data['result'] = $this->news_model->student_photo();

    $this->load->view('pages/ajax_upload_s_photo',$data);


}
public function update_s()
{


     $original_user = $this->session->userdata('username');
	 
  /* if($original_user=="")
   {

    exit();
   }*/
 

if(isset($_POST['update']))
{
    
 $b_day=$this->input->post('b_day');
             $b_month=$this->input->post('b_month');
             $b_year = $this->input->post('b_year');
             $date_o_b = $b_day."-".$b_month ."-". $b_year;
             //$bd=date_create($date_o_b);
             //$bd=date_format($bd,'Y-m-d');

            
             

        
        $this->form_validation->set_rules('user','User','trim|required|callback_check_update_name|callback_check_space|callback_check_characters');
      
        	
        	$this->form_validation->set_rules('name', 'Name', 'trim|required');
      
         
         $this->form_validation->set_rules('qualifications','Qualifications','trim|required');
          $this->form_validation->set_rules('major','Major','trim|required');
          $this->form_validation->set_rules('experience','Work Experience','trim|required');
          $this->form_validation->set_rules('gender','Gender','trim|required');
         
          $this->form_validation->set_rules('b_day','Date Of Birth',"trim|required|callback_check_b_date[$b_year]");
           $this->form_validation->set_rules('b_month','Date Of Birth','trim|required');
           $this->form_validation->set_rules('b_year','Date Of Birth','trim|required');          

$this->form_validation->set_rules('nationality','Nationality','trim|required');
          $this->form_validation->set_rules('phone','Phone','trim|required');
          $this->form_validation->set_rules('email','Email','trim|required|valid_emails|callback_check_update_email');
          $this->form_validation->set_rules('c_email','Confirm Email','trim|required|valid_emails|matches[email]');
          $this->form_validation->set_rules('address','Address','trim|required');
          $this->form_validation->set_rules('township','Township','trim|required');
          $this->form_validation->set_rules('city','City','trim|required');
          $this->form_validation->set_rules('country','Country','trim|required');
           $this->form_validation->set_rules('photo_path','Photo','trim|required');
          
	        if ($this->form_validation->run() === FALSE)
	             {
		               
                     
                      $data['result']['photo_path']=$this->input->post('photo_path');
                      $data['result']['existing_server_path']=$this->input->post('existing_server_path');
                      $data['result']['photo_server_path']= $this->input->post('photo_server_path');
                     
                     
                      $data['date_o_b']=$date_o_b;
                      $data['message']="Update Unsuccessful";	
                      $data['message2']="Please Check The Input Fields";
                	           $data['days']=$this->news_model->get_days();
        $data['months']=$this->news_model->get_months();

                		$this->load->view('templates/Cheader');
                		$this->load->view('pages/update_profile_s',$data);
                		$this->load->view('templates/footer');
		
		           }
		       else{

                 

                     
              $date_o_b= date_create($date_o_b);
              $date_o_b= date_format($date_o_b,"Y-m-d");
               
               $user= $this->input->post('user');
          
                  
                  $name = $this->input->post('name');
               
              
                  $qualifications = $this->input->post('qualifications');
                  $major = $this->input->post('major');
                  $experience = $this->input->post('experience'); 
                  $gender = $this->input->post('gender');
                  $date_o_b = $date_o_b;
                  $nationality = $this->input->post('nationality');
                  $phone = $this->input->post('phone');
                  $email = $this->input->post('email');

                  $address = $this->input->post('address');
                  $township = $this->input->post('township');
                  $city = $this->input->post('city');
                  $country =$this->input->post('country');
                 $photo= $this->input->post('photo_path');
    $photo_server_path = $this->input->post('photo_server_path');
    $existing_server_path = $this->input->post('existing_server_path');

     if($photo_server_path!==$existing_server_path)
                  {
                      if(!empty($existing_server_path))
                      {
                    @unlink($existing_server_path);
                      }
                  /*  if($before_delete)
                    {
                    unlink($before_delete);
                      }*/
                  }


                   $data = array('user'=>$user,

                      'name'=>$name,'qualifications'=>$qualifications,'experience'=>$experience,'major'=>$major, 'gender'=>$gender,'date_o_b'=>$date_o_b,
                       'nationality'=>$nationality, 'phone'=>$phone, 'email'=>$email,
                      'address'=>$address, 'township'=>$township,'city'=>$city,
                      'country'=>$country,'photo'=>$photo,'photo_server_path'=>$photo_server_path);
                  $this->db->where('user', $original_user);
                  $result =$this->db->update('students',$data);
              $this->db->where('user',$original_user);
                  $this->db->update('students_jobs',array('user'=>$user));
                  $this->db->where('user',$original_user);
                  $this->db->update('applicants',array('user'=>$user));
                  $this->db->where('user',$original_user);
                  $this->db->update('abused_reports',array('user'=>$user));
                   $this->db->where('user',$original_user);
                  $this->db->update('save_jobs',array('user'=>$user));
              

                  if($result)
                  {

    	
    	             $data = array('username'=> $user,'name'=>$name,
                
                   'role'=>'student','is_logged_in' => true);
                    $this->session->set_userdata($data);
                   $user = $this->session->userdata('username'); 
    	             $data['account'] = $this->news_model->get_each_student($user);
                    redirect("login/is_logged_in?message=Update Completed");
               /*     $data['message'] = "Update Completed.";
    	             $this->load->view('templates/Cheader');
    	             $this->load->view('pages/update_profile_s', $data);
    	             $this->load->view('templates/footer');*/
                   }
            }
  }
 
   elseif(!isset($_POST['update']))
   {

            $user = $this->session->userdata('username');
            $data['account']= $this->news_model->get_each_student($user);
                       $data['days']=$this->news_model->get_days();
        $data['months']=$this->news_model->get_months();
          $data['message'] = "";
            $this->load->view('templates/cheader');
            $this->load->view('pages/update_profile_s',$data);
            $this->load->view('templates/footer');


   }
            





    

  
   

}
public function example()
{
 // $some="thing";
  echo json_encode(array('message'=>'json1','message2'=>'json2'));
  //echo json_encode(array('message2'=>'json2')); 
/*   echo "<p id=some>in</p>";
   echo "<p id=some>something</p>";*/

//  print_r($_GET);
  //print_r($_POST);
}



public function login_confirmation($email,$number)
{
       $result = $this->news_model->login_confirmation($email,$number);
       if(empty($email)||empty($number))
       {

        redirect('login');
       }

       if($result==true)
       {
           
        $this->load->view('templates/header');
        $this->load->view('pages/login_form');
        $this->load->view('templates/footer');


       }
       else
       {
          
            //$data['message']='Login confirmation failed';
        $this->load->view('templates/header');
        $this->load->view('pages/login_form');
        $this->load->view('templates/footer');
       }



}
public function login_error()
{

     $this->load->view('templates/header');
           $this->load->view('pages/login_error');
           $this->load->view('templates/footer');





}
public function student_account()
{
     $user=$this->session->userdata('username');
      $data['my_profile'] = $this->news_model->get_each_student($user); 
//  print_r($data);
  $data['message']= "";
     $this->load->view('templates/Cheader');
  $this->load->view('pages/student_account',$data);
  $this->load->view('templates/footer');

}
public function update_student()
{



$user = $this->session->userdata('username');


       $data['account'] = $this->news_model->get_each_student($user); 

	$data['message']= (isset($_GET['message'])?$_GET['message']:"");
   
            $data['days']=$this->news_model->get_days();
        $data['months']=$this->news_model->get_months();
    $this->load->view('templates/Cheader');
	$this->load->view('pages/update_profile_s',$data);
	$this->load->view('templates/footer');
 
}

public function update_job($job_id='')
{ 

        
$job= $this->news_model->get_detail_job($job_id,true);
if($job_id=="" || empty($job))
  {

    redirect('pages/display_job_entries');
    exit();
  }
  

  
  if(isset($_GET['checked'])&&!empty($_GET['checked']))
  {
  $data['checked']=$_GET['checked'];
  
     
  }

  
  

 $data['back_url']=(isset($_GET['back_url'])?$_GET['back_url']:"");

  $data['job']= $job;
  $data['applicants']=$this->db->select('*')->from('students_jobs')->where('job_id',$job_id)->get()->num_rows();

  $data['days']=$this->news_model->get_days();
        $data['months']=$this->news_model->get_months();

$data['categories']= $this->news_model->get_categories(true);
         $data['job_types'] = $this->news_model->get_job_types(true);
         $data['cities'] = $this->news_model->get_cities(true);
         $data['salaries']=$this->news_model->get_salaries();
         
         
 
$data['message']=(isset($_GET['message'])?$_GET['message']:"");
      $this->load->view('templates/Cheader');
  $this->load->view('pages/update_job',$data);
$this->load->view('templates/footer');


}
public function job_entries_details($job_id="")
{


        
$job= $this->news_model->get_detail_job($job_id,true);
if($job_id=="" || empty($job))
  {

    redirect('pages/display_job_entries');
    exit();
  }
  

  
  if(isset($_GET['checked'])&&!empty($_GET['checked']))
  {
  $data['checked']=$_GET['checked'];
  
     
  }

  
  

 $data['back_url']=(isset($_GET['back_url'])?$_GET['back_url']:"");

  $data['job']= $job;

         
         
 

      $this->load->view('templates/Cheader');
  $this->load->view('pages/job_entries_details',$data);
$this->load->view('templates/footer');



}


public function update_jo($job_id='')
{
  $job= $this->news_model->get_detail_job($job_id);
    if($job_id=="" || empty($job))
  {

    redirect('pages/display_job_entries');
    exit();
  }
  
  
  if(isset($_POST['update_job']))
  {

   
             
      $vt_day=$this->input->post('vt_day');
             $vt_month=$this->input->post('vt_month');
             $vt_year = $this->input->post('vt_year');
             $valid_to = $vt_day."-".$vt_month ."-". $vt_year;
             $vt=date_create($valid_to);
             $vt=date_format($vt,'Y-m-d');
             $orig_valid_to=$this->db->select('valid_to')->from('jobs')->where('id',$job_id)->get()->row_array();
             //$orig_valid_to=date_create($orig_valid_to['valid_to']);
             //$orig_valid_to=date_format($orig_valid_to,'d-m-Y');
             $a='';

             if($vt<$orig_valid_to)
             {
               $a='incorrect';
             
             }
              

            
       if($this->input->post('category')=='Other(Specify)')
       {
         $this->form_validation->set_rules('s_category','Specify Category'
         ,'trim|required');
                }
        $this->form_validation->set_rules('category','Job Category','trim|required');
          //$this->form_validation->set_rules('user', 'User', 'required');
          //$this->form_validation->set_rules('password', 'Password', 'trim|required');
        
          $this->form_validation->set_rules('c_name','Company Name','trim|required');
          $this->form_validation->set_rules('c_information','Company Information','trim|required');
          $this->form_validation->set_rules('title', 'Job Title', 'trim|required');
          $this->form_validation->set_rules('salary', 'Job Salary', 'trim|required');
          $this->form_validation->set_rules('descriptions','Job Descriptions','trim|required');
          $this->form_validation->set_rules('responsibilities','Responsibilities','trim|required');
          $this->form_validation->set_rules('requirements','Requirements','trim|required');
                    $this->form_validation->set_rules('job_type','Job Type','trim|required');
          $this->form_validation->set_rules('phone','Phone','trim|required');
          $this->form_validation->set_rules('email','Email','trim|required|valid_emails');
          $this->form_validation->set_rules('c_email','Confirm Email','trim|required|valid_emails|matches[email]');
          $this->form_validation->set_rules('website','Website','trim');
          $this->form_validation->set_rules('address','Address','trim|required');
          $this->form_validation->set_rules('township','Township','trim|required');
        if($this->input->post('city')=='Other(Specify)')
          {
          $this->form_validation->set_rules('s_city','Specify City','trim|required');
        }
          $this->form_validation->set_rules('city','City','trim|required');
          $this->form_validation->set_rules('country','Country','trim|required');
        if($this->input->post('applicants')>=1)
        {

          $this->form_validation->set_rules('vt_day','Apply Valid To',"trim|required|callback_check_valid_to[$valid_to]|callback_valid_to_extend[$a]");

        }
        else
        {
          
          $this->form_validation->set_rules('vt_day','Apply Valid To',"trim|required|callback_check_valid_to[$valid_to]");
        }
          $this->form_validation->set_rules('vt_month','Date Of Birth','trim|required');
          $this->form_validation->set_rules('vt_year','Date Of Birth','trim|required');
      
          //$this->form_validation->set_rules('email','Email','trim|required');
                if ($this->form_validation->run() === FALSE)
                {
                  
                    $data['back_url']=$this->input->post('back_url');
                    $data['message']="Update Incomplete";
                    $data['message2']="Please Check The Input Fields";
                    if(isset($_POST['checked']))
                    {
                    $data['checked']=$this->input->post('checked');
                    }  
                    $data['job_id'] = $this->input->post('job_id');
                    $data['logo']=$this->input->post('logo');
                    $data['photo']=$this->input->post('photo');
                    $data['applicants']=$this->input->post('applicants');
                      $data['days']=$this->news_model->get_days();
        $data['months']=$this->news_model->get_months();
              $data['categories']= $this->news_model->get_categories(true);
                  
                     
                       $data['job_types'] = $this->news_model->get_job_types(true);
                       

                       $data['cities'] = $this->news_model->get_cities(true);
                       $data['salaries']=$this->news_model->get_salaries();
                
                  $this->load->view('templates/Cheader');
                  $this->load->view('pages/update_job',$data);
                  $this->load->view('templates/footer');
                  
                  }
                else{
               if($this->input->post('s_category'))
           {
              $this->news_model->insert_category();
           }
           if($this->input->post('s_city'))
           { 
               


               $result = $this->news_model->insert_city();
         
           }  

           
  
         $type="";
         $s_category="";
         $s_city="";
         if($this->input->post('s_category'))
         {
            $s_category = "Others";
           
         }
        if($this->input->post('s_city'))
         {
             $s_city = "Others";
         }
         $job_id=$this->input->post('job_id');
      
       
        $valid_to = date_create($valid_to);
        $valid_to = date_format($valid_to,"Y-m-d"); 
//$no =$this->input->post('no');
 $c_name = $this->input->post('c_name');
 
 $c_information = $this->input->post('c_information');
 $category=(!empty($s_category)?$s_category:$this->input->post('category'));
    $title = $this->input->post('title');
    $salary = $this->input->post('salary');
   
    $descriptions = $this->input->post('descriptions');
    $phone = $this->input->post('phone');
    $email = $this->input->post('email');
    $website = $this->input->post('website');
    $responsibilities = $this->input->post('responsibilities');
    $requirements = $this->input->post('requirements');
    $job_type = $this->input->post('job_type');
    $address = $this->input->post('address');
    $township = $this->input->post('township');
    $city=(!empty($s_city)?$s_city:$this->input->post('city'));
    $country = $this->input->post('country');
   
    $valid_to = $valid_to;
    
     
    $lu_date =date('Y-m-d H:i:s');
    if(($category && $city)!==('Others'||'Other(Specify)'))
    {
        $specified='';

    }

    
     $data = array('c_name'=>$c_name,'c_information'=>$c_information,'category'=>$category,
        'title'=>$title,'salary'=>$salary, 'descriptions'=>$descriptions, 'phone'=>$phone,
        'email'=>$email,'website'=>$website, 'responsibilities'=>$responsibilities,'requirements'=>$requirements,
         'job_type'=>$job_type,'address'=>$address,'township'=>$township,'city'=>$city,'country'=>$country
         ,'valid_to'=>$valid_to,'last_updated'=>$lu_date,'specified'=>$specified);
    $this->db->where('id', $job_id);
    $result =$this->db->update('jobs',$data);
    $this->db->where('job_id',$job_id);
    $this->db->update('applicants',array('job_valid_to'=>$valid_to));
   // $this->db->where('id', $no);
   // $this->db->update('companies_jobs',$data);
  

   
    if($result)
    {

    	
            $data['job'] = $this->news_model->get_detail_job($job_id);
         $data['categories']= $this->news_model->get_categories(true);



         $data['job_types']= $this->news_model->get_job_types(true);
         $data['cities'] = $this->news_model->get_cities(true);
         $data['salaries']=$this->news_model->get_salaries();
            $back_url=$this->input->post('back_url');
            $back_url=urlencode($back_url);

          // $data['back_url']=$this->input->post('back_url');
            $data['job_id']=$job_id;
            if(isset($_POST['checked']))
            {
            $data['checked']=$this->input->post('checked');
          

          }
            //$data['message'] = "Update Completed.";
          redirect("pages/update_job/$job_id?message=Update Completed&back_url=$back_url");
    /*  $this->load->view('templates/Cheader');
      $this->load->view('pages/update_job', $data);
      $this->load->view('templates/footer');
    	 */
    }
  }
}
else
{

        if(empty($job_id))
        {
          echo "Page Expired";
          exit();
        }
          $data['days']=$this->news_model->get_days();
        $data['months']=$this->news_model->get_months();
  $data['applicants']=$this->db->select('*')->from('students_jobs')->where('job_id',$job_id)->get()->num_rows();
$data['job'] = $this->news_model->get_detail_job($job_id);
 $data['categories']= $this->news_model->get_categories(true);
 $data['job_types']= $this->news_model->get_job_types();
$data['cities'] = $this->news_model->get_cities(true);
$data['salaries']=$this->news_model->get_salaries();

        

$data['message'] = "";

  
     $this->load->view('templates/Cheader');
  $this->load->view('pages/update_job',$data);
  $this->load->view('templates/footer');


}

}
public function details_job($job_id='',$url='')
{

  $job=$this->news_model->get_detail_job($job_id,false);
  
     
  $url=$this->uri->segment(4);
  if($job_id=='' || empty($job)||empty($url))
  {

    redirect('pages/display_applicants');
    exit();
  }  
  
    

$data['job'] = $job;
  
     $this->load->view('templates/Cheader');
  $this->load->view('pages/details_job',$data);
  $this->load->view('templates/footer');


}
public function save_jobs_details($save_job_id="")


{

   if(isset($_GET['checked']))
   {

    $data['checked']=$_GET['checked'];
   }
   $job=$this->news_model->get_save_job($save_job_id);
      
      


   
 if(empty($job)||empty($save_job_id))
  {

    redirect('pages/display_save_jobs');
    exit();
  }
  $data['job']= $job;
  $data['back_url']=$this->input->get('back_url');

      $this->load->view('templates/Cheader');
  $this->load->view('pages/save_job_details',$data);
$this->load->view('templates/footer');
}
public function check_session()
{

  $is_logged_in=$this->session->userdata('is_logged_in');
 echo "login:".$is_logged_in;
 $this->load->view('pages/members_student');
/*  if(!$is_logged_in)
  {

    redirect('login');
  }*/
}
/*public function _remap($method)
{
 echo "remap";
 //echo $method;
 //echo ($this->$method());
 //$this->default_method();
 if ($method == 'display_jobs')
    {
        $this->$method();
         
    }
    else
    {
      $this->display_jobs();
      //  $this->default_method();
    }

}*/

public function _remap($method, $params = array())
    {

        // Check if the requested route exists
        if (method_exists($this, $method))
        {
            // Method exists - so just continue as normal
            return call_user_func_array(array($this, $method), $params);
        }
        else
        {
        redirect('login/is_logged_in');
      }
        }
public function details_student_job($applied_id='')
{

  
  if(isset($_GET['checked']))
  {
  $data['checked']=$_GET['checked'];
}
   $job= $this->news_model->get_detail_students_jobs($applied_id);
  if($applied_id=="" || empty($job))
  {

    redirect('pages/display_students_jobs');
    exit();
  }
  
  
  $data['job']=$job;
  $data['back_url'] = $this->input->get('back_url');
 
 
    
     
    
 
      $this->load->view('templates/Cheader');
  $this->load->view('pages/details_students_jobs',$data);
$this->load->view('templates/footer');

}

public function update_company()
{

  $user= $this->session->userdata('username');
	//$user = $this->input->post('user');
	
  /*if($user=="")
  {

    exit();
  }
*/
	$data['account'] = $this->news_model->get_company($user); 
	
	$data['message']= (isset($_GET['message'])?$_GET['message']:"");
     $this->load->view('templates/Cheader');
	$this->load->view('pages/update_profile_c',$data);
	$this->load->view('templates/footer');
    

}

public function update_co()
{

$original_user = $this->session->userdata('username');


  if(isset($_POST['update']))
  {
 

$this->form_validation->set_rules('user','Username','trim|required|callback_check_update_name|callback_check_space|callback_check_characters');
  
  
  $this->form_validation->set_rules('name', 'Full Name', 'trim|required');

  $this->form_validation->set_rules('crn', 'Company Registration Number', 'trim|required');
  $this->form_validation->set_rules('c_name','Company Name','trim|required');
  $this->form_validation->set_rules('c_information','Company Information','trim|required');
  $this->form_validation->set_rules('b_type','Business Type','trim|required');
  $this->form_validation->set_rules('phone','Phone','trim|required');
  $this->form_validation->set_rules('email','Email','trim|required|valid_emails|callback_check_update_email');
  $this->form_validation->set_rules('c_email','Confirm Email','trim|required|valid_emails|matches[email]');
  $this->form_validation->set_rules('website','Website','trim');
  $this->form_validation->set_rules('address','Address','trim|required');
  $this->form_validation->set_rules('township','Township','trim|required');
  $this->form_validation->set_rules('city','City','trim|required');
  $this->form_validation->set_rules('country','Country','trim|required');
  $this->form_validation->set_rules('logo_path','Company Logo','trim|required');
  
            if ($this->form_validation->run() === FALSE)
            {
              
           $data['result']['photo_path']=$this->input->post('photo_path');
                      
                      
                     $data['result']['e_server_path']=$this->input->post('e_server_path');
                      $data['result']['photo_server_path']= $this->input->post('photo_server_path');
                      $data['logo']['photo_path']=$this->input->post('logo_path');
                       $data['logo']['photo_server_path']= $this->input->post('logo_server_path');
                        $data['logo']['logo_e_server_path']=$this->input->post('logo_e_server_path');
                $data['message']="Update Incompleted";  
                $data['message2']="Please Check The Input Fields";
                $data['current_user'] = $original_user;

              $this->load->view('templates/Cheader');
              $this->load->view('pages/update_profile_c',$data);
              $this->load->view('templates/footer');
              
              }
             else{
                
        

           $user = $this->input->post('user');
          
           
              $name = $this->input->post('name');
              $c_name = $this->input->post('c_name');
              $crn=$this->input->post('crn');
              $c_information = $this->input->post('c_information');
              $b_type=$this->input->post('b_type');
              $phone = $this->input->post('phone');
              $email = $this->input->post('email');
              $website = $this->input->post('website');
              $address = $this->input->post('address');
              $township = $this->input->post('township');
              $city = $this->input->post('city');
              $country =$this->input->post('country');
              $photo= $this->input->post('photo_path');
           //   echo "photo : " . $photo . "<br>";
              $photo_server_path = $this->input->post('photo_server_path');
             // echo "photo server_path is : " . $photo_server_path . "<br>";
              $existing_path = $this->input->post('e_server_path');
             if(!empty($photo_server_path)&&$photo_server_path!==$existing_path)
             {
               
                            
                                if(!empty($existing_path))
                                {
                              @unlink($existing_path);
                                }
                          
                            
              }

                    $logo= $this->input->post('logo_path');
           //   echo "photo : " . $photo . "<br>";
              $logo_server_path = $this->input->post('logo_server_path');
             // echo "photo server_path is : " . $photo_server_path . "<br>";
              $existing_path = $this->input->post('logo_e_server_path');
             if(!empty($logo_server_path)&&$logo_server_path!==$existing_path)
             {
               
                            
                                if(!empty($existing_path))
                                {
                              @unlink($existing_path);
                                }
                          
                            
              }
               $data = array('username'=>$user,
                  'name'=>$name,'c_name'=>$c_name,'crn'=>$crn,'c_information'=>$c_information,'b_type'=>$b_type, 'phone'=>$phone,
                   'email'=>$email,'website'=>$website,
                  'address'=>$address, 'township'=>$township,'city'=>$city,
                  'country'=>$country,'photo'=>$photo,'photo_server_path'=>$photo_server_path,'logo'=>$logo,'logo_server_path'=>$logo_server_path);
              
              
              $this->db->where('username', $original_user);
              $result =$this->db->update('caccounts',$data);
              $this->db->where('user_c',$original_user);
              $this->db->update('jobs',array('user_c'=>$user,'logo'=>$logo,'photo'=>$photo));
              $this->db->where('user_c',$original_user);
              $this->db->update('applicants',array('user_c'=>$user));
                  $this->db->where('user_c',$original_user);
                  $this->db->update('abused_reports',array('user_c'=>$user));
              $this->db->where('user_c',$original_user);
              $this->db->update('students_jobs',array('user_c'=>$user));

   
                  if($result)
                  {

                      
                     $data = array('username'=> $user,'name'=>$name, 'role'=>'company'
                      ,'is_logged_in' => true);
                               $this->session->set_userdata($data);
                               $user = $this->session->userdata('username'); 
                        $data['account'] = $this->news_model->get_company($user);

                       //   $data['message'] = "Update Profile Successful.";
                    redirect('login/is_logged_in?message=Update Successful');
                  /* $this->load->view('templates/Cheader');
                    $this->load->view('pages/update_profile_c', $data);
                    $this->load->view('templates/footer');
                  */

                  }
          }

}
 

            elseif(!isset($_POST['update']))
   {

            $user = $this->session->userdata('username');
            $data['account']= $this->news_model->get_company($user);
              
               
          $data['message'] = "";
            $this->load->view('templates/cheader');
            $this->load->view('pages/update_profile_c',$data);
            $this->load->view('templates/footer');


   }




   
   

//echo "the result is " . $result;


}
public function delete_company_photo()
{
 $server_path = $this->input->post('server_path');
     $existing_path = $this->input->post('existing_path');
     if($server_path !== $existing_path)
     {
     /* $user = $this->session->userdata('username');
      $this->db->update('students',array('photo'=>'','photo_server_path'=>''))*/
     /* ->where('user',$user);*/
      @unlink($server_path);
    }

}

public function delete_company_logo()
{
 $server_path = $this->input->post('server_path');
     $existing_path = $this->input->post('existing_path');
     if($server_path !== $existing_path)
     {
     /* $user = $this->session->userdata('username');
      $this->db->update('students',array('photo'=>'','photo_server_path'=>''))*/
     /* ->where('user',$user);*/
      @unlink($server_path);
    }

}

public function view_job($job_id="")
{
  
  $jobs=$this->news_model->get_available_job($job_id);
   if($job_id==""||empty($jobs))
   {
       redirect('pages/display_jobs');
       exit();
   }
	$is_logged_in = $this->session->userdata('is_logged_in');
  $role=$this->session->userdata('role');
  $user=$this->session->userdata('username');
	$data['job'] = $jobs;
  $data['job_no'] = $job_id;
  $user = $this->session->userdata('username');
  $data['student'] = $this->news_model->get_each_student($user);
  $applied=$this->db->select('*')->from('applicants')->where(array('user'=>$user,'job_id'=>$job_id))->get()->num_rows();
   $data['applied']=$applied;
   $data['recommended2']=$this->news_model->recommended2($job_id);


  
		


               
               $this->load->view('templates/cheader');
               $this->load->view('pages/jobdetails',$data);
               $this->load->view('templates/footer');
          

       
               


}
public function recommended($job_id="")
{

 $recommended = $this->news_model->get_available_job($job_id);
  if($job_id==""||empty($recommended))
  {

    redirect('pages/display_jobs');
  }
  $data['recommended']=$recommended;
  
  $is_logged_in = $this->session->userdata('is_logged_in');
  $role=$this->session->userdata('role');


  $this->load->view('templates/Cheader');
  $this->load->view('pages/recommended_details',$data);
  $this->load->view('templates/footer');
}
public function view_history()
{
    $user=$this->session->userdata('username');
    $job_id=$this->input->post('job_id');

   $data= array('job_id'=>$this->input->post('job_id'),
    'user'=>$user,'view_times'=>1);
   $a=$this->db->select('*')->from('view_history')->where(array('job_id'=>$job_id,'user'=>$user))->get()->row_array();
    
   if(!empty($a))
   {
      
      $count=$a['view_times'];
      $count++;
      $this->db->where(array('job_id'=>$job_id,'user'=>$user));
      $this->db->update('view_history',array('view_times'=>$count));
      return;

   }   
   else
   {
  
   $this->db->insert('view_history',$data);
   }

}
public function report_abuse()
{
      $user =$this->input->post('user');
     
      $job_id = $this->input->post('job_id');
      $user_c = $this->input->post('user_c');
      if(empty($user)||empty($job_id)||empty($user_c))
      {

        redirect('pages/display_jobs');
        exit();
      }
     


       $result = $this->db->get_where('abused_reports',array('user'=>$user,
        'job_id'=>$job_id));
     
      
      
       if($result->num_rows() == 0)
      {
       
      $data=array('user'=>$user,'job_id'=>$job_id,'number'=>'true','user_c'=>$user_c);
      $this->db->insert('abused_reports',$data);
      

      $answer= $this->db->select('user,job_id,user_c, Count("user_c") as user_total')->from('abused_reports')
  ->group_by('user_c')->where(array('user_c'=>$user_c))->order_by('user_total','desc');
   $answer = $answer->get()->result_array();
   $answer = (empty($answer)?0:$answer[0]['user_total']);
   $this->db->where(array('username'=>$user_c));
   $this->db->update('caccounts',array('abused'=>$answer));
   

/*   $result2 = $this->db->get('students');
   $result['total_students'] = $result2->num_rows();*/
  return $result;


      }
      else
      { 
         
        $data = array('message'=>'you already reported abuse');
        echo json_encode($data);
      }
  
}
public function report()
{ 
  $user ="student";

    $result = $this->db->get_where('abused_reports',array('user'=>'student8'));
   // echo $result->num_rows();

  /*if(empty($result))
  {
    echo "empty";
  }
  else
  {
    echo "not empty";
  }*/
 // print_r($result);
}
public function top_job_details($job_id)
{
 
   $top_job = $this->news_model->get_available_job($job_id);
  if($job_id==""||empty($top_job))
  {

    redirect('pages/display_jobs');
  }
  $data['top_job']=$top_job;
  
  $is_logged_in = $this->session->userdata('is_logged_in');
  $role=$this->session->userdata('role');
  
  

     
  
  
  $this->load->view('templates/Cheader');
  $this->load->view('pages/top_job_details',$data);
  $this->load->view('templates/footer');
     

    

}
public function check_bday($day)
{
  if(preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])$/",$day))
  {
    return true;
  }
  else
  {
     $this->form_validation->set_message('check_bday', ' The %s date format is dd-mm-yy');
     return FALSE;
  }

}
public function check_string($string,$string2)
{

  if($string2=='in')
  {
    return TRUE;
  }
  else
  {
    $this->form_validation->set_message('check_string',"the string is not in");
    return false;
  }
}
public function check_b_date($date,$date2)
{
if (preg_match("/^([0-9]{4})$/",$date2))
  //(preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])-(0[1-9]|1[0-2])-[0-9]{4}$/",$date2))
{//$date=date_create($date);
  
  return TRUE;
}
else{

  $this->form_validation->set_message('check_b_date', 'The birth date format is day-month-yyyy');
   return FALSE;  
}

}

public function check_valid_to($date,$date2)
{
  if(preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])-([a-zA-Z]+)-[0-9]{4}$/",$date2))
//if (preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])-(0[1-9]|1[0-2])-[0-9]{4}$/",$date2))
{//$date=date_create($date);
  
  return TRUE;
}
else{

  $this->form_validation->set_message('check_valid_to', 'The date format is day-month-yyyy');
   return FALSE;  
}

}

public function valid_to_date($date,$date2)
{
  if($date2<date('Y-m-d'))
//if (preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])-(0[1-9]|1[0-2])-[0-9]{4}$/",$date2))
{//$date=date_create($date);
   $this->form_validation->set_message('valid_to_date','Valid To Date Can Not Be Before Today');
  return false;
}
else{

  $this->form_validation->set_message('check_valid_to', 'The date format is day-month-yyyy');
   return true;  
}

}
public function valid_to_extend($date,$vari)
{
if($vari=="incorrect")
{
  
                                 
  $this->form_validation->set_message('valid_to_extend', 'The date can only be extended');
   return FALSE;  
}
else{

   return true;  
   
  
}

}

public function check_date($date)
{
if (preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])-(0[1-9]|1[0-2])-[0-9]{4}$/",$date))
{//$date=date_create($date);
  
  return TRUE;
}
else{

  $this->form_validation->set_message('check_date', 'The %s date format is dd-mm-yyyy');
   return FALSE;  
}

   

}
public function check_characters($string)
{
           /////^[a-z0-9:_!#@$%^&*(){}+|\\\<>\/\[\]\-]+$/i";   
if(! preg_match("/^[a-z0-9:_!@#$%^&*()<>{}+=\/-]+$/i", $string))
{
    
    $this->form_validation->set_message('check_characters','The username have disallowed characters');
    return false;
   }
   else
   {
    return true;
   }

}
public function check_space($string)
{
   if(preg_match("/[\s]+/",$string))
   {
    
    $this->form_validation->set_message('check_space','white space is not allowed');
    return false;
   }
   else
   {
    return true;
   }

}


 public function check_name($str)
	{
		    $query = $this->news_model->check_user($str);
	      
		if ($query)
		{
			 return TRUE;
		}
		else
		{$this->form_validation->set_message('check_name', 'Choose another user name');
			return FALSE;
			
		}      
	

			
	}

public function check_email($str)
{
  $result= $this->news_model->check_duplicate_email($str);
  if($result)
  {
     return true;

  } 
  else
  {
     $this->form_validation->set_message('check_email', 'Choose Another Email');
      return FALSE;

  }

}
public function check_update_email($stri)
{     
      $result = $this->news_model->check_duplicate_email($stri);
      $result2 = $this->news_model->check_update_email($stri);
      if($result2==true|| $result== true)
    {
        return true;


      }
      else
      {
         $this->form_validation->set_message('check_update_email','Choose Another Email');
         return false;

      }

}  

public function create_student()
{
	
 
  

  if(isset($_POST['register']))
  {
    
             $b_day=$this->input->post('b_day');
             $b_month=$this->input->post('b_month');
             $b_year = $this->input->post('b_year');
             $date_o_b = $b_day."-".$b_month ."-". $b_year;
            
    
	
$this->form_validation->set_rules('user','User Name','trim|required|callback_check_name|callback_check_space|callback_check_characters');
	
	$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]');
	$this->form_validation->set_rules('c_password', 'Confirm Password', 'trim|required|min_length[4]|matches[password]');
  $this->form_validation->set_rules('nrc','National Registration Number','trim|required|callback_check_characters');
  $this->form_validation->set_rules('no','Student Registration Number','trim|required|callback_check_characters');
	$this->form_validation->set_rules('name', 'Name', 'trim|required');
	
  $this->form_validation->set_rules('gender', 'Gender', 'trim|required');
  $this->form_validation->set_rules('m_status', 'Marital Status', 'trim|required');
 
  $this->form_validation->set_rules('b_day', 'Date Of Birth', "trim|required|callback_check_b_date[$b_year]");
  $this->form_validation->set_rules('b_month','Date Of Birth','trim|required');
  $this->form_validation->set_rules('b_year',"Date Of Birth",'trim|required');
  $this->form_validation->set_rules('nationality', 'Nationality', 'trim|required');
  $this->form_validation->set_rules('qualifications', 'Qualifications', 'trim|required');
  $this->form_validation->set_rules('major', 'Major', 'trim|required');
 
  $this->form_validation->set_rules('experience','Work Experience','trim|required');
  
  $this->form_validation->set_rules('phone', 'Phone', 'trim|required|');
  $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_emails|callback_check_email|max_length[800]');
  $this->form_validation->set_rules('c_email','Confirm Email','trim|required|valid_emails|matches[email]|max_length[800]');
  $this->form_validation->set_rules('address', 'Address', 'trim|required|max_length[800]');
  $this->form_validation->set_rules('township', 'Township', 'trim|required|max_length[800]');
  $this->form_validation->set_rules('city', 'City', 'trim|required|max_length[800]');
  $this->form_validation->set_rules('country', 'Country', 'trim|required|max_length[800]');
   
   $this->form_validation->set_rules('photo_path', 'Photo', 'trim|required');
  
  
	
	if ($this->form_validation->run() === FALSE)
	{ 
    $is_logged_in = $this->session->userdata('is_logged_in');
       if(!isset($is_logged_in) || $is_logged_in != true)
		     {
             
                     // $data['upload_result']['server_path']=$this->input->post('photo_server_path');
                      $data['result']['photo_path']=$this->input->post('photo_path');
                      
                      $data['result']['photo_server_path']= $this->input->post('photo_server_path');
                        $days=$this->news_model->get_days();
                           $data['days']=$days;
                    $months=$this->news_model->get_months();
                    $data['months']=$months; 
           $data['message'] = "Registering Incompleted.";
           $data['message2']="Please Check The Input Fields";
          //  redirect('pages/create_student');
			     $this->load->view('templates/header');
	         $this->load->view('pages/studentr',$data);
            $this->load->view('templates/footer');
            
		     }
		  

	}
	else
	{
		$result= $this->news_model->create_students();
           if($result)
           {
           $email = $this->input->post('email');
               
		        $this->load->view('templates/header');
		       $this->load->view('pages/success');
		       $this->load->view('templates/footer');
            }
             else{
             // $this->load->view('templates/header');
            echo "Registration Unsuccessful";
            }

	}
}
 elseif(!isset($_POST['register']))
      {
                       $days=$this->news_model->get_days();
                           $data['days']=$days;
                    $months=$this->news_model->get_months();
                    $data['months']=$months; 
                   $this->load->view('templates/header');
                  $this->load->view('pages/studentR',$data);
                  $this->load->view('templates/footer');   
      }




}

public function student_photo()
{
     
  ///print_r($_POST);
   //echo "here";
  // echo $_POST['submitted'];
    



}
public function delete_u_student_photo()
{
     $server_path = $this->input->post('server_path');
     $existing_path = $this->input->post('existing_path');
     if($server_path !== $existing_path)
     {
     /* $user = $this->session->userdata('username');
      $this->db->update('students',array('photo'=>'','photo_server_path'=>''))*/
     /* ->where('user',$user);*/
     @unlink($server_path);
    }

}
public function delete_student_photo()
{
     $server_path = $this->input->post('server_path');
    
    
      @unlink($server_path);
   

}


public function ViewJobs()
{


}

public function createComp()
{/*$this->load->helper('form');
	$this->load->library('form_validation');
	
     echo "This is create method at pages controller";
	$data['title'] = 'Create a news item';

	$this->form_validation->set_rules('user', 'user', 'required');
	$this->form_validation->set_rules('name', 'Name', 'required');
	$this->form_validation->set_rules('password', 'Password', 'required');
	$this->form_validation->set_rules('cpassword', 'Confirm Password', 'required|matches[password]');
	//$this->form_validation->set_rules('address', 'address', 'required');
	
	if ($this->form_validation->run() === FALSE)
	{
		$this->load->view('templates/header', $data);
		$this->load->view('pages/'.'CompanyR');
		$this->load->view('templates/footer');

	}
	else
	{   $this->load->model('news_model');
		$this->news_model->set_company();
		$this->load->view('pages/success');
	}*/
}

/*public function members()
{
	echo " this is members function";
 //  $this->load->view('pages/members');

}*/

 public function JobCreate()

 {   
       



 }
 public function get_job_photo()
 {


 }

 public function practice($q=0)
 {
  print_r($_POST);
  
  echo "<br>".$q ."<br>";
  
  
  ////echo "url". $this->uri->uri_string();
  ////echo "<br>";
  ////$q=Base64_decode($q);
  ////echo "<br>";
      // $q=urlencode($q);
  
    //  $q=urldecode($q);echo "<br>";
      //$q=rawurldecode($q)."<br>";
    //  echo $_GET['intro'];
     // echo "server: ". $_SERVER['QUERY_STRING']."<br>";
   //  $q=urlencode($q);
    ////  echo "base64 encoded q:" .$q ."<br>";
      /*echo "string position:";echo strpos($q,"letters");
      $length=strlen('letters');
      echo substr($q,0,($length-1));
      echo substr($q,$length+1);*/
      //echo "<br>";
    

     
    /* $q= urlencode($q);
     echo '<br>urlencode:'.$q;*/
 //$q=str_replace('&#47;','%2F',$q);
//    $q=str_replace('&#41;','%29',$q);*/
     // $q=preg_replace('/(/','where',$q);
      // echo "after replaces:" .$q. "<br>";
    //// parse_str($q,$_POST);
//      $_POST=str_replace('letters','where',$_POST);
     //// echo "after parse str: ";print_r($_POST);echo"<br>";
     //// echo $this->input->post('letters');
     // print_r($_POST);
     // echo "from data: " .$this->input->post('letters');echo"<br>";
      //$c=$this->input->post('c');
      //parse_str($c,$string);
      //print_r($string);
//       parse_str($_POST['c'],$string);
 //      echo urlencode($string['letters']);
     //  print_r($string);
      // echo $string['letters']['job_type'];
 //     parse_str($q,$string4);
       
     //  echo $this->input->post('letters');
    // $data['info'] =  $this->news_model->do_upload();
   //  print_r($data); echo "<br>";
  //echo json_decode($this->input->post('b'));
  //print_r($string4);
  //print_r($string4);
 


     
      
             
   

 }
 

 public function upload_resume()
 {

       
     $data['result'] =  $this->news_model->upload_file();
     
    $job_no = $this->input->post('job_no');
    $data['job_no'] = $job_no;
    $data['job_details']  = $this->news_model->get_detail_job($job_no);
    $data['segment'] = $this->input->post('segment');
    $user = $this->session->userdata('username');
    $data['my_profile'] = $this->news_model->get_each_student($user);
   
   //a echo "image infromation is:" . $image;
    // $data['images'] = $this->news_model->get_images();
    //$this->load->view('templates/cheader');
    $this->load->view('pages/ajax_upload_resume',$data);
  //  $this->load->view('templates/footer');
 }
	
 public function apply_job()
{     
 
$job_id = $this->input->post('job_id');
$user = $this->input->post('user');
$user_c = $this->input->post('user_c');
$file_path = $this->input->post('file_path');
if($job_id=="" ||$user==""||$user_c==""||$file_path == "")
{
  redirect('pages/display_jobs');
  exit();
   
}

  $role = $this->session->userdata('role');
  if($role !=="student")
  {

    echo "You must be registered as student to apply job";
    exit();
  }


  $this->news_model->create_applicants();
 
     $data['job_id'] = $job_id;

     $c_name= $this->input->post('c_name');
          $title=$this->input->post('title');

  redirect("pages/success_apply?id=$job_id&c_name=$c_name&title=$title");
/*  $this->load->view('templates/cheader');
  $this->load->view('pages/success_apply',$data);
  $this->load->view('templates/footer');

*/



}
public function success_apply()
{

  $data['job_id']=(isset($_GET['id'])?$_GET['id']:"");
  $data['c_name']=(isset($_GET['c_name'])?$_GET['c_name']:"");
  $data['title']=(isset($_GET['title'])?$_GET['title']:"");
  $this->load->view('templates/cheader');
  $this->load->view('pages/success_apply',$data);
  $this->load->view('templates/footer');





}

public function get_file($file_id)
{
      $file_type =$this->db->select('file_type')->from('applicants')->
      where('file_id',$file_id);

      $file_type = $file_type->get()->result_array();
      //print_r($file_type);
    // echo $file_type[0]['file_type'];

      $file_name = $this->db->select('file_name')->from('applicants')->
      where('file_id',$file_id);
      $file_name= $file_name->get()->result_array();
      
      $file = $this->db->select('file')->from('applicants')->
      where('file_id',$file_id);
      $file = $file->get()->result_array();

      header("Content-Type:". $file_type[0]['file_type']);
      header("Content-Disposition:attachment;filename=". $file_name[0]['file_name']);
    //  $file= $file[0]['file'];
      //readfile($file);
      //echo $file[0]['file'];
       exit();
}

public function send_email($email)
{
   $config = Array( 'protocol'=>'smtp',
    'smtp_host'=>'ssl://smtp.googlemail.com',
    'smtp_port'=> 465,
    'smtp_user'=> 'jobmatchingmm@gmail.com',
    'smtp_pass'=> '12february'
   );
$this->load->library('email',$config);
/*$config['protocol'] = 'sendmail';
$config['mailpath'] = '/usr/sbin/sendmail';
$config['charset'] = 'iso-8859-1';
$config['wordwrap'] = TRUE;

$this->email->initialize($config);
*/$this->email->set_newline("\r\n");
$this->email->from('jobmatchingmm@gmail.coom','Job Matching');
$this->email->to($email);
$this->email->subject('Email from Job Match');
$this->email->message("Thank you for registering with Job Career Development Site.\n
 Remember you username and password. Please wait for another confirmation email for log in.");

if(@$this->email->send())
{

  echo 'Your email was sent';



}
else
{

  //show_error($this->email->print_debugger());
 //echo "Internet is not connected, email can not be sent";
  return false;
}
}
public function save_job()
{
    $user = $this->input->post('user');
   $no = $this->input->post('job_no');
   if(empty($user)||empty($no))
   {

    redirect('pages/display_jobs');
    exit();
   }
    //echo "this is from save_jobs function ". $user. "job id:" . $no;

    
     $field1 = array('user'=>$user,'job_id'=>$no,'delete_status !='=>'true');
     $result = $this->db->get_where('save_jobs',$field1);

     if($result->num_rows()>0)
     {
         $status=array('name'=>'false');
         echo json_encode($status);

     }
     if($result->num_rows() == 0)
     {   // $total= $this->db->count_all('save_jobs');
          //$id='SJ'+($total+1);
    $field2 = array('user'=>$user,'job_id'=>$no,'delete_status'=>'true');
     $result2 = $this->db->get_where('save_jobs',$field2);
   
    
      if($result2->num_rows())
      {
         $result2=$result2->row_array();
         $save_job_id=$result2['save_job_id'];
         $data= array('user'=>$user,'saved_date'=>date('Y-m-d'),'job_id'=>$no,'delete_status'=>'');
         $this->db->where('save_job_id',$save_job_id);
          $this->db->update('save_jobs',$data);
      }
      else
      {
          $data= array('user'=>$user,'saved_date'=>date('Y-m-d'),'job_id'=>$no);
          $this->db->insert('save_jobs',$data);
        }
      }

  



}
public function direct_to_save_jobs()
{     
     $user = $this->input->post('user');
  echo "hi from save jobs";
      $data['save_jobs']= $this->news_model->get_save_jobs($user);
      $this->load->view('templates/cheader');
      $this->load->view('pages/save_jobs',$data);
      $this->load->view('templates/footer');

  

}
public function delete_save_job()
{

  //$save_jobs_segment = "http://localhost:81/ci1/pages/delete_save_job/".$q."/".$sort_by."/".$sort_order."/".$offset;
  //$data['offset'] = $offset;
  
 //$this->load->view('pages/about');
  $user=$this->session->userdata('username');
 
if(!empty($_POST['save_job_id']))
{
 $save_job_id=$this->input->post('save_job_id');
}
if(!empty($_POST['selected']))
{

 $save_job_id=$_POST['selected'];
}
if(empty($save_job_id))
{
  redirect('pages/display_save_jobs');
  exit();
}
 

 //print_r($_POST);


 


   
 
 
 $previous_url= $this->input->post('previous_url');
 

 
 
 
 
 
 
//$segment = $this->input->post('segment');
    $data = array('delete_status'=>'true');
       $this->db->where('user',$user);
       $this->db->where_in('save_job_id',$save_job_id);
      $result = $this->db->update('save_jobs',$data);
               
    if($result)
      {
 
             redirect($previous_url);
           
                 }
                 

   
   
                 

}
public function refresh_applied_jobs()
{

  
}
public function delete_file()
{
  
  
  //$file = $this->input->post('file');
  
  $server_path = $this->input->post('server_path');
 
   
        
  @unlink($server_path);
     

  
    

}

public function recover()
{
      $data['message']="";
     $this->load->view('templates/header');
     $this->load->view('pages/recovery_page',$data);
     $this->load->view('templates/footer');

}



public function recover_u_p($email,$number)
{
         $result = $this->news_model->recover_u_p($email,$number);

       if(empty($email)||empty($number))
       {

          redirect('login');

       }
       if(isset($_POST['change_password']))
       {
$data['user'] = $this->input->post('user');
  
  
$this->form_validation->set_rules('password','Password','trim|required|min_length[4]');
$this->form_validation->set_rules('c_password','Confirm Password','trim|required|matches[password]|min_length[4]');
if ($this->form_validation->run() === FALSE)
{
      $data['email']=$email;
      $data['number']=$number;
       $data['message']="";
       $data['user']=$this->input->post('user');
    $this->load->view('templates/header');
  $this->load->view('pages/renew_u_p',$data);
  $this->load->view('templates/footer');

     

}
else{
          
      $user = $this->input->post('user');
    $result = $this->news_model->renew_password($user);
     if($result)
     {
     
          $data['message']="Password Update Completed";
          $this->news_model->erase_random_number($user);
          $data['email']="";
          $data['number']="";
    $this->load->view('templates/header');
  $this->load->view('pages/admin_renew_success', $data);
  $this->load->view('templates/footer');

     }

}

       }
       else
       {

       $result = $this->news_model->recover_u_p($email,$number);
       
       $data['email']=$email;
       $data['number']=$number;
       $data['profile']=$result['profile'];
       $data['expired']=$result['expired'];
       $data['message']="";
     $profile=$result['profile'];
       if(!empty($profile))
       {
            if(isset($profile[0]['username']))
            {
           $user=$profile[0]['username'];
             }
             else
             {
              $user=$profile[0]['user'];
             }
       }
       else
       {
        $user='';
       }
       $this->news_model->erase_random_number($user);
       if($result['expired']==false)
       {

        $this->load->view('templates/header');
        $this->load->view('pages/renew_u_p',$data);
        $this->load->view('templates/footer');


       }
       else
       {
            $data['message']='Link to recover username and password has expired.
            Please apply for username/password recovery again.';

        $this->load->view('templates/header');
        $this->load->view('pages/renew_u_p_expired',$data);
        $this->load->view('templates/footer');
       }

}

}


public function renew_password()
{
 
  $data['user'] = $this->input->post('user');
  
  
$this->form_validation->set_rules('password','Password','trim|required');
$this->form_validation->set_rules('c_password','Confirm Password','trim|required|matches[password]');
if ($this->form_validation->run() === FALSE)
{
       $data['message']="";
       $data['user']=$this->input->post('user');
    $this->load->view('templates/header');
  $this->load->view('pages/renew_u_p',$data);
  $this->load->view('templates/footer');

     

}
else{
          
      $user = $this->input->post('user');
    $result = $this->news_model->renew_password($user);
     if($result)
     {
     
          $data['message']="Password Update Completed";
          $this->news_model->erase_random_number($user);
    $this->load->view('templates/header');
  $this->load->view('pages/renew_u_p', $data);
  $this->load->view('templates/footer');

     }

}

}

public function view_applicant($applicant_id='')
{
   $applicant= $this->news_model->get_applicant_details($applicant_id);
   if($applicant_id=="" || empty($applicant))
  {

    redirect('pages/display_applicants');
    exit();
  }  
  

  

  $data['applicant']= $applicant;
  
  
      $this->load->view('templates/Cheader');
  $this->load->view('pages/applicant_details',$data);
$this->load->view('templates/footer');

}




}
?>