<?php
class Login extends CI_Controller
{
    
	public function __construct()
	{
		parent::__construct();
 /*$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
$this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
$this->output->set_header('Pragma: no-cache');*/

 header("cache-Control: no-store, no-cache, must-revalidate");
header("cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
 
date_default_timezone_set('Asia/Rangoon');
 session_start();
		$this->load->model('news_model');
$this->load->library('session');
$this->load->driver('cache');
$this->load->helper('form');
$this->load->library('form_validation');// $this->load->helper('form');
$this->form_validation->set_error_delimiters('<span class="error" 
    style="color:red;">','</span>'); 

    $this->load->library('pagination');
                       $this->load->library('table');   
//$this->load->library('form_validation');               
	}

	public function is_logged_in()
	{  
/*$this->CI->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
$this->CI->output->set_header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");*/
/*$this->CI->output->set_header("Pragma: no-cache"); */
        
		$is_logged_in = $this->session->userdata('is_logged_in');
		
		if(!isset($is_logged_in) || $is_logged_in != true)
		{

			echo "You don't have permission to access this page.  
			<a href='../login'>Login</a>";
      exit();

		}

		else
		{             
               
               
               $role = $this->session->userdata('role');
               
               
               if($role == "company")
               {
                $data['user'] = $this->session->userdata('username');
                $user = $this->session->userdata('username'); 
                $password= $this->session->userdata('password'); 
                $data1 = array('job_status'=>'expired');
         $today = date("Y-m-d");
         $this->db->where('valid_to <',$today);
        $result =$this->db->update('jobs',$data1);

              
                //print_r($total_rows);                
                $data['account'] = $this->news_model->get_company($user);
              
        
       
                $this->load->view('templates/Cheader');
                $this->load->view('pages/members',$data);
                $this->load->view('templates/footer');
               }
   elseif ($role == "student")
               {
               
               $data['user'] = $this->session->userdata('username');
               $user = $this->session->userdata('username'); 
               $data['account'] = $this->news_model
                ->get_each_student($user); 	
              
                $this->load->view('templates/Cheader');
                $this->load->view('pages/members_student',$data);
                $this->load->view('templates/footer');
              
               }
        }

	}
  public function paginate_applicants()
  {

  $data['user'] = $this->session->userdata('username');
                $user = $this->session->userdata('username'); 
                $password= $this->session->userdata('password'); 
                //$data['jobs'] = $this->news_model->get_jobs($user);
                $total_rows = $this->news_model->get_jobs_rows($user);
                             
                $data['myaccount'] = $this->news_model->get_account($user); 
                $total_rows_ap = $this->news_model->get_applicants_rows($user);
                $config['base_url'] = base_url()."login/paginate_applicants/";
        $config['total_rows']= $total_rows_ap;
        $config['per_page'] = 5; 
        $config['uri_segment'] = 3;
         $config['full_tag_open']='<p style=text-align:center; id="pagination">';
         $config['full_tag_close']='</p>';

         $query2 = $this->db->select('*')->from('applicants')->where('user_c',$user)
      ->limit(5,$this->uri->segment(3));
      $data['my_applicants'] = $query2->get()->result_array();

$query= $this->db->select('*')->from('jobs')->where('user_c',
          $user)->limit(5);
 $data['jobs'] = $query->get()->result_array();

         
        $this->pagination->initialize($config);
        $data['pagination2'] = $this->pagination->create_links();
        $data['pagination']="";
        $data['segment'] = $this->uri->uri_string();
         $data['sort_by'] = "id" ;
         $data['sort_order'] = "asc";  
         $data['q']=0;  
 $this->load->view('templates/Cheader');
                $this->load->view('pages/members',$data);
                $this->load->view('templates/footer');


  }
	public function SignOut()
	{
             $this->session->sess_destroy();
            // $_SESSION['some']='out';
            session_destroy();
             $this->cache->clean();
             
             redirect(base_url() .'login');
	}

    function index()
    {
      //$data['main_content']='login_form';
     // $this->load->view('templates/header');
     $is_logged_in = $this->session->userdata('is_logged_in');
		/*if(!isset($is_logged_in) || $is_logged_in != true)
		{
            $this->load->view('templates/header');
			$this->load->view('pages/Login_Form');
             $this->load->view('templates/footer');
		}*/
  //  else
    ////{
            $this->load->view('templates/header');
      $this->load->view('pages/login_form');
             $this->load->view('templates/footer');
         

    //}     
      

    }
    function view($page="practice")
    {
    	//$this->load->view('pages/practice');


    }
    function validate()

    {
    	
          
           $result=  $this->news_model->cvalidate();
         $result_student = $this->news_model->validate_student();
         
         
           if($result)
           {
                 $user=$this->input->post('username');
                 $name=$this->news_model->get_company_user_name($user);
                 $data = array('username'=> $this->input->post('username'),
                  'name'=>$name,'role'=>'company','is_logged_in' => true);
                 $this->session->set_userdata($data); 
                $data['user'] = $this->session->userdata('username');
                $user = $this->session->userdata('username'); 
                $password= $this->session->userdata('password'); 
                $data['jobs'] = $this->news_model->get_jobs($user);
                $data['myaccount'] = $this->news_model->get_company($user); 
                redirect('login/is_logged_in');
                
                
           }
           elseif($result_student)
           {    
                
              
                 
               
            
                $user=$this->input->post('username'); 
                $name= $this->news_model->get_student_name($user);
                  
                   $data = array('username'=> $this->input->post('username'),'name'=>$name,
              'role'=>'student','is_logged_in' => true);
               $this->session->set_userdata($data); 
               $data['role'] = $this->session->userdata('role'); 
                  $user = $this->session->userdata('username');
                $data['student_profile'] = $this->news_model
                ->get_each_student($user);
                $data['user'] = $this->session->userdata('username');
              
                redirect('login/is_logged_in');
                
                
                //redirect('pages/view/members',$user);
                $this->load->view('templates/Cheader');
                $this->load->view('pages/members_student',$data);
                $this->load->view('templates/footer');
           }

         
           else
           {
            $data['username']=$this->input->post('username');
            $data['password']=$this->input->post('password');
            $data['error']="Login Failed";
           	$this->load->view('templates/header');
            $this->load->view('pages/login_form',$data);
            $this->load->view('templates/footer');
           }
              

           


    }

    public function signup()
    {
          $this->load->view('templates/header');
          $this->load->view('pages/companyr');
          $this->load->view('templates/footer');


    }
   public function check_name($str)
	{
		   
           $query = $this->news_model->check_user($str);
           if($query)
           {

           	return TRUE;
           }
           else
           {

           	    $this->form_validation->set_message('check_name','Choose another user name');
           	    return FALSE;


           }

		   

			
	}
  public function check_email($str)
{
  $result= $this->news_model->check_duplicate_email($str);
  if($result)
  {
     return true;

  } 
  else
  {
     $this->form_validation->set_message('check_email', 'Choose another email');
      return FALSE;

  }

}  

public function check_characters($string)
{
           /////^[a-z0-9:_!#@$%^&*(){}+|\\\<>\/\[\]\-]+$/i";   
if(! preg_match("/^[a-z0-9:_!@#$%^&*()<>{}+=\/-]+$/i", $string))
{
    
    $this->form_validation->set_message('check_characters','The username have disallowed characters');
    return false;
   }
   else
   {
    return true;
   }

}
public function check_space($string)
{
   if(preg_match("/[\s]+/",$string))
   {
    
    $this->form_validation->set_message('check_space','white space is not allowed');
    return false;
   }
   else
   {
    return true;
   }

}

    public function create_company()
    {


          if(isset($_POST['register']))
          {
	$this->form_validation->set_rules('user','UserName','trim|required|callback_check_name|callback_check_space|callback_check_characters|max_length[800]');
	
	$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|max_length[800]');
	$this->form_validation->set_rules('c_password', 'Confirm Password', 'trim|required|matches[password]|min_length[4]|max_length[800]');
	$this->form_validation->set_rules('name','Full Name','trim|required|max_length[800]');
  $this->form_validation->set_rules('c_information','Company Information','trim|required');  
  $this->form_validation->set_rules('crn','Company Registration Number','trim|required');  

  $this->form_validation->set_rules('b_type','Business Type','trim|required|max_length[800]');
  $this->form_validation->set_rules('phone','Phone','trim|required|max_length[800]');
  $this->form_validation->set_rules('email','Email','trim|required|valid_emails|callback_check_email|max_length[800]');
  $this->form_validation->set_rules('c_email','Confirm Email','trim|required|valid_emails|matches[email]|max_length[800]');
  $this->form_validation->set_rules('website','Website','trim|max_length[800]');
  $this->form_validation->set_rules('address','Address','trim|required|max_length[800]');
  $this->form_validation->set_rules('township','Township','trim|required|max_length[800]');
  $this->form_validation->set_rules('city','City','trim|required|max_length[800]');
  $this->form_validation->set_rules('country','Country','trim|required|max_length[800]');
  $this->form_validation->set_rules('c_name','Company Name','trim|required|max_length[800]');
 $this->form_validation->set_rules('logo_path','Logo','trim|required');	//$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
	 
	          if ($this->form_validation->run() === FALSE)
	               {
           
                    $data['message']="Registering Incomplete";
                    $data['message2']="Please Check The Data Fields";
                   
                    $data['result']['photo_path']=$this->input->post('photo_path');
                       $data['result']['photo_server_path']= $this->input->post('photo_server_path');
                        $data['logo']['photo_path']=$this->input->post('logo_path');
                       $data['logo']['photo_server_path']= $this->input->post('logo_server_path');
                     
                              $this->load->view('templates/header');
                              $this->load->view('pages/Companyr',$data);
                               $this->load->view('templates/footer');  

                                                             
                   //$data['logo']['photo_path']=$this->input->post('logo_photo_path');
                     //  $data['logo']['photo_server_path']= $this->input->post('logo_photo_server_path');
                      
                 

	                  }
	                  else
	                  {

        
		               
		                      
                            
		                     if($result = $this->news_model->create_company())
		                       {
                               $email=$this->input->post('email');
                              $this->load->view('templates/header');
           $this->load->view('pages/email_confirmation');
           $this->load->view('templates/footer');//Need to remove this.


                             //  $this->news_model->send_login_email($email);
                       
		                        }
                       		else
		                        {
                              $this->load->view('templates/header');
                              $this->load->view('pages/companyr');
                              $this->load->view('templates/footer');

                         		}
		
	                 }

}
else
{
  
       
                   $this->load->view('templates/header');
                  $this->load->view('pages/companyr');
                  $this->load->view('templates/footer');   
             
      


}


	

    

    


}
public function validate_recovery_email()
{

      if(isset($_POST['action']))
      {
    
      $this->form_validation->set_rules('email','Email','trim|required');
      $this->form_validation->set_rules('c_email','Confirm Email','trim|required|matches[email]');

      if($this->form_validation->run()=== FALSE)
      {   $data['message']="";
          $this->load->view('templates/header');
          $this->load->view('pages/recovery_page',$data);
          $this->load->view('templates/footer');
      }
      else
      {          

              


        $email = $this->input->post('email');
                 $result = $this->news_model->check_recovery_email($email);
                 if($result)
                 {
                       $this->news_model->send_recovery_email($email);

                 }
                 else
                 {
                    $data['message']="The email does not exist";
                    $this->load->view('templates/header');
                    $this->load->view('pages/recovery_page',$data);
                    $this->load->view('templates/footer');

                 }




      }
    }
    else
    {
                    $data['message']="";
                    $this->load->view('templates/header');
                    $this->load->view('pages/recovery_page',$data);
                    $this->load->view('templates/footer');

    }

}
public function email_error()
{


  $this->load->view('templates/header');
  $this->load->view('pages/email_error');
  $this->load->view('templates/footer');

}

}