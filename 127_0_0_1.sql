-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 17, 2015 at 10:27 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mydb2`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrator`
--

CREATE TABLE IF NOT EXISTS `administrator` (
  `user` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `role` text NOT NULL,
  PRIMARY KEY (`user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `administrator`
--

INSERT INTO `administrator` (`user`, `password`, `role`) VALUES
('admin', '21232f297a57a5a743894a0e4a801fc3', 'administrator');

-- --------------------------------------------------------

--
-- Table structure for table `applicants`
--

CREATE TABLE IF NOT EXISTS `applicants` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `id` int(50) NOT NULL,
  `user` varchar(50) NOT NULL,
  `name` text NOT NULL,
  `u_name` text NOT NULL,
  `qualifications` text NOT NULL,
  `industry` text NOT NULL,
  `gender` text NOT NULL,
  `date_o_b` date NOT NULL,
  `nationality` text NOT NULL,
  `phone` text NOT NULL,
  `email` text NOT NULL,
  `address` text NOT NULL,
  `township` text NOT NULL,
  `city` text NOT NULL,
  `country` text NOT NULL,
  `file` text NOT NULL,
  `user_c` text NOT NULL,
  `date_applied` date NOT NULL,
  `file_type` text NOT NULL,
  `file_name` text NOT NULL,
  `file_size` int(11) NOT NULL,
  `job_valid_to` date NOT NULL,
  `status` text NOT NULL,
  `expired_status` text NOT NULL,
  `job_delete_status` text NOT NULL,
  PRIMARY KEY (`record_id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `applicants`
--

INSERT INTO `applicants` (`record_id`, `job_id`, `id`, `user`, `name`, `u_name`, `qualifications`, `industry`, `gender`, `date_o_b`, `nationality`, `phone`, `email`, `address`, `township`, `city`, `country`, `file`, `user_c`, `date_applied`, `file_type`, `file_name`, `file_size`, `job_valid_to`, `status`, `expired_status`, `job_delete_status`) VALUES
(1, 1, 0, 'kevin', 'hi', 'university of yangon', '', '', '0', '0000-00-00', '', 'hi', 'hi', '', '', '', '', 'http://localhost:81/ci1/files/job5.docx', 'company', '2015-09-12', '0', 'job5.docx', 0, '2015-10-10', '', 'false', 'false'),
(2, 5, 2222, 'student', 'student@', 'university of yangon', '', '', '0', '0000-00-00', '', '526877', 'student@gmail.com', '', '', '', '', 'http://localhost:81/ci1/files/', 'company', '2015-10-17', '0', '', 0, '2015-11-24', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `board`
--

CREATE TABLE IF NOT EXISTS `board` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(225) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

--
-- Dumping data for table `board`
--

INSERT INTO `board` (`id`, `name`, `message`) VALUES
(2, 'practice', 'practice'),
(3, 'kevin', 'message2'),
(4, 'Kevin', 'messagehere4'),
(6, 'Kevin', 'message10'),
(7, 'Kevin', 'message14'),
(9, 'Kevin', 'message14'),
(10, 'hi', 'here2'),
(11, 'hi', 'here4'),
(12, 'hi', 'here5'),
(13, 'hi', 'here14'),
(14, 'practice', 'practice'),
(15, 'hi', 'here16'),
(16, 'Kevin', 'message5'),
(17, 'Kevin', 'message5'),
(18, '', ''),
(19, 'hi', 'here16'),
(20, 'Kevin', 'message5'),
(21, 'Kevin', 'editmessage5'),
(22, 'Kevin', 'message5'),
(38, 'hi', 'hereEdited'),
(42, 'hi', 'here42');

-- --------------------------------------------------------

--
-- Table structure for table `caccounts`
--

CREATE TABLE IF NOT EXISTS `caccounts` (
  `username` varchar(40) NOT NULL,
  `name` text NOT NULL,
  `password` varchar(32) NOT NULL,
  `c_name` text NOT NULL,
  `phone` text NOT NULL,
  `email` varchar(80) NOT NULL,
  `role` text NOT NULL,
  `country` text NOT NULL,
  `address` text NOT NULL,
  `township` text NOT NULL,
  `city` text NOT NULL,
  `type` text NOT NULL,
  `registered_date` date NOT NULL,
  `status` text NOT NULL,
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `caccounts`
--

INSERT INTO `caccounts` (`username`, `name`, `password`, `c_name`, `phone`, `email`, `role`, `country`, `address`, `township`, `city`, `type`, `registered_date`, `status`) VALUES
('company', 'Dell', '5f4dcc3b5aa765d61d8327deb882cf99', 'Dell', 'hi', 'hi', '0', 'hi', 'hi', 'hi', 'hi', 'hi', '2015-09-12', 'Approved');

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE IF NOT EXISTS `chat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `message` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`id`, `name`, `message`) VALUES
(1, 'Kevin', 'hi'),
(2, 'Kevin2', 'hi'),
(3, 'Kevin4', ' hi');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `cities` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`cities`) VALUES
('Yangon'),
('Mandalay'),
('Naypyidaw'),
('Mawlamyaing'),
('Bago'),
('Pathein'),
('Monywa'),
('Meiktila'),
('Sittwe'),
('Mergui'),
('Taunggyi');

-- --------------------------------------------------------

--
-- Table structure for table `companies_jobs`
--

CREATE TABLE IF NOT EXISTS `companies_jobs` (
  `id` int(11) NOT NULL,
  `JobC` varchar(50) NOT NULL,
  `JobT` varchar(50) NOT NULL,
  `JobS` int(11) NOT NULL,
  `JobD` text NOT NULL,
  `phone` text NOT NULL,
  `email` text NOT NULL,
  `user_c` varchar(100) NOT NULL,
  `password` varchar(40) NOT NULL,
  `responsibilities` text NOT NULL,
  `type` text NOT NULL,
  `requirements` text NOT NULL,
  `address` text NOT NULL,
  `township` text NOT NULL,
  `city` text NOT NULL,
  `country` text NOT NULL,
  `valid_from` date NOT NULL,
  `valid_to` date NOT NULL,
  `published_date` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `job_categories`
--

CREATE TABLE IF NOT EXISTS `job_categories` (
  `category` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_categories`
--

INSERT INTO `job_categories` (`category`) VALUES
('Accounting'),
('Arts'),
('Administrative'),
('Advertising'),
('Automotive'),
('Banking'),
('Biotech'),
('College'),
('Construction'),
('Consultant'),
('Contractual'),
('Design'),
('Education'),
('Engineering'),
('Entertainment'),
('Entry Level'),
('Executives'),
('Facilities'),
('Finance'),
('Franchise'),
('Federal'),
('General Labor'),
('Government'),
('Grocery'),
('Healthcare'),
('Hospitality'),
('Human Resources'),
('Information Technology'),
('Insurance'),
('Inventory'),
('Internet'),
('Journalism'),
('Law Enforcement'),
('Legal'),
('Management Consulting'),
('Manufacturing'),
('Marketing'),
('Media'),
('Non-profit'),
('Nurse'),
('Operations'),
('Pharmaceutical'),
('Publishing'),
('Purchasing Procurement'),
('QA-Quality Control'),
('Real Estate'),
('Research'),
('Restaurant'),
('Retail'),
('Sales'),
('Science'),
('Skilled Labor Trades'),
('Strategy Planning'),
('Supply Chaing'),
('Telecommunications'),
('Training'),
('Transportation'),
('Veterinary'),
('Warehouse'),
('university'),
('phone'),
('Accounting'),
('Accounting'),
('Accounting'),
('accounting'),
('accounting'),
('Accounting'),
('Accounting'),
('Accounting'),
('hi'),
('hi'),
('here'),
('Hi'),
('other'),
('house'),
('sports'),
('sports');

-- --------------------------------------------------------

--
-- Table structure for table `job_types`
--

CREATE TABLE IF NOT EXISTS `job_types` (
  `type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_types`
--

INSERT INTO `job_types` (`type`) VALUES
('Full Time'),
('Part Time'),
('Contract/Temp'),
('Freelance'),
('Internship'),
('Temporary'),
('company2'),
('house');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE IF NOT EXISTS `jobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `c_name` text NOT NULL,
  `c_background` text NOT NULL,
  `JobC` varchar(50) NOT NULL,
  `JobT` varchar(50) NOT NULL,
  `JobS` int(32) NOT NULL,
  `JobD` text NOT NULL,
  `phone` text NOT NULL,
  `email` text NOT NULL,
  `user_c` varchar(100) NOT NULL,
  `password` varchar(40) NOT NULL,
  `responsibilities` text NOT NULL,
  `type` text NOT NULL,
  `requirements` text NOT NULL,
  `address` text NOT NULL,
  `township` text NOT NULL,
  `city` text NOT NULL,
  `country` text NOT NULL,
  `valid_from` date NOT NULL,
  `valid_to` date NOT NULL,
  `published_date` date NOT NULL,
  `expired_status` text NOT NULL,
  `status2` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `c_name`, `c_background`, `JobC`, `JobT`, `JobS`, `JobD`, `phone`, `email`, `user_c`, `password`, `responsibilities`, `type`, `requirements`, `address`, `township`, `city`, `country`, `valid_from`, `valid_to`, `published_date`, `expired_status`, `status2`) VALUES
(1, 'Dell', '', 'Accounting', 'hi', 800, 'hi', 'hi', 'hi', 'company', '', 'hi', 'Full Time', 'hirrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr', 'hi', 'hi', 'Bago', 'hi', '2014-09-08', '2015-10-28', '2015-09-12', '', 'false'),
(2, 'dell', '', 'Accounting', 'hi', 0, 'hi', 'hi', 'hi', 'company', '', 'hi', 'house', 'hi', 'hi', 'hi', 'Bago', 'myanmar', '2015-08-08', '2015-10-10', '2015-09-23', 'false', 'false'),
(3, 'microsoft', '', 'sports', 'here', 1400, 'iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii\r\n\r\niiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii\r\n\r\niiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii', 'hi', 'here', 'company', '', 'here', 'Full Time', 'here', 'here', 'here', 'Naypyidaw', 'myanmar', '2014-08-08', '2015-10-28', '2015-09-23', '', ''),
(4, 'yahoo', '', 'sports', 'hi', 0, 'hi', 'hi', 'hi', 'company', '', 'hi', 'house', 'hi', 'hi', 'hi', 'Bago', 'myanmar', '2015-08-08', '2015-10-10', '2015-09-23', 'false', ''),
(5, 'Yahoo', '', 'Accounting', 'marketing', 100000, 'hi', 'hi', 'hi', 'company', '', 'hi', 'house', 'hi', 'hi', 'hi', 'Bago', 'hi', '1015-10-24', '2015-11-24', '2015-10-14', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `jobs_archive`
--

CREATE TABLE IF NOT EXISTS `jobs_archive` (
  `id` int(11) NOT NULL,
  `JobC` varchar(50) NOT NULL,
  `JobT` varchar(50) NOT NULL,
  `JobS` int(11) NOT NULL,
  `JobD` text NOT NULL,
  `phone` text NOT NULL,
  `email` text NOT NULL,
  `user_c` varchar(100) NOT NULL,
  `password` varchar(40) NOT NULL,
  `responsibilities` text NOT NULL,
  `type` text NOT NULL,
  `requirements` text NOT NULL,
  `address` text NOT NULL,
  `township` text NOT NULL,
  `city` text NOT NULL,
  `country` text NOT NULL,
  `valid_from` date NOT NULL,
  `valid_to` date NOT NULL,
  `published_date` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jobs_archive`
--

INSERT INTO `jobs_archive` (`id`, `JobC`, `JobT`, `JobS`, `JobD`, `phone`, `email`, `user_c`, `password`, `responsibilities`, `type`, `requirements`, `address`, `township`, `city`, `country`, `valid_from`, `valid_to`, `published_date`) VALUES
(1, '0', 'hi', 800, 'hi', 'hi', 'hi', 'company', '', 'hi', 'company2', 'hi', 'hi', 'hi', 'Bago', 'hi', '2015-08-28', '2015-10-10', '2015-08-28'),
(2, 'IT', 'Yahoo', 800, 'manager', '0', '0', 'company2', '', '0', '', '', '', '', '', '', '0000-00-00', '0000-00-00', '0000-00-00'),
(4, 'Marketing', 'manager', 800, 'Google', '0', '', 'company2', '', '', '', '', '', '', '', '', '0000-00-00', '0000-00-00', '0000-00-00'),
(5, 'it', 'it', 800, 'it', '0', '', 'dell', '', '', '', '', '', '', '', '', '0000-00-00', '0000-00-00', '0000-00-00'),
(35, 'Accounting', 'hi', 0, 'hi', 'hi', 'hi', 'company', '', 'hi', 'Contract/Temp', 'hi', 'hi', 'hi', 'Bago', 'hi', '2015-08-17', '0000-00-00', '2015-08-17'),
(36, '0', 'hi', 800, 'hi', 'hi', 'hi', 'company4', '', 'hi', 'Full Time', 'hi', 'hi', 'hi', 'Bago', 'hi', '2015-05-05', '2015-06-06', '2015-08-22'),
(37, '0', 'hi', 800, 'hi', 'hi', 'hi', 'company4', '', 'hi', 'company2', 'hi', 'hi', 'hi', 'Bago', 'hi', '2015-05-05', '2015-06-06', '2015-08-22'),
(38, '0', 'here', 800, 'here', 'here', 'here', 'company4', '', 'here', 'company2', 'here', 'here', 'here', 'Bago', 'here', '2015-05-05', '2015-06-06', '2015-08-22'),
(39, '0', 'hi', 800, 'hi', 'hi', 'hi', 'company', '', 'hi', 'company2', 'hi', 'hi', 'hi', 'Bago', 'hi', '2015-05-05', '2015-10-10', '2015-08-25');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `slug` varchar(128) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `slug` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `slug`, `text`) VALUES
(1, 'Phyo Min', 'Phyo Min', 'hi'),
(2, 'Kevin', 'Kevin', 'hi'),
(3, 'Kevin2', 'Kevin2', 'hi'),
(4, 'Kevin4', 'Kevin4', 'hi'),
(5, 'Kevin8', 'Kevin8', 'hi'),
(6, 'codeigniter', 'codeigniter', 'hi'),
(7, 'codeigniter2', 'codeigniter2', 'hi'),
(8, 'car', 'car', 'hi'),
(9, 'cars2', 'cars2', 'hi'),
(10, 'new1', 'new1', 'new1'),
(11, 'item', 'item', 'item'),
(12, 'product', 'product', 'product'),
(13, '0', '', '0');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `No` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Name` text NOT NULL,
  `Category` text NOT NULL,
  `Description` text NOT NULL,
  `Price` float NOT NULL,
  PRIMARY KEY (`No`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`No`, `Name`, `Category`, `Description`, `Price`) VALUES
(1, 'Sony Memory Stick', 'Memory card', 'The largest capacity of Memory Stick', 12500),
(2, 'IBM PC/AT 1490', 'Computer', 'Now classic granddaddy of PC', 49690),
(3, 'Canon EOS 5D', 'Camera', 'Affordable 35mm full-size digital SLR', 122000),
(4, 'Inspiron 15 7000 Series', 'Computer', 'Touch Display', 1000),
(5, 'Inspiron 16 7000 Series', 'Computer', 'Touch Display', 1000),
(6, 'Samsung galaxy s5', 'Mobile Phone', 'many applications', 500),
(7, 'Samsung galaxy s5', 'mobile phone', 'many applications', 500),
(8, 'Inspiron 15 7000 Series', 'Computer', 'Touch Display', 1000),
(9, 'Inspiron 16 7000 Series', 'Computer', 'Touch Display', 0),
(10, 'Samsung galaxy s5', 'Mobile phone', 'many applications', 500),
(11, 'Samsung galaxy s5', 'Mobile phone', 'many applications', 500),
(12, 'Panasonic speaker', 'Speakers', 'Wireless', 200),
(13, 'Panasonic speakers', 'Speakers', 'wireless', 200),
(14, 'Samsung galaxy s5', 'Mobile phone', 'many applications', 500),
(15, 'Samsung galaxy s5', 'Mobile phone', 'many applications', 500),
(16, 'Samsung galaxy s5', 'Mobile phone', 'many applications', 500),
(17, 'Samsung galaxy s5', 'Mobile phone', 'many applicationis', 500),
(18, 'Samsung galaxy s5', 'Mobile phone', 'many applications', 500),
(19, 'Samsung galaxy s5', 'Mobile phone', 'many applications', 500),
(20, 'Samsung galaxy s5', 'Mobile phone', 'many applications', 500),
(21, 'Samsung galaxy s5', 'Mobile phone', 'many applications', 500);

-- --------------------------------------------------------

--
-- Table structure for table `registration`
--

CREATE TABLE IF NOT EXISTS `registration` (
  `id` int(11) NOT NULL,
  `password` text NOT NULL,
  `email` text NOT NULL,
  `last_login` date NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registration`
--

INSERT INTO `registration` (`id`, `password`, `email`, `last_login`, `name`) VALUES
(1, 'password', 'email', '2015-09-24', 'name'),
(2, 'password', 'email2', '2015-09-24', 'name2'),
(3, 'password', 'email3', '2015-09-24', 'name3'),
(4, 'password', 'email4', '2015-09-24', 'name4'),
(5, 'password', 'email5', '2015-09-24', 'name5'),
(6, 'password', 'email6', '2015-09-24', 'name6');

-- --------------------------------------------------------

--
-- Table structure for table `save_jobs`
--

CREATE TABLE IF NOT EXISTS `save_jobs` (
  `user` text NOT NULL,
  `job_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `save_jobs`
--

INSERT INTO `save_jobs` (`user`, `job_id`) VALUES
('student', 5),
('student', 5);

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE IF NOT EXISTS `students` (
  `user` varchar(50) NOT NULL,
  `id` int(11) NOT NULL,
  `nrc` text NOT NULL,
  `password` varchar(32) NOT NULL,
  `name` varchar(80) NOT NULL,
  `u_name` text NOT NULL,
  `qualifications` text NOT NULL,
  `industry` text NOT NULL,
  `experience` text NOT NULL,
  `gender` varchar(40) NOT NULL,
  `date_o_b` date NOT NULL,
  `nationality` text NOT NULL,
  `phone` text NOT NULL,
  `email` text NOT NULL,
  `address` text NOT NULL,
  `township` text NOT NULL,
  `city` text NOT NULL,
  `country` text NOT NULL,
  `registered_date` date NOT NULL,
  `role` text NOT NULL,
  `status` text NOT NULL,
  PRIMARY KEY (`user`),
  UNIQUE KEY `user` (`user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`user`, `id`, `nrc`, `password`, `name`, `u_name`, `qualifications`, `industry`, `experience`, `gender`, `date_o_b`, `nationality`, `phone`, `email`, `address`, `township`, `city`, `country`, `registered_date`, `role`, `status`) VALUES
('hi', 0, 'hi', '5f4dcc3b5aa765d61d8327deb882cf99', 'hi', 'hi', 'hi', 'hi', 'hi', 'female', '1998-04-12', 'hi', 'hi', 'hi', 'hi', 'hi', 'hi', 'hi', '2015-09-12', 'student', 'Unchecked'),
('kevin', 0, 'hi', '5f4dcc3b5aa765d61d8327deb882cf99', 'hi', 'university of yangon', 'hi', 'hi', 'hi', 'female', '1998-08-14', 'hi', 'hi', 'hi', 'hi', 'hi', 'hi', 'hi', '2015-09-12', 'student', 'Approved'),
('student', 2222, '2222', '5f4dcc3b5aa765d61d8327deb882cf99', 'student@', 'university of yangon', 'hi', 'hi', 'hi', 'Female', '1994-08-14', 'myanmar', '526877', 'student@gmail.com', 'hi', 'hi', 'hi', 'hi', '2015-10-08', 'student', 'Approved'),
('student2', 2222, '2222', '5f4dcc3b5aa765d61d8327deb882cf99', 'student2', 'u_yangon', 'hi', 'hi', '', 'female', '2015-10-05', 'myanmar', '22222222', 'student2@gmail.com', 'hi', 'hi', 'hi', 'hi', '2015-10-09', 'student', 'Unchecked'),
('student4', 2222, '2222', '5f4dcc3b5aa765d61d8327deb882cf99', 'student4', 'University Of Yangon', 'hi', 'hi', '', 'female', '1992-05-10', 'myanmar', '2222222', 'student4@gmail.com', 'hi', 'hi', 'hi', 'hi', '2015-10-09', 'student', 'Approved');

-- --------------------------------------------------------

--
-- Table structure for table `students_jobs`
--

CREATE TABLE IF NOT EXISTS `students_jobs` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(50) NOT NULL,
  `job_id` int(11) NOT NULL,
  `c_name` text NOT NULL,
  `file` text NOT NULL,
  `category` text NOT NULL,
  `title` text NOT NULL,
  `salary` int(11) NOT NULL,
  `description` text NOT NULL,
  `responsibilities` text NOT NULL,
  `requirements` text NOT NULL,
  `type` text NOT NULL,
  `phone` text NOT NULL,
  `email` text NOT NULL,
  `address` text NOT NULL,
  `township` text NOT NULL,
  `city` text NOT NULL,
  `country` text NOT NULL,
  `valid_from` date NOT NULL,
  `valid_to` date NOT NULL,
  `published_date` date NOT NULL,
  `applied_date` date NOT NULL,
  `file_path` text NOT NULL,
  `file_name` text NOT NULL,
  `status` text NOT NULL,
  PRIMARY KEY (`record_id`),
  UNIQUE KEY `record_id` (`record_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `students_jobs`
--

INSERT INTO `students_jobs` (`record_id`, `user`, `job_id`, `c_name`, `file`, `category`, `title`, `salary`, `description`, `responsibilities`, `requirements`, `type`, `phone`, `email`, `address`, `township`, `city`, `country`, `valid_from`, `valid_to`, `published_date`, `applied_date`, `file_path`, `file_name`, `status`) VALUES
(1, 'kevin', 1, 'Dell', '', 'Accounting', 'hi', 800, 'hi', 'hi', '0', 'Full Time', 'hi', 'hi', 'hi', 'hi', 'Bago', 'hi', '2014-09-08', '2015-10-10', '2015-09-12', '2015-09-12', 'http://localhost:81/ci1/files/job5.docx', 'job5.docx', ''),
(2, 'student', 5, 'Yahoo', '', 'Accounting', 'marketing', 100000, 'hi', 'hi', '0', 'house', 'hi', 'hi', 'hi', 'hi', 'Bago', 'hi', '1015-10-24', '2015-11-24', '2015-10-14', '2015-10-17', 'http://localhost:81/ci1/files/', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `university`
--

CREATE TABLE IF NOT EXISTS `university` (
  `user` varchar(40) NOT NULL COMMENT 'hi',
  `password` varchar(32) NOT NULL,
  `name` varchar(40) NOT NULL,
  `u_name` text NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` text NOT NULL,
  `address` text NOT NULL,
  `township` text NOT NULL,
  `city` text NOT NULL,
  `country` text NOT NULL,
  `role` text NOT NULL,
  `status` text NOT NULL,
  `registered_date` date NOT NULL,
  UNIQUE KEY `user` (`user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `university`
--

INSERT INTO `university` (`user`, `password`, `name`, `u_name`, `email`, `phone`, `address`, `township`, `city`, `country`, `role`, `status`, `registered_date`) VALUES
('University', '5f4dcc3b5aa765d61d8327deb882cf99', 'hi', 'University Of Yangon', 'hi', '55,11', 'hi', 'hi', 'hi', 'hi', 'university', 'Approved', '2015-09-12');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `users` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`users`, `password`, `reg_date`) VALUES
('George', '578ad8e10dc4edb52ff2bd4ec9bc93a3', '2015-04-28 16:49:06'),
('George', '578ad8e10dc4edb52ff2bd4ec9bc93a3', '2015-04-28 16:49:16'),
('Kevin', 'f1cd318e412b5f7226e5f377a9544ff7', '2015-04-28 16:50:41'),
('Kevin', 'f1cd318e412b5f7226e5f377a9544ff7', '2015-04-28 16:50:56'),
('Kevin', 'Kevin', '2015-04-29 15:50:02');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
