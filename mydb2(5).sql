-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 25, 2016 at 08:41 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mydb2`
--

-- --------------------------------------------------------

--
-- Table structure for table `abused_reports`
--

CREATE TABLE IF NOT EXISTS `abused_reports` (
  `user` text NOT NULL,
  `job_id` text NOT NULL,
  `number` text NOT NULL,
  `user_c` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `abused_reports`
--

INSERT INTO `abused_reports` (`user`, `job_id`, `number`, `user_c`) VALUES
('dagon', 'J1', 'true', 'company'),
('dagon', 'J3', 'true', 'company');

-- --------------------------------------------------------

--
-- Table structure for table `administrator`
--

CREATE TABLE IF NOT EXISTS `administrator` (
  `user` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `u_name` text NOT NULL,
  `email` text NOT NULL,
  `random_number` int(32) NOT NULL,
  `random_number_date` date NOT NULL,
  `role` text NOT NULL,
  PRIMARY KEY (`user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `administrator`
--

INSERT INTO `administrator` (`user`, `password`, `u_name`, `email`, `random_number`, `random_number_date`, `role`) VALUES
('admin', '21232f297a57a5a743894a0e4a801fc3', 'University Of Yangon', 'paungster.com', 0, '0000-00-00', 'administrator');

-- --------------------------------------------------------

--
-- Table structure for table `applicants`
--

CREATE TABLE IF NOT EXISTS `applicants` (
  `applicant_id` varchar(32) NOT NULL,
  `job_id` varchar(32) NOT NULL,
  `id` int(50) NOT NULL,
  `user` varchar(50) NOT NULL,
  `user_c` text NOT NULL,
  `name` text NOT NULL,
  `u_name` text NOT NULL,
  `qualifications` text NOT NULL,
  `industry` text NOT NULL,
  `gender` text NOT NULL,
  `date_o_b` date NOT NULL,
  `nationality` text NOT NULL,
  `phone` text NOT NULL,
  `email` text NOT NULL,
  `address` text NOT NULL,
  `township` text NOT NULL,
  `city` text NOT NULL,
  `country` text NOT NULL,
  `date_applied` date NOT NULL,
  `file_name` text NOT NULL,
  `file` text NOT NULL,
  `file_orig_name` text NOT NULL,
  `photo` text NOT NULL,
  `job_valid_to` date NOT NULL,
  `status` text NOT NULL,
  `expired` text NOT NULL,
  `delete` text NOT NULL,
  PRIMARY KEY (`applicant_id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `applicants`
--

INSERT INTO `applicants` (`applicant_id`, `job_id`, `id`, `user`, `user_c`, `name`, `u_name`, `qualifications`, `industry`, `gender`, `date_o_b`, `nationality`, `phone`, `email`, `address`, `township`, `city`, `country`, `date_applied`, `file_name`, `file`, `file_orig_name`, `photo`, `job_valid_to`, `status`, `expired`, `delete`) VALUES
('A1', 'J1', 2, 'student ', 'company', 'student', 'University Of Yangon', 'bachelor', 'science', 'Female', '1987-04-14', 'myanmar', '2', '2', '', '', '', '', '2016-03-04', 'job13.doc', 'http://localhost:81/ci1/files/job13.doc', 'job.doc', '', '2016-04-14', '', '', ''),
('A2', 'J2', 2, 'student ', 'company', 'student', 'University Of Yangon', 'bachelor', 'science', 'Female', '1987-04-14', 'myanmar', '2', '2', '', '', '', '', '2016-03-04', 'job15.doc', 'http://localhost:81/ci1/files/job15.doc', 'job.doc', '', '2016-04-14', '', '', ''),
('A3', 'J3', 2, 'student ', 'company', 'student', 'University Of Yangon', 'bachelor', 'science', 'Female', '1987-04-14', 'myanmar', '2', '2', '', '', '', '', '2016-03-04', 'job16.doc', 'http://localhost:81/ci1/files/job16.doc', 'job.doc', '', '2016-04-14', '', '', ''),
('A4', 'J4', 2, 'student ', 'company', 'student', 'University Of Yangon', 'bachelor', 'science', 'Female', '1987-04-14', 'myanmar', '2', '2', '', '', '', '', '2016-03-04', 'job_-_Copy.doc', 'http://localhost:81/ci1/files/job_-_Copy.doc', 'job_-_Copy.doc', '', '2016-04-14', '', '', ''),
('A5', 'J1', 2, 'student', 'company', 'student', 'University Of Yangon', 'bachelor', 'science', 'Female', '1987-04-14', 'myanmar', '2', '2', '2', 'bahan', 'yangon', 'myanmar', '2016-03-25', 'job20.doc', 'http://localhost:81/ci1/files/job20.doc', 'job.doc', '', '2016-04-14', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `board`
--

CREATE TABLE IF NOT EXISTS `board` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(225) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

--
-- Dumping data for table `board`
--

INSERT INTO `board` (`id`, `name`, `message`) VALUES
(2, 'practice', 'practice'),
(3, 'kevin', 'message2'),
(4, 'Kevin', 'messagehere4'),
(6, 'Kevin', 'message10'),
(7, 'Kevin', 'message14'),
(9, 'Kevin', 'message14'),
(10, 'hi', 'here2'),
(11, 'hi', 'here4'),
(12, 'hi', 'here5'),
(13, 'hi', 'here14'),
(14, 'practice', 'practice'),
(15, 'hi', 'here16'),
(16, 'Kevin', 'message5'),
(17, 'Kevin', 'message5'),
(18, '', ''),
(19, 'hi', 'here16'),
(20, 'Kevin', 'message5'),
(21, 'Kevin', 'editmessage5'),
(22, 'Kevin', 'message5'),
(38, 'hi', 'hereEdited'),
(42, 'hi', 'here42');

-- --------------------------------------------------------

--
-- Table structure for table `caccounts`
--

CREATE TABLE IF NOT EXISTS `caccounts` (
  `username` varchar(40) NOT NULL,
  `name` text NOT NULL,
  `password` varchar(32) NOT NULL,
  `c_name` text NOT NULL,
  `c_background` text NOT NULL,
  `b_type` text NOT NULL,
  `phone` text NOT NULL,
  `email` varchar(80) NOT NULL,
  `website` text NOT NULL,
  `country` text NOT NULL,
  `address` text NOT NULL,
  `township` text NOT NULL,
  `city` text NOT NULL,
  `registered_date` date NOT NULL,
  `role` text NOT NULL,
  `random_number` int(32) NOT NULL,
  `random_number_date` date NOT NULL,
  `status` text NOT NULL,
  `photo` text NOT NULL,
  `photo_server_path` text NOT NULL,
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `caccounts`
--

INSERT INTO `caccounts` (`username`, `name`, `password`, `c_name`, `c_background`, `b_type`, `phone`, `email`, `website`, `country`, `address`, `township`, `city`, `registered_date`, `role`, `random_number`, `random_number_date`, `status`, `photo`, `photo_server_path`) VALUES
('company', 'company', '5f4dcc3b5aa765d61d8327deb882cf99', 'Dell', 'create computers', 'science', '2', '2222', '2', 'myanmar', '14', 'bahan', 'yangon', '2016-03-04', 'company', 0, '0000-00-00', 'Approved', 'http://localhost:81/ci1/images/company_photo/students-teenagers-laptop-computers.jpg', ''),
('company2', 'company2', '5f4dcc3b5aa765d61d8327deb882cf99', 'telenor', 'telecommunications', 'in', '2', '4', 'telenor.com', 'myanmar', '24', 'bahan', 'yangon', '2016-03-04', 'company', 0, '0000-00-00', 'Approved', 'http://localhost:81/ci1/images/company_photo/s.jpg', ''),
('company4', 'company4', '5f4dcc3b5aa765d61d8327deb882cf99', 'nokia', 'phone', 'telecommnications', '2', '14', '4', 'myanmar', '18', 'bahan', 'yangon', '2016-03-04', 'company', 0, '0000-00-00', 'Approved', 'http://localhost:81/ci1/images/company_photo/Nokia_500_76.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE IF NOT EXISTS `chat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `message` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`id`, `name`, `message`) VALUES
(1, 'Kevin', 'hi'),
(2, 'Kevin2', 'hi'),
(3, 'Kevin4', ' hi');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `cities` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`cities`) VALUES
('Yangon'),
('Mandalay'),
('Naypyidaw'),
('Mawlamyaing'),
('Bago'),
('Pathein'),
('Monywa'),
('Meiktila'),
('Sittwe'),
('Mergui'),
('Taunggyi'),
('Lashio');

-- --------------------------------------------------------

--
-- Table structure for table `companies_jobs`
--

CREATE TABLE IF NOT EXISTS `companies_jobs` (
  `id` int(11) NOT NULL,
  `c_name` text NOT NULL,
  `JobC` varchar(50) NOT NULL,
  `JobT` varchar(50) NOT NULL,
  `JobS` int(11) NOT NULL,
  `JobD` text NOT NULL,
  `phone` text NOT NULL,
  `email` text NOT NULL,
  `user_c` varchar(100) NOT NULL,
  `password` varchar(40) NOT NULL,
  `responsibilities` text NOT NULL,
  `type` text NOT NULL,
  `requirements` text NOT NULL,
  `address` text NOT NULL,
  `township` text NOT NULL,
  `city` text NOT NULL,
  `country` text NOT NULL,
  `valid_from` date NOT NULL,
  `valid_to` date NOT NULL,
  `published_date` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE IF NOT EXISTS `jobs` (
  `id` varchar(32) NOT NULL,
  `c_name` text NOT NULL,
  `c_background` text NOT NULL,
  `JobC` text NOT NULL,
  `JobT` text NOT NULL,
  `JobS` text NOT NULL,
  `JobD` text NOT NULL,
  `phone` text NOT NULL,
  `email` text NOT NULL,
  `website` text NOT NULL,
  `user_c` varchar(100) NOT NULL,
  `password` varchar(40) NOT NULL,
  `responsibilities` text NOT NULL,
  `type` text NOT NULL,
  `requirements` text NOT NULL,
  `address` text NOT NULL,
  `township` text NOT NULL,
  `city` text NOT NULL,
  `country` text NOT NULL,
  `valid_from` date NOT NULL,
  `valid_to` date NOT NULL,
  `published_date` date NOT NULL,
  `last_updated` date NOT NULL,
  `expired` text NOT NULL,
  `delete` text NOT NULL,
  `photo` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `c_name`, `c_background`, `JobC`, `JobT`, `JobS`, `JobD`, `phone`, `email`, `website`, `user_c`, `password`, `responsibilities`, `type`, `requirements`, `address`, `township`, `city`, `country`, `valid_from`, `valid_to`, `published_date`, `last_updated`, `expired`, `delete`, `photo`) VALUES
('J1', 'Dell', 'Something', 'Accounting', 'manager', '4(lakhs)-7.99999(lakhs)', 'something', '2345', '44444', 'dell.com', 'company', '', 'something', 'Contract/Temp', 'something and something', '42', 'kamayut', 'Yangon', 'myanmar', '2015-04-14', '2016-04-14', '2016-03-04', '2016-03-04', '', '', 'http://localhost:81/ci1/images/company_photo/students-teenagers-laptop-computers.jpg'),
('J2', 'Dell', 'science', 'Accounting', 'manager', '4(lakhs)-7.99999(lakhs)', 'something', '2345', '5555', 'dell.com', 'company', '', 'something', 'Full Time', 'something and something', '18', 'bahan', 'Yangon', 'myanmar', '2015-04-14', '2016-04-14', '2016-03-04', '2016-03-04', '', '', 'http://localhost:81/ci1/images/company_photo/students-teenagers-laptop-computers.jpg'),
('J3', 'Dell', 'create computers', 'Accounting', 'manager', '4(lakhs)-7.99999(lakhs)', 'something', '2222', '2345', 'dell.com', 'company', '', 'something', 'Contract/Temp', 'something and something', '14', 'bahan', 'Yangon', 'myanmar', '2015-04-14', '2016-04-14', '2016-03-04', '2016-03-04', '', '', 'http://localhost:81/ci1/images/company_photo/students-teenagers-laptop-computers.jpg'),
('J4', 'Dell', 'something', 'Accounting', 'manager', '12(lakhs)-15.99999(lakhs)', 'something', '22224', '2345', 'dell.com', 'company', '', 'something', 'Contract/Temp', 'something and something', '14', 'bahan', 'Yangon', 'myanmar', '2015-04-14', '2016-04-14', '2016-03-04', '2016-03-04', '', '', 'http://localhost:81/ci1/images/company_photo/students-teenagers-laptop-computers.jpg'),
('J5', 'nokia', 'phone', 'Administrative', 'salesperson', '4(lakhs)-7.99999(lakhs)', 'phone', '2', '25', 'nokia.com', 'company2', '', 'phone', 'Contract/Temp', 'phone and phone', '4', 'kamayut', 'Yangon', 'myanmar', '2015-04-14', '2016-04-14', '2016-03-04', '2016-03-04', '', '', 'http://localhost:81/ci1/images/company_photo/s.jpg'),
('J6', 'nokia', 'phone', 'Arts', 'salesperson', '4(lakhs)-7.99999(lakhs)', 'phone', '2', 'nokia', 'nokiamyanmar', 'company2', '', 'phone and phone', 'Full Time', 'need to be highschool graduate', '42', 'kamayut', 'Yangon', 'myanmar', '2015-04-14', '2016-04-14', '2016-03-04', '2016-03-04', '', '', 'http://localhost:81/ci1/images/company_photo/s.jpg'),
('J7', 'Nokia', 'phone', 'Arts', 'salesperson', '4(lakhs)-7.99999(lakhs)', 'phone', '2', 'nokia', 'nokiamyanmar', 'company2', '', 'phone', 'Full Time', 'phone and phone', '47', 'kamayut', 'Yangon', 'myanmar', '2015-04-14', '2016-04-14', '2016-03-04', '2016-03-04', '', '', 'http://localhost:81/ci1/images/company_photo/s.jpg'),
('J8', 'Nokia', 'phone', 'Arts', 'salesperson', '4(lakhs)-7.99999(lakhs)', 'phone', '2', 'nokia', 'nokiamyanmar', 'company2', '', 'phone', 'Full Time', 'phone and phone', '54', 'kamayut', 'Yangon', 'myanmar', '2015-04-14', '2016-04-14', '2016-03-04', '2016-03-04', '', '', 'http://localhost:81/ci1/images/company_photo/s.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `jobs_archive`
--

CREATE TABLE IF NOT EXISTS `jobs_archive` (
  `id` int(11) NOT NULL,
  `JobC` varchar(50) NOT NULL,
  `JobT` varchar(50) NOT NULL,
  `JobS` int(11) NOT NULL,
  `JobD` text NOT NULL,
  `phone` text NOT NULL,
  `email` text NOT NULL,
  `user_c` varchar(100) NOT NULL,
  `password` varchar(40) NOT NULL,
  `responsibilities` text NOT NULL,
  `type` text NOT NULL,
  `requirements` text NOT NULL,
  `address` text NOT NULL,
  `township` text NOT NULL,
  `city` text NOT NULL,
  `country` text NOT NULL,
  `valid_from` date NOT NULL,
  `valid_to` date NOT NULL,
  `published_date` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jobs_archive`
--

INSERT INTO `jobs_archive` (`id`, `JobC`, `JobT`, `JobS`, `JobD`, `phone`, `email`, `user_c`, `password`, `responsibilities`, `type`, `requirements`, `address`, `township`, `city`, `country`, `valid_from`, `valid_to`, `published_date`) VALUES
(1, '0', 'hi', 800, 'hi', 'hi', 'hi', 'company', '', 'hi', 'company2', 'hi', 'hi', 'hi', 'Bago', 'hi', '2015-08-28', '2015-10-10', '2015-08-28'),
(2, 'IT', 'Yahoo', 800, 'manager', '0', '0', 'company2', '', '0', '', '', '', '', '', '', '0000-00-00', '0000-00-00', '0000-00-00'),
(4, 'Marketing', 'manager', 800, 'Google', '0', '', 'company2', '', '', '', '', '', '', '', '', '0000-00-00', '0000-00-00', '0000-00-00'),
(5, 'it', 'it', 800, 'it', '0', '', 'dell', '', '', '', '', '', '', '', '', '0000-00-00', '0000-00-00', '0000-00-00'),
(35, 'Accounting', 'hi', 0, 'hi', 'hi', 'hi', 'company', '', 'hi', 'Contract/Temp', 'hi', 'hi', 'hi', 'Bago', 'hi', '2015-08-17', '0000-00-00', '2015-08-17'),
(36, '0', 'hi', 800, 'hi', 'hi', 'hi', 'company4', '', 'hi', 'Full Time', 'hi', 'hi', 'hi', 'Bago', 'hi', '2015-05-05', '2015-06-06', '2015-08-22'),
(37, '0', 'hi', 800, 'hi', 'hi', 'hi', 'company4', '', 'hi', 'company2', 'hi', 'hi', 'hi', 'Bago', 'hi', '2015-05-05', '2015-06-06', '2015-08-22'),
(38, '0', 'here', 800, 'here', 'here', 'here', 'company4', '', 'here', 'company2', 'here', 'here', 'here', 'Bago', 'here', '2015-05-05', '2015-06-06', '2015-08-22'),
(39, '0', 'hi', 800, 'hi', 'hi', 'hi', 'company', '', 'hi', 'company2', 'hi', 'hi', 'hi', 'Bago', 'hi', '2015-05-05', '2015-10-10', '2015-08-25');

-- --------------------------------------------------------

--
-- Table structure for table `job_categories`
--

CREATE TABLE IF NOT EXISTS `job_categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category` text NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=98 ;

--
-- Dumping data for table `job_categories`
--

INSERT INTO `job_categories` (`category_id`, `category`) VALUES
(1, 'Accounting'),
(2, 'Arts'),
(3, 'Administrative'),
(4, 'Advertising'),
(5, 'Automotive'),
(6, 'Banking'),
(7, 'Biotech'),
(8, 'College2'),
(9, 'Construction'),
(10, 'Consultant'),
(11, 'Contractual'),
(12, 'Design'),
(13, 'Education'),
(14, 'Engineering'),
(15, 'Entertainment'),
(16, 'Entry Level'),
(17, 'Executives'),
(18, 'Facilities'),
(19, 'Finance'),
(20, 'Franchise'),
(21, 'Federal'),
(22, 'General Labor'),
(23, 'Government'),
(24, 'Grocery'),
(25, 'Healthcare'),
(26, 'Hospitality'),
(27, 'Human Resources'),
(28, 'Information Technology'),
(29, 'Insurance'),
(30, 'Inventory'),
(31, 'Internet'),
(32, 'Journalism'),
(33, 'Law Enforcement'),
(34, 'Legal'),
(35, 'Management Consulting'),
(36, 'Manufacturing'),
(37, 'Marketing'),
(38, 'Media'),
(39, 'Non-profit'),
(40, 'Nurse'),
(41, 'Operations'),
(42, 'Pharmaceutical'),
(43, 'Publishing'),
(44, 'Purchasing Procurement'),
(45, 'QA-Quality Control'),
(46, 'Real Estate'),
(47, 'Research'),
(48, 'Restaurant'),
(49, 'Retail'),
(50, 'Sales'),
(51, 'Science'),
(52, 'Skilled Labor Trades'),
(53, 'Strategy Planning'),
(54, 'Supply Chaing'),
(55, 'Telecommunications'),
(56, 'Training'),
(57, 'Transportation'),
(58, 'Veterinary'),
(59, 'Warehouse'),
(60, 'university'),
(61, 'phone'),
(72, 'here'),
(74, 'other'),
(75, 'house'),
(76, 'sports'),
(77, 'sports'),
(78, 'panaSONIC'),
(79, 'soNY'),
(82, 'iinininin'),
(83, 'herehere'),
(86, 'inininin'),
(87, 'inhereinhere'),
(96, 'herehere'),
(97, 'somethingin');

-- --------------------------------------------------------

--
-- Table structure for table `job_types`
--

CREATE TABLE IF NOT EXISTS `job_types` (
  `type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_types`
--

INSERT INTO `job_types` (`type`) VALUES
('Full Time'),
('Part Time'),
('Contract/Temp'),
('Freelance'),
('Internship'),
('Temporary'),
('company2'),
('house'),
('Sony');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `slug` varchar(128) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `slug` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `slug`, `text`) VALUES
(1, 'Phyo Min', 'Phyo Min', 'hi'),
(2, 'Kevin', 'Kevin', 'hi'),
(3, 'Kevin2', 'Kevin2', 'hi'),
(4, 'Kevin4', 'Kevin4', 'hi'),
(5, 'Kevin8', 'Kevin8', 'hi'),
(6, 'codeigniter', 'codeigniter', 'hi'),
(7, 'codeigniter2', 'codeigniter2', 'hi'),
(8, 'car', 'car', 'hi'),
(9, 'cars2', 'cars2', 'hi'),
(10, 'new1', 'new1', 'new1'),
(11, 'item', 'item', 'item'),
(12, 'product', 'product', 'product'),
(13, '0', '', '0');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `No` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Name` text NOT NULL,
  `Category` text NOT NULL,
  `Description` text NOT NULL,
  `Price` float NOT NULL,
  PRIMARY KEY (`No`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`No`, `Name`, `Category`, `Description`, `Price`) VALUES
(1, 'Sony Memory Stick', 'Memory card', 'The largest capacity of Memory Stick', 12500),
(2, 'IBM PC/AT 1490', 'Computer', 'Now classic granddaddy of PC', 49690),
(3, 'Canon EOS 5D', 'Camera', 'Affordable 35mm full-size digital SLR', 122000),
(4, 'Inspiron 15 7000 Series', 'Computer', 'Touch Display', 1000),
(5, 'Inspiron 16 7000 Series', 'Computer', 'Touch Display', 1000),
(6, 'Samsung galaxy s5', 'Mobile Phone', 'many applications', 500),
(7, 'Samsung galaxy s5', 'mobile phone', 'many applications', 500),
(8, 'Inspiron 15 7000 Series', 'Computer', 'Touch Display', 1000),
(9, 'Inspiron 16 7000 Series', 'Computer', 'Touch Display', 0),
(10, 'Samsung galaxy s5', 'Mobile phone', 'many applications', 500),
(11, 'Samsung galaxy s5', 'Mobile phone', 'many applications', 500),
(12, 'Panasonic speaker', 'Speakers', 'Wireless', 200),
(13, 'Panasonic speakers', 'Speakers', 'wireless', 200),
(14, 'Samsung galaxy s5', 'Mobile phone', 'many applications', 500),
(15, 'Samsung galaxy s5', 'Mobile phone', 'many applications', 500),
(16, 'Samsung galaxy s5', 'Mobile phone', 'many applications', 500),
(17, 'Samsung galaxy s5', 'Mobile phone', 'many applicationis', 500),
(18, 'Samsung galaxy s5', 'Mobile phone', 'many applications', 500),
(19, 'Samsung galaxy s5', 'Mobile phone', 'many applications', 500),
(20, 'Samsung galaxy s5', 'Mobile phone', 'many applications', 500),
(21, 'Samsung galaxy s5', 'Mobile phone', 'many applications', 500);

-- --------------------------------------------------------

--
-- Table structure for table `registration`
--

CREATE TABLE IF NOT EXISTS `registration` (
  `id` int(11) NOT NULL,
  `password` text NOT NULL,
  `email` text NOT NULL,
  `last_login` date NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registration`
--

INSERT INTO `registration` (`id`, `password`, `email`, `last_login`, `name`) VALUES
(1, 'password', 'email', '2015-09-24', 'name'),
(2, 'password', 'email2', '2015-09-24', 'name2'),
(3, 'password', 'email3', '2015-09-24', 'name3'),
(4, 'password', 'email4', '2015-09-24', 'name4'),
(5, 'password', 'email5', '2015-09-24', 'name5'),
(6, 'password', 'email6', '2015-09-24', 'name6');

-- --------------------------------------------------------

--
-- Table structure for table `save_jobs`
--

CREATE TABLE IF NOT EXISTS `save_jobs` (
  `save_job_id` int(11) NOT NULL AUTO_INCREMENT,
  `user` text NOT NULL,
  `job_id` varchar(32) NOT NULL,
  `saved_date` date NOT NULL,
  `delete` text NOT NULL,
  PRIMARY KEY (`save_job_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `save_jobs`
--

INSERT INTO `save_jobs` (`save_job_id`, `user`, `job_id`, `saved_date`, `delete`) VALUES
(1, 'student', 'J1', '2016-03-04', ''),
(2, 'student', 'J2', '2016-03-04', ''),
(3, 'student', 'J3', '2016-03-04', ''),
(4, 'student', 'J4', '2016-03-04', '');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE IF NOT EXISTS `students` (
  `user` varchar(50) NOT NULL,
  `id` varchar(24) NOT NULL,
  `nrc` text NOT NULL,
  `password` varchar(32) NOT NULL,
  `name` varchar(80) NOT NULL,
  `u_name` text NOT NULL,
  `qualifications` text NOT NULL,
  `industry` text NOT NULL,
  `experience` text NOT NULL,
  `gender` varchar(40) NOT NULL,
  `date_o_b` date NOT NULL,
  `nationality` text NOT NULL,
  `phone` text NOT NULL,
  `email` text NOT NULL,
  `address` text NOT NULL,
  `township` text NOT NULL,
  `city` text NOT NULL,
  `country` text NOT NULL,
  `registered_date` date NOT NULL,
  `role` text NOT NULL,
  `random_number` int(32) DEFAULT NULL,
  `random_number_date` date NOT NULL,
  `status` text NOT NULL,
  `photo` text NOT NULL,
  `photo_server_path` text NOT NULL,
  PRIMARY KEY (`user`),
  UNIQUE KEY `user` (`user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`user`, `id`, `nrc`, `password`, `name`, `u_name`, `qualifications`, `industry`, `experience`, `gender`, `date_o_b`, `nationality`, `phone`, `email`, `address`, `township`, `city`, `country`, `registered_date`, `role`, `random_number`, `random_number_date`, `status`, `photo`, `photo_server_path`) VALUES
('student', '2', '2', '5f4dcc3b5aa765d61d8327deb882cf99', 'student', 'University Of Yangon', 'bachelor', 'science', '', 'Female', '1987-04-14', 'myanmar', '2', '2', '2', 'bahan', 'yangon', 'myanmar', '2016-03-04', 'student', NULL, '0000-00-00', 'Approved', 'http://localhost:81/ci1/images/students_photos/studentsC.jpg', 'C:/xampp/htdocs/ci1/images/students_photos/studentsC.jpg'),
('student2', '44444', '44444', '5f4dcc3b5aa765d61d8327deb882cf99', 'student2', 'University Of Yangon', 'bachelor', 'science', '', 'female', '1987-04-14', 'myanmar', '22222', '4567', '2', 'bahan', 'yangon', 'myanmar', '2016-03-04', 'student', NULL, '0000-00-00', 'Approved', 'http://localhost:81/ci1/images/students_photos/s.jpg', 'C:/xampp/htdocs/ci1/images/students_photos/s.jpg'),
('student4', '14', '14', '5f4dcc3b5aa765d61d8327deb882cf99', 'student4', 'University of Dagon', '14', '14', '', 'female', '1987-04-04', 'myanmar', '14', 'student4', 'bahan', 'bahan', 'bahan', 'bhan', '2016-03-22', 'student', NULL, '0000-00-00', 'Approved', 'http://localhost:81/ci1/images/students_photos/s1.jpg', 'C:/xampp/htdocs/ci1/images/students_photos/s1.jpg'),
('student5', '5', '5', '5f4dcc3b5aa765d61d8327deb882cf99', 'student5', 'University of Dagon', 'student5', 'student5', '', 'female', '1987-04-14', 'myanmar', '5', '5', '5', '5', '5', '5', '2016-03-22', 'student', NULL, '0000-00-00', 'Approved', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `students_jobs`
--

CREATE TABLE IF NOT EXISTS `students_jobs` (
  `applied_id` varchar(32) NOT NULL,
  `user` varchar(50) NOT NULL,
  `job_id` varchar(11) NOT NULL,
  `c_name` text NOT NULL,
  `file` text NOT NULL,
  `category` text NOT NULL,
  `title` text NOT NULL,
  `salary` text NOT NULL,
  `c_background` text NOT NULL,
  `description` text NOT NULL,
  `responsibilities` text NOT NULL,
  `requirements` text NOT NULL,
  `type` text NOT NULL,
  `phone` text NOT NULL,
  `email` text NOT NULL,
  `address` text NOT NULL,
  `township` text NOT NULL,
  `city` text NOT NULL,
  `country` text NOT NULL,
  `valid_from` date NOT NULL,
  `valid_to` date NOT NULL,
  `published_date` date NOT NULL,
  `applied_date` date NOT NULL,
  `file_path` text NOT NULL,
  `file_name` text NOT NULL,
  `file_orig_name` text NOT NULL,
  `delete` text NOT NULL,
  PRIMARY KEY (`applied_id`),
  UNIQUE KEY `record_id` (`applied_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students_jobs`
--

INSERT INTO `students_jobs` (`applied_id`, `user`, `job_id`, `c_name`, `file`, `category`, `title`, `salary`, `c_background`, `description`, `responsibilities`, `requirements`, `type`, `phone`, `email`, `address`, `township`, `city`, `country`, `valid_from`, `valid_to`, `published_date`, `applied_date`, `file_path`, `file_name`, `file_orig_name`, `delete`) VALUES
('JA1', 'student ', 'J1', 'Dell', '', 'Accounting', 'manager', '4(lakhs)-7.99999(lakhs)', 'Something', 'something', 'something', '0', 'Contract/Temp', '2345', '44444', '42', 'kamayut', 'Yangon', 'myanmar', '2015-04-14', '2016-04-14', '2016-03-04', '2016-03-04', 'http://localhost:81/ci1/files/job13.doc', 'job13.doc', 'job.doc', ''),
('JA2', 'student ', 'J2', 'Dell', '', 'Accounting', 'manager', '4(lakhs)-7.99999(lakhs)', 'science', 'something', 'something', 'something and something', 'Full Time', '2345', '5555', '18', 'bahan', 'Yangon', 'myanmar', '2015-04-14', '2016-04-14', '2016-03-04', '2016-03-04', 'http://localhost:81/ci1/files/job15.doc', 'job15.doc', 'job.doc', ''),
('JA3', 'student ', 'J3', 'Dell', '', 'Accounting', 'manager', '4(lakhs)-7.99999(lakhs)', 'create computers', 'something', 'something', 'something and something', 'Contract/Temp', '2222', '2345', '14', 'bahan', 'Yangon', 'myanmar', '2015-04-14', '2016-04-14', '2016-03-04', '2016-03-04', 'http://localhost:81/ci1/files/job16.doc', 'job16.doc', 'job.doc', ''),
('JA4', 'student ', 'J4', 'Dell', '', 'Accounting', 'manager', '12(lakhs)-15.99999(lakhs)', 'something', 'something', 'something', 'something and something', 'Contract/Temp', '22224', '2345', '14', 'bahan', 'Yangon', 'myanmar', '2015-04-14', '2016-04-14', '2016-03-04', '2016-03-04', 'http://localhost:81/ci1/files/job_-_Copy.doc', 'job_-_Copy.doc', 'job_-_Copy.doc', ''),
('JA5', 'student', 'J1', 'Dell', '', 'Accounting', 'manager', '4(lakhs)-7.99999(lakhs)', 'Something', 'something', 'something', 'something and something', 'Contract/Temp', '2345', '44444', '42', 'kamayut', 'Yangon', 'myanmar', '2015-04-14', '2016-04-14', '2016-03-04', '2016-03-25', 'http://localhost:81/ci1/files/job20.doc', 'job20.doc', 'job.doc', '');

-- --------------------------------------------------------

--
-- Table structure for table `university`
--

CREATE TABLE IF NOT EXISTS `university` (
  `user` varchar(40) NOT NULL COMMENT 'hi',
  `password` varchar(32) NOT NULL,
  `name` varchar(40) NOT NULL,
  `u_name` text NOT NULL,
  `email` text NOT NULL,
  `phone` text NOT NULL,
  `website` text NOT NULL,
  `address` text NOT NULL,
  `township` text NOT NULL,
  `city` text NOT NULL,
  `country` text NOT NULL,
  `role` text NOT NULL,
  `random_number` int(32) NOT NULL,
  `random_number_date` date NOT NULL,
  `status` text NOT NULL,
  `registered_date` date NOT NULL,
  UNIQUE KEY `user` (`user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `university`
--

INSERT INTO `university` (`user`, `password`, `name`, `u_name`, `email`, `phone`, `website`, `address`, `township`, `city`, `country`, `role`, `random_number`, `random_number_date`, `status`, `registered_date`) VALUES
('dagon', '5f4dcc3b5aa765d61d8327deb882cf99', 'dagon', 'University of Dagon', 'dagon', '2', 'dagonmyanmar', '2', 'bahan', 'yangon', 'myanmar', 'university', 0, '0000-00-00', 'Approved', '2016-03-04'),
('mandalay', '5f4dcc3b5aa765d61d8327deb882cf99', 'mandalay', 'University of Mandaly', 'mandalay', '2', 'universityof_mandalay', '42', 'bahan', 'yangon', 'myanmar', 'university', 0, '0000-00-00', 'Approved', '2016-03-04');

-- --------------------------------------------------------

--
-- Table structure for table `university_name`
--

CREATE TABLE IF NOT EXISTS `university_name` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `u_name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `university_name`
--

INSERT INTO `university_name` (`id`, `u_name`) VALUES
(3, 'University Of Yangon'),
(4, 'University of Dagon'),
(6, 'in'),
(7, 'University of Mandaly');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `users` int(11) NOT NULL,
  `password` varchar(40) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`users`, `password`, `reg_date`) VALUES
(1, '', '2016-03-02 18:49:23'),
(4, '', '2016-03-02 18:49:23'),
(8, '', '2016-03-02 18:49:38'),
(14, '', '2016-03-02 18:49:38');

DELIMITER $$
--
-- Events
--
CREATE DEFINER=`root`@`localhost` EVENT `delete_class_data` ON SCHEDULE EVERY 1 MINUTE STARTS '2016-02-19 23:19:23' ON COMPLETION NOT PRESERVE ENABLE DO DELETE FROM users$$

CREATE DEFINER=`root`@`localhost` EVENT `random_number` ON SCHEDULE EVERY 1 DAY STARTS '2016-02-19 23:49:17' ON COMPLETION NOT PRESERVE ENABLE DO UPDATE students SET random_number = "" WHERE random_number_date <= DATE_SUB(NOW(),INTERVAL 7 DAY)$$

DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
